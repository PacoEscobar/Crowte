//
//  PerfilLugarViewController.m
//  Crowte
//
//  Created by Paco Escobar on 18/02/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIImageView+WebCache.h>
#import <Lottie/Lottie.h>
#import "PerfilLugarViewController.h"
#import "VotarLugarViewController.h"
#import "ImagenPubLugarViewController.h"
#import "ReportarMensajeTableViewController.h"
#import "ReportarLugarTableViewController.h"
#import "ReclamarLugarV1ViewController.h"

@interface PerfilLugarViewController ()

@end

@implementation PerfilLugarViewController

@synthesize idLugar, fotoPrincipalLugar, fotoSecundariaLugar, nombreLugar, location, navBarYInicial, tabla, geocoder, mensajes, tituloTutoCheckinVotos, backgroundTutoCheckinVotos, opcionesAmbienteLugar;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    geocoder = [[CLGeocoder alloc] init];
    
    navBarYInicial = self.navigationController.navigationBar.frame.origin.y;
        
    UIView *division = [self.navigationController.navigationBar viewWithTag:1];
    
    if(division){
        
        [division removeFromSuperview];
    }
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    UIImage *imageConfig = [UIImage imageNamed:@"iconmoreperfil.png"];
    
    UIButton *config = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imageConfig.size.width, imageConfig.size.height)];
    
    [config setImage:imageConfig forState:UIControlStateNormal];
    
    [config addTarget:self action:@selector(mostrarOpcionesLugar:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barbtnconfig = [[UIBarButtonItem alloc] initWithCustomView:config];
    
    self.navigationItem.rightBarButtonItem = barbtnconfig;
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error){
    
        UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        UILabel *lugar = (UILabel*)[celda viewWithTag:3];
        
        CLPlacemark *placemark = [placemarks lastObject];
        
        [lugar setText:[NSString stringWithFormat:@"%@, %@, %@",[[placemark addressDictionary] objectForKey:@"Name"],[[placemark addressDictionary] objectForKey:@"City"],[[placemark addressDictionary] objectForKey:@"State"]]];
    }];
    
    [tituloTutoCheckinVotos setText:[NSString stringWithFormat:@"El número de check-in y votos dejan de contar después de 2 horas, por lo que los números que ves están ocurriendo en este preciso momento %C.",0xe405]];
    
    [self getMensajes];
    
    if(opcionesAmbienteLugar == nil){
        
        [self getOpcionesAmbiente];
    }
    
}

-(void)viewDidAppear:(BOOL)animated{

    if([self.navigationController isNavigationBarHidden]){
    
        [self.navigationController setNavigationBarHidden:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    unsigned long mensajesCount = 0;
    
    if(![[mensajes objectForKey:@"mensajes"] isEqual:[NSNull null]]){
    
        mensajesCount = [[mensajes objectForKey:@"mensajes"] count];
    }
    
    return mensajesCount+1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.row == 0){
        
        return 411;
    }else{
        
        NSString *texto = [[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensaje"];
        
        float adicional = 0;
        
        if(![texto isEqualToString:@""]){

            if([texto length] > 44){
                
                adicional = ceil([texto length]/44.0);
                adicional = adicional*55;

            }else{
                
                adicional = 55;
            }
        }

        if([[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]]){

            return 166+adicional;
        }else{

            return 375+adicional;
        }
    }
    
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        
        UIScrollView *scrollViewOpcionesAmbiente = (UIScrollView*)[cell viewWithTag:9];
        
        if([scrollViewOpcionesAmbiente viewWithTag:1]){
            
            for(UIView *fondoAnimacion in [scrollViewOpcionesAmbiente subviews]){
                
                LOTAnimationView *animacion = (LOTAnimationView*)[fondoAnimacion viewWithTag:1];
                
                if([[fondoAnimacion viewWithTag:1] isKindOfClass:[LOTAnimationView class]]){
                
                    [animacion play];
                }
            }
            
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda;
    
    if(indexPath.row == 0){
    
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaDatosLugar"];
        
        UIImageView *fotoPrincipal = (UIImageView*)[celda viewWithTag:1];
        
        [[fotoPrincipal layer] setBorderWidth:1];
        [[fotoPrincipal layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
        [[fotoPrincipal layer] setCornerRadius:50];
        [[fotoPrincipal layer] setMasksToBounds:YES];

        [fotoPrincipal sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%d/%@",idLugar,fotoPrincipalLugar]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *imag, NSError *error, SDImageCacheType tipo, NSURL *url){
            
            if(error != nil){
                [fotoPrincipal setContentMode:UIViewContentModeCenter];
                [[fotoPrincipal layer] setBorderColor:[[UIColor whiteColor] CGColor]];
            }
        }];
        
        UIImageView *fotoSecundaria = (UIImageView*)[celda viewWithTag:7];
        
        [fotoSecundaria sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%d/%@",idLugar,fotoSecundariaLugar]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
        
        UILabel *nombre = (UILabel*)[celda viewWithTag:2];
        [nombre setText:nombreLugar];
        
        
        UILabel *checkins = (UILabel*)[celda viewWithTag:4];
        
        [checkins setText:[[mensajes objectForKey:@"personas"] objectForKey:@"cantidad"]];

        if([[mensajes objectForKey:@"ahi"] isEqual:[NSNumber numberWithInt:1]]){

            UIButton *botonVotar = (UIButton*)[celda viewWithTag:6];
            
            [botonVotar setImage:[UIImage imageNamed:@"botonvotarlugar.png"] forState:UIControlStateNormal];
            
            [botonVotar addTarget:self action:@selector(irAVotar:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        UIScrollView *scrollAnimaciones = (UIScrollView*)[celda viewWithTag:9];
        
        if(![scrollAnimaciones viewWithTag:1]){
            
            [scrollAnimaciones.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            
            bool derecha = YES;
            int conteoOpciones = 0;
            int i = 0;
            for(id key in opcionesAmbienteLugar){

                if([key isEqualToString:@"meGusta"]){
                    
                    i++;

                    if([[opcionesAmbienteLugar objectForKey:@"meGusta"] isEqual:[NSNumber numberWithInt:1]] || [[opcionesAmbienteLugar objectForKey:@"meGusta"] isEqual:[NSNumber numberWithInt:0]]){
                        
                        LOTAnimationView *animacion;
                        
                        if([[opcionesAmbienteLugar objectForKey:@"meGusta"] isEqual:[NSNumber numberWithInt:1]]){
                        
                            animacion = [LOTAnimationView animationNamed:@"me_gusta"];
                        
                            [[animacion layer] setBorderColor:[[UIColor colorWithRed:39.0/255.0 green:140.0/255.0 blue:233.0/255.0 alpha:1] CGColor]];
                        }else{
                            
                            animacion = [LOTAnimationView animationNamed:@"no_me_gusta"];
                            
                            [[animacion layer] setBorderColor:[[UIColor colorWithRed:39.0/255.0 green:140.0/255.0 blue:233.0/255.0 alpha:1] CGColor]];
                        }
                        
                        [animacion setFrame:CGRectMake(0, 0, 42, 42)];
                        
                        [animacion setTag:1];
                        
                        [animacion setAnimationProgress:0.2];
                        
                        [[animacion layer] setBorderWidth:3];
                        
                        [[animacion layer] setCornerRadius:21];
                            
                        int distanciaDelPrimero = 0;

                        if(conteoOpciones != 0){
                            
                            if(derecha){
                                
                                distanciaDelPrimero = 60*conteoOpciones;

                                derecha = NO;
                            }else{

                                distanciaDelPrimero = -60*conteoOpciones;
                                
                                conteoOpciones++;
                                
                                derecha = YES;
                            }
                        }else{
                            
                            conteoOpciones++;
                        }
                        
                        UIView *fondo = [[UIView alloc] initWithFrame:CGRectMake(((scrollAnimaciones.frame.size.width/2)-21)+distanciaDelPrimero, 0, 42, 42)];
                        
                        [fondo setTag:i];
                        
                        [fondo setBackgroundColor:[UIColor whiteColor]];
                        
                        [[fondo layer] setCornerRadius:21];
                        
                        [fondo addSubview:animacion];
                        
                        [scrollAnimaciones addSubview:fondo];
                    }
                }
                
                if([key isEqualToString:@"estaLleno"]){
                    
                    i++;

                    if([[opcionesAmbienteLugar objectForKey:@"estaLleno"] isEqual:[NSNumber numberWithInt:1]] || [[opcionesAmbienteLugar objectForKey:@"estaLleno"] isEqual:[NSNumber numberWithInt:0]]){
                        
                        LOTAnimationView *animacion;
                        
                        if([[opcionesAmbienteLugar objectForKey:@"estaLleno"] isEqual:[NSNumber numberWithInt:1]]){
                            animacion = [LOTAnimationView animationNamed:@"aun_lugares"];
                        
                            [[animacion layer] setBorderColor:[[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] CGColor]];
                        }else{
                            
                            animacion = [LOTAnimationView animationNamed:@"esta_lleno"];
                            
                            [[animacion layer] setBorderColor:[[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] CGColor]];
                        }
                        
                        [animacion setFrame:CGRectMake(0, 0, 42, 42)];
                        
                        [animacion setTag:1];
                        
                        [animacion setAnimationProgress:0.2];
                        
                        [[animacion layer] setBorderWidth:3];
                        
                        [[animacion layer] setCornerRadius:21];
                        
                        int distanciaDelPrimero = 0;

                        if(conteoOpciones != 0){
                            
                            if(derecha){
                                
                                distanciaDelPrimero = 60*conteoOpciones;

                                derecha = NO;
                            }else{
                                
                                distanciaDelPrimero = -60*conteoOpciones;
                                
                                conteoOpciones++;
                                
                                derecha = YES;
                            }
                        }else{
                            
                            conteoOpciones++;
                        }

                        UIView *fondo = [[UIView alloc] initWithFrame:CGRectMake(((scrollAnimaciones.frame.size.width/2)-21)+distanciaDelPrimero, 0, 42, 42)];
                        
                        [fondo setTag:i];
                        
                        [fondo setBackgroundColor:[UIColor whiteColor]];
                        
                        [[fondo layer] setCornerRadius:21];
                        
                        [fondo addSubview:animacion];
                        
                        [scrollAnimaciones addSubview:fondo];
                    }
                }
                
                if([key isEqualToString:@"meGustaMusica"]){
                    
                    i++;

                    if([[opcionesAmbienteLugar objectForKey:@"meGustaMusica"] isEqual:[NSNumber numberWithInt:1]] || [[opcionesAmbienteLugar objectForKey:@"meGustaMusica"] isEqual:[NSNumber numberWithInt:0]]){
                        
                        LOTAnimationView *animacion;
                        
                        if([[opcionesAmbienteLugar objectForKey:@"meGustaMusica"] isEqual:[NSNumber numberWithInt:1]]){
                            
                            animacion = [LOTAnimationView animationNamed:@"check_nota"];
                        
                            [[animacion layer] setBorderColor:[[UIColor purpleColor] CGColor]];
                        }else{
                            
                            animacion = [LOTAnimationView animationNamed:@"check_nota_nogusta"];
                            
                            [[animacion layer] setBorderColor:[[UIColor grayColor] CGColor]];
                        }
                        
                        [animacion setFrame:CGRectMake(0, 0, 42, 42)];
                        
                        [animacion setTag:1];
                        
                        [animacion setAnimationProgress:0.2];
                        
                        [[animacion layer] setBorderWidth:3];
                        
                        [[animacion layer] setCornerRadius:21];
                        
                        int distanciaDelPrimero = 0;
                        
                        if(conteoOpciones != 0){

                            if(derecha){

                                distanciaDelPrimero = 60*conteoOpciones;
                                
                                derecha = NO;
                            }else{

                                distanciaDelPrimero = -60*conteoOpciones;
                                
                                conteoOpciones++;
                                
                                derecha = YES;
                            }
                        }else{
                            
                            conteoOpciones++;
                        }
                        
                        UIView *fondo = [[UIView alloc] initWithFrame:CGRectMake(((scrollAnimaciones.frame.size.width/2)-21)+distanciaDelPrimero, 0, 42, 42)];
                        
                        [fondo setTag:i];
                        
                        [fondo setBackgroundColor:[UIColor whiteColor]];
                        
                        [[fondo layer] setCornerRadius:21];
                        
                        [fondo addSubview:animacion];
                        
                        [scrollAnimaciones addSubview:fondo];
                    }
                }

                if([key isEqualToString:@"servicioRapido"]){
                    
                    i++;

                    if([[opcionesAmbienteLugar objectForKey:@"servicioRapido"] isEqual:[NSNumber numberWithInt:1]] || [[opcionesAmbienteLugar objectForKey:@"servicioRapido"] isEqual:[NSNumber numberWithInt:0]]){
                        
                        LOTAnimationView *animacion;
                        
                        if([[opcionesAmbienteLugar objectForKey:@"servicioRapido"] isEqual:[NSNumber numberWithInt:1]]){
                            
                            animacion = [LOTAnimationView animationNamed:@"servicio_rapido"];
                        
                            [[animacion layer] setBorderColor:[[UIColor blueColor] CGColor]];
                        }else{
                            
                            animacion = [LOTAnimationView animationNamed:@"servicio_lento"];
                            
                            [[animacion layer] setBorderColor:[[UIColor grayColor] CGColor]];
                        }
                        
                        [animacion setFrame:CGRectMake(0, 0, 42, 42)];
                        
                        [animacion setTag:1];
                        
                        [animacion setAnimationProgress:0.2];
                        
                        [[animacion layer] setBorderWidth:3];
                        
                        [[animacion layer] setCornerRadius:21];
                        
                        int distanciaDelPrimero = 0;

                        if(conteoOpciones != 0){
                            
                            if(derecha){
                                
                                distanciaDelPrimero = 60*conteoOpciones;
                                derecha = NO;
                            }else{
                                
                                distanciaDelPrimero = -60*conteoOpciones;
                                
                                conteoOpciones++;
                                derecha = YES;
                            }
                        }else{
                            
                            conteoOpciones++;
                        }

                        UIView *fondo = [[UIView alloc] initWithFrame:CGRectMake(((scrollAnimaciones.frame.size.width/2)-21)+distanciaDelPrimero, 0, 42, 42)];

                        [fondo setTag:i];
                        
                        [fondo setBackgroundColor:[UIColor whiteColor]];
                        
                        [[fondo layer] setCornerRadius:21];
                        
                        [fondo addSubview:animacion];
                        
                        [scrollAnimaciones addSubview:fondo];
                    }
                }
                
                if([key isEqualToString:@"servicioBueno"]){
                    
                    i++;

                    if([[opcionesAmbienteLugar objectForKey:@"servicioBueno"] isEqual:[NSNumber numberWithInt:1]] || [[opcionesAmbienteLugar objectForKey:@"servicioBueno"] isEqual:[NSNumber numberWithInt:0]]){
                        
                        LOTAnimationView *animacion;
                        
                        if([[opcionesAmbienteLugar objectForKey:@"servicioBueno"] isEqual:[NSNumber numberWithInt:1]]){
                            
                            animacion = [LOTAnimationView animationNamed:@"buen_servicio"];
                        
                            [[animacion layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:0 alpha:1] CGColor]];
                        }else{
                            
                            animacion = [LOTAnimationView animationNamed:@"mal_servicio"];
                            
                            [[animacion layer] setBorderColor:[[UIColor redColor] CGColor]];
                        }
                        
                        [animacion setFrame:CGRectMake(0, 0, 42, 42)];
                        
                        [animacion setTag:1];
                        
                        [animacion setAnimationProgress:0.2];
                        
                        [[animacion layer] setBorderWidth:3];
                        
                        [[animacion layer] setCornerRadius:21];
                        
                        int distanciaDelPrimero = 0;
                        
                        if(conteoOpciones != 0){
                            
                            if(derecha){
                                
                                distanciaDelPrimero = 60*conteoOpciones;
                                derecha = NO;
                            }else{
                                
                                distanciaDelPrimero = -60*conteoOpciones;
                                
                                conteoOpciones++;
                                derecha = YES;
                            }
                        }else{
                            
                            conteoOpciones++;
                        }
                        
                        UIView *fondo = [[UIView alloc] initWithFrame:CGRectMake(((scrollAnimaciones.frame.size.width/2)-21)+distanciaDelPrimero, 0, 42, 42)];
                        
                        [fondo setTag:i];
                        
                        [fondo setBackgroundColor:[UIColor whiteColor]];
                        
                        [[fondo layer] setCornerRadius:21];
                        
                        [fondo addSubview:animacion];
                        
                        [scrollAnimaciones addSubview:fondo];
                    }
                }
                
            }
            
        }
    }else{
    
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaMensaje"];
        
        UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldanuevo.png"]];
        
        [celda setBackgroundView:fondo];
        [celda setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *fotoPrincipal = (UIImageView*)[celda viewWithTag:1];
        
        [[fotoPrincipal layer] setBorderWidth:1];
        [[fotoPrincipal layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
        [[fotoPrincipal layer] setCornerRadius:22.5];
        [[fotoPrincipal layer] setMasksToBounds:YES];
        
        [fotoPrincipal sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioID"] intValue],[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"] completed:^(UIImage *imag, NSError *error, SDImageCacheType tipo, NSURL *url){

            if(error != nil){
                [fotoPrincipal setContentMode:UIViewContentModeCenter];
                [[fotoPrincipal layer] setBorderColor:[[UIColor whiteColor] CGColor]];
            }else{
            
                [fotoPrincipal setContentMode:UIViewContentModeScaleToFill];
                [[fotoPrincipal layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
            }
        }];
        
        UIImageView *fotovideo = (UIImageView*)[celda viewWithTag:4];
        
        if([[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]]){
            
            [fotovideo setHidden:YES];
            [[celda viewWithTag:5] setHidden:YES];
        }else{
            
            [fotovideo setHidden:NO];
        }
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(abrirImagen:)];
        
        [fotovideo addGestureRecognizer:tapGesture];
        [fotovideo setUserInteractionEnabled:YES];
        
        UILabel *nombreylugar = (UILabel*)[celda viewWithTag:2];
        
        
        if([[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioApellido"] isEqualToString:@"(null)"]){
            
            [[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] setObject:@"" forKey:@"mensajeUsuarioApellido"];
        }
        
        [nombreylugar setText:[NSString stringWithFormat:@"%@ %@ estuvo en %@",[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioNombre"],[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioApellido"],nombreLugar]];
        
        unsigned long sizenombre = [[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioNombre"] length]+[[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioApellido"] length]+1;
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:nombreylugar.attributedText];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:[tableView backgroundColor] range:NSMakeRange(0, sizenombre)];
        [nombreylugar setAttributedText:attributedString];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:[tableView backgroundColor] range:NSMakeRange(sizenombre+11, [nombreLugar length])];
        [nombreylugar setAttributedText:attributedString];
        
        
        UILabel *fecha = (UILabel*)[celda viewWithTag:3];
        
        [fecha setText:[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"fechaFormateada"]];
        
        NSArray *constraints = [[celda contentView] constraints];
        NSLayoutConstraint *constraintInferior;
        
        NSString *mensaje = [[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensaje"];
        
        NSData *data = [mensaje dataUsingEncoding:NSUTF8StringEncoding];
        
        mensaje = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        
        mensaje = [mensaje stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        float adicional = 0;
        
        if(![[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]]){
            
            if([[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"esVideo"] intValue] == 0){
                
                [fotovideo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/publicaciones/%@",[[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioID"] intValue], [[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoPublicacion"]]]];
                
                [[celda viewWithTag:5] setHidden:YES];
                
            }else{
                
                NSArray *nombreVideoA = [[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoPublicacion"] componentsSeparatedByString:@"."];
                
                NSString *nombreVideo = [NSString stringWithFormat:@"%@.jpg",[nombreVideoA objectAtIndex:0]];
                
                NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/thumbnail/%@",[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioID"],nombreVideo]];
                
                [[celda viewWithTag:5] setHidden:NO];
                
                [fotovideo sd_setImageWithURL:videoURL];
            }
            
            for (NSLayoutConstraint *constraint in constraints) {
                
                if([[constraint identifier] isEqualToString:@"constraintImagenBottom"]){
                    
                    constraintInferior = constraint;
                }
            }
            
            if(constraintInferior != nil){
                
                if(![mensaje isEqualToString:@""]){
                    
                    if([mensaje length] > 44){
                        
                        adicional = ceil([mensaje length]/44.0);
                        adicional = adicional*55;
                    }else{
                        
                        adicional = 55;
                    }
                }

                [constraintInferior setConstant:62+adicional];
            }
            
            
        }
        
        if([celda viewWithTag:11]){
        
            [[celda viewWithTag:11] removeFromSuperview];
        }
        
        if(![mensaje isEqualToString:@""]){

            UITextView *texto = [[UITextView alloc] initWithFrame:CGRectMake(9, 96, celda.frame.size.width-18, 100)];
            
            [texto setText:mensaje];
            
            [texto setFont:[UIFont fontWithName:@"Raleway-Regular" size:13]];
            [texto setTextColor:[UIColor colorWithRed:48.0/255.0 green:52.0/255.0 blue:63.0/255.0 alpha:1]];
            
            [texto setBackgroundColor:[UIColor colorWithRed:246.0/255.0 green:244.0/255.0 blue:243.0/255.0 alpha:1]];
            
            [texto setTextContainerInset:UIEdgeInsetsMake(20, 20, 20, 20)];
            
            [texto setTag:11];
            
            [texto setUserInteractionEnabled:NO];
            [texto setScrollEnabled:NO];
            [texto setEditable:NO];
            
            [texto setContentMode:UIViewContentModeCenter];
            
            texto.translatesAutoresizingMaskIntoConstraints = NO;
            
            [celda addSubview:texto];
            
            for (NSLayoutConstraint *constraint in [[[celda viewWithTag:11] superview] constraints]) {
                
                if([[constraint identifier] isEqualToString:@"constraintTextoTop"] || [[constraint identifier] isEqualToString:@"constraintTextoLeading"] || [[constraint identifier] isEqualToString:@"constraintTextoTrailing"] || [[constraint identifier] isEqualToString:@"constraintTextBottom"]){
                    
                    [[celda viewWithTag:11] removeConstraint:constraint];
                }
            }
            
            if([[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]]){
                
                NSLayoutConstraint *constraintSuperior = [NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:[texto superview] attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:96];
                
                [constraintSuperior setIdentifier:@"constraintTextoTop"];
                
                [[texto superview] addConstraint: constraintSuperior];
            }else{
                
                NSLayoutConstraint *constraintSuperior = [NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:fotovideo attribute:NSLayoutAttributeBottom multiplier:1.0 constant:8];
                
                [constraintSuperior setIdentifier:@"constraintTextoTop"];
                
                [[texto superview] addConstraint:constraintSuperior];
            }
            
            NSLayoutConstraint *constraintLeading = [NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:[texto superview] attribute:NSLayoutAttributeLeadingMargin multiplier:1.0 constant:1];
            
            [constraintLeading setIdentifier:@"constraintTextoLeading"];
            
            [[texto superview] addConstraint: constraintLeading];
            
            NSLayoutConstraint *constraintTrailing = [NSLayoutConstraint constraintWithItem:[texto superview] attribute:NSLayoutAttributeTrailingMargin relatedBy:NSLayoutRelationEqual toItem:texto attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:1];
            
            [constraintTrailing setIdentifier:@"constraintTextoTrailing"];
            
            [[texto superview] addConstraint: constraintTrailing];
            
            NSLayoutConstraint *constraintBottom = [NSLayoutConstraint constraintWithItem:[texto superview] attribute:NSLayoutAttributeBottomMargin relatedBy:NSLayoutRelationEqual toItem:texto attribute:NSLayoutAttributeBottom multiplier:1.0 constant:50];
            
            [constraintBottom setIdentifier:@"constraintTextBottom"];
            
            [[texto superview] addConstraint: constraintBottom];
            
        }else{
            
            [[celda viewWithTag:11] removeFromSuperview];
            
            for (NSLayoutConstraint *constraint in [[[celda viewWithTag:11] superview] constraints]) {
                
                if([[constraint identifier] isEqualToString:@"constraintTextoTop"] || [[constraint identifier] isEqualToString:@"constraintTextoLeading"] || [[constraint identifier] isEqualToString:@"constraintTextoTrailing"] || [[constraint identifier] isEqualToString:@"constraintTextBottom"]){
                    
                    [[celda viewWithTag:11] removeConstraint:constraint];
                }
            }
            
        }
        
        UIButton *corazon = (UIButton*)[celda viewWithTag:6];
        
        if([[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"likeMio"] intValue] == 0){
            
            [corazon setSelected:NO];
        }else{
            
            [corazon setSelected:YES];
        }
        
        [corazon removeTarget:self action:@selector(meGustaComentario:) forControlEvents:UIControlEventTouchUpInside];
        
        [corazon addTarget:self action:@selector(meGustaComentario:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *numCorazon = (UIButton*)[celda viewWithTag:7];
        
        if([[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"likes"] intValue] == 0){
            
            [numCorazon setTitle:@"" forState:UIControlStateNormal];
        }else{
            
            [numCorazon setTitle:[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"likes"] forState:UIControlStateNormal];
        }
        
        UIButton *opciones = (UIButton*)[celda viewWithTag:10];
        
        [opciones addTarget:self action:@selector(mostrarOpcionesMensaje:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    return celda;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [self.navigationController.navigationBar setFrame:CGRectMake(self.navigationController.navigationBar.frame.origin.x, ([scrollView contentOffset].y*-1)+navBarYInicial, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
}


#pragma mark - hechos por mi

-(void)getMensajes{

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getdatoslugar.php"]];
    
    [request setHTTPMethod:@"POST"];

    NSData *cuerpo = [[NSString stringWithFormat:@"id=%d&usuario=%@",idLugar,[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){
            
            if(data != nil){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSError *error;
                    
                    mensajes = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                    
                    if([[mensajes objectForKey:@"tutorial"] intValue] == 0){
                    
                        [backgroundTutoCheckinVotos setHidden:NO];
                    }
                    
                    [tabla reloadData];
                });
                
            }
        }
        
    }];
    
    [uploadTask resume];
}

-(IBAction)meGustaComentario:(id)sender{
    
    UIButton *corazon = (UIButton*)sender;
    
    CGPoint posicionBoton = [corazon convertPoint:CGPointZero toView:tabla];
    
    NSIndexPath *indexPath = [tabla indexPathForRowAtPoint:posicionBoton];
    
    UITableViewCell *celda = [tabla cellForRowAtIndexPath:indexPath];
    
    if(![corazon isSelected]){
        
        LOTAnimationView *corazonLot = [LOTAnimationView animationNamed:@"data"];
        
        [corazonLot setFrame:corazon.frame];
        [corazonLot setTag:12];
        
        [celda addSubview:corazonLot];
        
        [corazon setAlpha:0];
        
        [corazonLot playWithCompletion:^(BOOL animationFinished){
            
            if(animationFinished){
                
                [corazonLot removeFromSuperview];
                [corazon setAlpha:1];
                
                [corazon setSelected:YES];
                
                [[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] setValue:@"1" forKey:@"likeMio"];
                
                int numMegusta = [[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"likes"] intValue];
                
                numMegusta++;
                
                UIButton *numCorazones = (UIButton*)[celda viewWithTag:7];
                
                [numCorazones setTitle:[NSString stringWithFormat:@"%d",numMegusta] forState:UIControlStateNormal];
                
                [[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] setValue:[NSString stringWithFormat:@"%d",numMegusta] forKey:@"likes"];
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
                
                [request setHTTPMethod:@"POST"];
                
                NSData *cuerpo = [[NSString stringWithFormat:@"id=%d&mensajeID=%@&act=%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue], [[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensajeID"], 1] dataUsingEncoding:NSUTF8StringEncoding];
                
                NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                
                NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                
                NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){

                    if(error == nil){
                        
                        if(data != nil){
                            
                            NSError *error = nil;
                            
                            NSDictionary *numMeGusta = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                            
                            if(error == nil){
                                
                                if([[numMeGusta objectForKey:@"status"] isEqualToString:@"si"]){
                                    
                                    UITableViewCell *celda = [tabla cellForRowAtIndexPath:indexPath];
                                    
                                    UIButton *numCorazones = (UIButton*)[celda viewWithTag:7];
                                    
                                    if([[numMeGusta objectForKey:@"numero"] intValue] == 0){
                                        
                                        [numCorazones setTitle:@"" forState:UIControlStateNormal];
                                    }else{
                                        
                                        [numCorazones setTitle:[numMeGusta objectForKey:@"numero"] forState:UIControlStateNormal];
                                    }
                                }
                            }
                        }
                    }
                    
                }];
                
                [uploadTask resume];
                
            }
        }];
    }else{
        
        LOTAnimationView *corazonLot = [LOTAnimationView animationNamed:@"corazon_roto"];
        
        [corazonLot setFrame:corazon.frame];
        [corazonLot setTag:12];
        
        [celda addSubview:corazonLot];
        
        [corazon setAlpha:0];
        
        [corazonLot playWithCompletion:^(BOOL animationFinished){
            
            if(animationFinished){
                
                [corazonLot removeFromSuperview];
                [corazon setAlpha:1];
                
                [corazon setSelected:NO];
                
                [[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] setValue:@"0" forKey:@"likeMio"];
                
                int numMegusta = [[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"likes"] intValue];
                
                numMegusta--;
                
                UIButton *numCorazones = (UIButton*)[celda viewWithTag:7];
                
                if(numMegusta == 0){
                
                    
                    [numCorazones setTitle:@"" forState:UIControlStateNormal];
                }else{
                
                    
                    [numCorazones setTitle:[NSString stringWithFormat:@"%d",numMegusta] forState:UIControlStateNormal];
                }
                
                [[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] setValue:[NSString stringWithFormat:@"%d",numMegusta] forKey:@"likes"];
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
                
                [request setHTTPMethod:@"POST"];
                
                NSData *cuerpo = [[NSString stringWithFormat:@"id=%d&mensajeID=%@&act=%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue], [[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensajeID"], 2] dataUsingEncoding:NSUTF8StringEncoding];
                
                NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                
                NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                
                NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){

                    if(error == nil){
                        
                        if(data != nil){
                            
                            NSError *error = nil;
                            
                            NSDictionary *numMeGusta = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                            
                            if(error == nil){
                                
                                if([[numMeGusta objectForKey:@"status"] isEqualToString:@"si"]){
                                    
                                    UITableViewCell *celda = [tabla cellForRowAtIndexPath:indexPath];
                                    
                                    UIButton *numCorazones = (UIButton*)[celda viewWithTag:7];
                                    
                                    if([[numMeGusta objectForKey:@"numero"] intValue] == 0){
                                        
                                        [numCorazones setTitle:@"" forState:UIControlStateNormal];
                                    }else{
                                        
                                        [numCorazones setTitle:[numMeGusta objectForKey:@"numero"] forState:UIControlStateNormal];
                                    }
                                }
                            }
                        }
                    }
                    
                }];
                
                [uploadTask resume];
            }
        }];
    }
    
}

-(IBAction)irAVotar:(id)sender{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    VotarLugarViewController *votarLugarVC = [storyboard instantiateViewControllerWithIdentifier:@"votarLugar"];
    
    [votarLugarVC setVotos: [@{@"voto":[mensajes objectForKey:@"votado"], @"votos":@{@"numeroDeVotos":[[mensajes objectForKey:@"votos"] objectForKey:@"numeroDeVotos"], @"votos":@"<null>"}} mutableCopy]];
    
    [votarLugarVC setIdLugar:idLugar];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.navigationController pushViewController:votarLugarVC animated:YES];
}


-(void)abrirImagen:(UIPanGestureRecognizer*)gesto{
    
    UIImageView *imagenPresionada = (UIImageView*)[gesto view];
    
    CGPoint posicionBoton = [imagenPresionada convertPoint:CGPointZero toView:tabla];
    
    NSIndexPath *indexPath = [tabla indexPathForRowAtPoint:posicionBoton];
    
    UITableViewCell *celda = [tabla cellForRowAtIndexPath:indexPath];
    
    UIImageView *fotoUsuario = (UIImageView*)[celda viewWithTag:1];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [self.view.layer renderInContext:context];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    
    ImagenPubLugarViewController *imgPub = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"imgPubLugarView"];
    
    imgPub.fotoAnterior = img;
    imgPub.fotoAmostrar = [imagenPresionada image];
    imgPub.imagenUsuario = UIImageJPEGRepresentation([fotoUsuario image], 90);
    imgPub.json = [[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1];
    [self presentViewController:imgPub animated:NO completion:^{}];
    
}

-(IBAction)mostrarOpcionesMensaje:(id)sender{
    
    UIButton *botonOpciones = (UIButton*)sender;
    
    CGPoint posicionBoton = [botonOpciones convertPoint:CGPointZero toView:tabla];
    
    NSIndexPath *indexPath = [tabla indexPathForRowAtPoint:posicionBoton];
    
    UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *reportarCheckin = [UIAlertAction actionWithTitle:@"Reportar mensaje" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ReportarMensajeTableViewController *reporte = [storyboard instantiateViewControllerWithIdentifier:@"ReportarMensaje"];
        
        [reporte setIdRelMensajeLugar:[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensajeID"]];
        
        [reporte setIdUsuarioABloquear:[[[mensajes objectForKey:@"mensajes"] objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioID"]];

        [self presentViewController:reporte animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
        
        [opciones dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [opciones addAction:reportarCheckin];
    [opciones addAction:cancelar];
    
    [self presentViewController:opciones animated:YES completion:nil];
}

-(void)mostrarOpcionesLugar:(id)sender{
    
    UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *reclamarLugar = [UIAlertAction actionWithTitle:@"Reclamar lugar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *accion){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ReclamarLugarV1ViewController *reclamaLugar = [storyboard instantiateViewControllerWithIdentifier:@"ReclamarLugar"];
        
        [reclamaLugar setId_lugar:idLugar];
        
        [self presentViewController:reclamaLugar animated:YES completion:nil];
    }];
    
    UIAlertAction *reportarLugar = [UIAlertAction actionWithTitle:@"Reportar lugar" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ReportarLugarTableViewController *reporte = [storyboard instantiateViewControllerWithIdentifier:@"ReportarLugar"];
        
        [reporte setIdLugar:[NSString stringWithFormat:@"%d",idLugar]];
        
        [self presentViewController:reporte animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
        
        [opciones dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [opciones addAction:reclamarLugar];
    [opciones addAction:reportarLugar];
    [opciones addAction:cancelar];
    
    [self presentViewController:opciones animated:YES completion:nil];
}

- (IBAction)cerrarTutoCheckinVotos:(id)sender {
    
    [backgroundTutoCheckinVotos removeFromSuperview];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarvotochecklugartuto.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){}];
    
    [uploadTask resume];
}

-(void)playMicro:(UITapGestureRecognizer *)sender{

    UITableViewCell *celda = (UITableViewCell*)[[sender view] superview];
    
    LOTAnimationView *animacion = (LOTAnimationView*)[celda viewWithTag:12];
    
    [animacion playWithCompletion:^(BOOL animationFnished){
    
        if(animationFnished){
        
            [animacion removeFromSuperview];
        }
    }];
}

-(void)getOpcionesAmbiente{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getopcionesambiente.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"id_lugar=%d",idLugar] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){
            
            if(data != nil){
              
                opcionesAmbienteLugar = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [tabla reloadData];
                });
                
            }
        }
        
    }];
    
    [uploadTask resume];
}

#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
