//
//  CondicionesNoLoginViewController.h
//  Crowte
//
//  Created by Paco Escobar on 28/12/15.
//  Copyright © 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CondicionesNoLoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *condicionesContainer;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;



@end
