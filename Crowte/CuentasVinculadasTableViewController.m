//
//  CuentasVinculadasTableViewController.m
//  Crowte
//
//  Created by Paco Escobar on 29/05/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import "CuentasVinculadasTableViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface CuentasVinculadasTableViewController ()

@end

@implementation CuentasVinculadasTableViewController

@synthesize publicarFacebookSwitch;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStyleDone target:self action:nil];
    self.navigationItem.backBarButtonItem = atras;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([[defaults objectForKey:@"publicarFacebook"] intValue] == 1){
    
        [publicarFacebookSwitch setOn:YES];
    }
    
    [publicarFacebookSwitch addTarget:self action:@selector(actualizarCompartirFacebook:) forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 2    ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.row == 1){
    
        FBSDKAppInviteContent *content = [[FBSDKAppInviteContent alloc] init];
        
        content.appLinkURL = [NSURL URLWithString:@"https://fb.me/801579789972147"];
        content.appInvitePreviewImageURL = [NSURL URLWithString:@"https://www.crowte.com/imagenes/comparte.jpg"];
        
        [FBSDKAppInviteDialog showFromViewController:self withContent:content delegate:self];
    }
}

#pragma mark - appinvite

-(void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error{

    NSLog(@"app link error %@",error);
}

-(void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results{

    NSLog(@"results %@",results);
}

#pragma mark - hechos por mi

-(void)actualizarCompartirFacebook:(id)sender{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    __block int isOn =  0;

    if([sender isOn]){
    
        
        if(![[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]){
            
            FBSDKLoginManager *fbloginmanager = [[FBSDKLoginManager alloc] init];
            [fbloginmanager logInWithPublishPermissions:@[@"publish_actions"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
                
                if(result.isCancelled){
                    
                    [sender setOn:NO];
                }else{
                
                    isOn = 1;
                    [defaults setObject:[NSNumber numberWithInt:1] forKey:@"publicarFacebook"];
                    
                    NSMutableURLRequest *url = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setcompartirfb.php"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
                    
                    [url setHTTPMethod:@"POST"];
                    [url setHTTPBody:[[NSString stringWithFormat:@"id=%@&tof=%d",[defaults objectForKey:@"identifier"], isOn] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                    
                    [NSURLConnection sendAsynchronousRequest:url queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                    }];
                }
            }];
        }else{
        
            isOn = 1;
            [defaults setObject:[NSNumber numberWithInt:1] forKey:@"publicarFacebook"];
            
            NSMutableURLRequest *url = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setcompartirfb.php"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
            
            [url setHTTPMethod:@"POST"];
            [url setHTTPBody:[[NSString stringWithFormat:@"id=%@&tof=%d",[defaults objectForKey:@"identifier"], isOn] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:url queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            }];
        }
    }else{
    
        isOn = 0;
        [defaults setObject:[NSNumber numberWithInt:0] forKey:@"publicarFacebook"];
        
        NSMutableURLRequest *url = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setcompartirfb.php"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
        
        [url setHTTPMethod:@"POST"];
        [url setHTTPBody:[[NSString stringWithFormat:@"id=%@&tof=%d",[defaults objectForKey:@"identifier"], isOn] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:url queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
