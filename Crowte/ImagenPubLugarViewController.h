//
//  ImagenPubLugarViewController.h
//  Crowte
//
//  Created by Paco Escobar on 06/02/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface ImagenPubLugarViewController : UIViewController <UIScrollViewDelegate, NSURLSessionDelegate, NSURLSessionDownloadDelegate>

@property BOOL zoomed;
@property float primerToqueMover;
@property NSData *imagenUsuario;
@property UIImage *fotoAnterior;
@property UIImage *fotoAmostrar;
@property (strong, nonatomic) IBOutlet UIView *fullScreenVideo;
@property UIPanGestureRecognizer *panGesture;
@property NSDictionary *json;
@property UIView *panel;
@property UIButton *cerrar;
@property CGPoint centroInicial;
@property (weak, nonatomic) IBOutlet UIImageView *fondoImgAnterior;
@property (weak, nonatomic) IBOutlet UIImageView *fotoQueSeMuestra;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray* constraints;
@property (weak, nonatomic) IBOutlet UIView *ContentView;


#pragma mark - reproductor de video

@property AVPlayer *reproductor;
@property AVPlayerLayer *layer;
@property UIView *controlesReproductor;
@property NSURL *urlVideoApasar;
@property UIProgressView *progress;

-(void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer;
-(void)imagenMovida:(UIPanGestureRecognizer*)recognizer;
-(void)unTapeo:(UITapGestureRecognizer*)recognizer;
-(void)cerrarImagen;
-(IBAction)likeComentario:(id)sender;
-(void)cargarVideo;

-(void)loopVideo:(NSNotification *)notification;

@end
