//
//  AceptarCondicionesLugarViewController.h
//  Crowte
//
//  Created by Paco Escobar on 30/03/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AceptarCondicionesLugarViewController : UIViewController<NSURLSessionDelegate, NSURLSessionDownloadDelegate>

@property (strong, nonatomic) IBOutlet UITextView *textView;


- (IBAction)aceptarCondiciones:(id)sender;

@end
