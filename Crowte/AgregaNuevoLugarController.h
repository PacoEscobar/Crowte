//
//  AgregaNuevoLugarController.h
//  Crowte
//
//  Created by Paco Escobar on 21/05/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AgregaNuevoLugarController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, CLLocationManagerDelegate>


@property int numVecesLugar;
@property (weak, nonatomic) IBOutlet UIPickerView *cercaniaLugar;
@property (strong, nonatomic) NSArray *distanciasDelLugar;
@property (weak, nonatomic) IBOutlet UITextField *nombreLugar;
@property CLLocationManager *locationManager;

-(IBAction)enviar:(id)sender;
-(void)getCoordenadas;

@end
