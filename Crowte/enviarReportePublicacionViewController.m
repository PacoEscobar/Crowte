//
//  enviarReportePublicacionViewController.m
//  Crowte
//
//  Created by Paco Escobar on 04/07/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import "enviarReportePublicacionViewController.h"
#import "DeEjemplo.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface enviarReportePublicacionViewController ()

@end

@implementation enviarReportePublicacionViewController

@synthesize aconID, userID, nombreUsuario, fotoUsuario, dataFoto, nombreLugar, publicacion, tabla;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tabla view

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    int numero;
    
    if(indexPath.row == 0){
    
        numero = 171;
    }else if(indexPath.row == 1){
    
        float numeroCaracteres = [publicacion length];
        
        if(numeroCaracteres == 0){
            
            numeroCaracteres = 0;
        }else{
            
            numeroCaracteres = numeroCaracteres/33;
            
            numeroCaracteres = ceilf(numeroCaracteres);
            
            numeroCaracteres *= 20;
        }
        
        numero = 101+numeroCaracteres;
        
    }else if(indexPath.row == 2){
    
        numero = 217;
    }
    return numero;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda;

    if(indexPath.row == 0){

        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaUsuario"];
        
        
        UIImageView *foto = (UIImageView *)[celda viewWithTag:1];
        foto.image = [UIImage imageWithData:dataFoto];
        
        if(fotoUsuario != nil){
            
            NSString *url = [NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",userID,fotoUsuario];
            
            [foto sd_setImageWithURL:[NSURL URLWithString:url]];
        }
        
        UILabel *nombre = (UILabel *)[celda viewWithTag:2];
        nombre.text = nombreUsuario;
        
    }else if(indexPath.row == 1){
    
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaPublicacion"];
        
        if(![nombreLugar isEqual:[NSNull null]] && ![nombreLugar isEqualToString:@""] && ![nombreLugar isEqualToString:@"(null)"]){
        
            UILabel *lugar = (UILabel *)[celda viewWithTag:2];
            lugar.text = nombreLugar;
        }else{
        
            
            UILabel *en = (UILabel *)[celda viewWithTag:1];
            en.text = @"";
            UILabel *lugar = (UILabel *)[celda viewWithTag:2];
            lugar.text = @"";
        }
        
        if(![publicacion isEqualToString:@""] && ![publicacion isEqualToString:@"(null)"]){
        
            UITextView *acontesimiento = (UITextView *)[celda viewWithTag:3];
            
            NSData *data = [publicacion dataUsingEncoding:NSUTF8StringEncoding];
            
            NSString *publicacionFormateada = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
            
            acontesimiento.text = publicacionFormateada;
        }else{
        
            UITextView *acontesimiento = (UITextView *)[celda viewWithTag:3];
            acontesimiento.text = @"";
        }
        
    }else if(indexPath.row == 2){
    
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaEnviarReporte"];
        
        UITextView *textview = (UITextView *)[celda viewWithTag:1];
        textview.returnKeyType = UIReturnKeyDone;
        textview.text = @"Escribe un comentario";
        
        UIButton *enviar = (UIButton*)[celda viewWithTag:2];
        [enviar addTarget:self action:@selector(enviar) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return celda;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *celda = (UITableViewCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    
    UITextView *textview = (UITextView *)[celda viewWithTag:1];

    [textview resignFirstResponder];
}

#pragma mark - text view

-(void)textViewDidBeginEditing:(UITextView *)textView{

    if([textView.text isEqualToString:@"Escribe un comentario"]){
        textView.text = @"";
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.view.frame = CGRectMake(self.view.frame.origin.x, -200, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}

-(void)textViewDidEndEditing:(UITextView *)textView{

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{

    if([text isEqualToString:@"\n"]){

        [self enviar];
        [textView resignFirstResponder];
    }
    
    return YES;
}


#pragma mark - por mi

-(void)enviar{
    
    __block UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    
    UITextView *comentario = (UITextView*)[celda viewWithTag:1];
    __block UIButton *boton = (UIButton*)[celda viewWithTag:2];
    
   [boton setEnabled:NO];
    NSString *texto = [[NSString alloc] init];
    if(![[comentario text] isEqualToString:@""] && ![[comentario text] isEqualToString:@"Escribe un comentario"]){
    
        texto = [comentario text];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/denuncia/denunciarpost.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"ID=%d&publicacion=%d&denuncia=%@&token=%@",[[defaults objectForKey:@"identifier"] intValue],aconID,texto,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            NSLog(@"error");
        }else{
 
            [UIView animateWithDuration:0.5 animations:^{
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [boton setBackgroundColor:[UIColor colorWithRed:0 green:0.8 blue:0 alpha:1]];
                    [boton setTitle:@"Gracias!" forState:UIControlStateNormal];
                    app.networkActivityIndicatorVisible = NO;
                    
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    DeEjemplo *deejemplo = [storyboard instantiateViewControllerWithIdentifier:@"deejemplo"];
                    
                    [self presentViewController:deejemplo animated:YES completion:nil];
                    
                });
            }];
        }
        
    }];
    
}


-(IBAction)cancelar{

    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
