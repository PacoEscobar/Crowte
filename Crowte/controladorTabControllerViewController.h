//
//  controladorTabControllerViewController.h
//  Crowte
//
//  Created by Paco Escobar on 23/04/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface controladorTabControllerViewController : UITabBarController <UITabBarControllerDelegate>

@property BOOL enLugar;
@property int configuracionBandera;
@property NSString *nombre;
@property NSMutableDictionary *datosUsuario;
@property (retain, nonatomic) UIImage *fotoDeData;

@end
