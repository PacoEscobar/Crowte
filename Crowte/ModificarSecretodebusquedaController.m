//
//  ModificarSecretodebusquedaController.m
//  Crowte
//
//  Created by Paco Escobar on 24/08/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import "ModificarSecretodebusquedaController.h"
#import "controladorTabControllerViewController.h"

@interface ModificarSecretodebusquedaController ()

@end

@implementation ModificarSecretodebusquedaController

@synthesize secreto, tiene, cargador, constraintTop;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [cargador startAnimating];
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar" style:UIBarButtonItemStylePlain target:self action:@selector(atras:)];
    self.navigationItem.leftBarButtonItem = backbutton;
    
    UIBarButtonItem *enviar = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"Enviar"] style:UIBarButtonItemStylePlain target:self action:@selector(enviar)];
    
    self.navigationItem.rightBarButtonItem = enviar;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getsecreto.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"ID=%@&token=%@",[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil];
            
            [error show];
        }else{
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                secreto.text = [json objectForKey:@"secreto"];
            });
        
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if([[json objectForKey:@"tiene"] intValue] == 1){
        
                    [tiene setOn:YES animated:YES];
                }else if([[json objectForKey:@"tiene"] intValue] == 0){
        
                    [tiene setOn:NO animated:YES];
                }
            
        
                [cargador stopAnimating];
                [cargador removeFromSuperview];
                
                if([[UIScreen mainScreen] bounds].size.height <= 480){
                    
                    [UIView animateWithDuration:0.4 animations:^{
                        
                        [constraintTop setConstant:10];
                    }];
                }
            });
        }
    }];

}

-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark controles navegacion

-(void)enviar{
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    if([tiene isOn]){
        
        if(![[secreto text] isEqualToString:@""]){
            
            [secreto resignFirstResponder];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setsecreto.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%@&secreto=%@&tiene=si&token=%@",[defaults objectForKey:@"identifier"], [secreto text],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
                if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                    
                    app.networkActivityIndicatorVisible = NO;
                    
                    [[[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil] show];
                }else{
                
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        app.networkActivityIndicatorVisible = NO;
                        
                        [self atras:self];
                    });
                }
            }];
            
        }else{
        
            app.networkActivityIndicatorVisible = NO;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"Escribe el secreto" message:@"Debes especificar un secreto de búsqueda" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil] show];
            });
        }
            
    }else{
    
        [secreto resignFirstResponder];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setsecreto.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%@&secreto=%@&tiene=no&token=%@",[defaults objectForKey:@"identifier"], [secreto text],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                app.networkActivityIndicatorVisible = NO;
                [[[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil] show];
            }else{
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    app.networkActivityIndicatorVisible = NO;
                    
                    [self atras:self];
                });
            }
        }];
    }
}

-(IBAction)atras:(id)sender{

    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
}

#pragma mark textfield

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [secreto resignFirstResponder];
    
    return YES;
}


#pragma mark - begintouches

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

    
    UITouch *toque = [[event allTouches] anyObject];
    
    if([secreto isFirstResponder] && [toque view] != secreto){
        
        [secreto resignFirstResponder];
    }

}

@end
