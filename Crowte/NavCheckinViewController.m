//
//  NavCheckinViewController.m
//  Crowte
//
//  Created by Paco Escobar on 23/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import "NavCheckinViewController.h"
#import "CheckinViewController.h"

@interface NavCheckinViewController ()

@end

@implementation NavCheckinViewController

@synthesize datosCheckin;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CheckinViewController *checkinVC = (CheckinViewController*)[self topViewController];
    
    [checkinVC setDatosEnLugar:datosCheckin];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
