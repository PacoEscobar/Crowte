//
//  RegistroFaceViewController.h
//  Crowte
//
//  Created by Paco Escobar on 31/01/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistroFaceViewController : UIViewController <UITextViewDelegate, UITextFieldDelegate>


@property int fbID;

@property NSString *strNombre;
@property NSString *strApellido;
@property NSString *strFoto;
@property NSString *correo;
@property (weak, nonatomic) IBOutlet UITextView *textViewCondiciones;

@property (weak, nonatomic) IBOutlet UIImageView *foto;
@property (weak, nonatomic) IBOutlet UILabel *nombre;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;


- (IBAction)enviar:(id)sender;
- (IBAction)cancelar:(id)sender;


@end
