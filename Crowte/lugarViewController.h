//
//  lugarViewController.h
//  Crowte
//
//  Created by Paco Escobar on 03/07/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CamaraOverlayView.h"
#import "CamaraOverlayViewController.h"
#import "KLCPopup.h"

@interface lugarViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CamaraOverlayViewControllerDelegate, NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate>

@property BOOL alreadyVideo;
@property BOOL privado;
@property BOOL seTomaFoto;
@property BOOL propietario;
@property BOOL imagenCargada;
@property BOOL timerMensajesAlready;
@property BOOL timerDatosAlready;
@property BOOL ocultarTutoMensajeDirecto;
@property int idLugar;
@property int numeroDeFotos;
@property int ultimoMensaje;
@property int votado;
@property int enlugar;
@property int terminaCarga;
@property int videofoto;
@property unsigned long totalBytesArchivoAsubir;
@property unsigned long bytesSubidosArchivoAsubir;


@property NSMutableArray *comentariosLugar;
@property NSMutableDictionary *fotoComentario;
@property __block NSData *dataFoto;
@property UIActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet UILabel *lugar;
@property __block NSString *nombreLugar;
@property __block NSString *fotoLugar;
@property NSTimer *mensajes;
@property NSTimer *datos;
@property NSData *dataFotoNueva;
@property UIImagePickerController *picker;
@property CamaraOverlayView *camara;
@property KLCPopup *popupFullSVideo;
@property AVPlayer *reproductor;
@property AVPlayerLayer *layer;
@property UIView *controlesReproductor;
@property NSURL *urlVideoApasar;
@property (retain, nonatomic) UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *vista;
@property (weak, nonatomic) IBOutlet UITableView *tabla;
@property (weak, nonatomic) IBOutlet UIImageView *imagenLugar;
@property (weak, nonatomic) IBOutlet UILabel *numeroPersonas;
@property (weak, nonatomic) IBOutlet UILabel *numeroAmigos;
@property (weak, nonatomic) IBOutlet UILabel *topPlace;
@property (weak, nonatomic) IBOutlet UILabel *numeroVotos;
@property (weak, nonatomic) IBOutlet UIView *barraVotos;
@property (weak, nonatomic) IBOutlet UILabel *textoNoEstasAqui;
@property (weak, nonatomic) IBOutlet UIButton *votoPositivo;
@property (weak, nonatomic) IBOutlet UIButton *votoNegativo;
@property (weak, nonatomic) IBOutlet UILabel *etiquetaMensajes;
@property (weak, nonatomic) IBOutlet UITextField *comentarLugar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;

@property (weak, nonatomic) IBOutlet UIButton *enviarMensajeBoton;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthTextField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintEnviaMensajeB;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintRightEnviaM;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthEnviar;





-(void)getDatos;
-(void)getMensajes;
-(void)getNuevosMensajes;
-(IBAction)votoPositivo:(id)sender;
-(IBAction)votoNegativo:(id)sender;
-(IBAction)enviarMensaje:(id)sender;
-(IBAction)opcionesLugar:(id)sender;
- (IBAction)seleccionDeControl:(UISegmentedControl *)sender;
-(IBAction)likeComentario:(id)sender;
-(IBAction)privatizarMensaje:(id)sender;
- (IBAction)textoCambio:(id)sender;
-(IBAction)cerrarImagen:(id)sender;
-(void)expandirImagen:(UIGestureRecognizer*)gesto;
-(IBAction)cerrarTutoMensajeDirecto:(id)sender;
-(void)cerrarVideoFullScreen:(UIPanGestureRecognizer *)gesto;

#pragma mark - camara

-(void)tomarVideo:(UILongPressGestureRecognizer *)gesto;
-(void)tomarFoto:(UILongPressGestureRecognizer *)gesto;
-(void)cambiarCamara;
-(void)flash;
-(void)cerrarCamara;

#pragma mark - circular progress

-(void)dibujarProgresoCircular:(UIImageView*)imagenView progreso:(float)progreso;

@end
