//
//  AmigosFromFacebookViewController.m
//  Crowte
//
//  Created by Paco Escobar on 30/05/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import "AmigosFromFacebookViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface AmigosFromFacebookViewController ()

@end

@implementation AmigosFromFacebookViewController

@synthesize activity, tabla, infoAmigos;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    infoAmigos = [NSMutableArray array];
    
    NSString *fbID = [FBSDKAccessToken currentAccessToken].userID;
    
    if(fbID == nil){
    
        [infoAmigos removeAllObjects];
        [activity stopAnimating];
        [tabla reloadData];
        [tabla setHidden:false];
    
    }else{

        FBSDKGraphRequest *amigosRequest = [[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"/%@/friends",fbID] parameters:@{@"fields":@"id, first_name, last_name, email"} HTTPMethod:@"GET"];
        
        [amigosRequest startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error){
            
            NSDictionary *amigos = [[NSDictionary alloc] initWithDictionary:(NSDictionary*)result];

            NSString *fbids = @"(";
            int iteracion = 0;
            for (NSDictionary *amigo in [amigos objectForKey:@"data"]) {

                if(iteracion < [[amigos objectForKey:@"data"] count]-1){
                    
                    fbids = [NSString stringWithFormat:@"%@%@,",fbids,[amigo objectForKey:@"id"]];
                }else{
                    
                    fbids = [NSString stringWithFormat:@"%@%@",fbids,[amigo objectForKey:@"id"]];
                }
                
                iteracion++;
            }
            
            fbids = [NSString stringWithFormat:@"%@)",fbids];

            NSMutableURLRequest *url = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/getamigosconfbid.php"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
            
            [url setHTTPMethod:@"POST"];
            [url setHTTPBody:[[NSString stringWithFormat:@"fbids=%@",fbids] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:url queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
                
                if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                    
                    [[[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil] show];
                }else{
                    
                    NSDictionary *amigos_fb = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                    NSLog(@"amigos fb %@",amigos_fb);
                    if([[amigos_fb objectForKey:@"cantidad"] intValue] > 0){

                        for (NSDictionary *amigos_fb_iteracion in [amigos_fb objectForKey:@"amigos"]){
                            
                            [infoAmigos addObject:amigos_fb_iteracion];
                        }
                    }else if([[amigos_fb objectForKey:@"cantidad"] intValue] == 0){
                    
                        [infoAmigos addObject:amigos_fb];
                    }

                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [tabla reloadData];
                        [activity stopAnimating];
                        [tabla setHidden:false];
                    });
                }
                
            }];
        }];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    NSUInteger cantidad = 0;
    
    if([infoAmigos count] > 0){
        if([infoAmigos count] == 0){
            
            cantidad = 1;
        }else{
            
            cantidad = [infoAmigos count];
        }
    }else{
    
        cantidad = 1;
    }
    return cantidad;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    CGFloat size = 0;
    
    if([infoAmigos count] > 0){
        
        if([[[infoAmigos objectAtIndex:0] objectForKey:@"cantidad"] isEqual:[NSNumber numberWithInt:0]]){
            
            size =  169;
        }else{
            
            size = 97;
        }
    }else{
    
        size = 185;
    }
    
    return size;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda;

    if([infoAmigos count] > 0){

        if(![[[infoAmigos objectAtIndex:0] objectForKey:@"cantidad"] isEqual:[NSNumber numberWithInt:0]]){
            
            celda = [tableView dequeueReusableCellWithIdentifier:@"celdaamigofacebook"];
            
            UIImageView *foto = (UIImageView*)[celda viewWithTag:1];
            [foto sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/%@",[[infoAmigos objectAtIndex:indexPath.row] objectForKey:@"ID"],[[infoAmigos objectAtIndex:indexPath.row] objectForKey:@"foto"]]]];
            foto.layer.cornerRadius = 10;
            foto.layer.masksToBounds = YES;
            
            UILabel *nombre = (UILabel*)[celda viewWithTag:2];
            
            if(![[[infoAmigos objectAtIndex:indexPath.row] objectForKey:@"apellido"] isEqualToString:@"(null)"]){
                
                nombre.text = [NSString stringWithFormat:@"%@ %@",[[infoAmigos objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[infoAmigos objectAtIndex:indexPath.row] objectForKey:@"apellido"]];
            }else{
                
                nombre.text = [[infoAmigos objectAtIndex:indexPath.row] objectForKey:@"nombre"];
            }
        }else{
        
            celda = [tableView dequeueReusableCellWithIdentifier:@"noAmigosFacebook"];
            
            UIButton *invitarAmigos = (UIButton*)[celda viewWithTag:1];
            
            [invitarAmigos addTarget:self action:@selector(invitarAmigos:) forControlEvents:UIControlEventTouchUpInside];
            
            [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        }
    }else{

        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaNoConectado"];
        
        UIButton *fblogin = (UIButton*)[celda viewWithTag:1];
        
        [fblogin addTarget:self action:@selector(loginFacebook:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return celda;
}


-(IBAction)loginFacebook:(id)sender{

    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    
    [loginManager logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){

        if(!result.isCancelled && !error){
        
            NSString *fbID = [FBSDKAccessToken currentAccessToken].userID;
            
            FBSDKGraphRequest *amigosRequest = [[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"/%@/friends",fbID] parameters:@{@"fields":@"id, first_name, last_name, email"} HTTPMethod:@"GET"];
            
            [amigosRequest startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error){
                
                NSDictionary *amigos = [[NSDictionary alloc] initWithDictionary:(NSDictionary*)result];
                
                NSString *fbids = @"(";
                int iteracion = 0;
                for (NSDictionary *amigo in [amigos objectForKey:@"data"]) {
                    
                    if(iteracion < [[[amigos objectForKey:@"summary"] objectForKey:@"total_count"] intValue]-1){
                        
                        fbids = [NSString stringWithFormat:@"%@%@,",fbids,[amigo objectForKey:@"id"]];
                    }else{
                        
                        fbids = [NSString stringWithFormat:@"%@%@",fbids,[amigo objectForKey:@"id"]];
                    }
                    
                    iteracion++;
                }
                
                fbids = [NSString stringWithFormat:@"%@)",fbids];
                
                NSMutableURLRequest *url = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/getamigosconfbid.php"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
                
                [url setHTTPMethod:@"POST"];
                [url setHTTPBody:[[NSString stringWithFormat:@"fbids=%@",fbids] dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                
                [NSURLConnection sendAsynchronousRequest:url queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                    
                    
                    if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                        
                        [[[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil] show];
                    }else{
                        
                        NSDictionary *amigos_fb = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                        
                        if([[amigos_fb objectForKey:@"cantidad"] intValue] > 0){
                            
                            for (NSDictionary *amigos_fb_iteracion in [amigos_fb objectForKey:@"amigos"]){
                                
                                [infoAmigos addObject:amigos_fb_iteracion];
                            }
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [tabla reloadData];
                            [activity stopAnimating];
                            [tabla setHidden:false];
                        });
                    }
                    
                }];
            }];
        }
    }];
}

-(IBAction)invitarAmigos:(id)sender{

    FBSDKAppInviteContent *content = [[FBSDKAppInviteContent alloc] init];
    
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/801579789972147"];
    content.appInvitePreviewImageURL = [NSURL URLWithString:@"https://www.crowte.com/imagenes/comparte.jpg"];
    
    [FBSDKAppInviteDialog showFromViewController:self withContent:content delegate:self];
}

#pragma mark - facebook invites

-(void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error{}

-(void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results{}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
