//
//  OlvidoPassViewController.h
//  Crowte
//
//  Created by Paco Escobar on 29/07/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OlvidoPassViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *correo;
@property (strong, nonatomic) IBOutlet UIButton *botonEnviar;



-(IBAction)enviar:(id)sender;
-(BOOL)checarCorreo:(NSString *) correotxt;
-(void)ocultarTeclado;

@end
