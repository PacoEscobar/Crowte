//
//  registro.h
//  crowte
//
//  Created by Paco Escobar on 10/05/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface registro : UIViewController<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nombreR;
@property (weak, nonatomic) IBOutlet UITextField *apellidoR;
@property (weak, nonatomic) IBOutlet UITextField *correoR;
@property (weak, nonatomic) IBOutlet UITextField *passwordR;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alturaLogoCrowte;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCrowteCancelar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLogo;
@property (weak, nonatomic) IBOutlet UITextView *textViewCondiciones;



-(BOOL)checarCorreo:(NSString *) correo;
-(IBAction)clickFondo:(id)sender;
-(IBAction)enviar:(id)boton;

@end
