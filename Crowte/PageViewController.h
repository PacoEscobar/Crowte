//
//  PageViewController.h
//  Crowte
//
//  Created by Paco Escobar on 19/01/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TutorialPrincipal.h"

@interface PageViewController : UIViewController <UIPageViewControllerDataSource>

@property NSMutableDictionary *datosUsuario;
@property (strong, nonatomic) UIPageViewController *pageViewController;

-(TutorialPrincipal *)viewControllerAtIndex:(NSUInteger)index;

@end
