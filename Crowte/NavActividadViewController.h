//
//  NavActividadViewController.h
//  Crowte
//
//  Created by Paco Escobar on 12/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavActividadViewController : UINavigationController

@property NSMutableArray *checkinsTodosOtros;
@property NSMutableArray *checkinsTodosUltimos;

@end
