//
//  RegistroFaceViewController.m
//  Crowte
//
//  Created by Paco Escobar on 31/01/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import "RegistroFaceViewController.h"
#import "NSString+MD5.h"
#import "ActivarCuentaViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface RegistroFaceViewController ()

@end

@implementation RegistroFaceViewController

@synthesize correo,password,strFoto,strNombre,strApellido,nombre,foto,activity,fbID,textViewCondiciones;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [foto sd_setImageWithURL:[NSURL URLWithString:strFoto] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType ,NSURL *imageURL){
    
        [activity stopAnimating];
        [activity removeFromSuperview];
    }];
    
    [nombre setText:strNombre];
    
    NSMutableAttributedString *stringCondiciones = textViewCondiciones.attributedText.mutableCopy;
    
    [stringCondiciones setAttributes:@{NSLinkAttributeName:@"condiciones"} range:NSMakeRange(28, 18)];
    [stringCondiciones setAttributes:@{NSLinkAttributeName:@"privacidad"} range:NSMakeRange(78, 22)];
    textViewCondiciones.attributedText = stringCondiciones;
    textViewCondiciones.delegate = self;
}

-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    
    if([URL.absoluteString isEqualToString:@"condiciones"]){
        
        [self performSegueWithIdentifier:@"condicionesSegue" sender:self];
        
    }else if([URL.absoluteString isEqualToString:@"privacidad"]){
        
        [self performSegueWithIdentifier:@"privacidadSegue" sender:self];
    }
    
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)enviar:(id)sender {
    
    sender = (UIButton *)sender;
    
    [sender setEnabled:NO];
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/registrar.php"]];
    
    UIApplication *app = [UIApplication sharedApplication];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%@&apeido=%@&correo=%@&contra=%@&recurso=2&fbid=%d&foto=%@",strNombre,strApellido,correo,[[password text] MD5String],fbID,strFoto] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            app.networkActivityIndicatorVisible = NO;
        }else{
            
            NSString *strid = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            if([strid intValue] == 0){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController *yaexiste = [UIAlertController alertControllerWithTitle:nil message:@"Este usuario ya está registrado" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *muybien = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [sender setEnabled:YES];
                    }];
                    
                    [yaexiste addAction:muybien];
                    
                    [self presentViewController:yaexiste animated:YES completion:nil];
                });
                
            }else{
                
                NSUserDefaults *ide = [NSUserDefaults standardUserDefaults];
                [ide setObject:[NSNumber numberWithInt:1] forKey:@"id"];
                [ide setObject:strid forKey:@"identifier"];
                [ide synchronize];
                
                [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
                [[UIApplication sharedApplication] registerForRemoteNotifications];
                
                
                NSURL *url = [NSURL URLWithString:@"https://www.crowte.com/app/getdatos.php"];
                
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[[NSString stringWithFormat:@"id=%d&recurso=1",[strid intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
                
                NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                
                [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                    
                    NSMutableDictionary *datosUsuario = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        app.networkActivityIndicatorVisible = NO;
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        ActivarCuentaViewController *ida = [storyboard instantiateViewControllerWithIdentifier:@"insertacodigo"];
                        
                        ida.datosUsuario = datosUsuario;
                        [self presentViewController:ida animated:YES completion:NULL];
                        
                    });
                }];
                
            }
        }
        
    }];
}

- (IBAction)cancelar:(id)sender {
    
    [[FBSDKLoginManager new] logOut];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    if([password isFirstResponder]){
        [password resignFirstResponder];
        [UIView animateWithDuration:0.4 animations:^{
            
            self.view.center = CGPointMake(self.view.center.x, self.view.center.y+100);
        }];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{

    [UIView animateWithDuration:0.4 animations:^{
    
        self.view.center = CGPointMake(self.view.center.x, self.view.center.y-100);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
