//
//  NavegacionPrincipalController.h
//  Crowte
//
//  Created by Paco Escobar on 19/05/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavegacionPrincipalController : UINavigationController

@property NSDictionary *tutoriales;

@end
