//
//  ReportarUsuarioViewController.h
//  Crowte
//
//  Created by Paco Escobar on 02/08/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportarUsuarioViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>


@property long idUsuario;
@property NSData *dataFoto;
@property NSString *nombre;
@property NSString *nombreFoto;
@property (weak, nonatomic) IBOutlet UITableView *tabla;

- (IBAction)enviar:(id)sender;
- (IBAction)regresar:(id)sender;


@end
