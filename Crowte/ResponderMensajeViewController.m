//
//  ResponderMensajeViewController.m
//  Crowte
//
//  Created by Paco Escobar on 12/02/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ResponderMensajeViewController.h"
#import "ImagenPubLugarViewController.h"

@interface ResponderMensajeViewController ()

@end

@implementation ResponderMensajeViewController

@synthesize nombre,fecha,fotoUsuario,publicacion,numDeAmor,comentarios, datosPublicacion,ComentarTextField,BotonEnviar, constraintResponderBottom, constraintBotonEnviarBottom, imagenComentario, tabla,dataImagenComentario, esVideo, urlVideo, reproductor, layer;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"fondonavigationbar.jpg"] forBarMetrics:UIBarMetricsDefault];
    
    comentarios = [[NSMutableArray alloc] init];
    
    ComentarTextField.layer.borderColor = [[UIColor colorWithRed:0 green:180.0/255.0 blue:180.0/255.0 alpha:1] CGColor];
    ComentarTextField.layer.borderWidth = 1.0;
    ComentarTextField.layer.cornerRadius = 0;
    ComentarTextField.layer.masksToBounds = YES;
    
    if(dataImagenComentario != nil && esVideo == 0){
    
        imagenComentario = [[UIImage alloc] initWithData:dataImagenComentario];
       
    }
    
    [self getComentarios];
}

-(void)viewDidDisappear:(BOOL)animated{

    if(reproductor){
        
        [reproductor removeObserver:self forKeyPath:@"status"];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
        layer = nil;
        reproductor = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    NSInteger numero;

    if([comentarios count] > 0){
    
        numero = [comentarios count]+1;
    }else{
    
        numero = 1;
    }
    
    return numero;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    float size;
    float numeroCaracteres=0;
    
    if(indexPath.row == 0){
    
        numeroCaracteres = [publicacion length];
        
        if(numeroCaracteres == 0){
            
            numeroCaracteres = 0;
        }else{
            
            numeroCaracteres = numeroCaracteres/29;
            
            numeroCaracteres = ceilf(numeroCaracteres);
            
            numeroCaracteres *= 15;
        }
    }else{
    
        numeroCaracteres = [[[comentarios objectAtIndex:indexPath.row-1] objectForKey:@"comentario"] length];
        
        if(numeroCaracteres == 0){
            
            numeroCaracteres = 0;
        }else{
            
            numeroCaracteres = numeroCaracteres/31;
            
            numeroCaracteres = ceilf(numeroCaracteres);
            
            numeroCaracteres *= 15;
        }
    }
    
    if((imagenComentario != nil && indexPath.row == 0) || (urlVideo != nil && indexPath.row == 0)){
        
        size = 350.0+numeroCaracteres;
    }else if(indexPath.row == 0){
    
        size = 123.0+numeroCaracteres;
    }else{
    
        size = 71.0+numeroCaracteres;
    }
        
    return size;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda;
    
    if(indexPath.row == 0){
    
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaPrimera"];
        
        UIImageView *fotoPublicacion = (UIImageView *)[celda viewWithTag:1];
        
        [fotoPublicacion setImage:[UIImage imageWithData:fotoUsuario]];
        
        UILabel *nombreUsuario = (UILabel *)[celda viewWithTag:2];
        [nombreUsuario setText:nombre];
        
        NSData *data = [publicacion dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *publicacionFormateada = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        
        UILabel *publicacionPrincipal = (UILabel *)[celda viewWithTag:3];
        [publicacionPrincipal setText:publicacionFormateada];
        
        float numeroCaracteres = [publicacion length];
        
        if(numeroCaracteres == 0){
            
            numeroCaracteres = 0;
        }else{
            
            numeroCaracteres = numeroCaracteres/31;
            
            numeroCaracteres = ceilf(numeroCaracteres);
            
            numeroCaracteres *= 15;
        }
        
        
        if(imagenComentario != nil){
            
            NSLayoutConstraint *constraintLabelBottom = [publicacionPrincipal superview].constraints[10];
            
            if([constraintLabelBottom.identifier isEqualToString:@"botom"]){
                
                [constraintLabelBottom setConstant:264];
            }
        }
                
        UILabel *fechaPublicacion = (UILabel *)[celda viewWithTag:4];
        
        NSDate *fechacomentario = [NSDate date];
        
        NSDateComponents *componentes = [[NSDateComponents alloc] init];
        [componentes setSecond:([[NSDate date] timeIntervalSince1970] - [fecha doubleValue])];
        
        NSCalendar *calendario = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        
        fechacomentario = [calendario dateFromComponents:componentes];
        
        NSDateComponents *fechaComponentes = [calendario components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:fechacomentario];
        
        if([fechaComponentes hour] != 0){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                fechaPublicacion.text = [NSString stringWithFormat:@"Hace %ld hora",(long)[fechaComponentes hour]];
            });
        }else if([fechaComponentes minute] != 0){
            
            if([fechaComponentes minute] == 1){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    fechaPublicacion.text = [NSString stringWithFormat:@"Hace %ld minuto",(long)[fechaComponentes minute]];
                });
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    fechaPublicacion.text = [NSString stringWithFormat:@"Hace %ld minutos",(long)[fechaComponentes minute]];
                });
            }
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                fechaPublicacion.text = @"Hace unos segundos";
            });
        }
        
        if(imagenComentario != nil && esVideo == 0){
        
            UIImageView *fotoPublicacion = [[UIImageView alloc] initWithFrame:CGRectMake(50, 100+numeroCaracteres, 180, 200)];
            
            fotoPublicacion.layer.cornerRadius = 5;
            fotoPublicacion.layer.borderWidth = 0.5;
            fotoPublicacion.layer.masksToBounds = YES;
            fotoPublicacion.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4] CGColor];
            fotoPublicacion.contentMode = UIViewContentModeScaleAspectFill;
            fotoPublicacion.clipsToBounds = YES;
            [fotoPublicacion setCenter:CGPointMake(celda.center.x, fotoPublicacion.center.y)];
            [fotoPublicacion setUserInteractionEnabled:YES];
            [fotoPublicacion setTag:7];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(expandirImagen:)];
            
            [tap setNumberOfTapsRequired:1];
            [fotoPublicacion addGestureRecognizer:tap];
            
            
            [fotoPublicacion setImage:imagenComentario];
            
            
            
            [celda addSubview:fotoPublicacion];
        }else if(esVideo == 1){
            
            NSString *urlEmpieza = [[urlVideo absoluteString] substringWithRange:NSMakeRange(0, 4)];

            if(reproductor == nil){
                
                if([urlEmpieza isEqualToString:@"http"]){
                    
                    UIProgressView *progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
                    
                    [progress setFrame:CGRectMake(50, 150, 150, 50)];
                    
                    [progress setTrackTintColor:[UIColor redColor]];
                    [progress setProgressTintColor:[UIColor blueColor]];
                    [progress setBackgroundColor:[UIColor yellowColor]];
                    
                    progress.progress = .25;
                    
                    [progress setTag:100];
                    
                    [celda addSubview:progress];
                    
                    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                    
                    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
                    
                    NSURLSessionDownloadTask *uploadTask = [urlSession downloadTaskWithURL:urlVideo];
                    
                    [uploadTask resume];
                }else if([urlEmpieza isEqualToString:@"file"]){
                    
                    UIView *controlesReproductor = [[UIView alloc] init];
                    
                    AVAsset *asset = [AVAsset assetWithURL:urlVideo];
                    
                    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
                    
                    if(reproductor){
                        
                        [reproductor removeObserver:self forKeyPath:@"status"];
                        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
                    }
                    reproductor = [AVPlayer playerWithPlayerItem:item];
                    
                    reproductor.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loopVideo:) name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
                    
                    layer = [AVPlayerLayer playerLayerWithPlayer:reproductor];
                    layer.frame = CGRectMake(100, 100+0, 180, 200);
                    
                    [layer setBackgroundColor:[[UIColor whiteColor] CGColor]];
                    
                    if([celda viewWithTag:10]){
                        
                        controlesReproductor = [celda viewWithTag:10];
                        controlesReproductor.frame = CGRectMake(50, 100+0, 180, 200);
                    }else{
                        controlesReproductor = [[UIView alloc] init];
                        controlesReproductor.frame = CGRectMake(50, 100+0, 180, 200);
                        [controlesReproductor setTag:10];
                    }
                    
                    UITapGestureRecognizer *pausaVideo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pausarVideo)];
                    
                    [controlesReproductor addGestureRecognizer:pausaVideo];
                    
                    [celda.layer addSublayer:layer];
                    [celda addSubview:controlesReproductor];
                    
                    [reproductor addObserver:self forKeyPath:@"status" options:0 context:nil];
                    
                    [reproductor play];
                    
                }
            }
        }
        
        UIButton *heart = (UIButton*)[celda viewWithTag:5];
        
        UIButton *numDeCorazones = (UIButton *)[celda viewWithTag:6];
        
        if(![numDeAmor isEqual:@"(null)"]){
            [numDeCorazones setTitle:numDeAmor forState:UIControlStateNormal];
        }else{
            [numDeCorazones setTitle:@"" forState:UIControlStateNormal];
        }
        
        if([[datosPublicacion objectForKey:@"likeMio"] intValue] == 1){
        
            [heart setBackgroundImage:[UIImage imageNamed:@"redheart.png"] forState:UIControlStateNormal];
            [numDeCorazones setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        }else{
        
            [heart setBackgroundImage:[UIImage imageNamed:@"grayheart.png"] forState:UIControlStateNormal];
            [numDeCorazones setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
    }else{
        
        celda = [tableView dequeueReusableCellWithIdentifier:@"comentariodecomentario"];
        
        UIImageView *fotoComentario = (UIImageView*)[celda viewWithTag:1];
        [fotoComentario sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[[comentarios objectAtIndex:indexPath.row-1] objectForKey:@"ID"] intValue],[[comentarios objectAtIndex:indexPath.row-1] objectForKey:@"foto"]]]];
        
        fotoComentario.layer.cornerRadius = 5;
        fotoComentario.layer.borderWidth = 0.5;
        fotoComentario.layer.masksToBounds = YES;
        fotoComentario.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4] CGColor];
        
        UILabel *nombreComentario = (UILabel*)[celda viewWithTag:2];
        [nombreComentario setText:[NSString stringWithFormat:@"%@ %@",[[comentarios objectAtIndex:indexPath.row-1] objectForKey:@"nombre"],[[comentarios objectAtIndex:indexPath.row-1] objectForKey:@"apellido"]]];
        
        UILabel *comentarioComentario = (UILabel*)[celda viewWithTag:3];
        
        NSData *dataComentario = [[[comentarios objectAtIndex:indexPath.row-1] objectForKey:@"comentario"] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *comentarioFormateado = [[NSString alloc] initWithData:dataComentario encoding:NSNonLossyASCIIStringEncoding];
        
        [comentarioComentario setText:comentarioFormateado];
        
    }
    
    return celda;
}

#pragma mark - textfield

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.4 animations:^{
            
            [constraintResponderBottom setConstant:220];
            [constraintBotonEnviarBottom setConstant:220];
        }];
    });
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{

    [UIView animateWithDuration:0.4 animations:^{
        
        [constraintResponderBottom setConstant:9];
        [constraintBotonEnviarBottom setConstant:9];
    }];
    
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - hechos por mi

- (IBAction)enviarComentario:(id)sender {
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    [BotonEnviar setEnabled:NO];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *data =[[ComentarTextField text] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *comentario = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/enviarcomentariodecomentario.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&idcomentario=%d&comentario=%@",[[defaults objectForKey:@"identifier"] intValue],[[datosPublicacion objectForKey:@"mensajeID"] intValue],comentario] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            app.networkActivityIndicatorVisible = NO;
            [ComentarTextField setText:@""];
            [ComentarTextField resignFirstResponder];
            [BotonEnviar setEnabled:YES];
        });
    }];
}


-(void)expandirImagen:(UIGestureRecognizer *)gesto{
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [self.view.layer renderInContext:context];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UITableViewCell *celda = (UITableViewCell*)[gesto.view superview];
    UIImageView *imagenView = (UIImageView*)[celda viewWithTag:1];
    
    ImagenPubLugarViewController *imgPub = [storyboard instantiateViewControllerWithIdentifier:@"imgPubLugarView"];
    
    imgPub.fotoAnterior = img;
    imgPub.fotoAmostrar = [(UIImageView*)gesto.view image];
    imgPub.imagenUsuario = UIImageJPEGRepresentation([imagenView image], 90);
    imgPub.json = datosPublicacion;
    [self presentViewController:imgPub animated:NO completion:^{}];
    
}

-(void)getComentarios{
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getcomentariosdecomentarios.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"idcomentario=%d",[[datosPublicacion objectForKey:@"mensajeID"] intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

        if(data == NULL){
            
            [self getComentarios];
            
        }else{
            
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

            if([json count] > 0){
                
                for(NSDictionary *intervalo in json){
                    
                    if([[intervalo objectForKey:@"valido"] intValue] == 1){
                        
                        [comentarios addObject:intervalo];
                    }else{
                        
                        app.networkActivityIndicatorVisible = NO;
                    }
                    
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [tabla reloadData];
                });
            }
        }
        
    }];
}

-(void)loopVideo:(NSNotification *)notification{
    
    AVPlayerItem *item = [notification object];
    
    [item seekToTime:kCMTimeZero];
}

-(void)pausarVideo{
    
    if([reproductor rate] == 0.0){
        [reproductor play];
    }else{
        
        [reproductor pause];
    }
}

#pragma mark - observer

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
    if(object == reproductor && [keyPath isEqualToString:@"status"]){
        
        if(reproductor.status == AVPlayerStatusReadyToPlay){
            
            //[reproductor play];
        }else if(reproductor.status == AVPlayerStatusFailed){
            
            NSLog(@"Error: %@",reproductor.error);
        }
    }
}

#pragma mark - nsurlsession

-(void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
    
    
    NSLog(@"se envió data %ld esperados %ld de %ld",(long)bytesWritten, (long)totalBytesWritten, (long)totalBytesExpectedToWrite);
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    
    NSError *errormv;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"crowte.mov"];
    
    [fileManager copyItemAtPath:[location path] toPath:filePath error:&errormv];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UITableViewCell *celda = (UITableViewCell *)[tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        UIView *controlesReproductor = [[UIView alloc] init];
      
        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:filePath]];
        
        AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
        
        if(reproductor){
            
            [reproductor removeObserver:self forKeyPath:@"status"];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
        }
        reproductor = [AVPlayer playerWithPlayerItem:item];
        
        reproductor.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loopVideo:) name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
        
        layer = [AVPlayerLayer playerLayerWithPlayer:reproductor];
        layer.frame = CGRectMake(100, 100+0, 180, 200);

        [layer setBackgroundColor:[[UIColor whiteColor] CGColor]];
        
        if([celda viewWithTag:10]){
            
            controlesReproductor = [celda viewWithTag:10];
            controlesReproductor.frame = CGRectMake(50, 100+0, 180, 200);
        }else{
            controlesReproductor = [[UIView alloc] init];
            controlesReproductor.frame = CGRectMake(50, 100+0, 180, 200);
            [controlesReproductor setTag:10];
        }
        
        UITapGestureRecognizer *pausaVideo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pausarVideo)];
        
        [controlesReproductor addGestureRecognizer:pausaVideo];
        
        [celda.layer addSublayer:layer];
        [celda addSubview:controlesReproductor];
        
        [reproductor addObserver:self forKeyPath:@"status" options:0 context:nil];
        
        [reproductor play];
        
        UIProgressView *prog = (UIProgressView*)[celda viewWithTag:100];
        
        [prog removeFromSuperview];
        
    });
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes{}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    
}


@end
