//
//  ReclamarLugarV1ViewController.h
//  Crowte
//
//  Created by Paco Escobar on 23/03/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReclamarLugarV1ViewController : UIViewController <NSURLSessionDelegate, NSURLSessionDownloadDelegate, UITextViewDelegate>

@property int id_lugar;
@property (strong, nonatomic) IBOutlet UITextView *textViewCondiciones;


- (IBAction)reclamarLugar:(id)sender;
- (IBAction)cancelar:(id)sender;

@end
