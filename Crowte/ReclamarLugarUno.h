//
//  ReclamarLugarUno.h
//  Crowte
//
//  Created by Paco Escobar on 10/04/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReclamarLugarUno : UIViewController

@property int idlugar;

@property (weak, nonatomic) IBOutlet UITextView *texto1;
@property (weak, nonatomic) IBOutlet UITextView *texto2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintT1_T2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintT2_comenzar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintT1height;


-(IBAction)cancelar;
-(IBAction)comenzar;
@end
