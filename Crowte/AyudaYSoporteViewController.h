//
//  AyudaYSoporteViewController.h
//  Crowte
//
//  Created by Paco Escobar on 12/07/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AyudaYSoporteViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>


@property NSMutableArray *preguntas;

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UITableView *tabla;



- (IBAction)enviarPregunta:(id)sender;


@end
