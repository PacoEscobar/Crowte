//
//  CerrarReportarMensajeViewController.h
//  Crowte
//
//  Created by Paco Escobar on 11/03/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CerrarReportarMensajeViewController : UIViewController<NSURLSessionDelegate, NSURLSessionDownloadDelegate>

// objetivo 1 usuario, objetivo 2 checkin, objetivo 3 mensaje
@property int objetivoDelReporte;

@property NSString *idUsuarioABloquear;
@property (strong, nonatomic) IBOutlet UIButton *botonBloquear;

@property (strong, nonatomic) IBOutlet UILabel *mensaje1;
@property (strong, nonatomic) IBOutlet UILabel *mensaje2;



- (IBAction)cerrarTodo:(id)sender;
- (IBAction)bloquearUsuario:(id)sender;

@end
