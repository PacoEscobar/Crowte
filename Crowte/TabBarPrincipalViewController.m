//
//  TabBarPrincipalViewController.m
//  Crowte
//
//  Created by Paco Escobar on 10/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import "TabBarPrincipalViewController.h"
#import "NavActividadViewController.h"
#import "NavPerfilMioViewController.h"
#import "NavCheckinViewController.h"
#import "Actividad2ViewController.h"

@interface TabBarPrincipalViewController ()

@end

@implementation TabBarPrincipalViewController

@synthesize checkinsTodosOtros,checkinsTodosUltimos, datosCheckinEnLugar, datosMios, locationManager, locationContador;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    locationContador = NO;
    
    [locationManager requestWhenInUseAuthorization];
    
    UITabBar *tabBar = self.tabBar;
    
    [self setDelegate:self];
    
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
    
    [tabBarItem1 setSelectedImage:[[UIImage imageNamed:@"tabhome.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem1 setImage:[[UIImage imageNamed:@"tabhomenot.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem2 setSelectedImage:[[UIImage imageNamed:@"tabexp.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem2 setImage:[[UIImage imageNamed:@"tabexpnot.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem3 setSelectedImage:[[UIImage imageNamed:@"tabcheck.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem3 setImage:[[UIImage imageNamed:@"tabchecknot.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem4 setSelectedImage:[[UIImage imageNamed:@"tabnotif.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem4 setImage:[[UIImage imageNamed:@"tabnotifnot.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem5 setSelectedImage:[[UIImage imageNamed:@"tabperfil.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem5 setImage:[[UIImage imageNamed:@"tabperfilnot.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    self.tabBar.barTintColor = [UIColor colorWithWhite:1 alpha:0.9];
    self.tabBar.translucent = NO;
    
    NavCheckinViewController *navCheckinVC = (NavCheckinViewController*)[[self viewControllers] objectAtIndex:2];
    
    [navCheckinVC setDatosCheckin:datosCheckinEnLugar];
    
    NavPerfilMioViewController *navPerfilVC = (NavPerfilMioViewController*)[[self viewControllers] objectAtIndex:4];
    
    [navPerfilVC setDatosMios:datosMios];
    
    [locationManager startUpdatingLocation];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    if([viewController isKindOfClass:[NavCheckinViewController class] ]){
        
        NavActividadViewController *navActividadVC = (NavActividadViewController*) [[self viewControllers] objectAtIndex:0];
        
        Actividad2ViewController *actividadVC = (Actividad2ViewController*)[navActividadVC topViewController];

        [actividadVC cerrarTutorial:nil];
    }
}

#pragma mark - hechos por mi

-(void)actualizarCoordenadas{}

#pragma mark - location

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    NavActividadViewController *navActividadVC = (NavActividadViewController*) [[self viewControllers] objectAtIndex:0];
    
    if([[navActividadVC topViewController] isKindOfClass:[Actividad2ViewController class]]){
    
        Actividad2ViewController *actividadVC = (Actividad2ViewController*)[navActividadVC topViewController];
        
        [actividadVC setSiLocalizacion];
        
        [actividadVC setTutoriales:[datosCheckinEnLugar objectForKey:@"tutoriales"]];
        [actividadVC setMiLocation:manager.location];
        [actividadVC getCheckins];

        if([[[datosCheckinEnLugar objectForKey:@"tutoriales"] objectForKey:@"hacercheckin1"] intValue] == 0){

            [[actividadVC backgroundTutorial] setHidden:NO];
        }
    }
    
    if(locationContador){
        return;
    }
    
    locationContador = YES;
        
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarcoordenadas.php"]];
    
    [request setHTTPMethod:@"POST"];

    NSData *cuerpo = [[NSString stringWithFormat:@"latitud=%f&longitud=%f&ID=%@",manager.location.coordinate.latitude,manager.location.coordinate.longitude,[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){}];
    
    [uploadTask resume];
    
    [locationManager stopUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{}


#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
