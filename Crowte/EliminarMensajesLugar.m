//
//  EliminarMensajesLugar.m
//  Crowte
//
//  Created by Paco Escobar on 07/08/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "EliminarMensajesLugar.h"

@implementation EliminarMensajesLugar

@synthesize arreglo, elementosAEliminar;

-(id)init{

    self = [super init];
    arreglo = [NSMutableArray array];
    elementosAEliminar = [NSMutableArray array];
    
    return self;
}

-(NSMutableArray*)ejecutar{

    if([elementosAEliminar count] > 0){
    
        [elementosAEliminar removeAllObjects];
    }
    
    for(NSDictionary *elemento in arreglo){
        
        if(([[NSDate date] timeIntervalSince1970] - [[elemento objectForKey:@"fecha"] intValue]) > 7200){
            
            NSUInteger index = [arreglo indexOfObject:elemento];
            
            [elementosAEliminar addObject:[NSNumber numberWithInteger:index]];
            NSLog(@"index %ld",(long)index);
        }
    }
    return elementosAEliminar;
}

-(void)agregarAlArreglo:(id)elemento{

    [arreglo addObject:elemento];
}

-(void)eliminarAlArreglo:(NSMutableArray *)array{

    [arreglo removeObjectsInArray:array];
}

@end
