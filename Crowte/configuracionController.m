//
//  configuracionController.m
//  Crowte
//
//  Created by Paco Escobar on 18/05/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import "configuracionController.h"
#import "controladorTabControllerViewController.h"
#import "principal.h"
#import "CambiarFotoPerfil.h"
#import "AgregaNuevoLugarController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <SDWebImage/UIButton+WebCache.h>

@interface configuracionController ()

@end

@implementation configuracionController

@synthesize nombreUsuario, apellidoUsuario, foto, datosUsuario, nombre, apellido, fotoView, fotoActual, bandera, fotoPerfil, nuevaFoto, fotoCambiada, defaults, fotoBotonPerfil, navBarYInicial;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    defaults = [NSUserDefaults standardUserDefaults];
    navBarYInicial = self.navigationController.navigationBar.frame.origin.y;
    
    UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
    
    self.navigationItem.backBarButtonItem = atras;
    
    nuevaFoto = 0;
        
    [nombre setText:nombreUsuario];
    [apellido setText:apellidoUsuario];
    
    [fotoBotonPerfil setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
    
    [fotoBotonPerfil setTintColor:[UIColor whiteColor]];
    
    [[fotoBotonPerfil layer] setBorderWidth:1];
    [[fotoBotonPerfil layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
    [[fotoBotonPerfil layer] setCornerRadius:50];
    [[fotoBotonPerfil layer] setMasksToBounds:YES];
        
    [fotoBotonPerfil sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/%@",[defaults objectForKey:@"identifier"],fotoActual]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"]];
    
    [fotoBotonPerfil addTarget:self action:@selector(cambiarFotoPerfil:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)viewDidAppear:(BOOL)animated{

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewWillAppear:(BOOL)animated{

    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"transparente.png"]];
    
    [self.navigationController.navigationBar setBackgroundColor:[self.view backgroundColor]];
}

-(void)viewWillDisappear:(BOOL)animated{

    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        
    [self.navigationController.navigationBar setFrame:CGRectMake(self.navigationController.navigationBar.frame.origin.x, navBarYInicial, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
    
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    
    /*if(section == 0){
        
        UIView *vista = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
        
        NSLog(@"vista %f",self.view.frame.size.width);
        
        [vista setBackgroundColor:[UIColor redColor]];
        
        fotoPerfil = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-25, 30, 100, 100)];
        
        [fotoPerfil setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
        
        [[fotoPerfil layer] setBorderWidth:1];
        [[fotoPerfil layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
        [[fotoPerfil layer] setCornerRadius:50];
        [[fotoPerfil layer] setMasksToBounds:YES];
        
        [vista addSubview:fotoPerfil];
        
        [fotoPerfil sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/%@",[defaults objectForKey:@"identifier"],fotoActual]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"imageprofileempty50.png"]];
        
        [fotoPerfil addTarget:self action:@selector(cambiarFotoPerfil:) forControlEvents:UIControlEventTouchUpInside];
        
        return vista;
    }else{
    
        return nil;
    }*/
    
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    if(section == 0){
    
       // return 150.0;
        return UITableViewAutomaticDimension;
    }else{
    
        return UITableViewAutomaticDimension;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor grayColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
    
    [[UINavigationBar appearance] setTranslucent:YES];
        
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setTintColor:[UIColor grayColor]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)checkInfo{
    
    controladorTabControllerViewController *controlador = (controladorTabControllerViewController *)self.tabBarController;
    
    if(controlador.configuracionBandera == 1){
        
        bandera = 1;
        
        datosUsuario = controlador.datosUsuario;

        nombre.text = [[datosUsuario objectForKey:@"datos"] objectForKey:@"nombre"];
        
        if(![[[datosUsuario objectForKey:@"datos"] objectForKey:@"apellido"] isEqualToString:@"(null)"]){
        
            apellido.text = [[datosUsuario objectForKey:@"datos"] objectForKey:@"apellido"];
        }else{
        
            apellido.text = @"Escribe tu apellido";
        }
        
        if(nuevaFoto == 0){
        
            [fotoPerfil setImage:controlador.fotoDeData forState:UIControlStateNormal];
        }else if(nuevaFoto == 1){
        
            [fotoPerfil setImage:fotoCambiada forState:UIControlStateNormal];
        }
        
        
        
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [self.navigationController.navigationBar setFrame:CGRectMake(self.navigationController.navigationBar.frame.origin.x, ([scrollView contentOffset].y*-1)+navBarYInicial, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
}

#pragma mark - beging editing

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    UIBarButtonItem *enviar = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Enviar",nil) style:UIBarButtonItemStylePlain target:self action:@selector(enviar)];
    
    UIBarButtonItem *cancelar = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancelar",nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelar)];
    
    self.navigationItem.rightBarButtonItem = enviar;
    self.navigationItem.leftBarButtonItem = cancelar;

    
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return 11;
    }else{
    
        return 2;
    }
}


#pragma mark - metodos cancelar y enviar

-(void)cancelar{

    [nombre resignFirstResponder];
    [apellido resignFirstResponder];
    
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.leftBarButtonItem = nil;
}

-(void)enviar{

    
    NSUserDefaults *datoUsuario = [NSUserDefaults standardUserDefaults];
    
    NSCharacterSet *seta = [NSCharacterSet whitespaceCharacterSet];
    
    if(![[nombre text] isEqualToString:@""] && ![[apellido text] isEqualToString:@""] && [[[nombre text] stringByTrimmingCharactersInSet:seta] length] > 0 && [[[apellido text] stringByTrimmingCharactersInSet:seta] length] > 0){
        [nombre resignFirstResponder];
        [apellido resignFirstResponder];
    
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/cambiardatos.php"]];
    
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"codigo=1&nombre=%@&apellido=%@&id=%@&token=%@",[nombre text],[apellido text],[datoUsuario objectForKey:@"identifier"],[datoUsuario objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
    
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil];
                
                [error show];
            }else{
                NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
                if([respuesta intValue] == 0){
        
                    UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Ups ! Hubo un error, por favor inténtalo más tarde" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Aceptar", nil];
            
                    [alerta show];
                }
            }
        }];
    
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = nil;
    }else{
    
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Escribe un nombre por favor" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Aceptar", nil];
        
        [alerta show];
    }
}

-(IBAction)cerrarsesion:(id)sender{
    

    defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/cerrarsesion.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%@&token=%@",[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){}];
    
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    
    [[FBSDKLoginManager new] logOut];

    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [defaults removePersistentDomainForName:appDomain];
    [defaults synchronize];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    principal *principal = [storyboard instantiateViewControllerWithIdentifier:@"principal"];
    [self presentViewController:principal animated:YES completion:NULL];
}

#pragma mark campo de texto de nombre

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if([text isEqualToString:@"\n"]){
    
        [nombre resignFirstResponder];
        [apellido resignFirstResponder];
    }

    return YES;
}

#pragma mark - cambiar foto perfil

-(IBAction)cambiarFotoPerfil:(id)sender{

    CambiarFotoPerfil *cambiarfoto = [self.storyboard instantiateViewControllerWithIdentifier:@"cambiarFotoPerfil"];
    
    cambiarfoto.delegate = self;
    
    [self.navigationController pushViewController:cambiarfoto animated:YES];
}

-(void)pasarFoto:(UIImage *)fotoNueva{
    
    fotoCambiada = fotoNueva;
    
    [fotoBotonPerfil setBackgroundImage:fotoNueva forState:UIControlStateNormal];
   
    nuevaFoto = 1;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.destinationViewController isKindOfClass:[CambiarFotoPerfil class]]){
    
        CambiarFotoPerfil *cambiarFoto = (CambiarFotoPerfil *)segue.destinationViewController;
        
        cambiarFoto.delegate = self;
    }
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
