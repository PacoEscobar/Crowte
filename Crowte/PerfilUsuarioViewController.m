//
//  PerfilUsuarioViewController.m
//  Crowte
//
//  Created by Paco Escobar on 13/02/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "PerfilUsuarioViewController.h"
#import "ReportarUsuarioTableViewController.h"
#import "ReportarCheckinTableViewController.h"
#import "PerfilLugarViewController.h"

@interface PerfilUsuarioViewController ()

@end

@implementation PerfilUsuarioViewController

@synthesize datosUsuario, idUsuario, nombreUsuario, fotoUsuario, navBarYInicial, checkins, tabla, locationManager, locationContador, geocoder, fondoTutorialAgregar, textoTutorialAgregar;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    geocoder = [[CLGeocoder alloc] init];
    locationContador = NO;
    
    navBarYInicial = self.navigationController.navigationBar.frame.origin.y;
    
    UIView *division = [self.navigationController.navigationBar viewWithTag:1];
    
    if(division){
    
        [division removeFromSuperview];
    }
    
    UIImage *imageConfig = [UIImage imageNamed:@"iconmoreperfil.png"];
    NSLog(@"width %f heifht %f",imageConfig.size.width, imageConfig.size.height);
    UIButton *config = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imageConfig.size.width, imageConfig.size.height)];
    
    [config setImage:imageConfig forState:UIControlStateNormal];
    
    [config addTarget:self action:@selector(opcionesPerfil:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barbtnconfig = [[UIBarButtonItem alloc] initWithCustomView:config];
    
    self.navigationItem.rightBarButtonItem = barbtnconfig;
    
    
    [[UINavigationBar appearance] setTranslucent:YES];
    
    self.navigationItem.title = @"Perfil";
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"transparente.png"]];
    
    [self getCheckins];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if([[[[checkins objectForKey:@"checkins"] objectAtIndex:0] objectForKey:@"valido"] isEqual:[NSNumber numberWithInt:0]]){
    
        return 1;
    }else{
        return [[checkins objectForKey:@"checkins"] count]+1;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        
        return 236;
    }else{
        
        float adicional;

        NSString *texto = [[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"acontesimiento"];

        texto = [texto stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if(![texto isEqualToString:@""]){
            
            if([texto length] > 44){
                
                adicional = ceil([texto length]/44);
                adicional = adicional*55;
            }else{
                
                adicional = 55;
            }
        }
        
        return 375;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *celda;
    
    if(indexPath.row == 0){
        
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaInfo"];
        
        UIImageView *fotoPerfil = (UIImageView*)[celda viewWithTag:1];
        
        [fotoPerfil setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
        
        [[fotoPerfil layer] setBorderWidth:1];
        [[fotoPerfil layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
        [[fotoPerfil layer] setCornerRadius:50];
        [[fotoPerfil layer] setMasksToBounds:YES];
        
        [fotoPerfil sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",idUsuario,fotoUsuario]] placeholderImage:[UIImage imageNamed:@"imageprofileempty50.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
            
            if(error != nil){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[fotoPerfil layer] setBorderColor:[[UIColor whiteColor] CGColor]];
                    [fotoPerfil setContentMode:UIViewContentModeCenter];
                });
            }
        }];
        
        UILabel *nombre = (UILabel*)[celda viewWithTag:4];
        [nombre setText:nombreUsuario];
        
        UIImageView *estrella = (UIImageView*)[celda viewWithTag:6];

        if([[checkins objectForKey:@"estado"] isEqualToString:@"amigos"]){
        
            [estrella setImage:[UIImage imageNamed:@"iconestrellaperfilamigo.png"]];
        }else{
        
            [estrella setImage:[UIImage imageNamed:@"iconestrellaperfilnoamigo.png"]];
        }
        
        [[estrella layer] setBorderWidth:1];
        [[estrella layer] setBorderColor:[[UIColor clearColor] CGColor]];
        [[estrella layer] setCornerRadius:21];
        [[estrella layer] setMasksToBounds:YES];
        
    }else{
        
        
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaCheckin"];
        
        UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldanuevo.png"]];
        
        [celda setBackgroundView:fondo];
        [celda setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *fotoUsuarioCheckin = (UIImageView*)[celda viewWithTag:1];
        
        [fotoUsuarioCheckin.layer setBorderWidth:0];
        [fotoUsuarioCheckin.layer setCornerRadius:22];
        [fotoUsuarioCheckin.layer setMasksToBounds:YES];
        
        [fotoUsuarioCheckin sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://crowte.com/usuarios/%d/%@",idUsuario,fotoUsuario]] placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"]];
        
        
        UILabel *nombreylugar = (UILabel*)[celda viewWithTag:2];
        
        if([[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"apellido"] isEqualToString:@"(null)"]){
            
            [[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] setValue:@"" forKey:@"apellido"];
        }
        
        [nombreylugar setText:[NSString stringWithFormat:@"%@ %@ estuvo en %@",[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"nombre"],[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"apellido"],[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"lugar"]]];
        
        unsigned long sizenombre = [[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"nombre"] length]+[[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"apellido"] length]+1;
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:nombreylugar.attributedText];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:48.0/255.0 green:52.0/255.0 blue:63.0/255.0 alpha:1] range:NSMakeRange(0, sizenombre)];
        [nombreylugar setAttributedText:attributedString];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:48.0/255.0 green:52.0/255.0 blue:63.0/255.0 alpha:1] range:NSMakeRange(sizenombre+11, [[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"lugar"] length])];
        [nombreylugar setAttributedText:attributedString];
        
        
        UILabel *tiempoDeCheckin = (UILabel*)[celda viewWithTag:3];
        [tiempoDeCheckin setText:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"fecha"]];
        
        
        NSArray *constraints = [[celda contentView] constraints];
        NSLayoutConstraint *constraintInferior;
        
        for (NSLayoutConstraint *constraint in constraints) {
            
            if([[constraint identifier] isEqualToString:@"constraintImagenBottom"]){
                
                constraintInferior = constraint;
            }
        }
        
        UIImageView *fotoPrincipalLugar = (UIImageView*)[celda viewWithTag:5];
        [fotoPrincipalLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
        
        [[fotoPrincipalLugar layer] setBorderWidth:1];
        [[fotoPrincipalLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
        [[fotoPrincipalLugar layer] setCornerRadius:50];
        [[fotoPrincipalLugar layer] setMasksToBounds:YES];
        
        [fotoPrincipalLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"lugarID"],[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoLugar"]]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
            
            if(error.code == 0){
                
                [fotoPrincipalLugar.layer setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                [fotoPrincipalLugar setContentMode:UIViewContentModeScaleToFill];
            }else{
            
                [[fotoPrincipalLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
                [fotoPrincipalLugar setContentMode:UIViewContentModeCenter];
            }
        }];
        
        UIImageView *fotoSecundariaLugar = (UIImageView*)[celda viewWithTag:4];
        
        [fotoSecundariaLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"lugarID"],[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoSecundaria"]]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
        
        NSString *mensaje = [[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"acontesimiento"];
        
        NSData *data = [mensaje dataUsingEncoding:NSUTF8StringEncoding];
        
        mensaje = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        
        mensaje = [mensaje stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        float adicional = 0;
        
        if(constraintInferior != nil){
            
            if(![mensaje isEqualToString:@""]){
                
                if([mensaje length] > 44){
                    
                    adicional = ceil([mensaje length]/44);
                    adicional = adicional*55;
                }else{
                    
                    adicional = 55;
                }
            }
            
            [constraintInferior setConstant:62+adicional];
        }
        
        
        if(![mensaje isEqualToString:@""]){
            
            UITextView *texto = [[UITextView alloc] initWithFrame:CGRectMake(fotoSecundariaLugar.frame.origin.x, fotoSecundariaLugar.frame.origin.y+fotoSecundariaLugar.frame.size.height, fotoSecundariaLugar.frame.size.width-100, 100)];
            
            [texto setText:mensaje];
            
            [texto setFont:[UIFont fontWithName:@"Raleway-Regular" size:13]];
            [texto setTextColor:[UIColor colorWithRed:48.0/255.0 green:52.0/255.0 blue:63.0/255.0 alpha:1]];
            
            [texto setBackgroundColor:[UIColor colorWithRed:246.0/255.0 green:244.0/255.0 blue:243.0/255.0 alpha:1]];
            
            [texto setTextContainerInset:UIEdgeInsetsMake(20, 20, 20, 20)];
            
            [texto setTag:11];
            
            [texto setUserInteractionEnabled:NO];
            [texto setScrollEnabled:NO];
            [texto setEditable:NO];
            
            [texto setContentMode:UIViewContentModeCenter];
            
            texto.translatesAutoresizingMaskIntoConstraints = NO;
            
            [celda addSubview:texto];
            
            [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:fotoSecundariaLugar attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-302]];
            
            [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:[texto superview] attribute:NSLayoutAttributeTrailingMargin relatedBy:NSLayoutRelationEqual toItem:texto attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:1]];
            
            [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:fotoSecundariaLugar attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
            
        }else{
            
            UITextView *texto = (UITextView*)[celda viewWithTag:11];
            
            [texto removeFromSuperview];
        }
        
        UIButton *join = (UIButton*)[celda viewWithTag:6];
        [join addTarget:self action:@selector(join:) forControlEvents:UIControlEventTouchUpInside];
        
        if([[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"finguer"] intValue] == 1){
            
            [join setImage:[UIImage imageNamed:@"joinmanoverde.png"] forState:UIControlStateNormal];
        }else{
            
            [join setImage:[UIImage imageNamed:@"joinmanonegra.png"] forState:UIControlStateNormal];
        }
        
        UIButton *numJoins = (UIButton*)[celda viewWithTag:7];
        [numJoins setTitle:[[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"numDePersonas"] description] forState:UIControlStateNormal];
        
        UIButton *numComentarios = (UIButton*)[celda viewWithTag:9];
        
        if([[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"comentarios"] intValue] == 0){
            
            [numComentarios setTitle:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@""] forState:UIControlStateNormal];
        }else{
            
            [numComentarios setTitle:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"comentarios"] forState:UIControlStateNormal];
        }
        
        UIButton *opciones = (UIButton*)[celda viewWithTag:10];
        
        [opciones addTarget:self action:@selector(opcionesCheckin:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return celda;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row > 0){
        
        UIStoryboard *storybard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PerfilLugarViewController *lugar = (PerfilLugarViewController*)[storybard instantiateViewControllerWithIdentifier:@"perfilLugar"];
        
        [lugar setIdLugar:[[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"lugarID"] intValue]];
        
        [lugar setNombreLugar:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"lugar"]];
        
        [lugar setFotoPrincipalLugar:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoLugar"]];
        
        [lugar setFotoSecundariaLugar:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoSecundaria"]];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"localizacionLugar"] objectForKey:@"latitud"] floatValue] longitude:[[[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"localizacionLugar"] objectForKey:@"longitud"] floatValue]];
        
        [lugar setLocation:location];
        
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
        UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
        self.navigationItem.backBarButtonItem = atras;
        
        [self.navigationController pushViewController:lugar animated:YES];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [self.navigationController.navigationBar setFrame:CGRectMake(self.navigationController.navigationBar.frame.origin.x, ([scrollView contentOffset].y*-1)+navBarYInicial, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
}

#pragma mark - hechos por mi

-(void)getCheckins{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getfriendposts.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"amigo=%d&usuario=%d&m=0&minimo=0",idUsuario,[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){
            
            if(data != nil){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSError *error = nil;
                    
                    checkins = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                    if(error == nil){
                        
                        UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                        
                        UILabel *numCheckins = (UILabel*)[celda viewWithTag:2];
                        
                        [numCheckins setText:[checkins objectForKey:@"cantidadCheckins"]];
                        
                        UILabel *numAmigos = (UILabel*)[celda viewWithTag:3];
                        
                        [numAmigos setText:[checkins objectForKey:@"cantidadAmigos"]];
                        
                        CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[checkins objectForKey:@"localizacion"] objectForKey:@"lat"] doubleValue] longitude:[[[checkins objectForKey:@"localizacion"] objectForKey:@"lon"] doubleValue]];
                        
                        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error){
                            
                            CLPlacemark *placemark = [placemarks lastObject];
                            
                            UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                            
                            UILabel *lugarActual = (UILabel*)[celda viewWithTag:5];
                            
                            if(location.coordinate.latitude == 0 && location.coordinate.longitude == 0){
                            
                                [lugarActual setText:@""];
                            }else{
                                
                                [lugarActual setText:[NSString stringWithFormat:@"%@, %@",[placemark administrativeArea],[placemark country]]];
                            }
                            
                        }];
                        
                        [tabla reloadData];

                        if([[checkins objectForKey:@"tutorialAgregarAmigo"] intValue] == 0){

                            [fondoTutorialAgregar setHidden:NO];
                        }
                    }
                });
            }
        }
        
    }];
    
    [uploadTask resume];
}

-(IBAction)opcionesPerfil:(id)sender{
    
    UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *estadoAmistad;

    if([[checkins objectForKey:@"estado"] isEqualToString:@"amigos"]){
        
        estadoAmistad = [UIAlertAction actionWithTitle:@"Eliminar amigo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *accion){
            
            UIAlertController *confirmacion = [UIAlertController alertControllerWithTitle:@"¿Estás seguro?" message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *aceptar = [UIAlertAction actionWithTitle:@"Aceptar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/eliminaramigo.php"]];
                
                [request setHTTPMethod:@"POST"];
                
                NSData *cuerpo = [[NSString stringWithFormat:@"amigo=%d&id=%d",idUsuario,[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
                
                NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                
                NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                
                NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                    
                    if(error == nil){
                        
                        if(data != nil){
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self getCheckins];
                            });
                        }
                    }
                    
                }];
                
                [uploadTask resume];
            }];
            
            UIAlertAction *noAceptar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
            
            [confirmacion addAction:aceptar];
            [confirmacion addAction:noAceptar];
            
            [self presentViewController:confirmacion animated:YES completion:nil];
            
        }];
    }else if([[checkins objectForKey:@"estado"] isEqualToString:@"esperando"]){
    
        estadoAmistad = [UIAlertAction actionWithTitle:@"Cancelar solicitud" style:UIAlertActionStyleDefault handler:^(UIAlertAction *accion){
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/aceptarsolicitudamistad.php"]];
            
            [request setHTTPMethod:@"POST"];
            
            NSData *cuerpo = [[NSString stringWithFormat:@"es=no&solnte=%d&solado=%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue],idUsuario] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
            
            NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
            
            NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                
                if(error == nil){
                    
                    if(data != nil){
                        
                        dispatch_async(dispatch_get_main_queue(), ^{

                            [checkins setObject:[NSString stringWithFormat:@"no"] forKey:@"estado"];
                        });
                    }
                }
                
            }];
            
            [uploadTask resume];
        }];
    }else if([[checkins objectForKey:@"estado"] isEqualToString:@"preguntado"]){
    
        estadoAmistad = [UIAlertAction actionWithTitle:@"Aceptar solicitud" style:UIAlertActionStyleDefault handler:^(UIAlertAction *accion){
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/aceptarsolicitudamistad.php"]];
            
            [request setHTTPMethod:@"POST"];
            
            NSData *cuerpo = [[NSString stringWithFormat:@"es=si&solnte=%d&solado=%d",idUsuario,[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
            
            NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
            
            NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                
                if(error == nil){
                    
                    if(data != nil){
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self getCheckins];
                        });
                    }
                }
                
            }];
            
            [uploadTask resume];
        }];
    }else if([[checkins objectForKey:@"estado"] isEqualToString:@"no"]){
    
        estadoAmistad = [UIAlertAction actionWithTitle:@"Enviar solicitud" style:UIAlertActionStyleDefault handler:^(UIAlertAction *accion){
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/enviarsolicitudamigo.php"]];
            
            [request setHTTPMethod:@"POST"];
            
            NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%d&ID=%d",idUsuario,[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
            
            NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
            
            NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                
                if(error == nil){
                    
                    if(data != nil){
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [checkins setObject:@"esperando" forKey:@"estado"];
                        });
                    }
                }
                
            }];
            
            [uploadTask resume];
        }];
    }
    
    UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
        
        [opciones dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *reportar = [UIAlertAction actionWithTitle:@"Reportar usuario" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ReportarUsuarioTableViewController *reporte = [storyboard instantiateViewControllerWithIdentifier:@"ReportarUser"];
        
        [reporte setIdUsuarioABloquear:[NSString stringWithFormat:@"%d",idUsuario]];
                
        [self presentViewController:reporte animated:YES completion:nil];
    }];
    
    
    UIAlertAction *bloquear = [UIAlertAction actionWithTitle:@"Bloquear usuario" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
        
        UIAlertController *confirmacion = [UIAlertController alertControllerWithTitle:@"¿Estás seguro?" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *aceptar = [UIAlertAction actionWithTitle:@"Aceptar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/bloqueos/bloquear.php"]];
            
            [request setHTTPMethod:@"POST"];
            
            NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%d&bloqueador=%@",idUsuario,[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
            
            NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
            
            NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
            
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [uploadTask resume];
        }];
        
        UIAlertAction *noAceptar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
        
        [confirmacion addAction:aceptar];
        [confirmacion addAction:noAceptar];
        
        [self presentViewController:confirmacion animated:YES completion:nil];
    }];
    
    [opciones addAction:estadoAmistad];
    [opciones addAction:reportar];
    [opciones addAction:bloquear];
    [opciones addAction:cancelar];
    
    [self presentViewController:opciones animated:YES completion:nil];
}

-(IBAction)join:(id)sender{
    
    NSLog(@"join");
}

-(IBAction)opcionesCheckin:(id)sender{
    
    UIButton *botonOpciones = (UIButton*)sender;
    
    CGPoint posicionBoton = [botonOpciones convertPoint:CGPointZero toView:tabla];
    
    NSIndexPath *indexPath = [tabla indexPathForRowAtPoint:posicionBoton];
    
    UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *reportarCheckin = [UIAlertAction actionWithTitle:@"Reportar check-in" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ReportarCheckinTableViewController *reporte = [storyboard instantiateViewControllerWithIdentifier:@"ReportarCheckin"];

        [reporte setIdRelObjDia:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"relID"]];
        
        [reporte setIdUsuarioABloquear:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"uID"]];
        
        [self presentViewController:reporte animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
        
        [opciones dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [opciones addAction:reportarCheckin];
    [opciones addAction:cancelar];
    
    [self presentViewController:opciones animated:YES completion:nil];
}

- (IBAction)cerrarTutorialAgregar:(id)sender {
    
    [fondoTutorialAgregar removeFromSuperview];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizaragregaramigotuto.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){}];
    
    [uploadTask resume];
}

#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}

#pragma mark - location

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    if(locationContador){
        return;
    }
    
    locationContador = YES;
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
