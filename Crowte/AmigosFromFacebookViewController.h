//
//  AmigosFromFacebookViewController.h
//  Crowte
//
//  Created by Paco Escobar on 30/05/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface AmigosFromFacebookViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, FBSDKAppInviteDialogDelegate>


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet UITableView *tabla;
@property NSMutableArray *infoAmigos;


-(IBAction)loginFacebook:(id)sender;
-(IBAction)invitarAmigos:(id)sender;

@end
