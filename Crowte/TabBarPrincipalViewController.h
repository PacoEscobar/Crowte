//
//  TabBarPrincipalViewController.h
//  Crowte
//
//  Created by Paco Escobar on 10/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface TabBarPrincipalViewController : UITabBarController<CLLocationManagerDelegate, NSURLSessionDelegate, NSURLSessionDownloadDelegate, UITabBarControllerDelegate>

#pragma mark - actividad vc
@property NSMutableArray *checkinsTodosOtros;
@property NSMutableArray *checkinsTodosUltimos;

#pragma mark - checkin vc
@property NSMutableDictionary *datosCheckinEnLugar;

#pragma mark - perfil vc
@property NSDictionary *datosMios;

#pragma mark - locatin
@property BOOL locationContador;
@property CLLocationManager *locationManager;

-(void)actualizarCoordenadas;

@end
