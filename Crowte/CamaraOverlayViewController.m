//
//  CamaraOverlayViewController.m
//  Crowte
//
//  Created by Paco Escobar on 26/07/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <ImageIO/CGImageProperties.h>
#import <Photos/Photos.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Lottie/Lottie.h>
#import "CamaraOverlayViewController.h"
#import "CamaraOverlayView.h"

@interface CamaraOverlayViewController ()

@end

@implementation CamaraOverlayViewController

@synthesize session, alreadyGrabando, MovieFileOutput, trigger, cambiarCamara, tacha, flash, alreadyGrabandoAnimacion, flashActivado, delegate, contadorVideo, timer, tiempoVideo, stillOutput, conexionStillOutput, defaults, fotoFinal, enviarComentario, reproductor, layerVideo, vistaVideo, isVideo, seComparteEnFacebook, urlAssetVideo, tachaPreview, tutoTomarFotoVideo, totalBytesArchivoAsubir, dataFotoVideo, idLugar;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    dispatch_async(dispatch_get_main_queue(), ^{

    
        self.view.opaque = NO;
        self.view.backgroundColor = [UIColor clearColor];
        [UIApplication sharedApplication].statusBarHidden = YES;
    });
    
    
    alreadyGrabando = NO;
    alreadyGrabandoAnimacion = NO;
    flashActivado = NO;
    seComparteEnFacebook = NO;
    
    tiempoVideo = 0;
    
    session = [[AVCaptureSession alloc] init];
    
    [session beginConfiguration];
    session.sessionPreset = AVCaptureSessionPresetPhoto;
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDevice *audioDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
    AVCaptureDeviceInput *audioInput = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:nil];
    
    NSError *error;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryRecord error:&error];
    
    [audioSession setActive:YES error:&error];
    
    [session addInput:input];
    [session addInput:audioInput];
    
    stillOutput = [[AVCaptureStillImageOutput alloc] init];
    conexionStillOutput = nil;
    
    if([session canAddOutput:stillOutput]){
        
        [session addOutput:stillOutput];
        
        
        AVCaptureInput* currentCameraInput = [session.inputs objectAtIndex:0];
        
        if(((AVCaptureDeviceInput*)currentCameraInput).device.position == AVCaptureDevicePositionFront){
        
            NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
            AVCaptureDevice *captureDevice = nil;
            
            for (AVCaptureDevice *device in videoDevices){

                
                if (device.position == AVCaptureDevicePositionFront){
                    
                    captureDevice = device;
                    break;
                }
            }
        }
    
        for(AVCaptureConnection *connection in stillOutput.connections){

            for(AVCaptureInputPort *puerto in [connection inputPorts]){
                
                if([[puerto mediaType] isEqual:AVMediaTypeVideo]){
                    
                    conexionStillOutput = connection;
                    break;
                }
            }
            
            if(conexionStillOutput){ break; }
        }
    }
    
    [device lockForConfiguration:nil];
    [device setFlashMode:AVCaptureFlashModeOff];
    [device unlockForConfiguration];
    
    MovieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
    
    MovieFileOutput.maxRecordedDuration = CMTimeMakeWithSeconds(60, 30);
    
    MovieFileOutput.minFreeDiskSpaceLimit = 2048;
    
    if([session canSetSessionPreset:AVCaptureSessionPreset640x480]){

        [session setSessionPreset:AVCaptureSessionPreset640x480];
    }
    
    [session addOutput:MovieFileOutput];
    
    AVCaptureVideoPreviewLayer *layer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    
    layer.frame = self.view.bounds;
    layer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:layer];
    
    [session commitConfiguration];
    [session startRunning];
    
    trigger = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
    trigger.layer.cornerRadius = 35;
    trigger.layer.borderWidth = 1;
    trigger.layer.masksToBounds = YES;
    trigger.layer.borderColor = [[UIColor blackColor] CGColor];
    
    [trigger setBackgroundColor:[UIColor whiteColor]];
    
    UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tomarVideo:)];
    
    [trigger addGestureRecognizer:longTap];
    [trigger addTarget:self action:@selector(tomarFoto) forControlEvents:UIControlEventTouchUpInside];
    [trigger addTarget:self action:@selector(setBotonRojo) forControlEvents:UIControlEventTouchDown];
    
    trigger.center = self.view.center;
    trigger.frame = CGRectMake(trigger.frame.origin.x, 450, trigger.frame.size.width, trigger.frame.size.height);
    
    cambiarCamara = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-50, 15, 25, 25)];
    
    [cambiarCamara setBackgroundImage:[UIImage imageNamed:@"rotatecamara.png"] forState:UIControlStateNormal];
    [cambiarCamara addTarget:self action:@selector(cambiarCam) forControlEvents:UIControlEventTouchUpInside];
    
    flash = [[UIButton alloc] initWithFrame:CGRectMake(50, 15, 25, 25)];
    
    [flash setBackgroundImage:[UIImage imageNamed:@"flashno.png"] forState:UIControlStateNormal];
    
    [flash addTarget:self action:@selector(toggleFlash) forControlEvents:UIControlEventTouchUpInside];
    
    tacha = [[UIButton alloc] initWithFrame:CGRectMake(10, 15, 15, 15)];
    
    [tacha setBackgroundImage:[UIImage imageNamed:@"tachablanca.png"] forState:UIControlStateNormal];
    
    [tacha addTarget:self action:@selector(cerrarCamara) forControlEvents:UIControlEventTouchUpInside];
    
    contadorVideo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 5)];
    
    [contadorVideo setBackgroundColor:[UIColor blueColor]];
    
    [self.view bringSubviewToFront:trigger];
    [trigger setUserInteractionEnabled:YES];
    
    [self.view addSubview:trigger];
    [self.view addSubview:cambiarCamara];
    [self.view addSubview:flash];
    [self.view addSubview:tacha];
    [self.view addSubview:contadorVideo];
    
    [session commitConfiguration];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    NSLog(@"shit, memory warning");
    // Dispose of any resources that can be recreated.
}

-(void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error{

    BOOL seGrabo = YES;
    if([error code] != noErr){

        id value = [[error userInfo] objectForKey:AVErrorRecordingSuccessfullyFinishedKey];

        if(value){

            seGrabo = [value boolValue];
        }
    }
    
    if(seGrabo){

        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        
        [device lockForConfiguration:nil];
        [device setTorchMode:AVCaptureTorchModeOff];
        [device unlockForConfiguration];
        
        //----empieza mp4
        
        AVURLAsset *asset = [AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), @"output.mov"]] options:nil];
        
        NSString *videoPath = [NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), @"output.mp4"];
        
        NSError *error;
        
        if([[NSFileManager defaultManager] fileExistsAtPath:videoPath]){
            
            [[NSFileManager defaultManager] removeItemAtPath:videoPath error:&error];
        }

        
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
        
        exportSession.outputURL = [NSURL fileURLWithPath:videoPath];
        
        
        exportSession.outputFileType = AVFileTypeMPEG4;
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
        
            switch ([exportSession status]) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"fallo mp4");
                    break;

                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"se cancelo mp4");
                    break;
                    
                default:
                    NSLog(@"quedo");
                    break;
            }
            
            unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:videoPath error:nil] fileSize];
                        
            /*dispatch_async(dispatch_get_main_queue(), ^{
                if([self.delegate respondsToSelector:@selector(getImageVideoTomado:videoImagen:)]){
                    
                    [self.delegate getImageVideoTomado:dataVideo videoImagen:2];
                }

                [self dismissViewControllerAnimated:YES completion:^{
                    
                    [UIApplication sharedApplication].statusBarHidden = NO;
                }];
            });*/
            
            __block PHObjectPlaceholder *assetPlaceHolder;
            
            [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                
                PHAssetChangeRequest *createAsset = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:[NSURL fileURLWithPath:videoPath]];
                assetPlaceHolder = [createAsset placeholderForCreatedAsset];
                
            } completionHandler:^(BOOL success, NSError *error){

                PHFetchResult *result = [PHAsset fetchAssetsWithLocalIdentifiers:@[[assetPlaceHolder localIdentifier]] options:nil];
                
                PHAsset *asset = [result objectAtIndex:0];
                
                [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset  * _Nullable asset2, AVAudioMix  * _Nullable audiomix, NSDictionary *dictionaryo){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        urlAssetVideo = (AVURLAsset*)asset2;
                        
                        reproductor = [[AVPlayer alloc] init];
                        
                        layerVideo = [AVPlayerLayer playerLayerWithPlayer:reproductor];
                        [layerVideo setBackgroundColor:[[UIColor blackColor] CGColor]];
                        
                        [layerVideo setFrame:[UIScreen mainScreen].bounds];
                        
                        vistaVideo = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                        [vistaVideo.layer addSublayer:layerVideo];
                        
                        [self.view addSubview:vistaVideo];
                        
                        AVAsset *asset = [AVAsset assetWithURL:urlAssetVideo.URL];
                        
                        AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
                        
                        reproductor = [AVPlayer playerWithPlayerItem:item];
                        
                        reproductor.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loopVideo:) name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
                        
                        [layerVideo setPlayer:reproductor];
                        
                        UITapGestureRecognizer *pausarVideo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pausarVideoGesture:)];
                        
                        [vistaVideo addGestureRecognizer:pausarVideo];
                        
                        UIView *fullScreenVideo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
                        
                        layerVideo.frame = fullScreenVideo.frame;
                        
                        [reproductor play];
                        
                        tachaPreview = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 15, 15)];
                        [tachaPreview setBackgroundImage:[UIImage imageNamed:@"tachablanca.png"] forState:UIControlStateNormal];
                        
                        [tachaPreview addTarget:self action:@selector(cerrarFotoPreview:) forControlEvents:UIControlEventTouchUpInside];
                        
                        enviarComentario = [[UIButton alloc] initWithFrame:CGRectMake(vistaVideo.frame.size.width-50, vistaVideo.frame.size.height-50, 35, 35)];
                        
                        [enviarComentario.layer setBorderWidth:0.1];
                        [enviarComentario.layer setMasksToBounds:YES];
                        
                        [enviarComentario setBackgroundImage:[UIImage imageNamed:@"avion.png"] forState:UIControlStateNormal];
                        
                        [enviarComentario addTarget:self action:@selector(enviar:) forControlEvents:UIControlEventTouchUpInside];
                        
                        [vistaVideo addSubview:enviarComentario];
                        [vistaVideo addSubview:tachaPreview];
                        
                    });
                    
                }];
                
            }];
        }];

        //--- termina export mp4
        
    }
}

-(void)viewDidAppear:(BOOL)animated{

    if(tutoTomarFotoVideo){
        
        UIView *globo = [[UIView alloc] initWithFrame:CGRectMake(0, trigger.frame.origin.y-130, 320, 150)];
        
        UIImageView *cola = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"flechanegra.png"]];
        
        cola.frame = CGRectMake((globo.frame.size.width/2)-25, globo.frame.size.height-50, 50, 25);
        
        [globo addSubview:cola];
        
        UIView *globo2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, globo.frame.size.width, cola.frame.origin.y)];
        
        [globo2 setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.81]];
        
        UILabel *texto = [[UILabel alloc] initWithFrame:CGRectMake(25, 25, globo2.frame.size.width-100, 100)];
        [texto setText:@"Tap para una foto. Deja presionado para video."];
        [texto setTextColor:[UIColor whiteColor]];
        
        [texto setNumberOfLines:0];
        [texto setFrame:CGRectMake(texto.frame.origin.x, texto.frame.origin.y, globo.frame.size.width-50, texto.frame.size.height)];
        [texto sizeToFit];
        
        [globo2 addSubview:texto];
        [globo addSubview:globo2];
        
        [globo setTag:1];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(desaparecerMensajeTuto)];
        
        [globo addGestureRecognizer:tapGesture];
        
        [self.view addSubview:globo];
    }
}

-(void)captureOutput:(AVCaptureFileOutput *)captureOutput didStartRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections{
}

-(void)cambiarCam{
    
    AVCaptureDevice *frontCamera = nil;
    AVCaptureDevice *backCamera = nil;
    
    AVCaptureInput* currentCameraInput = [session.inputs objectAtIndex:0];
    
    NSError *error;
    
    
    NSArray * devices = [ AVCaptureDevice devicesWithMediaType: AVMediaTypeVideo ];
    
    for(AVCaptureDevice *device in devices){
        
        if([device position] == AVCaptureDevicePositionFront){
            
            frontCamera = device;
        }else if([device position] == AVCaptureDevicePositionBack){
            
            backCamera = device;
        }
    }
    
    [session beginConfiguration];
    [session removeInput:currentCameraInput];
    
    if(((AVCaptureDeviceInput*)currentCameraInput).device.position == AVCaptureDevicePositionBack){
        
        AVCaptureDeviceInput *inp = [[AVCaptureDeviceInput alloc] initWithDevice:frontCamera error:&error];
        
        [session addInput:inp];
    }else if(((AVCaptureDeviceInput*)currentCameraInput).device.position == AVCaptureDevicePositionFront){
        
        AVCaptureDeviceInput *inp = [[AVCaptureDeviceInput alloc] initWithDevice:backCamera error:&error];
        
        [session addInput:inp];
    }

    [session commitConfiguration];
}


-(void)tomarVideo:(UILongPressGestureRecognizer *)recognizer{

    [self desaparecerMensajeTuto];
    
    isVideo = YES;
    
    [trigger setBackgroundColor:[UIColor whiteColor]];
    
    if(!alreadyGrabandoAnimacion){
        
        AVCaptureInput* currentCameraInput = [session.inputs objectAtIndex:0];
        
        if(((AVCaptureDeviceInput*)currentCameraInput).device.position == AVCaptureDevicePositionBack){
            
            if(flashActivado){
                
                AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
                
                if([device hasTorch]){
                    
                    [device lockForConfiguration:nil];
                    if([device torchMode] == AVCaptureTorchModeOff){
                        
                        [device setTorchMode:AVCaptureTorchModeOn];
                    }else if([device torchMode] == AVCaptureTorchModeOn){
                        
                        [device setTorchMode:AVCaptureTorchModeOff];
                    }
                    
                    [device unlockForConfiguration];
                }
            }
        }
        
        UIView *sombra = [[UIView alloc] initWithFrame:trigger.frame];
        [sombra setBackgroundColor:[UIColor colorWithRed:200.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:0.4]];
        [sombra.layer setBorderColor:[[UIColor clearColor] CGColor]];
        [sombra.layer setCornerRadius:trigger.layer.cornerRadius];
        [sombra.layer setBorderWidth:trigger.layer.borderWidth];
        [sombra.layer setMasksToBounds:YES];
        [self.view addSubview:sombra];
        [UIView animateWithDuration:2 delay:0.0 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse | UIViewAnimationOptionCurveLinear animations:^{
            sombra.transform = CGAffineTransformMakeScale(1.3,1.3);
            trigger.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        } completion:nil];
        
        alreadyGrabandoAnimacion = YES;
        
    }
    
    if(recognizer.state == UIGestureRecognizerStateBegan){
        
        if(![timer isValid]){
            
            timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(contarSegundosVideo) userInfo:nil repeats:YES];
        }
        
        if(!alreadyGrabando){
            
            int screen = [UIScreen mainScreen].bounds.size.width;
            
            [UIView animateWithDuration:10 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
                
                [contadorVideo setFrame:CGRectMake(contadorVideo.frame.origin.x, contadorVideo.frame.origin.y, screen, contadorVideo.frame.size.height)];
            } completion:nil];
            
            NSString *outputPath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"output.mov"];
            
            NSURL *outputURL = [[NSURL alloc] initFileURLWithPath:outputPath];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            if ([fileManager fileExistsAtPath:outputPath])
            {
                
                NSError *error;
                if ([fileManager removeItemAtPath:outputPath error:&error] == NO)
                {
                    
                    NSLog(@"error %@",error);
                    //Error - handle if requried
                }
            }
            //Start recording
            @try{
                
                [MovieFileOutput startRecordingToOutputFileURL:outputURL recordingDelegate:self];
                alreadyGrabando = YES;
            }@catch(NSException *e){
            }
        }
    }else if(recognizer.state == UIGestureRecognizerStateEnded){

        [self.view.layer removeAllAnimations];
        [timer invalidate];
        
        [MovieFileOutput stopRecording];
        alreadyGrabando = NO;
    }
    
}

-(void)tomarFoto{
    
    isVideo = NO;
    
    [trigger setEnabled:NO];
    [tacha setEnabled:NO];
    [flash setEnabled:NO];
    [cambiarCamara setEnabled:NO];
    
    [self desaparecerMensajeTuto];

    conexionStillOutput = nil;
    
    for(AVCaptureConnection *connection in stillOutput.connections){
        
        for(AVCaptureInputPort *puerto in [connection inputPorts]){
            
            if([[puerto mediaType] isEqual:AVMediaTypeVideo]){
                
                conexionStillOutput = connection;
                break;
            }
        }
        
        if(conexionStillOutput){ break; }
    }
    
    [stillOutput captureStillImageAsynchronouslyFromConnection:conexionStillOutput completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error){
        
        AVCaptureInput* currentCameraInput = [session.inputs objectAtIndex:0];
        
        
        if(((AVCaptureDeviceInput*)currentCameraInput).device.position == AVCaptureDevicePositionFront){
            
            UIView *flashFron = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
            
            [flashFron setBackgroundColor:[UIColor whiteColor]];
            
            [self.view addSubview:flashFron];
            [flashFron removeFromSuperview];
        }
        
        NSData *data = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
        
        fotoFinal = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        
        [fotoFinal setBackgroundColor:[UIColor blackColor]];
        
        [fotoFinal setContentMode:UIViewContentModeScaleAspectFit];
        
        UIImage *image = [[UIImage alloc] initWithData:data];
        
        [fotoFinal setImage:image];
        [fotoFinal setUserInteractionEnabled:YES];
        
        tachaPreview = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 15, 15)];
        [tachaPreview setBackgroundImage:[UIImage imageNamed:@"tachablanca.png"] forState:UIControlStateNormal];
        
        [tachaPreview addTarget:self action:@selector(cerrarFotoPreview:) forControlEvents:UIControlEventTouchUpInside];
        
        enviarComentario = [[UIButton alloc] initWithFrame:CGRectMake(fotoFinal.frame.size.width-50,fotoFinal.frame.size.height-50, 35, 35)];
        
        [enviarComentario.layer setBorderWidth:0.1];

        [enviarComentario.layer setMasksToBounds:YES];
        
        [enviarComentario setBackgroundImage:[UIImage imageNamed:@"avion.png"] forState:UIControlStateNormal];
        
        [enviarComentario addTarget:self action:@selector(enviar:) forControlEvents:UIControlEventTouchUpInside];
    
        
        [self.view addSubview:fotoFinal];
        [self.view addSubview:enviarComentario];
        [self.view addSubview:tachaPreview];
        
    }];
    
    [trigger setBackgroundColor:[UIColor whiteColor]];
                       
}

-(void)toggleFlash{

    
    
    
    AVCaptureInput* currentCameraInput = [session.inputs objectAtIndex:0];
    
    
    if(((AVCaptureDeviceInput*)currentCameraInput).device.position == AVCaptureDevicePositionBack){
        //si flash

        if(flashActivado){
            
            [flash setBackgroundImage:[UIImage imageNamed:@"flashno.png"] forState:UIControlStateNormal];
            
        }else{
            
            [flash setBackgroundImage:[UIImage imageNamed:@"flashsi.png"] forState:UIControlStateNormal];
        }
        
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        
        if([device hasFlash]){
            
            [device lockForConfiguration:nil];
            if([device flashMode] == AVCaptureFlashModeOff){
                
                [device setFlashMode:AVCaptureFlashModeOn];
            }else if([device flashMode] == AVCaptureFlashModeOn){
                
                [device setFlashMode:AVCaptureFlashModeOff];
            }
            
            [device unlockForConfiguration];
        }
        
        flashActivado = !flashActivado;
        
    }else if(((AVCaptureDeviceInput*)currentCameraInput).device.position == AVCaptureDevicePositionFront){
        // no flash
        
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        
        [device lockForConfiguration:nil];
        [device setFlashMode:AVCaptureFlashModeOff];
        [device unlockForConfiguration];
        
        if(flashActivado){
            
            [flash setBackgroundImage:[UIImage imageNamed:@"flashno.png"] forState:UIControlStateNormal];
            
        }else{
            
            [flash setBackgroundImage:[UIImage imageNamed:@"flashsi.png"] forState:UIControlStateNormal];
        }
        
        flashActivado = !flashActivado;
        
    }
    
}

-(void)cerrarCamara{

    [UIApplication sharedApplication].statusBarHidden = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setBotonRojo{

    [trigger setBackgroundColor:[UIColor colorWithRed:200.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1]];
}

-(void)contarSegundosVideo{

    tiempoVideo++;

    if(tiempoVideo == 9){
    
        [timer invalidate];
        
        [MovieFileOutput stopRecording];
        alreadyGrabando = NO;
    }    
}

-(void)desaparecerMensajeTuto{

    [[self.view viewWithTag:1] removeFromSuperview];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizartriggertuto.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[defaults objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
    
    }];
    
    [uploadTask resume];
}

#pragma mark - delegados text field


-(void)cerrarFotoPreview:(id)sender{

    [tachaPreview removeFromSuperview];
    [fotoFinal removeFromSuperview];
    [enviarComentario removeFromSuperview];
    [vistaVideo removeFromSuperview];
    [reproductor pause];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
    reproductor = NULL;
    [self.view.layer removeAllAnimations];
    [timer invalidate];
    [contadorVideo setFrame:CGRectMake(contadorVideo.frame.origin.x, contadorVideo.frame.origin.y, 0, contadorVideo.frame.size.height)];
    [trigger setBackgroundColor:[UIColor whiteColor]];
    alreadyGrabando = NO;
    alreadyGrabandoAnimacion = NO;
    
    [trigger setEnabled:YES];
    [tacha setEnabled:YES];
    [flash setEnabled:YES];
    [cambiarCamara setEnabled:YES];
}

#pragma mark - acciones video

-(void)loopVideo:(NSNotification *)notification{
    
    AVPlayerItem *item = [notification object];
    
    [item seekToTime:kCMTimeZero];
}

-(void)pausarVideo{
    
    if([reproductor rate] == 0.0){
        [reproductor play];
    }else{
        
        [reproductor pause];
    }
}

-(void)pausarVideoGesture:(UITapGestureRecognizer *)recognizer{

    if([reproductor rate] > 0.0){
        
        [reproductor pause];
    }else if([reproductor rate] == 0.0){
        
        [reproductor play];
    }
}

#pragma mark - compartir

-(void)enviar:(UIButton *)sender{
    
    LOTAnimationView *uploader = [LOTAnimationView animationNamed:@"uploader"];
    
    [uploader setFrame:CGRectMake((self.view.frame.size.width/2)-50, self.view.frame.size.height-75, 100, 75)];

    [uploader setTag:104];
    
    [enviarComentario setAlpha:0];
    
    if(isVideo){
        
        [vistaVideo addSubview:uploader];
    }else{
    
        [self.view addSubview:uploader];
    }
    
    NSData *dataArchivoASubir;
    
    if(isVideo){
        
        dataArchivoASubir = [[NSData alloc] initWithContentsOfURL:urlAssetVideo.URL];
    }else{
        
        dataArchivoASubir = UIImageJPEGRepresentation([fotoFinal image], 90);
    }
    
    totalBytesArchivoAsubir = [dataArchivoASubir length];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/subirfotoenlugar.php"]];
        
        [request setHTTPMethod:@"POST"];
        
        NSMutableData *cuerpo = [NSMutableData data];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contenido = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        NSString *linea;
        
        [request addValue:contenido forHTTPHeaderField:@"Content-Type"];
        
        [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        if(!isVideo){
            
            linea = @"foto";
            [cuerpo appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"foto\"; filename=\"%@.jpg\"\r\n",linea] dataUsingEncoding:NSUTF8StringEncoding]];
        }else{
            
            linea = @"video";
            [cuerpo appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"foto\"; filename=\"%@.mp4\"\r\n",linea] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        linea = @"Content-Type: application/octet-stream\r\n\r\n";
        [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            
        [cuerpo appendData:dataArchivoASubir];
        
        linea = @"\r\n";
        [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        linea = @"Content-Disposition: form-data; name=\"id\"\r\n\r\n";
        [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
        [cuerpo appendData:[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding]];
        linea = @"\r\n";
        [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
        
        [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
        [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        linea = @"Content-Disposition: form-data; name=\"lugar\"\r\n\r\n";
        [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
        [cuerpo appendData:[[NSString stringWithFormat:@"%d",idLugar] dataUsingEncoding:NSUTF8StringEncoding]];
        linea = @"\r\n";
        [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
        
        [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
        [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        linea = @"Content-Disposition: form-data; name=\"mensaje\"\r\n\r\n";
        [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
        [cuerpo appendData:[[NSString stringWithFormat:@"%@",@""] dataUsingEncoding:NSUTF8StringEncoding]];
        linea = @"\r\n";
        [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
        
        [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [request setHTTPBody:cuerpo];
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
        
        NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){

            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                    
                    NSLog(@"error al subir video/foto");
                }else{
                    
                    NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    if(![respuesta isEqualToString:@"error"]){
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [uploader playWithCompletion:^(BOOL finished){
                                
                                [uploader playWithCompletion:^(BOOL animationFinished){
                                    
                                    if(animationFinished){
                                        
                                        [UIApplication sharedApplication].statusBarHidden = NO;
                                        [self dismissViewControllerAnimated:YES completion:^{
                                            
                                            if(isVideo){
                                                
                                                [self pausarVideo];
                                                reproductor = nil;
                                            }
                                            
                                            [UIApplication sharedApplication].statusBarHidden = NO;
                                        }];
                                    }
                                }];
                                
                            }];
                            
                            [uploader setAnimationProgress:0.789];
                            
                            UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:dataFotoVideo], nil, nil, nil);
                        });
                    }else{
                        
                        [[[UIAlertView alloc] initWithTitle:@"Hubo un error" message:@"La foto no pudo subirse al servidor, por favor intentalo nuevamente" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                        
                    }
                }
            });
        }];
        
        [uploadTask resume];
        
    });
    
    
    
   /* [uploader playWithCompletion:^(BOOL completion){
        
        if(completion){
            
            [UIApplication sharedApplication].statusBarHidden = NO;
            [self dismissViewControllerAnimated:YES completion:^{
                
                [UIApplication sharedApplication].statusBarHidden = NO;
            }];
        }
    }];*/
    
    
    
    /********************** antes de modificarlo *************************/
    /*if(isVideo){
        
        NSData *dataVideo = [[NSData alloc] initWithContentsOfURL:urlAssetVideo.URL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if([self.delegate respondsToSelector:@selector(getImageVideoTomado:videoImagen:comentario:compartirFacebook:)]){
                
                [self.delegate getImageVideoTomado:dataVideo videoImagen:2 comentario:nil compartirFacebook:seComparteEnFacebook];
            }
            
            [UIApplication sharedApplication].statusBarHidden = NO;
            [self dismissViewControllerAnimated:YES completion:^{
                
                [self pausarVideo];
                reproductor = nil;
                [UIApplication sharedApplication].statusBarHidden = NO;
            }];
        });
        
    }else{
        
        NSData *dataImagen = UIImageJPEGRepresentation([fotoFinal image], 90);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if([self.delegate respondsToSelector:@selector(getImageVideoTomado:videoImagen:comentario:compartirFacebook:)]){
                
                [self.delegate getImageVideoTomado:dataImagen videoImagen:1 comentario:nil compartirFacebook:seComparteEnFacebook];
            }
            
            [UIApplication sharedApplication].statusBarHidden = NO;
            [self dismissViewControllerAnimated:YES completion:^{
                
                [UIApplication sharedApplication].statusBarHidden = NO;
            }];
        });
    }*/
}




#pragma mark - url session

-(void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
    
    
    NSLog(@"1 se envió data %ld esperados %ld de %ld",(long)bytesWritten, (long)totalBytesWritten, (long)totalBytesExpectedToWrite);
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes{}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    
}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        LOTAnimationView *uploader;
        
        if(isVideo){
            
            uploader = (LOTAnimationView*)[vistaVideo viewWithTag:104];
        }else{
            
            uploader = (LOTAnimationView*)[self.view viewWithTag:104];
        }
        
        
        [uploader playToProgress:((totalBytesSent*100)/totalBytesArchivoAsubir)*0.00627 withCompletion:^(BOOL animationFinished){}];
        
    });
}

-(void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session{
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
