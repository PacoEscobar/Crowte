//
//  CambiarFotoPerfil.m
//  Crowte
//
//  Created by Paco Escobar on 20/01/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "CambiarFotoPerfil.h"
#import "configuracionController.h"

@interface CambiarFotoPerfil ()

@end

@implementation CambiarFotoPerfil

@synthesize tomaFoto,seleccionaFoto, dataDeFoto,foto, constraintTopSeparador;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor grayColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
    
    [[UINavigationBar appearance] setTranslucent:YES];
    
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setTintColor:[UIColor grayColor]];
    
    if([[UIScreen mainScreen] bounds].size.height <= 480){
        
        [constraintTopSeparador setConstant:54];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)tomarfoto:(id)sender{

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:nil];
}

-(IBAction)seleccionarFoto:(id)sender{

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    foto = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIApplication *app = [UIApplication sharedApplication];
        app.networkActivityIndicatorVisible = YES;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
            NSData *dataFotoNueva = UIImageJPEGRepresentation(foto, 90);
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/subirfoto.php"]];
            
            [request setHTTPMethod:@"POST"];
            
            NSMutableData *cuerpo = [NSMutableData data];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            
            NSString *contenido = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            
            NSString *linea = @"foto";
            
            [request addValue:contenido forHTTPHeaderField:@"Content-Type"];
            
            [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [cuerpo appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"foto\"; filename=\"%@.jpg\"\r\n",linea] dataUsingEncoding:NSUTF8StringEncoding]];
            linea = @"Content-Type: application/octet-stream\r\n\r\n";
            [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            [cuerpo appendData:dataFotoNueva];
            linea = @"\r\n";
            [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            linea = @"Content-Disposition: form-data; name=\"id\"\r\n\r\n";
            [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            [cuerpo appendData:[[NSString stringWithFormat:@"%@",[defaults objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding]];
            linea = @"\r\n";
            [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            
            [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [request setHTTPBody:cuerpo];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
                if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                    
                    [[[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil] show];
                }else{
                
                    NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

                    if([respuesta isEqualToString:@"listo"]){
                    
                        dispatch_async(dispatch_get_main_queue(), ^{
                            app.networkActivityIndicatorVisible = NO;
                            
                            
                        });
                    }else{
                    
                        dispatch_async(dispatch_get_main_queue(), ^{
                        
                            [[[UIAlertView alloc] initWithTitle:@"Hubo un error" message:@"La foto no pudo subirse al servidor, por favor intentalo nuevamente" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                        });
                    }
                }
                
            }];

            
        });
        
        [_delegate pasarFoto:foto];
    }];

}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{

    [picker dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
