//
//  RespuestasFAQViewController.h
//  Crowte
//
//  Created by Paco Escobar on 14/07/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RespuestasFAQViewController : UIViewController <UIWebViewDelegate>


@property int idpregunta;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *carga;

@property (weak, nonatomic) IBOutlet UIWebView *web;


@end
