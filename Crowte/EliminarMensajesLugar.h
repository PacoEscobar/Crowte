//
//  EliminarMensajesLugar.h
//  Crowte
//
//  Created by Paco Escobar on 07/08/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EliminarMensajesLugar : NSObject

@property NSMutableArray *arreglo;
@property NSMutableArray *elementosAEliminar;

-(NSMutableArray*)ejecutar;
-(void)agregarAlArreglo:(id)elemento;
-(void)eliminarAlArreglo:(NSMutableArray*)array;

@end
