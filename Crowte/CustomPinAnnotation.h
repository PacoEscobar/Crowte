//
//  CustomPinAnnotation.h
//  Crowte
//
//  Created by Paco Escobar on 15/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface CustomPinAnnotation : MKAnnotationView

@property BOOL enVivo;
@property int idUsuario;
@property NSString *lugar;
@property NSString *persona;
@property NSString *fotoPersona;
@property NSString *fondoLugar;
@property NSString *fotoLugar;


- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation type:(BOOL)tipo idUsuario:(int)idUsuario lugar:(NSString*)lugar persona:(NSString*)persona fotoPersona:(NSString*)fotoPersona fotoLugar:(NSString*)fotoLugar fondoLugar:(NSString*)fondoLugar;

@end
