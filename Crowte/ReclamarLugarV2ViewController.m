//
//  ReclamarLugarV2ViewController.m
//  Crowte
//
//  Created by Paco Escobar on 23/03/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import "ReclamarLugarV2ViewController.h"

@interface ReclamarLugarV2ViewController ()

@end

@implementation ReclamarLugarV2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)regresar:(id)sender {
    
    [[[self presentingViewController] presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}
@end
