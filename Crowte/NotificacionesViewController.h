//
//  NotificacionesViewController.h
//  Crowte
//
//  Created by Paco Escobar on 22/02/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificacionesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSURLSessionDelegate, NSURLSessionDownloadDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tabla;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property NSDictionary *notificaciones;

@property NSTimer *intervaloGetNotificaciones;

- (IBAction)cambiarNotificacion:(id)sender;
- (IBAction)cancelarAmigo:(id)sender;
- (IBAction)aceptarAmigo:(id)sender;
- (void)intervaloNotificaciones;
- (void)getNotificaciones;

@end
