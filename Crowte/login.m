//
//  login.m
//  crowte
//
//  Created by Paco Escobar on 07/05/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "NSString+MD5.h"
#import "login.h"
#import "controladorTabControllerViewController.h"
#import "TabBarPrincipalViewController.h"
#import "PageViewController.h"

@implementation login

@synthesize correo, password, logo, iniciarS, loginfails, recordarpass, constraintLogo, constraintFormularioSuperior;

-(IBAction)ocultarTeclado:(id)sender
{
    [password becomeFirstResponder];
}

-(IBAction)clickFondo:(id)sender
{
    [correo resignFirstResponder];
    [password resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == correo) {
        [textField resignFirstResponder];
        [password becomeFirstResponder];
    } else if (textField == password) {
        
        [self checkUsuario:self];
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == self.password){

        [UIView animateWithDuration:0.5 animations:^{
            
            [constraintFormularioSuperior setConstant:29];
            [constraintLogo setConstant:155];
            [self.view layoutIfNeeded];
        }];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.password){
        [UIView animateWithDuration:0.5 animations:^{
            
            [constraintFormularioSuperior setConstant:129];
            [constraintLogo setConstant:40];
            [self.view layoutIfNeeded];
        }];
    }
}

-(IBAction)checkUsuario:(id)sender
{

    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    [password resignFirstResponder];
    [correo resignFirstResponder];
    
    if(self.view.frame.origin.y != 0){
    
        [UIView animateWithDuration:0.4 animations:^{
        
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
            self.logo.center = CGPointMake(logo.center.x, logo.center.y + 150);
        }];
    }
    
    if([correo.text isEqualToString:@""] || [password.text isEqualToString:@""]){
            
        UIAlertController *caNul = [UIAlertController alertControllerWithTitle:nil message:@"Tienes que rellenar los dos campos" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleDefault handler:nil];
        
        [caNul addAction:ok];
        
        [self presentViewController:caNul animated:YES completion:nil];
        
        
    }
    NSString *pass = password.text;
    NSString *enc = [pass MD5String];
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/inses.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"usr=%@&def=%@",correo.text,enc] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            NSLog(@"error");
            app.networkActivityIndicatorVisible = NO;
        }else{

            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            NSString *resultao = [json objectForKey:@"inicio"];
            NSString *ident = [json objectForKey:@"id"];
        
            if ([resultao isEqualToString:@"1"]) {
            
                NSUserDefaults *misDatos = [NSUserDefaults standardUserDefaults];
              
                [misDatos setObject:resultao forKey:@"id"];
                [misDatos setObject:ident forKey:@"identifier"];
                [misDatos synchronize];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
                
              
                
                NSURL *url = [NSURL URLWithString:@"https://www.crowte.com/app/getdatos.php"];
                
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:[[NSString stringWithFormat:@"id=%d&recurso=1&p=1",[ident intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
                
                NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                
                [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                    
                    NSMutableDictionary *datosUsuario = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        app.networkActivityIndicatorVisible = NO;
                        
                        [misDatos setObject:[[datosUsuario objectForKey:@"datos"] objectForKey:@"sesion"] forKey:@"token"];
                        
                        [misDatos setObject:[[datosUsuario objectForKey:@"datos"] objectForKey:@"verificado"] forKey:@"verificado"];
                        
                        [misDatos setObject:[[datosUsuario objectForKey:@"datos"] objectForKey:@"publicarFacebook"] forKey:@"publicarFacebook"];
                        
                        [misDatos synchronize];
                        
                        
                        
                        
                        [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
                        
                        [[UINavigationBar appearance] setTranslucent:YES];
                        
                        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
                        
                        UIStoryboard *storyboard;
                        NSLog(@"datos usuario %@",datosUsuario);
                        TabBarPrincipalViewController *tabBarPrincipalVC;
                        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        tabBarPrincipalVC= [storyboard instantiateViewControllerWithIdentifier:@"mainTabBar"];
                        
                        [tabBarPrincipalVC setCheckinsTodosOtros:[datosUsuario objectForKey:@"todosOtrosPost"]];
                        [tabBarPrincipalVC setCheckinsTodosUltimos:[datosUsuario objectForKey:@"todosUltimosPosts"]];
                        [tabBarPrincipalVC setDatosCheckinEnLugar:[datosUsuario objectForKey:@"checkinAlready"]];
                        [tabBarPrincipalVC setDatosMios:[datosUsuario objectForKey:@"datos"]];
                        
                        
                        app.networkActivityIndicatorVisible = NO;
                        
                        [self presentViewController:tabBarPrincipalVC animated:NO completion:nil];
                        
                    });
                }];
            
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    loginfails++;
                    
                    if(loginfails == 3){
                    
                        [recordarpass setHidden:NO];
                    }
                    
                    app.networkActivityIndicatorVisible = NO;
                    
                    UIAlertController *fallo = [UIAlertController alertControllerWithTitle:nil message:@"El usuario o contraseña no son correctos, por favor verifícalos." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *aceptar = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
                    
                    [fallo addAction:aceptar];
                    
                    [self presentViewController:fallo animated:YES completion:nil];
                    
                });
            }
        }
    
    }];
    
}

-(void)viewDidLoad{

    [super viewDidLoad];
    
    loginfails = 0;
}

- (void)viewDidUnload {
    [self setLogo:nil];
    [super viewDidUnload];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(IBAction)cerrarSesion:(id)sender
{
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    [defs removeObjectForKey:@"id"];
    [defs removeObjectForKey:@"identifier"];
    [defs synchronize];
}

@end
