//
//  CheckinViewController.h
//  Crowte
//
//  Created by Paco Escobar on 18/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface CheckinViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, NSURLSessionDelegate, NSURLSessionDownloadDelegate, UISearchBarDelegate>

@property BOOL locationContador;
@property int ultimoMensajeID;

@property NSString *direccionLugarString;

@property (strong, nonatomic) IBOutlet UISearchBar *buscarLugar;
@property NSDictionary *lugaresCercanos;
@property NSArray *lugaresCercanosSearchPlaceHolder;

@property NSMutableDictionary *datosEnLugar;
@property NSMutableArray *mensajes;

@property CLLocationManager *locationManager;
@property CLGeocoder *geocoder;

@property NSTimer *intervaloTiempoCheckin;
@property NSTimer *intervaloMensajesNuevos;

@property (weak, nonatomic) IBOutlet UIView *vistaTutorial2;


@property (strong, nonatomic) IBOutlet UITableView *tablaLugares;
@property (strong, nonatomic) IBOutlet UITableView *tablaCheckinDone;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintInferior;


@property (strong, nonatomic) IBOutlet UIView *vistaCheckinDone;

#pragma mark - empty state

@property (strong, nonatomic) IBOutlet UIImageView *fondoEmptyState;
@property (strong, nonatomic) IBOutlet UILabel *textoAgregaUno;
@property (strong, nonatomic) IBOutlet UIImageView *lupaEmptyState;
@property (strong, nonatomic) IBOutlet UILabel *textoNoSeEncontraron;




-(void)setCheckinDone;
-(void)checarTiempoCheckin;
-(void)terminaCheckin;
-(void)setFotoPrincipal:(NSString*)fotoPrincipal fotoSecundaria:(NSString*)fotoSecundaria nombreLugar:(NSString*)nombre latitud:(double)latitud longitud:(double)longitud id:(int)identificador;
-(void)actualizarCheckins;

-(IBAction)dejarVoto:(id)sender;
-(void)getMensajes;
-(void)getMensajesNuevos;
-(IBAction)noahi:(id)sender;
-(IBAction)aunahi:(id)sender;
-(IBAction)meGustaComentario:(id)sender;
-(IBAction)agregarLugar:(id)sender;
-(IBAction)mostrarOpcionesMensaje:(id)sender;

-(void)abrirImagen:(UIPanGestureRecognizer*)gesto;

-(void)lugarTutorial;

- (IBAction)cerrarLugarTutorial:(id)sender;
- (IBAction)cerrarTutorialPublicarAmbiente:(id)sender;
- (void)getTutorialPublicarAmbiente;


-(void)cerrarMensajeCerveza;


@end
