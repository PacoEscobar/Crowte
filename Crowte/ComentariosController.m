//
//  ComentariosController.m
//  Crowte
//
//  Created by Paco Escobar on 09/05/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIButton+WebCache.h>
#import "ComentariosController.h"
#import "DeEjemplo.h"
#import "lugarViewController.h"
#import "Perfil.h"

@interface ComentariosController ()

@end

@implementation ComentariosController

@synthesize posicionScroll, botonCancelar, botonEnviar, publicacion, nombreLugar, labelEn, nombrePersona, foto, textoDePublicacion, lugar, textCampo, textFieldContenedor, postID, modeloComentarios, modeloFotos, tabla, app, amigoID, json, joinsDePublicacion, joinFilled;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    textCampo.layer.cornerRadius = 0;
    textCampo.layer.masksToBounds = YES;
    textCampo.layer.borderColor = [[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor];
    textCampo.layer.borderWidth = 1.0;
    
    
    app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    modeloComentarios = [NSMutableArray array];
    modeloFotos = [NSMutableDictionary dictionary];
    
    if(foto != nil){
        
        [tabla reloadData];
    }
    
    if([textoDePublicacion isEqualToString:@"<nohay>"]){
    
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getdatoscomentarios.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d",postID] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil];
                    
                    [error show];
                });
            }else{
            
                NSString *regresa = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                if([regresa isEqualToString:@"0"]){
                
                    dispatch_async(dispatch_get_main_queue(), ^{
                    
                        UIAlertController *error = [UIAlertController alertControllerWithTitle:nil message:@"Ésta publicación ya no está disponible." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okas = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
                        
                            [self.navigationController popViewControllerAnimated:YES];
                        }];
                       
                        [error addAction:okas];
                        
                        [self presentViewController:error animated:YES completion:nil];
                        
                    });
                }else{
                
                    json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [tabla reloadData];
                        [self pedirComentarios];
                    });
                }
            }
        }];
    }else{
        
        [self pedirComentarios];
    }
    if([lugar isEqual:[NSNull null]] || lugar == NULL || [lugar isEqualToString:@""]){

        [labelEn removeFromSuperview];
        [nombreLugar removeFromSuperview];
    }else{
    
        [nombreLugar setTitle:lugar forState:UIControlStateNormal];
    }
    
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda;

    if(indexPath.row == 0){
    
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaPublicacion"];
        
        if([textoDePublicacion isEqualToString:@"<nohay>"]  && [json count] > 0){
            
            if([[json objectForKey:@"objeto"] intValue] > 0){
            
                UIButton *lugarPublicacion = (UIButton*)[celda viewWithTag:4];
                [lugarPublicacion setTitle:[json objectForKey:@"lugar"] forState:UIControlStateNormal];
                
            }else{
            
                UILabel *en = (UILabel*)[celda viewWithTag:3];
                [en setHidden:YES];
                UIButton *lugarPublicacion = (UIButton*)[celda viewWithTag:4];
                [lugarPublicacion setHidden:YES];
            }
            
            
            if(![[json objectForKey:@"acontecimiento"] isEqualToString:@"(null)"]){
                
                UITextView *textoPublicacion = (UITextView *)[celda viewWithTag:5];
                
                NSData *data = [[json objectForKey:@"acontecimiento"] dataUsingEncoding:NSUTF8StringEncoding];
                
                NSString *publicacionFormateada = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                
                [textoPublicacion setText:publicacionFormateada];
                
            }else{
            
                UITextView *textopublicacion = (UITextView *)[celda viewWithTag:5];
                [textopublicacion setHidden:YES];
            }
            
            UIButton *nombrePerfil = (UIButton*)[celda viewWithTag:2];
            
            [[nombrePerfil titleLabel] setNumberOfLines:2];
            [nombrePerfil setTitle:[NSString stringWithFormat:@"%@ %@",[json objectForKey:@"nombre"],[json objectForKey:@"apellido"]] forState:UIControlStateNormal];
            
            
            UIButton *imagenPerfil = (UIButton*)[celda viewWithTag:1];
            [imagenPerfil sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[json objectForKey:@"ID"] intValue],[json objectForKey:@"foto"]]] forState:UIControlStateNormal];
            
            imagenPerfil.layer.borderWidth = 0.0;
            imagenPerfil.layer.cornerRadius = 10;
            imagenPerfil.layer.masksToBounds = YES;

            
            UILabel *labelNumeroComentarios = (UILabel*)[celda viewWithTag:6];
            
            [labelNumeroComentarios setText:[json objectForKey:@"comentarios"]];
            
        }else if(![textoDePublicacion isEqualToString:@"<nohay>"]){
            
            UITextView *textoPublicacion = (UITextView *)[celda viewWithTag:5];

            textoPublicacion.text = textoDePublicacion;

            UIButton *nombrePerfil = (UIButton*)[celda viewWithTag:2];
            
            [[nombrePerfil titleLabel] setNumberOfLines:2];

            [nombrePerfil setTitle:nombrePersona forState:UIControlStateNormal];

            UIButton *imagenPerfil = (UIButton*)[celda viewWithTag:1];
            
            [imagenPerfil setBackgroundImage:[UIImage imageWithData:foto] forState:UIControlStateNormal];
            
            imagenPerfil.layer.borderWidth = 0.0;
            imagenPerfil.layer.cornerRadius = 10;
            imagenPerfil.layer.masksToBounds = YES;
            
            if([lugar isEqual:[NSNull null]]){
            
                UILabel *en = (UILabel*)[celda viewWithTag:3];
                [en setHidden:YES];
                
                UILabel *joins = (UILabel*)[celda viewWithTag:7];
                
                [joins setHidden:YES];
                
                UIButton *botonJoin = (UIButton *)[celda viewWithTag:8];
                [botonJoin setHidden:YES];
            }else{
                UIButton *place = (UIButton*)[celda viewWithTag:4];
                [place setTitle:lugar forState:UIControlStateNormal];
                
                UILabel *joins = (UILabel*)[celda viewWithTag:7];

                if(![[NSString stringWithFormat:@"%@",joinsDePublicacion] isEqualToString:@"0"]){

                    [joins setText:joinsDePublicacion];
                }else{
                
                    [joins setText:@""];
                }
                
                if(joinFilled){
                
                    UIButton *botonJoin = (UIButton *)[celda viewWithTag:8];
                    [botonJoin setImage:[UIImage imageNamed:@"joinFilled"] forState:UIControlStateNormal];
                }else{
                
                    UIButton *botonJoin = (UIButton *)[celda viewWithTag:8];
                    [botonJoin setImage:[UIImage imageNamed:@"joinT"] forState:UIControlStateNormal];
                }
            }
        }
        
    }else{
        
        if([modeloComentarios count] > 0){
            
            celda = [tableView dequeueReusableCellWithIdentifier:@"celdaComentarios"];
            
            celda.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"separadortabla.png"]];
            
            NSDate *fecha = [[NSDate alloc] initWithTimeIntervalSince1970:[[[modeloComentarios objectAtIndex:indexPath.row-1] objectForKey:@"fecha"] intValue]];
            
            NSDateComponents *componentesFecha = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:fecha];
            
            UILabel *labelFecha = (UILabel *)[celda viewWithTag:4];
            
            [labelFecha setText:[NSString stringWithFormat:@"%ld/%ld/%ld",(long)[componentesFecha day],(long)[componentesFecha month],(long)[componentesFecha year]]];
            
            if([modeloFotos objectForKey:[[modeloComentarios objectAtIndex:indexPath.row-1] objectForKey:@"foto"]] == nil){
                
                NSURL *fotourl = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/%@",[[modeloComentarios objectAtIndex:indexPath.row-1] objectForKey:@"autor"],[[modeloComentarios objectAtIndex:indexPath.row-1] objectForKey:@"foto"]]];
               
                
                UIButton *imgfoto = (UIButton *)[celda viewWithTag:1];
                [imgfoto sd_setBackgroundImageWithURL:fotourl forState:UIControlStateNormal];
                
                imgfoto.layer.borderWidth = 0.0;
                imgfoto.layer.cornerRadius = 5;
                imgfoto.layer.masksToBounds = YES;
                
            }else{
                
                UIButton *imgfoto = (UIButton *)[celda viewWithTag:1];
                [imgfoto setBackgroundImage: [modeloFotos objectForKey:[[modeloComentarios objectAtIndex:indexPath.row-1] objectForKey:@"foto"]] forState:UIControlStateNormal];
                imgfoto.layer.borderWidth = 0.0;
                imgfoto.layer.cornerRadius = 5;
                imgfoto.layer.masksToBounds = YES;
            }
            
            UILabel *nombre = (UILabel *)[celda viewWithTag:2];
            nombre.text = [NSString stringWithFormat:@"%@ %@",[[modeloComentarios objectAtIndex:indexPath.row-1] objectForKey:@"nombre"],[[modeloComentarios objectAtIndex:indexPath.row-1] objectForKey:@"apeido"]];
            
            UILabel *comentario = (UILabel *)[celda viewWithTag:3];
            
            NSData *dataComentario = [[[modeloComentarios objectAtIndex:indexPath.row-1] objectForKey:@"comentario"] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSString *publicacionFormateada = [[NSString alloc] initWithData:dataComentario encoding:NSNonLossyASCIIStringEncoding];
            
            [comentario setText:publicacionFormateada];
        }else{
            
            celda = [[UITableViewCell alloc] init];
            
            [celda setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.0]];
            
        }
    }
    
    return celda;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    float size;
    
    if(indexPath.row == 0){
    
        size = 215;
        
    }else{

        if([modeloComentarios count] > 0){
            
            float numeroCaracteres = [[[modeloComentarios objectAtIndex:indexPath.row-1] objectForKey:@"comentario"] length];
            
            numeroCaracteres = numeroCaracteres/32;
            
            numeroCaracteres = ceilf(numeroCaracteres);
            
            numeroCaracteres *= 20;
            
            size = 103+numeroCaracteres;
            
        }else if([modeloComentarios count] == 0){
            
            size = 100;
        }
    }
    
    return size;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    NSUInteger numero;
    
    if([modeloComentarios count] == 0){
    
        numero = 1;
    }else{
    
        numero = [modeloComentarios count]+1;
    }
    
    return numero;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if([[UIScreen mainScreen] bounds].size.height <= 480){
    
        [UIView animateWithDuration:0.4 animations:^{
            
            textFieldContenedor.frame = CGRectMake(textFieldContenedor.frame.origin.x, textFieldContenedor.frame.origin.y-180, textFieldContenedor.frame.size.width, textFieldContenedor.frame.size.height);
        }];
    }else{
        
        [UIView animateWithDuration:0.4 animations:^{
            
            textFieldContenedor.frame = CGRectMake(textFieldContenedor.frame.origin.x, textFieldContenedor.frame.origin.y-210, textFieldContenedor.frame.size.width, textFieldContenedor.frame.size.height);
        }];
    }

}

-(UIStatusBarStyle)preferredStatusBarStyle{

    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if([segue.destinationViewController isKindOfClass:[lugarViewController class]]){
        
        lugarViewController *controlador = (lugarViewController *)segue.destinationViewController;
        
        controlador.nombreLugar = [[nombreLugar titleLabel] text];
    }else if([segue.destinationViewController isKindOfClass:[Perfil class]]){
        
        Perfil *controlador = (Perfil *)segue.destinationViewController;
        
        
        controlador.stringNombre = [json objectForKey:@"nombre"];
        
        controlador.amigoID = amigoID;
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{

    if([textCampo isFirstResponder]){
    
        if([[UIScreen mainScreen] bounds].size.height <= 480){
        
            [UIView animateWithDuration:0.4 animations:^{
                
                textFieldContenedor.frame = CGRectMake(textFieldContenedor.frame.origin.x, textFieldContenedor.frame.origin.y+180, textFieldContenedor.frame.size.width, textFieldContenedor.frame.size.height);
            }];
        }else{
            
            [UIView animateWithDuration:0.4 animations:^{
                
                textFieldContenedor.frame = CGRectMake(textFieldContenedor.frame.origin.x, textFieldContenedor.frame.origin.y+210, textFieldContenedor.frame.size.width, textFieldContenedor.frame.size.height);
            }];
        }
        
    }
    
}

#pragma mark comentarios

-(IBAction)enviarComentario:(id)sender{

    if([[textCampo text] length] > 0){
        
        [botonEnviar setEnabled:NO];
        
        app.networkActivityIndicatorVisible = YES;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSData *data =[[textCampo text] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
        NSString *comentario = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/addcomentario.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"idPublicacion=%d&userID=%d&comment=%@&token=%@",postID, [[defaults objectForKey:@"identifier"] intValue], comentario,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *respuesta, NSData *data, NSError *error){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [textCampo setText:@""];
                [botonEnviar setEnabled:YES];
                app.networkActivityIndicatorVisible = NO;
                
                [self pedirComentarios];
            });
        }];
        
    }
        
        
}

-(void)pedirComentarios{

    if([modeloComentarios count] > 0){
    
        [modeloComentarios removeAllObjects];
    }
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getcomentarios.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d",postID] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil];
                
                [error show];
            });
        }else{
            NSArray *arregloComentarios = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            if(arregloComentarios == NULL){
                
                
            }else{
                
                for(NSDictionary *iteraciones in arregloComentarios){
                    
                    [modeloComentarios addObject:iteraciones];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tabla reloadData];
                });
                
            }
            
            app.networkActivityIndicatorVisible = NO;
        }
        
    }];
}

#pragma mark - mios

-(void)descargarImagen:(NSURL *)url completionBlock:(void (^)(BOOL, UIImage *))completionBlock{
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:peticion queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *respuesta, NSData *data, NSError *error){
        
        if(!error || data == NULL){
            
            UIImage *imagen = [[UIImage alloc] initWithData:data];
            completionBlock(YES, imagen);
            imagen = nil;
        }else{
            
            completionBlock(NO, nil);
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
