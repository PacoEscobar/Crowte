//
//  ReclamarLugarGraciasViewController.h
//  Crowte
//
//  Created by Paco Escobar on 20/04/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReclamarLugarGraciasViewController : UIViewController

- (IBAction)regresar:(id)sender;

@end
