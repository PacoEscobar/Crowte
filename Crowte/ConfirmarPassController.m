//
//  ConfirmarPassController.m
//  Crowte
//
//  Created by Paco Escobar on 24/08/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import "NSString+MD5.h"
#import "ConfirmarPassController.h"
#import "ModificarSecretodebusquedaController.h"

@interface ConfirmarPassController ()

@end

@implementation ConfirmarPassController

@synthesize textocontra, contenedor,constraintTop;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIBarButtonItem *enviar = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"Enviar"] style:UIBarButtonItemStylePlain target:self action:@selector(enviar)];
    
    self.navigationItem.rightBarButtonItem = enviar;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark custom

-(void)enviar{

    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    NSString *pass = [textocontra text];
    
    pass = [pass MD5String];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/autorizarusuario.php"]];
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"pass=%@&usuario=%@&token=%@", pass   ,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                app.networkActivityIndicatorVisible = NO;
                
                UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil];
            
                [error show];
            });
        }else{
            
            int resultado;
            NSString *numero = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            resultado = [numero intValue];
            
            if(resultado == 1){
            
                dispatch_async(dispatch_get_main_queue(), ^{

                    app.networkActivityIndicatorVisible = NO;
            
                    ModificarSecretodebusquedaController *modificar = [self.storyboard instantiateViewControllerWithIdentifier:@"modificarSB"];
            
                    [self.navigationController pushViewController:modificar animated:YES];
                });
            }else if(resultado == 0){
        
                UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Contraseña incorrecta" message:@"Inténtalo otra vez" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Aceptar", nil];
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    app.networkActivityIndicatorVisible = NO;
                    
                    [alerta show];
                });
            }
        }
    }];
}

#pragma mark uitexto

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [self enviar];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{

    if([[UIScreen mainScreen] bounds].size.height <= 480){
    
        [UIView animateWithDuration:0.4 animations:^{
        
            [constraintTop setConstant:50];
        }];
    }
}

@end
