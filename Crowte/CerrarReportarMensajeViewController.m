//
//  CerrarReportarMensajeViewController.m
//  Crowte
//
//  Created by Paco Escobar on 11/03/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "CerrarReportarMensajeViewController.h"

@interface CerrarReportarMensajeViewController ()

@end

@implementation CerrarReportarMensajeViewController

@synthesize botonBloquear, idUsuarioABloquear, objetivoDelReporte, mensaje1, mensaje2;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(objetivoDelReporte == 1){
        
        [mensaje1 setText:@"Gracias por ayudar a mantener Crowte limpio y seguro. Investigaremos las infracciones de este usuario a nuestras políticas de uso."];
        [mensaje2 setText:@"También puedes bloquear a este usuario para evitar ver sus check-in y mensajes."];
    }else if(objetivoDelReporte == 2){
        
        [mensaje1 setText:@"Gracias por ayudar a mantener Crowte limpio y seguro. Investigaremos las infracciones de este check-in a nuestras políticas de uso."];
        [mensaje2 setText:@"También puedes bloquear a este usuario para evitar ver sus check-in."];
    }else if(objetivoDelReporte == 3){
        
        [mensaje1 setText:@"Gracias por ayudar a mantener Crowte limpio y seguro. Investigaremos las infracciones de este lugar a nuestras políticas de uso."];
        [mensaje2 setText:@""];
        [botonBloquear setHidden:YES];
    }
    
    [botonBloquear.layer setBorderColor:[[UIColor redColor] CGColor]];
    [botonBloquear.layer setBorderWidth:1];
    [botonBloquear.layer setCornerRadius:15];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cerrarTodo:(id)sender {
    
    [[[self presentingViewController] presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)bloquearUsuario:(id)sender {
    
        [botonBloquear setEnabled:NO];
        
        [botonBloquear.layer setBorderColor:[[UIColor grayColor] CGColor]];
        [botonBloquear setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/bloqueos/bloquear.php"]];
        
        [request setHTTPMethod:@"POST"];
        
        NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@&bloqueador=%@",idUsuarioABloquear,[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
        
        NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){}];
        
        [uploadTask resume];
    
}

#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}

@end
