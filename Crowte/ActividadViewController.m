//
//  ActividadViewController.m
//  Crowte
//
//  Created by Paco Escobar on 09/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIImageView+WebCache.h>
#import <CoreLocation/CoreLocation.h>
#import <stdlib.h>
#import <Lottie/Lottie.h>
#import "ActividadViewController.h"
#import "CustomPinAnnotation.h"
#import "CustomPointAnnotation.h"
#import "BuscarViewController.h"
#import "ReportarCheckinTableViewController.h"
#import "CheckinViewController.h"
#import "NavCheckinViewController.h"
#import "PerfilLugarViewController.h"
#import "EasyTip/RCEasyTipView.h"

@interface ActividadViewController ()

@end

@implementation ActividadViewController

@synthesize seMuestranTodosCheckin, seMuestraMapa, segmentedControl, mapa, fondoMapa, listaCheckin, checkinsTodosAmigos, checkinsTodosOtros, checkinsTodosUltimos, checkinsUltimosAmigos, numeroCambiosMapa,previousScrollingViewYOffset, constraintTablaOriginY, intervaloNumSolicitudes, intervaloActualizarLista, miLocation, vistaEmptyState, ultimoRelIDTodos, ultimoRelIDAmigos, backgroundTutorial, opcionesSeleccionadas;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    seMuestranTodosCheckin = YES;
    seMuestraMapa = NO;
    numeroCambiosMapa = 0;
    ultimoRelIDTodos = 0;
    ultimoRelIDAmigos = 0;
    
    opcionesSeleccionadas = [NSMutableDictionary dictionary];
    
    UIImage *imageSearch = [UIImage imageNamed:@"search-icon.png"];
    
    UIButton *search = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imageSearch.size.width, imageSearch.size.height)];
    
    [search setImage:imageSearch forState:UIControlStateNormal];
    
    [search addTarget:self action:@selector(buscar:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barbtnsearch = [[UIBarButtonItem alloc] initWithCustomView:search];
    
    UIImage *imageMapview = [UIImage imageNamed:@"mapview.png"];
    
    UIButton *mapview = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imageMapview.size.width, imageMapview.size.height)];
    
    [mapview setImage:imageMapview forState:UIControlStateNormal];
    
    [mapview addTarget:self action:@selector(mostrarMapa:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barbtnmapview = [[UIBarButtonItem alloc] initWithCustomView:mapview];
    
    self.navigationItem.rightBarButtonItem = barbtnsearch;
    self.navigationItem.leftBarButtonItem = barbtnmapview;
    
    [[UINavigationBar appearance] setTranslucent:YES];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"transparente.png"]];
    
    UIView *division = [[UIView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.width, 0.5)];
    
    [division setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.3]];
    
    [division setTag:1];
    
    [self.navigationController.navigationBar addSubview:division];
    
    self.navigationItem.title = @"Actividad";
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
    
    [[UISegmentedControl appearance] setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [[UISegmentedControl appearance] setDividerImage:[UIImage imageNamed:@"transparente.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithWhite:1 alpha:0.5],NSForegroundColorAttributeName,[UIFont fontWithName:@"Hind-Light" size:15.0], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Hind-Medium" size:15.0], NSFontAttributeName, nil] forState:UIControlStateSelected];
    
    [self agregarPines:checkinsTodosUltimos viejos:checkinsTodosOtros];
    
    //intervaloNumSolicitudes = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getNotificacionesNuevas) userInfo:nil repeats:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - selector controller

-(void)activarTodos:(id)sender{

    if(!seMuestranTodosCheckin){
        
        seMuestranTodosCheckin = YES;
    }
}

-(void)activarMisAmigos:(id)sender{

    if(seMuestranTodosCheckin){
        
        seMuestranTodosCheckin = NO;
    }
}

#pragma mark - lista check-ins

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UIView *vista = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    
   [vista setBackgroundColor:[[tableView backgroundColor] colorWithAlphaComponent:0.8]];
    
    UILabel *titulo = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    
    [titulo setTextColor:[UIColor whiteColor]];
    [titulo setFont:[UIFont fontWithName:@"Hind-Regular" size:13]];
    
    if(section == 0){
    
        [titulo setText:@"Justo ahora"];
    }else{
    
        [titulo setText:@"Pasados"];
    }
    
    [titulo setTextAlignment:NSTextAlignmentCenter];
    
    [vista addSubview:titulo];
    
    return vista;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if(section == 0){
    
        if(segmentedControl.selectedSegmentIndex == 0){
            
            if([checkinsTodosUltimos count] > 0 && [[[checkinsTodosUltimos objectAtIndex:0] objectForKey:@"valido"] intValue] == 1){
                
                return [checkinsTodosUltimos count];
            }else{
                
                return 0;
            }
        }else{
        
            if([[[checkinsUltimosAmigos objectAtIndex:0] objectForKey:@"valido"] intValue] == 1){
                
                return [checkinsUltimosAmigos count];
            }else{
                
                return 0;
            }
        }
    }else{
    
        if(segmentedControl.selectedSegmentIndex == 0){
            
            if([[[checkinsTodosOtros objectAtIndex:0] objectForKey:@"valido"] intValue] == 1){
                
                return [checkinsTodosOtros count];
            }else{
            
                return 0;
            }
        }else{
        
            if([[[checkinsTodosAmigos objectAtIndex:0] objectForKey:@"valido"] intValue] == 1){
                
                return [checkinsTodosAmigos count];
            }else{
                
                return 0;
            }
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    float adicional = 0;
    NSString *texto;
    
    if(indexPath.section == 0 && segmentedControl.selectedSegmentIndex == 0){
    
        texto = [[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"acontesimiento"];
        
    }else if(indexPath.section == 0 && segmentedControl.selectedSegmentIndex == 1){
        
        texto = [[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"acontesimiento"];
        
    }else if(indexPath.section == 1 && segmentedControl.selectedSegmentIndex == 0){
        
        texto = [[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"acontesimiento"];
        
    }else if(indexPath.section == 1 && segmentedControl.selectedSegmentIndex == 1){
    
        texto = [[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"acontesimiento"];
    }
    
    texto = [texto stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    if(![texto isEqualToString:@""]){

        if([texto length] > 44){
            
            adicional = ceil([texto length]/44);
            adicional = adicional*55;
        }else{
            
            adicional = 55;
        }
    }
    
    return 375+adicional;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *celda;
    
    celda = [tableView dequeueReusableCellWithIdentifier:@"celdaCheckin"];

    if(indexPath.section == 0){
        
        UILabel *textoEnEsteMomento = (UILabel*)[celda viewWithTag:7];
        
        [textoEnEsteMomento setHidden:NO];
        if(segmentedControl.selectedSegmentIndex == 0){
            
            if([checkinsTodosUltimos count] > 0){
                UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldanuevo.png"]];
                
                [celda setBackgroundView:fondo];
                [celda setBackgroundColor:[UIColor clearColor]];
                
                UIImageView *fotoUsuario = (UIImageView*)[celda viewWithTag:1];
                
                [fotoUsuario.layer setBorderWidth:0];
                [fotoUsuario.layer setCornerRadius:22];
                [fotoUsuario.layer setMasksToBounds:YES];
                
                [fotoUsuario sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://crowte.com/usuarios/%@/%@",[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"uID"],[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"]];
                
                
                UILabel *nombreylugar = (UILabel*)[celda viewWithTag:2];
                
                if([[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"apellido"] isEqualToString:@"(null)"]){
                    
                    [[checkinsTodosUltimos objectAtIndex:indexPath.row] setValue:@"" forKey:@"apellido"];
                }
                
                [nombreylugar setText:[NSString stringWithFormat:@"%@ %@ se encuentra en %@",[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"apellido"],[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"lugar"]]];
                
                unsigned long sizenombre = [[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"nombre"] length]+[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"apellido"] length]+1;
                
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:nombreylugar.attributedText];
                
                [attributedString addAttribute:NSForegroundColorAttributeName value:[tableView backgroundColor] range:NSMakeRange(0, sizenombre)];
                [nombreylugar setAttributedText:attributedString];
                
                [attributedString addAttribute:NSForegroundColorAttributeName value:[tableView backgroundColor] range:NSMakeRange(sizenombre+17, [[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"lugar"] length])];
                [nombreylugar setAttributedText:attributedString];
                
                
                UILabel *tiempoDeCheckin = (UILabel*)[celda viewWithTag:3];
                [tiempoDeCheckin setText:[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"fecha"]];
                
                
                NSArray *constraints = [[celda contentView] constraints];
                NSLayoutConstraint *constraintInferior;
                
                for (NSLayoutConstraint *constraint in constraints) {
                    
                    if([[constraint identifier] isEqualToString:@"constraintImagenBottom"]){
                        
                        constraintInferior = constraint;
                    }
                }
                
                
                UIImageView *fondoLugar = (UIImageView*)[celda viewWithTag:4];
                [fondoLugar setImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
                
                NSString *mensaje = [[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"acontesimiento"];
                
                NSData *data = [mensaje dataUsingEncoding:NSUTF8StringEncoding];
                
                mensaje = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                
                mensaje = [mensaje stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                float adicional = 0;
                
                if(constraintInferior != nil){
                    
                    if(![mensaje isEqualToString:@""]){
                        
                        if([mensaje length] > 44){
                            
                            adicional = ceil([mensaje length]/44);
                            adicional = adicional*55;
                        }else{
                            
                            adicional = 55;
                        }
                    }
                    
                    [constraintInferior setConstant:62+adicional];
                }
                
                if([celda viewWithTag:11]){
                    
                    [[celda viewWithTag:11] removeFromSuperview];
                }
                
                
                if(![mensaje isEqualToString:@""]){
                    
                    UITextView *texto = [[UITextView alloc] initWithFrame:CGRectMake(fondoLugar.frame.origin.x, fondoLugar.frame.origin.y+fondoLugar.frame.size.height, fondoLugar.frame.size.width-100, 100)];
                    
                    [texto setText:mensaje];
                    
                    [texto setFont:[UIFont fontWithName:@"Raleway-Regular" size:13]];
                    [texto setTextColor:[UIColor colorWithRed:48.0/255.0 green:52.0/255.0 blue:63.0/255.0 alpha:1]];
                    
                    [texto setBackgroundColor:[UIColor colorWithRed:246.0/255.0 green:244.0/255.0 blue:243.0/255.0 alpha:1]];
                    
                    [texto setTextContainerInset:UIEdgeInsetsMake(20, 20, 20, 20)];
                    
                    [texto setTag:11];
                    
                    [texto setUserInteractionEnabled:NO];
                    [texto setScrollEnabled:NO];
                    [texto setEditable:NO];
                    
                    [texto setContentMode:UIViewContentModeCenter];
                    
                    texto.translatesAutoresizingMaskIntoConstraints = NO;
                    
                    [celda addSubview:texto];
                    
                    [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:fondoLugar attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-302]];
                    
                    [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:[texto superview] attribute:NSLayoutAttributeTrailingMargin relatedBy:NSLayoutRelationEqual toItem:texto attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:1]];
                    
                    [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:fondoLugar attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
                    
                }else{
                    
                    UITextView *texto = (UITextView*)[celda viewWithTag:11];
                    
                    [texto removeFromSuperview];
                }
                
                UIImageView *fotoPrincipalLugar = (UIImageView*)[celda viewWithTag:5];
                [fotoPrincipalLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
                
                [fotoPrincipalLugar setImage:[UIImage imageNamed:@"cblancav3.png"]];
                
                [[fotoPrincipalLugar layer] setBorderWidth:1];
                [[fotoPrincipalLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
                [[fotoPrincipalLugar layer] setCornerRadius:50];
                [[fotoPrincipalLugar layer] setMasksToBounds:YES];
                
                [fotoPrincipalLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"lugarID"],[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"fotoLugar"]]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
                    
                    if(error.code == 0){
                        
                        [fotoPrincipalLugar.layer setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                        [fotoPrincipalLugar setContentMode:UIViewContentModeScaleToFill];
                    }else{
                        
                        [fotoPrincipalLugar.layer setBorderColor:[[UIColor whiteColor] CGColor]];
                        [fotoPrincipalLugar setContentMode:UIViewContentModeCenter];
                    }
                }];
                
                UIImageView *fotoSecundariaLugar = (UIImageView*)[celda viewWithTag:4];
                
                [fotoSecundariaLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"lugarID"],[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
                
                UIScrollView *contenedorOpciones = (UIScrollView*)[celda viewWithTag:6];
                
                [contenedorOpciones.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
                
                [contenedorOpciones setHidden:NO];
                
                UIButton *opciones = (UIButton*)[celda viewWithTag:10];
                
                [opciones addTarget:self action:@selector(opcionesCheckin:) forControlEvents:UIControlEventTouchUpInside];
        
                
                if(![celda viewWithTag:109]){
                    
                    int i=0;
                    
                    for(id key in [checkinsTodosUltimos objectAtIndex:indexPath.row]){
                        
                        if(![contenedorOpciones viewWithTag:110+i]){
                            LOTAnimationView *animacion;
                            
                            if([key  isEqual: @"meGusta"]){

                                if([[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:key] isEqual:[NSNumber numberWithInt:1]]){
                                    
                                    animacion = [LOTAnimationView animationNamed:@"me_gusta"];
                                    
                                    [[animacion layer] setBorderColor:[[UIColor colorWithRed:39.0/255.0 green:140.0/255.0 blue:233.0/255.0 alpha:1] CGColor]];
                                    
                                    [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                                    
                                    
                                    [animacion setTag:110+i];
                                    
                                    [contenedorOpciones addSubview:animacion];
                                    
                                    [animacion setAnimationProgress:0.2];
                                    
                                    [[animacion layer] setBorderWidth:1];
                                    [[animacion layer] setCornerRadius:15];
                                    
                                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionMeGusta:)];
                                    
                                    [animacion addGestureRecognizer:tapGesture];
                                    
                                    i++;
                                    
                                }else if([[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:key] isEqual:[NSNumber numberWithInt:0]]){
                                    
                                    animacion = [LOTAnimationView animationNamed:@"no_me_gusta"];
                                    
                                    [[animacion layer] setBorderColor:[[UIColor colorWithRed:39.0/255.0 green:140.0/255.0 blue:233.0/255.0 alpha:1] CGColor]];
                                    
                                    [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                                    
                                    
                                    [animacion setTag:110+i];
                                    
                                    [contenedorOpciones addSubview:animacion];
                                    
                                    [animacion setAnimationProgress:0.2];
                                    
                                    [[animacion layer] setBorderWidth:1];
                                    [[animacion layer] setCornerRadius:15];
                                    
                                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionMeGustaNegativo:)];
                                    
                                    [animacion addGestureRecognizer:tapGesture];
                                    
                                    i++;
                                }

                                [animacion setAnimationProgress:0.9];
                                
                            }else if([key isEqual:@"estaLleno"]){

                                if([[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:key] isEqual:[NSNumber numberWithInt:1]]){
                                    animacion = [LOTAnimationView animationNamed:@"aun_lugares"];
                                    
                                    [[animacion layer] setBorderColor:[[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] CGColor]];
                                    
                                    [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                                    
                                    
                                    [animacion setTag:110+i];
                                    
                                    [contenedorOpciones addSubview:animacion];
                                    
                                    [animacion setAnimationProgress:0.2];
                                    
                                    [[animacion layer] setBorderWidth:1];
                                    [[animacion layer] setCornerRadius:15];
                                    
                                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionEstaLleno:)];
                                    
                                    [animacion addGestureRecognizer:tapGesture];
                                    
                                    i++;
                                }else if([[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:key] isEqual:[NSNumber numberWithInt:0]]){
                                    
                                    animacion = [LOTAnimationView animationNamed:@"esta_lleno"];
                                    
                                    [[animacion layer] setBorderColor:[[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] CGColor]];
                                    
                                    [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                                    
                                    
                                    [animacion setTag:110+i];
                                    
                                    [contenedorOpciones addSubview:animacion];
                                    
                                    [animacion setAnimationProgress:0.2];
                                    
                                    [[animacion layer] setBorderWidth:1];
                                    [[animacion layer] setCornerRadius:15];
                                    
                                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionEstaLlenoNegativo:)];
                                    
                                    [animacion addGestureRecognizer:tapGesture];
                                    
                                    i++;
                                }
                                
                                [animacion setAnimationProgress:0.9];
                                
                            }else if([key isEqual:@"meGustaMusica"]){

                                if([[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:key] isEqual:[NSNumber numberWithInt:1]]){
                                    animacion = [LOTAnimationView animationNamed:@"check_nota"];
                                    
                                    [[animacion layer] setBorderColor:[[UIColor purpleColor] CGColor]];
                                    
                                    [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                                    
                                    
                                    [animacion setTag:110+i];
                                    
                                    [contenedorOpciones addSubview:animacion];
                                    
                                    [animacion setAnimationProgress:0.2];
                                    
                                    [[animacion layer] setBorderWidth:1];
                                    [[animacion layer] setCornerRadius:15];
                                    
                                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionMeGustaMusica:)];
                                    
                                    [animacion addGestureRecognizer:tapGesture];
                                    
                                    i++;
                                    
                                }else if([[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:key] isEqual:[NSNumber numberWithInt:0]]){
                                    
                                    animacion = [LOTAnimationView animationNamed:@"check_nota_nogusta"];
                                    
                                    [[animacion layer] setBorderColor:[[UIColor grayColor] CGColor]];
                                    
                                    [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                                    
                                    
                                    [animacion setTag:110+i];
                                    
                                    [contenedorOpciones addSubview:animacion];
                                    
                                    [animacion setAnimationProgress:0.2];
                                    
                                    [[animacion layer] setBorderWidth:1];
                                    [[animacion layer] setCornerRadius:15];
                                    
                                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionMeGustaMusicaNegativo:)];
                                    
                                    [animacion addGestureRecognizer:tapGesture];
                                    
                                    i++;
                                }
                                
                                [animacion setAnimationProgress:0.9];
                            }else if([key isEqual:@"servicioRapido"]){

                                if([[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:key] isEqual:[NSNumber numberWithInt:1]]){
                                    
                                    animacion = [LOTAnimationView animationNamed:@"servicio_rapido"];
                                    
                                    [[animacion layer] setBorderColor:[[UIColor blueColor] CGColor]];
                                    
                                    [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                                    
                                    
                                    [animacion setTag:110+i];
                                    
                                    [contenedorOpciones addSubview:animacion];
                                    
                                    [animacion setAnimationProgress:0.2];
                                    
                                    [[animacion layer] setBorderWidth:1];
                                    [[animacion layer] setCornerRadius:15];
                                    
                                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionServicioRapido:)];
                                    
                                    [animacion addGestureRecognizer:tapGesture];
                                    
                                    i++;
                                }else if([[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:key] isEqual:[NSNumber numberWithInt:0]]){
                                    
                                    animacion = [LOTAnimationView animationNamed:@"servicio_lento"];
                                    
                                    [[animacion layer] setBorderColor:[[UIColor grayColor] CGColor]];
                                    
                                    [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                                    
                                    
                                    [animacion setTag:110+i];
                                    
                                    [contenedorOpciones addSubview:animacion];
                                    
                                    [animacion setAnimationProgress:0.2];
                                    
                                    [[animacion layer] setBorderWidth:1];
                                    [[animacion layer] setCornerRadius:15];
                                    
                                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionServicioRapidoNegativo:)];
                                    
                                    [animacion addGestureRecognizer:tapGesture];
                                    
                                    i++;
                                }
                                
                                [animacion setAnimationProgress:0.9];
                            }else if([key isEqual:@"servicioBueno"]){

                                if([[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:key] isEqual:[NSNumber numberWithInt:1]]){
                                    
                                    animacion = [LOTAnimationView animationNamed:@"buen_servicio"];
                                    
                                    [[animacion layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:0 alpha:1] CGColor]];
                                    
                                    [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                                    
                                    [animacion setTag:110+i];
                                    
                                    [contenedorOpciones addSubview:animacion];
                                    
                                    [animacion setAnimationProgress:0.2];
                                    
                                    [[animacion layer] setBorderWidth:1];
                                    [[animacion layer] setCornerRadius:15];
                                    
                                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionServicioBueno:)];
                                    
                                    [animacion addGestureRecognizer:tapGesture];
                                    i++;
                                }else if([[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:key] isEqual:[NSNumber numberWithInt:0]]){
                                    animacion = [LOTAnimationView animationNamed:@"mal_servicio"];
                                    
                                    [[animacion layer] setBorderColor:[[UIColor redColor] CGColor]];
                                    
                                    [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                                    
                                    [animacion setTag:110+i];
                                    
                                    [contenedorOpciones addSubview:animacion];
                                    
                                    [animacion setAnimationProgress:0.2];
                                    
                                    [[animacion layer] setBorderWidth:1];
                                    [[animacion layer] setCornerRadius:15];
                                    
                                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionServicioBuenoNegativo:)];
                                    
                                    [animacion addGestureRecognizer:tapGesture];
                                    i++;
                                }
                                
                                [animacion setAnimationProgress:0.9];
                            }
                        }
                    }
                    
                    [contenedorOpciones setContentSize:CGSizeMake((30*i)+(20*i)+30, contenedorOpciones.frame.size.height)];

                }
            }
        }else{
            
            
            UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldanuevo.png"]];
            
            [celda setBackgroundView:fondo];
            [celda setBackgroundColor:[UIColor clearColor]];
            
            UIImageView *fotoUsuario = (UIImageView*)[celda viewWithTag:1];
            
            [fotoUsuario.layer setBorderWidth:0];
            [fotoUsuario.layer setCornerRadius:22];
            [fotoUsuario.layer setMasksToBounds:YES];
            
            [fotoUsuario sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://crowte.com/usuarios/%@/%@",[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"uID"],[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"]];
            
            
            UILabel *nombreylugar = (UILabel*)[celda viewWithTag:2];
            
            if([[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"apellido"] isEqualToString:@"(null)"]){
                
                [[checkinsUltimosAmigos objectAtIndex:indexPath.row] setValue:@"" forKey:@"apellido"];
            }
            
            [nombreylugar setText:[NSString stringWithFormat:@"%@ %@ se encuentra en %@",[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"apellido"],[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugar"]]];
            
            unsigned long sizenombre = [[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"nombre"] length]+[[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"apellido"] length]+1;
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:nombreylugar.attributedText];
            
            [attributedString addAttribute:NSForegroundColorAttributeName value:[tableView backgroundColor] range:NSMakeRange(0, sizenombre)];
            [nombreylugar setAttributedText:attributedString];
            
            [attributedString addAttribute:NSForegroundColorAttributeName value:[tableView backgroundColor] range:NSMakeRange(sizenombre+17, [[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugar"] length])];
            [nombreylugar setAttributedText:attributedString];
            
            
            UILabel *tiempoDeCheckin = (UILabel*)[celda viewWithTag:3];
            [tiempoDeCheckin setText:[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"fecha"]];
            
            
            NSArray *constraints = [[celda contentView] constraints];
            NSLayoutConstraint *constraintInferior;
            
            for (NSLayoutConstraint *constraint in constraints) {
                
                if([[constraint identifier] isEqualToString:@"constraintImagenBottom"]){
                    
                    constraintInferior = constraint;
                }
            }
            
            
            UIImageView *fondoLugar = (UIImageView*)[celda viewWithTag:4];
            [fondoLugar setImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
            
            NSString *mensaje = [[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"acontesimiento"];
            
            NSData *data = [mensaje dataUsingEncoding:NSUTF8StringEncoding];
            
            mensaje = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
            
            mensaje = [mensaje stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            float adicional = 0;
            
            if(constraintInferior != nil){
                
                if(![mensaje isEqualToString:@""]){
                    
                    if([mensaje length] > 44){
                        
                        adicional = ceil([mensaje length]/44);
                        adicional = adicional*55;
                    }else{
                        
                        adicional = 55;
                    }
                }
                
                [constraintInferior setConstant:62+adicional];
            }
            
            if([celda viewWithTag:11]){
                
                [[celda viewWithTag:11] removeFromSuperview];
            }
            
            if(![mensaje isEqualToString:@""]){
                
                UITextView *texto = [[UITextView alloc] initWithFrame:CGRectMake(fondoLugar.frame.origin.x, fondoLugar.frame.origin.y+fondoLugar.frame.size.height, fondoLugar.frame.size.width-100, 100)];
                
                [texto setText:mensaje];
                
                [texto setFont:[UIFont fontWithName:@"Raleway-Regular" size:13]];
                [texto setTextColor:[UIColor colorWithRed:48.0/255.0 green:52.0/255.0 blue:63.0/255.0 alpha:1]];
                
                [texto setBackgroundColor:[UIColor colorWithRed:246.0/255.0 green:244.0/255.0 blue:243.0/255.0 alpha:1]];
                
                [texto setTextContainerInset:UIEdgeInsetsMake(20, 20, 20, 20)];
                
                [texto setTag:11];
                
                [texto setUserInteractionEnabled:NO];
                [texto setScrollEnabled:NO];
                [texto setEditable:NO];
                
                [texto setContentMode:UIViewContentModeCenter];
                
                texto.translatesAutoresizingMaskIntoConstraints = NO;
                
                [celda addSubview:texto];
                
                [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:fondoLugar attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-302]];
                
                [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:[texto superview] attribute:NSLayoutAttributeTrailingMargin relatedBy:NSLayoutRelationEqual toItem:texto attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:1]];
                
                [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:fondoLugar attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
                
            }else{
                
                UITextView *texto = (UITextView*)[celda viewWithTag:11];
                
                [texto removeFromSuperview];
            }
            
            UIImageView *fotoPrincipalLugar = (UIImageView*)[celda viewWithTag:5];
            [fotoPrincipalLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
            
            [fotoPrincipalLugar setImage:[UIImage imageNamed:@"cblancav3.png"]];
            
            [[fotoPrincipalLugar layer] setBorderWidth:1];
            [[fotoPrincipalLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
            [[fotoPrincipalLugar layer] setCornerRadius:50];
            [[fotoPrincipalLugar layer] setMasksToBounds:YES];
            
            [fotoPrincipalLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugarID"],[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"fotoLugar"]]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
                
                if(error.code == 0){
                    
                    [fotoPrincipalLugar.layer setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                    [fotoPrincipalLugar setContentMode:UIViewContentModeScaleToFill];
                }else{
                    
                    [fotoPrincipalLugar.layer setBorderColor:[[UIColor whiteColor] CGColor]];
                    [fotoPrincipalLugar setContentMode:UIViewContentModeCenter];
                }
            }];
            
            UIImageView *fotoSecundariaLugar = (UIImageView*)[celda viewWithTag:4];
            
            [fotoSecundariaLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugarID"],[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
            
        }
        
    }else if(indexPath.section == 1){
        
        UILabel *textoEnEsteMomento = (UILabel*)[celda viewWithTag:7];
        
        [textoEnEsteMomento setHidden:YES];
        if(segmentedControl.selectedSegmentIndex == 0){
            
            UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldanuevo.png"]];
            
            [celda setBackgroundView:fondo];
            [celda setBackgroundColor:[UIColor clearColor]];
            
            UIImageView *fotoUsuario = (UIImageView*)[celda viewWithTag:1];
            
            [fotoUsuario.layer setBorderWidth:0];
            [fotoUsuario.layer setCornerRadius:22];
            [fotoUsuario.layer setMasksToBounds:YES];
            
            [fotoUsuario sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://crowte.com/usuarios/%@/%@",[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"uID"],[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"]];
            
            
            UILabel *nombreylugar = (UILabel*)[celda viewWithTag:2];
            
            if([[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"apellido"] isEqualToString:@"(null)"]){
                
                [[checkinsTodosOtros objectAtIndex:indexPath.row] setValue:@"" forKey:@"apellido"];
            }
            
            [nombreylugar setText:[NSString stringWithFormat:@"%@ %@ estuvo en %@",[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"apellido"],[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"lugar"]]];
            
            unsigned long sizenombre = [[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"nombre"] length]+[[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"apellido"] length]+1;
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:nombreylugar.attributedText];
            
            [attributedString addAttribute:NSForegroundColorAttributeName value:[tableView backgroundColor] range:NSMakeRange(0, sizenombre)];
            [nombreylugar setAttributedText:attributedString];
            
            [attributedString addAttribute:NSForegroundColorAttributeName value:[tableView backgroundColor] range:NSMakeRange(sizenombre+11, [[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"lugar"] length])];
            [nombreylugar setAttributedText:attributedString];
            
            
            UILabel *tiempoDeCheckin = (UILabel*)[celda viewWithTag:3];
            [tiempoDeCheckin setText:[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"fecha"]];
            
            
            NSArray *constraints = [[celda contentView] constraints];
            NSLayoutConstraint *constraintInferior;
            
            for (NSLayoutConstraint *constraint in constraints) {
                
                if([[constraint identifier] isEqualToString:@"constraintImagenBottom"]){
                    
                    constraintInferior = constraint;
                }
            }
            
            
            UIImageView *fondoLugar = (UIImageView*)[celda viewWithTag:4];
            [fondoLugar setImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
            
            NSString *mensaje = [[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"acontesimiento"];
            
            NSData *data = [mensaje dataUsingEncoding:NSUTF8StringEncoding];
            
            mensaje = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
            
            mensaje = [mensaje stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            float adicional = 0;
            
            if(constraintInferior != nil){
                
                if(![mensaje isEqualToString:@""]){
                    
                    if([mensaje length] > 44){
                        
                        adicional = ceil([mensaje length]/44);
                        adicional = adicional*55;
                    }else{
                        
                        adicional = 55;
                    }
                }
                
                [constraintInferior setConstant:62+adicional];
            }
            
            if([celda viewWithTag:11]){
                
                [[celda viewWithTag:11] removeFromSuperview];
            }
            
            if(![mensaje isEqualToString:@""]){
                
                UITextView *texto = [[UITextView alloc] initWithFrame:CGRectMake(fondoLugar.frame.origin.x, fondoLugar.frame.origin.y+fondoLugar.frame.size.height, fondoLugar.frame.size.width-100, 100)];
                
                [texto setText:mensaje];
                
                [texto setFont:[UIFont fontWithName:@"Raleway-Regular" size:13]];
                [texto setTextColor:[UIColor colorWithRed:48.0/255.0 green:52.0/255.0 blue:63.0/255.0 alpha:1]];
                
                [texto setBackgroundColor:[UIColor colorWithRed:246.0/255.0 green:244.0/255.0 blue:243.0/255.0 alpha:1]];
                
                [texto setTextContainerInset:UIEdgeInsetsMake(20, 20, 20, 20)];
                
                [texto setTag:11];
                
                [texto setUserInteractionEnabled:NO];
                [texto setScrollEnabled:NO];
                [texto setEditable:NO];
                
                [texto setContentMode:UIViewContentModeCenter];
                
                texto.translatesAutoresizingMaskIntoConstraints = NO;
                
                [celda addSubview:texto];
                
                [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:fondoLugar attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-302]];
                
                [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:[texto superview] attribute:NSLayoutAttributeTrailingMargin relatedBy:NSLayoutRelationEqual toItem:texto attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:1]];
                
                [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:fondoLugar attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
                
            }else{
                
                UITextView *texto = (UITextView*)[celda viewWithTag:11];
                
                [texto removeFromSuperview];
            }
            
            UIImageView *fotoPrincipalLugar = (UIImageView*)[celda viewWithTag:5];
            [fotoPrincipalLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
            
            [[fotoPrincipalLugar layer] setBorderWidth:1];
            [[fotoPrincipalLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
            [[fotoPrincipalLugar layer] setCornerRadius:50];
            [[fotoPrincipalLugar layer] setMasksToBounds:YES];
            
            [fotoPrincipalLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"lugarID"],[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"fotoLugar"]]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
                
                if(error.code == 0){
                    
                    [fotoPrincipalLugar.layer setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                    [fotoPrincipalLugar setContentMode:UIViewContentModeScaleToFill];
                }else{
                    
                    [fotoPrincipalLugar.layer setBorderColor:[[UIColor whiteColor] CGColor]];
                    [fotoPrincipalLugar setContentMode:UIViewContentModeCenter];
                }
            }];
            
            UIImageView *fotoSecundariaLugar = (UIImageView*)[celda viewWithTag:4];
            
            [fotoSecundariaLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"lugarID"],[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
            
            
            UILabel *enEsteMomentoTexto = (UILabel*)[celda viewWithTag:7];
            
            [enEsteMomentoTexto setHidden:YES];
            
            UIScrollView *contenedorOpciones = (UIScrollView*)[celda viewWithTag:6];
            
            [contenedorOpciones setHidden:YES];
            
        }else if(segmentedControl.selectedSegmentIndex == 1){
            
            UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldanuevo.png"]];
            
            [celda setBackgroundView:fondo];
            [celda setBackgroundColor:[UIColor clearColor]];
            
            UIImageView *fotoUsuario = (UIImageView*)[celda viewWithTag:1];
            
            [fotoUsuario.layer setBorderWidth:0];
            [fotoUsuario.layer setCornerRadius:22];
            [fotoUsuario.layer setMasksToBounds:YES];
            
            [fotoUsuario sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://crowte.com/usuarios/%@/%@",[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"uID"],[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"]];
            
            
            UILabel *nombreylugar = (UILabel*)[celda viewWithTag:2];
            
            if([[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"apellido"] isEqualToString:@"(null)"]){
                
                [[checkinsTodosAmigos objectAtIndex:indexPath.row] setValue:@"" forKey:@"apellido"];
            }
            
            [nombreylugar setText:[NSString stringWithFormat:@"%@ %@ estuvo en %@",[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"apellido"],[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugar"]]];
            
            unsigned long sizenombre = [[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"nombre"] length]+[[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"apellido"] length]+1;
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:nombreylugar.attributedText];
            
            [attributedString addAttribute:NSForegroundColorAttributeName value:[tableView backgroundColor] range:NSMakeRange(0, sizenombre)];
            [nombreylugar setAttributedText:attributedString];
            
            [attributedString addAttribute:NSForegroundColorAttributeName value:[tableView backgroundColor] range:NSMakeRange(sizenombre+11, [[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugar"] length])];
            [nombreylugar setAttributedText:attributedString];
            
            
            UILabel *tiempoDeCheckin = (UILabel*)[celda viewWithTag:3];
            [tiempoDeCheckin setText:[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"fecha"]];
            
            UIImageView *fondoLugar = (UIImageView*)[celda viewWithTag:4];
            [fondoLugar setImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
            
            UIImageView *fotoPrincipalLugar = (UIImageView*)[celda viewWithTag:5];
            [fotoPrincipalLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
            
            [[fotoPrincipalLugar layer] setBorderWidth:1];
            [[fotoPrincipalLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
            [[fotoPrincipalLugar layer] setCornerRadius:50];
            [[fotoPrincipalLugar layer] setMasksToBounds:YES];
            
            [fotoPrincipalLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugarID"],[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"fotoLugar"]]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
                
                if(error.code == 0){
                    
                    [fotoPrincipalLugar.layer setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                    [fotoPrincipalLugar setContentMode:UIViewContentModeScaleToFill];
                }else{
                    
                    [fotoPrincipalLugar.layer setBorderColor:[[UIColor whiteColor] CGColor]];
                    [fotoPrincipalLugar setContentMode:UIViewContentModeCenter];
                }
            }];
            
            UIImageView *fotoSecundariaLugar = (UIImageView*)[celda viewWithTag:4];
            
            [fotoSecundariaLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugarID"],[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
            
        }
    }
    
    return celda;
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 1){
        
        if(segmentedControl.selectedSegmentIndex == 0){
            
            if([cell viewWithTag:109]){
                
                LOTAnimationView *animacion1 = (LOTAnimationView*)[cell viewWithTag:109];
                 [animacion1 setAnimationProgress:0.2];
                [animacion1 play];
                
                LOTAnimationView *animacion2 = (LOTAnimationView*)[cell viewWithTag:110];
                 [animacion2 setAnimationProgress:0.2];
                [animacion2 play];
                
                LOTAnimationView *animacion3 = (LOTAnimationView*)[cell viewWithTag:111];
                 [animacion3 setAnimationProgress:0.2];
                [animacion3 play];
                
            }
        }
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    UIStoryboard *storybard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    PerfilLugarViewController *lugar = (PerfilLugarViewController*)[storybard instantiateViewControllerWithIdentifier:@"perfilLugar"];

    
    if(indexPath.section == 0){
        
        if(segmentedControl.selectedSegmentIndex == 0){
            
            [lugar setIdLugar:[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"lugarID"] intValue]];
            
            [lugar setNombreLugar:[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
            
            [lugar setFotoPrincipalLugar:[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"fotoLugar"]];
            
            [lugar setFotoSecundariaLugar:[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]];
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"latitudLugar"] floatValue] longitude:[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"longitudLugar"] floatValue]];
            
            [lugar setLocation:location];
            
            [lugar setOpcionesAmbienteLugar:[@{@"meGusta":[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"meGusta"],@"estaLleno":[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"estaLleno"],@"meGustaMusica":[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"meGustaMusica"],@"servicioRapido":[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"servicioRapido"],@"servicioBueno":[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"servicioBueno"]} mutableCopy]];
            
            
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            
            UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
            self.navigationItem.backBarButtonItem = atras;
            
            [self.navigationController pushViewController:lugar animated:YES];
            
        }else{
            
            [lugar setIdLugar:[[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugarID"] intValue]];
            
            [lugar setNombreLugar:[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
            
            [lugar setFotoPrincipalLugar:[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"fotoLugar"]];
            
            [lugar setFotoSecundariaLugar:[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]];
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"latitudLugar"] floatValue] longitude:[[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"longitudLugar"] floatValue]];
            
            [lugar setLocation:location];
            
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            
            UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
            self.navigationItem.backBarButtonItem = atras;
            
            [self.navigationController pushViewController:lugar animated:YES];
        }
    }else{
    
        if(segmentedControl.selectedSegmentIndex == 0){
        
            
            [lugar setIdLugar:[[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"lugarID"] intValue]];
            
            [lugar setNombreLugar:[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
            
            [lugar setFotoPrincipalLugar:[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"fotoLugar"]];
            
            [lugar setFotoSecundariaLugar:[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]];
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"latitudLugar"] floatValue] longitude:[[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"longitudLugar"] floatValue]];
            
            [lugar setLocation:location];
            
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            
            UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
            self.navigationItem.backBarButtonItem = atras;
            
            [self.navigationController pushViewController:lugar animated:YES];
        }else{
            
            [lugar setIdLugar:[[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugarID"] intValue]];
            
            [lugar setNombreLugar:[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
            
            [lugar setFotoPrincipalLugar:[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"fotoLugar"]];
            
            [lugar setFotoSecundariaLugar:[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]];
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"latitudLugar"] floatValue] longitude:[[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"longitudLugar"] floatValue]];
            
            [lugar setLocation:location];
            
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            
            UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
            self.navigationItem.backBarButtonItem = atras;
            
            [self.navigationController pushViewController:lugar animated:YES];
        }
    }
}

#pragma mark - lista metodos

-(IBAction)join:(id)sender{
    
    UIButton *botonJoin = (UIButton*)sender;
    
    CGPoint posicionBoton = [botonJoin convertPoint:CGPointZero toView:listaCheckin];
    
    NSIndexPath *indexPath = [listaCheckin indexPathForRowAtPoint:posicionBoton];
    
    if([botonJoin isSelected]){
    
        [botonJoin setSelected:NO];
    }else{
    
        [botonJoin setSelected:YES];
        
        NSString *idLugar;
        NSString *nombreLugar;
        NSString *stringFotoPrincipalLugar;
        NSString *stringFotoTraseraLugar;
        
        CLLocation *locacion;
        
        if(segmentedControl.selectedSegmentIndex == 0){
        
            if(indexPath.section == 0){
                
                idLugar = [[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"lugarID"];
                
                nombreLugar = [[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"lugar"];
                
                stringFotoPrincipalLugar = [[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"fotolugar"];
                
                stringFotoTraseraLugar = [[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"fotolugar"];
                
                locacion = [[CLLocation alloc] initWithLatitude:[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"latitudLugar"] doubleValue] longitude:[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"longitudLugar"] doubleValue]];
                
            }else{
                
                idLugar = [[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"lugarID"];
                
                nombreLugar = [[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"lugar"];
                
                stringFotoPrincipalLugar = [[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"fotolugar"];
                
                stringFotoTraseraLugar = [[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"fotolugar"];
                
                locacion = [[CLLocation alloc] initWithLatitude:[[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"latitudLugar"] doubleValue] longitude:[[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"longitudLugar"] doubleValue]];
            }
        }else{
        
            if(indexPath.section == 0){
                
                idLugar = [[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugarID"];
                
                nombreLugar = [[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugar"];
                
                stringFotoPrincipalLugar = [[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"fotolugar"];
                
                stringFotoTraseraLugar = [[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"fotolugar"];
                
                locacion = [[CLLocation alloc] initWithLatitude:[[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"latitudLugar"] doubleValue] longitude:[[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"longitudLugar"] doubleValue]];
            }else{
                                
                idLugar = [[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugarID"];
                
                nombreLugar = [[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"lugar"];
                
                stringFotoPrincipalLugar = [[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"fotolugar"];
                
                stringFotoTraseraLugar = [[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"fotolugar"];
                
                locacion = [[CLLocation alloc] initWithLatitude:[[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"latitudLugar"] doubleValue] longitude:[[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"longitudLugar"] doubleValue]];
            }
        }
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/publicaracontesimiento.php"]];
        
        [request setHTTPMethod:@"POST"];
        
        NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@&word=%@&lugar=%@",[defaults objectForKey:@"identifier"],@"",idLugar] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
        
        NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
            
            if(error == nil){
                
                NSDictionary *datosRegreso = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            
                NavCheckinViewController *navCheckinVC = (NavCheckinViewController*)[[self.tabBarController viewControllers] objectAtIndex:2];
                
                CheckinViewController *checkinVC = (CheckinViewController*)[navCheckinVC.viewControllers objectAtIndex:0];
                
                [checkinVC setFotoPrincipal:stringFotoPrincipalLugar fotoSecundaria:stringFotoTraseraLugar nombreLugar:nombreLugar latitud:[locacion coordinate].latitude longitud:[locacion coordinate].longitude id:[idLugar intValue]];
                
                
                [checkinVC setDatosEnLugar:[@{@"lugar":@{@"ID": [NSNumber numberWithInt:[[datosRegreso objectForKey:@"ID"] intValue]], @"fecha":[NSNumber numberWithFloat:[[NSDate date] timeIntervalSince1970]], @"foto":[datosRegreso objectForKey:@"foto"],@"latitud":[NSNumber numberWithFloat:[locacion coordinate].latitude],@"longitud":[NSNumber numberWithFloat:[locacion coordinate].longitude],@"lugar":nombreLugar,@"objID":[NSNumber numberWithInt:[idLugar intValue]],@"postOrigen":[NSNumber numberWithInt:[[datosRegreso objectForKey:@"postOrigen"] intValue]]}, @"voto":[datosRegreso objectForKey:@"votos"], @"tiempoRestante":[NSNumber numberWithInt:0]} mutableCopy]];
                
                [checkinVC setCheckinDone];
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        }];
        
        [uploadTask resume];
        
    }
    
}

- (IBAction)cambiarListaTodosAmigos:(id)sender {
    
    [listaCheckin reloadData];
    
    if(segmentedControl.selectedSegmentIndex == 0){
        
        if([[[checkinsTodosUltimos objectAtIndex:0] objectForKey:@"valido"] intValue] == 0 && [[[checkinsTodosOtros objectAtIndex:0] objectForKey:@"valido"] intValue] == 0){
        
            [listaCheckin setHidden:YES];
            if(!seMuestraMapa){
            [vistaEmptyState setHidden:NO];
            }
        }else{
        
            if(!seMuestraMapa){
                [listaCheckin setHidden:NO];
            }
            [vistaEmptyState setHidden:YES];
        }
        
        [self agregarPines:checkinsTodosUltimos viejos:checkinsTodosOtros];
        
    }else{

        if([[[checkinsTodosAmigos objectAtIndex:0] objectForKey:@"valido"] intValue] == 0 && [[[checkinsUltimosAmigos objectAtIndex:0] objectForKey:@"valido"] intValue] == 0){
            
            [listaCheckin setHidden:YES];
            if(!seMuestraMapa){
                [vistaEmptyState setHidden:NO];
            }
        }else{
            
            if(!seMuestraMapa){
                [listaCheckin setHidden:NO];
            }
            [vistaEmptyState setHidden:YES];
        }
        
        [self agregarPines:checkinsUltimosAmigos viejos:checkinsTodosAmigos];
    }
    
}

#pragma mark - mapa

-(void)mostrarMapa:(id)sender{

    if(!seMuestraMapa){
        
        if(segmentedControl.selectedSegmentIndex == 0){
        
            [self agregarPines:checkinsTodosUltimos viejos:checkinsTodosOtros];
        }else{
        
            [self agregarPines:checkinsUltimosAmigos viejos:checkinsTodosAmigos];
        }
        
        [listaCheckin setHidden:YES];
        [fondoMapa setHidden:NO];
        [vistaEmptyState setHidden:YES];
        
        UIImage *imageLista = [UIImage imageNamed:@"lista.png"];
        
        UIButton *lista = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imageLista.size.width, imageLista.size.height)];
        
        [lista setImage:imageLista forState:UIControlStateNormal];
        
        [lista addTarget:self action:@selector(mostrarMapa:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barbtnlista = [[UIBarButtonItem alloc] initWithCustomView:lista];
        
        self.navigationItem.leftBarButtonItem = barbtnlista;

        
    }else{
    
        [listaCheckin setHidden:NO];
        [fondoMapa setHidden:YES];
        
        if(segmentedControl.selectedSegmentIndex == 0){
            if([[[checkinsTodosUltimos objectAtIndex:0] objectForKey:@"valido"] intValue] == 0 && [[[checkinsTodosOtros objectAtIndex:0] objectForKey:@"valido"] intValue] == 0){
                
                [vistaEmptyState setHidden:NO];
            }else{
                
                [vistaEmptyState setHidden:YES];
            }
        }else{
            
            if([[[checkinsTodosAmigos objectAtIndex:0] objectForKey:@"valido"] intValue] == 0 && [[[checkinsUltimosAmigos objectAtIndex:0] objectForKey:@"valido"] intValue] == 0){
                
                [vistaEmptyState setHidden:NO];
            }else{
                
                [vistaEmptyState setHidden:YES];
            }
        }
        
        UIImage *imageMapview = [UIImage imageNamed:@"mapview.png"];
        
        UIButton *mapview = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imageMapview.size.width, imageMapview.size.height)];
        
        [mapview setImage:imageMapview forState:UIControlStateNormal];
        
        [mapview addTarget:self action:@selector(mostrarMapa:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barbtnmapview = [[UIBarButtonItem alloc] initWithCustomView:mapview];
        
        self.navigationItem.leftBarButtonItem = barbtnmapview;

    }
    
    seMuestraMapa = !seMuestraMapa;
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{

    if(numeroCambiosMapa <= 1){

        float spanX = 0.09905913041;
        float spanY = 0.09905913041;
        
        MKCoordinateRegion region;
        region.center.latitude = mapa.userLocation.location.coordinate.latitude;
        region.center.longitude = mapa.userLocation.location.coordinate.longitude;
        region.span.latitudeDelta = spanX;
        region.span.longitudeDelta = spanY;
        
        [mapa setRegion:region animated:YES];
    }
    
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    if(![view.annotation isKindOfClass:[MKUserLocation class]]){
        
        CustomPinAnnotation *customPin = (CustomPinAnnotation*)view;
        
        UIView *vista = [[UIView alloc] initWithFrame:CGRectMake(view.bounds.origin.x-(mapa.frame.size.width/2),view.bounds.origin.y-150, mapa.frame.size.width-20, 150)];
        [vista setTag:1];
        
        UIImageView *fotoPersona = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
        [fotoPersona sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://crowte.com/usuarios/%d/%@",[customPin idUsuario],[customPin fotoPersona]]] placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"]];
        
        [[fotoPersona layer] setBorderColor:[[UIColor whiteColor] CGColor]];
        [[fotoPersona layer] setBorderWidth:1];
        [[fotoPersona layer] setCornerRadius:15];
        [[fotoPersona layer] setMasksToBounds:YES];
        
        UIImageView *fondo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, vista.frame.size.width, vista.frame.size.height)];
        
        [fondo setImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
        
        UIImageView *fotoLugar = [[UIImageView alloc] initWithFrame:CGRectMake(vista.frame.size.width-(vista.frame.size.width/2)-25, vista.frame.size.height-(vista.frame.size.height/2)-25, 50, 50)];
        [fotoLugar setImage:[UIImage imageNamed:@"cblanca.png"]];
        [[fotoLugar layer] setBorderColor:[[UIColor colorWithWhite:1 alpha:0] CGColor]];
        [[fotoLugar layer] setBorderWidth:1];
        [[fotoLugar layer] setCornerRadius:25];
        [[fotoLugar layer] setMasksToBounds:YES];
        
        UILabel *lugar = [[UILabel alloc] initWithFrame:CGRectMake(0, [vista frame].size.height-23, vista.frame.size.width, 23)];
        [lugar setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.2]];
        [lugar setTextColor:[UIColor whiteColor]];
        [lugar setTextAlignment:NSTextAlignmentCenter];
        [lugar setText:[customPin lugar]];
        
        [lugar setFont:[UIFont fontWithName:@"Hind-Light" size:17]];
        
        UILabel *nombre = [[UILabel alloc] initWithFrame:CGRectMake(fotoPersona.frame.origin.x+33, fotoPersona.frame.origin.y+5, vista.frame.size.width-(fotoPersona.frame.origin.x+33), 20)];
        
        [nombre setText:[customPin persona]];
        [nombre setTextColor:[UIColor whiteColor]];
        [nombre setFont:[UIFont fontWithName:@"Hind-Light" size:12]];
        
        [vista addSubview:fondo];
        [vista addSubview:fotoLugar];
        [vista addSubview:lugar];
        [vista addSubview:fotoPersona];
        [vista addSubview:nombre];
        
        [view addSubview:vista];
    }
    
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{

    UIView *vista = [view viewWithTag:1];
    [vista removeFromSuperview];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{

    numeroCambiosMapa++;
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{

    if([annotation isKindOfClass:[MKUserLocation class]]){
    
        return nil;
    }
    
    int idUsuario = [(CustomPinAnnotation*)annotation idUsuario];
    
    BOOL enVivo = [(CustomPointAnnotation*)annotation enVivo];
    NSString *lugar = [(CustomPointAnnotation*)annotation lugar];
    NSString *nombre = [(CustomPinAnnotation*)annotation persona];
    NSString *fotoUsuario = [(CustomPinAnnotation*)annotation fotoPersona];
    
    CustomPinAnnotation *pin = [[CustomPinAnnotation alloc] initWithAnnotation:annotation type:enVivo idUsuario:idUsuario lugar:lugar persona:nombre fotoPersona:fotoUsuario fotoLugar:nil fondoLugar:nil];
    
    if(enVivo){
    
        [[pin superview] bringSubviewToFront:pin];
    }
    
    return pin;
}

-(void)agregarPines:(NSArray *)checkinesNuevos viejos:(NSArray *)checkinesViejos{

    NSMutableDictionary *repetidos = [[NSMutableDictionary alloc] init];
    NSMutableArray *listaCheckinsRepetidos = [NSMutableArray array];

    [mapa removeAnnotations:[mapa annotations]];

    
    if([[[checkinesViejos objectAtIndex:0] objectForKey:@"valido"] intValue] != 0){
        
        for(int i = 0; i<[checkinesViejos count]; i++){
            
            if([repetidos objectForKey:[[checkinesViejos objectAtIndex:i] objectForKey:@"lugarID"]] == NULL){
                
                NSMutableArray *arreglo = [NSMutableArray array];
                
                [arreglo addObject:[checkinesViejos objectAtIndex:i]];

                [repetidos setObject:arreglo forKey:[[checkinesViejos objectAtIndex:i] objectForKey:@"lugarID"]];
                
                [listaCheckinsRepetidos addObject:[[checkinesViejos objectAtIndex:i] objectForKey:@"lugarID"]];
            }else{
                
                NSMutableArray *arreglo = [repetidos objectForKey:[[checkinesViejos objectAtIndex:i] objectForKey:@"lugarID"]];
                
                [arreglo addObject:[checkinesViejos objectAtIndex:i]];

                [repetidos setObject:arreglo forKey:[[checkinesViejos objectAtIndex:i] objectForKey:@"lugarID"]];
            }
            
        }
        
        
        for(int i=0; i<[listaCheckinsRepetidos count]; i++){
            
            NSArray *arreglo = [repetidos objectForKey:[listaCheckinsRepetidos objectAtIndex:i]];
            
            for(int j=0; j<[arreglo count]; j++){
                
                CustomPointAnnotation *punto = [[CustomPointAnnotation alloc] init];
                
                
                double latitud = 0.00004502695651/j;
                double longitud = 0.00004502695651/j;
                
                if(latitud == INFINITY){
                    
                    latitud = 0;
                }
                
                if(longitud == INFINITY){
                    
                    longitud = 0;
                }
                
                CLLocationCoordinate2D result = CLLocationCoordinate2DMake([[[arreglo objectAtIndex:j] objectForKey:@"latitudLugar"] doubleValue]+latitud, [[[arreglo objectAtIndex:j] objectForKey:@"longitudLugar"] doubleValue]+longitud);
                
                punto.coordinate = CLLocationCoordinate2DMake(result.latitude, result.longitude);
                
                [punto setIdUsuario:[[[arreglo objectAtIndex:j] objectForKey:@"uID"] intValue]];
                [punto setPersona:[NSString stringWithFormat:@"%@ %@",[[arreglo objectAtIndex:j] objectForKey:@"nombre"],[[arreglo objectAtIndex:j] objectForKey:@"apellido"]]];
                [punto setFotoPersona:[[arreglo objectAtIndex:j] objectForKey:@"foto"]];
                [punto setLugar:[[arreglo objectAtIndex:j] objectForKey:@"lugar"]];
                [punto setEnVivo:NO];
                
                [mapa addAnnotation:punto];
                
            }
        }
    }
    
    
    [repetidos removeAllObjects];
    [listaCheckinsRepetidos removeAllObjects];
    
    
    if([[[checkinesNuevos objectAtIndex:0] objectForKey:@"valido"] intValue] != 0){
        
        for(int i = 0; i<[checkinesNuevos count]; i++){
            
            if([repetidos objectForKey:[[checkinesNuevos objectAtIndex:i] objectForKey:@"lugarID"]] == NULL){
                
                NSMutableArray *arreglo = [NSMutableArray array];
                
                [arreglo addObject:[checkinesNuevos objectAtIndex:i]];

                [repetidos setObject:arreglo forKey:[[checkinesNuevos objectAtIndex:i] objectForKey:@"lugarID"]];
                
                [listaCheckinsRepetidos addObject:[[checkinesNuevos objectAtIndex:i] objectForKey:@"lugarID"]];
            }else{
                
                NSMutableArray *arreglo = [repetidos objectForKey:[[checkinesNuevos objectAtIndex:i] objectForKey:@"lugarID"]];
                
                [arreglo addObject:[checkinesNuevos objectAtIndex:i]];

                [repetidos setObject:arreglo forKey:[[checkinesNuevos objectAtIndex:i] objectForKey:@"lugarID"]];
            }
            
        }
        
        
        for(int i=0; i<[listaCheckinsRepetidos count]; i++){
            
            NSArray *arreglo = [repetidos objectForKey:[listaCheckinsRepetidos objectAtIndex:i]];
            
            for(int j=0; j<[arreglo count]; j++){
                
                CustomPointAnnotation *punto = [[CustomPointAnnotation alloc] init];
                
                
                double latitud = 0.00004502695651/j;
                double longitud = 0.00004502695651/j;
                
                if(latitud == INFINITY){
                    
                    latitud = 0;
                }
                
                if(longitud == INFINITY){
                    
                    longitud = 0;
                }
                
                CLLocationCoordinate2D result = CLLocationCoordinate2DMake([[[arreglo objectAtIndex:j] objectForKey:@"latitudLugar"] doubleValue]+latitud, [[[arreglo objectAtIndex:j] objectForKey:@"longitudLugar"] doubleValue]+longitud);
                
                punto.coordinate = CLLocationCoordinate2DMake(result.latitude, result.longitude);
                
                [punto setIdUsuario:[[[arreglo objectAtIndex:j] objectForKey:@"uID"] intValue]];
                [punto setPersona:[NSString stringWithFormat:@"%@ %@",[[arreglo objectAtIndex:j] objectForKey:@"nombre"],[[arreglo objectAtIndex:j] objectForKey:@"apellido"]]];
                [punto setFotoPersona:[[arreglo objectAtIndex:j] objectForKey:@"foto"]];
                [punto setLugar:[[arreglo objectAtIndex:j] objectForKey:@"lugar"]];
                [punto setEnVivo:YES];
                
                [mapa addAnnotation:punto];
                
            }
        }
    }
    
}

#pragma mark - miselaneos

-(IBAction)buscar:(id)sender{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    BuscarViewController *buscador = [storyboard instantiateViewControllerWithIdentifier:@"buscador"];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.navigationController pushViewController:buscador animated:YES];
}

-(void)getNotificacionesNuevas{
    NSLog(@"get not nuevas");
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getnotificacionesnuevas.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"id=%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(data != nil){
        
            if(error == nil){
            
                NSDictionary *numNotif = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                UITabBar *tabBar = self.tabBarController.tabBar;
                
                UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];

                if(![[numNotif objectForKey:@"notificaciones"] isEqualToString:@"0"] || ![[numNotif objectForKey:@"solicitudes"] isEqualToString:@"0"]){
                    
                    [tabBarItem4 setImage:[[UIImage imageNamed:@"tabnotifnotnotif.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                }else{
                
                    [tabBarItem4 setImage:[[UIImage imageNamed:@"tabnotifnot.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                }
            }
        }
    }];
    
    [uploadTask resume];
}

-(IBAction)opcionesCheckin:(id)sender{
    
    UIButton *botonAceptar = (UIButton*)sender;
    
    CGPoint posicionBoton = [botonAceptar convertPoint:CGPointZero toView:listaCheckin];
    
    NSIndexPath *indexPath = [listaCheckin indexPathForRowAtPoint:posicionBoton];
    
    if(indexPath.section == 0){
        
        
        if(segmentedControl.selectedSegmentIndex == 0){
            
            if([[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"uID"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]]){
            
                UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction *reportarCheckin = [UIAlertAction actionWithTitle:@"Eliminar check-in" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
                    
                    UIAlertController *confirmacion = [UIAlertController alertControllerWithTitle:@"¿Estás seguro?" message:@"Si eliminas este check-in no podrás recuperarlo." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *aceptar = [UIAlertAction actionWithTitle:@"Aceptar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        
                        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/eliminarpublicacion.php"]];
                        
                        [request setHTTPMethod:@"POST"];
                        
                        NSData *cuerpo = [[NSString stringWithFormat:@"id=%@&u=%d",[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"aconID"],[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
                        
                        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                        
                        NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                        
                        NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                            
                            if(error == nil){
                                
                                if(data != nil){
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        [checkinsTodosUltimos removeObjectAtIndex:indexPath.row];
                                        [listaCheckin deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
                                    });
                                }
                            }
                            
                        }];
                        
                        [uploadTask resume];
                        
                    }];
                    
                    UIAlertAction *noAceptar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
                    
                    [confirmacion addAction:aceptar];
                    [confirmacion addAction:noAceptar];
                    
                    [self presentViewController:confirmacion animated:YES completion:nil];
                }];
                
                UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
                    
                    [opciones dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [opciones addAction:reportarCheckin];
                [opciones addAction:cancelar];
                
                [self presentViewController:opciones animated:YES completion:nil];
            }else{
            
                UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction *reportarCheckin = [UIAlertAction actionWithTitle:@"Denunciar check-in" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    ReportarCheckinTableViewController *enviarReporte = [storyboard instantiateViewControllerWithIdentifier:@"ReportarCheckin"];
                    
                    
                    [enviarReporte setIdUsuarioABloquear:[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"uID"]];
                    
                    [enviarReporte setIdRelObjDia:[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"relID"]];
                    
                    [self presentViewController:enviarReporte animated:YES completion:nil];
                }];
                
                UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
                    
                    [opciones dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [opciones addAction:reportarCheckin];
                [opciones addAction:cancelar];
                
                [self presentViewController:opciones animated:YES completion:nil];
            }
        }else if(segmentedControl.selectedSegmentIndex == 1){
        
            if([[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"uID"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]]){
                
                UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction *reportarCheckin = [UIAlertAction actionWithTitle:@"Eliminar check-in" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
                    
                    UIAlertController *confirmacion = [UIAlertController alertControllerWithTitle:@"¿Estás seguro?" message:@"Si eliminas este check-in no podrás recuperarlo." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *aceptar = [UIAlertAction actionWithTitle:@"Aceptar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        
                        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/eliminarpublicacion.php"]];
                        
                        [request setHTTPMethod:@"POST"];
                        
                        NSData *cuerpo = [[NSString stringWithFormat:@"id=%@&u=%d",[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"aconID"],[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
                        
                        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                        
                        NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                        
                        NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                            
                            if(error == nil){
                                
                                if(data != nil){
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        [checkinsUltimosAmigos removeObjectAtIndex:indexPath.row];
                                        [listaCheckin deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
                                    });
                                }
                            }
                            
                        }];
                        
                        [uploadTask resume];
                        
                    }];
                    
                    UIAlertAction *noAceptar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
                    
                    [confirmacion addAction:aceptar];
                    [confirmacion addAction:noAceptar];
                    
                    [self presentViewController:confirmacion animated:YES completion:nil];
                }];
                
                UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
                    
                    [opciones dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [opciones addAction:reportarCheckin];
                [opciones addAction:cancelar];
                
                [self presentViewController:opciones animated:YES completion:nil];
            }else{
                
                UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction *reportarCheckin = [UIAlertAction actionWithTitle:@"Denunciar check-in" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    ReportarCheckinTableViewController *enviarReporte = [storyboard instantiateViewControllerWithIdentifier:@"ReportarCheckin"];
                    
                    [enviarReporte setIdUsuarioABloquear:[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"uID"]];
                    
                    [enviarReporte setIdRelObjDia:[[checkinsUltimosAmigos objectAtIndex:indexPath.row] objectForKey:@"relID"]];
                    
                    [self presentViewController:enviarReporte animated:YES completion:nil];
                }];
                
                UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
                    
                    [opciones dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [opciones addAction:reportarCheckin];
                [opciones addAction:cancelar];
                
                [self presentViewController:opciones animated:YES completion:nil];
            }
        }
        
        
    }else if(indexPath.section == 1){
        
        if(segmentedControl.selectedSegmentIndex == 0){
            
            if([[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"uID"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]]){
                
                UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction *reportarCheckin = [UIAlertAction actionWithTitle:@"Eliminar check-in" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
                    
                    UIAlertController *confirmacion = [UIAlertController alertControllerWithTitle:@"¿Estás seguro?" message:@"Si eliminas este check-in no podrás recuperarlo." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *aceptar = [UIAlertAction actionWithTitle:@"Aceptar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        
                        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/eliminarpublicacion.php"]];
                        
                        [request setHTTPMethod:@"POST"];
                        
                        NSData *cuerpo = [[NSString stringWithFormat:@"id=%@&u=%d",[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"aconID"],[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];

                        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                        
                        NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                        
                        NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                            
                            if(error == nil){
                                
                                if(data != nil){
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        [checkinsTodosOtros removeObjectAtIndex:indexPath.row];
                                        [listaCheckin deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
                                    });
                                }
                            }
                            
                        }];
                        
                        [uploadTask resume];
                        
                    }];
                    
                    UIAlertAction *noAceptar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
                    
                    [confirmacion addAction:aceptar];
                    [confirmacion addAction:noAceptar];
                    
                    [self presentViewController:confirmacion animated:YES completion:nil];
                }];
                
                UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
                    
                    [opciones dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [opciones addAction:reportarCheckin];
                [opciones addAction:cancelar];
                
                [self presentViewController:opciones animated:YES completion:nil];
            }else{

                UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction *reportarCheckin = [UIAlertAction actionWithTitle:@"Denunciar check-in" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    ReportarCheckinTableViewController *enviarReporte = [storyboard instantiateViewControllerWithIdentifier:@"ReportarCheckin"];
                    
                    [enviarReporte setIdUsuarioABloquear:[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"uID"]];
                    
                    [enviarReporte setIdRelObjDia:[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"relID"]];
                    
                    [self presentViewController:enviarReporte animated:YES completion:nil];
                }];
                
                UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
                    
                    [opciones dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [opciones addAction:reportarCheckin];
                [opciones addAction:cancelar];
                
                [self presentViewController:opciones animated:YES completion:nil];
            }
            
        }else if(segmentedControl.selectedSegmentIndex == 1){
            
            
            if([[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"uID"] isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]]){
                
                UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction *reportarCheckin = [UIAlertAction actionWithTitle:@"Eliminar check-in" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
                    
                    UIAlertController *confirmacion = [UIAlertController alertControllerWithTitle:@"¿Estás seguro?" message:@"Si eliminas este check-in no podrás recuperarlo." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *aceptar = [UIAlertAction actionWithTitle:@"Aceptar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        
                        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/eliminarpublicacion.php"]];
                        
                        [request setHTTPMethod:@"POST"];
                        
                        NSData *cuerpo = [[NSString stringWithFormat:@"id=%@&u=%d",[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"aconID"],[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
                        
                        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                        
                        NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                        
                        NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                            
                            if(error == nil){
                                
                                if(data != nil){
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        [checkinsTodosAmigos removeObjectAtIndex:indexPath.row];
                                        [listaCheckin deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
                                    });
                                }
                            }
                            
                        }];
                        
                        [uploadTask resume];
                        
                    }];
                    
                    UIAlertAction *noAceptar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
                    
                    [confirmacion addAction:aceptar];
                    [confirmacion addAction:noAceptar];
                    
                    [self presentViewController:confirmacion animated:YES completion:nil];
                }];
                
                UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
                    
                    [opciones dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [opciones addAction:reportarCheckin];
                [opciones addAction:cancelar];
                
                [self presentViewController:opciones animated:YES completion:nil];
            }else{
                
                UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction *reportarCheckin = [UIAlertAction actionWithTitle:@"Denunciar check-in" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    ReportarCheckinTableViewController *enviarReporte = [storyboard instantiateViewControllerWithIdentifier:@"ReportarCheckin"];
                    
                    [enviarReporte setIdUsuarioABloquear:[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"uID"]];
                    
                    [enviarReporte setIdRelObjDia:[[checkinsTodosAmigos objectAtIndex:indexPath.row] objectForKey:@"relID"]];
                    
                    [self presentViewController:enviarReporte animated:YES completion:nil];
                }];
                
                UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
                    
                    [opciones dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [opciones addAction:reportarCheckin];
                [opciones addAction:cancelar];
                
                [self presentViewController:opciones animated:YES completion:nil];
            }
        }
    }
}


-(void)actualizarCheckins{

    __block BOOL cambiaLista = NO;
    
    if([[[checkinsTodosUltimos objectAtIndex:0] objectForKey:@"valido"] intValue] != 0){
        
        for (NSDictionary *elemento in checkinsTodosUltimos){
            
            double fecha = [[NSDate date] timeIntervalSince1970];
            
            if(fecha - [[elemento objectForKey:@"unixtimestamp"] doubleValue] > 7200){
                
                [checkinsTodosOtros addObject:elemento];
                [checkinsTodosUltimos removeObject:elemento];
                
                if([checkinsTodosUltimos count] == 0){
                    
                    [checkinsTodosUltimos addObject:[[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:0],@"valido", nil]];
                }
                
                cambiaLista = YES;
                
            }
            
        }
    }
    
    if([[[checkinsUltimosAmigos objectAtIndex:0] objectForKey:@"valido"] intValue] != 0){
        
        for (NSDictionary *elemento in checkinsUltimosAmigos){
            
            double fecha = [[NSDate date] timeIntervalSince1970];
            
            if(fecha - [[elemento objectForKey:@"unixtimestamp"] doubleValue] > 7200){
                
                [checkinsTodosAmigos addObject:elemento];
                [checkinsUltimosAmigos removeObject:elemento];
                
                if([checkinsUltimosAmigos count] == 0){
                    
                    [checkinsUltimosAmigos addObject:[[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:0],@"valido", nil]];
                }
                
                cambiaLista = YES;
            }
            
        }
    }
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getallcheckins.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"id=%@&latitud=%f&longitud=%f&p=0&ultimoIDTodos=%ld&ultimoIDAmigos=%ld",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"],miLocation.coordinate.latitude, miLocation.coordinate.longitude,ultimoRelIDTodos, ultimoRelIDAmigos] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){

        if(error == nil){
            
            if(data != nil){

                NSError *error = nil;
                
                NSDictionary *checkinsNuevos = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                if([[[[checkinsNuevos objectForKey:@"todosUltimosPosts"] objectAtIndex:0] objectForKey:@"valido"] intValue] != 0){
                    
                    
                    for (NSArray *elemento in [checkinsNuevos objectForKey:@"todosUltimosPosts"]) {

                        [checkinsTodosUltimos addObject:elemento];
                        cambiaLista = YES;
                    }
                    
                    ultimoRelIDTodos = [[[[checkinsNuevos objectForKey:@"todosUltimosPosts"] objectAtIndex:0] objectForKey:@"relID"] intValue];
                    
                }

                if([[[[checkinsNuevos objectForKey:@"amigosUltimosPosts"] objectAtIndex:0] objectForKey:@"valido"] intValue] != 0){
                    
                    
                    for (NSArray *elemento in [checkinsNuevos objectForKey:@"amigosUltimosPosts"]) {
                        
                        [checkinsUltimosAmigos addObject:elemento];
                        cambiaLista = YES;
                    }
                    
                    ultimoRelIDAmigos = [[[[checkinsNuevos objectForKey:@"amigosUltimosPosts"] objectAtIndex:0] objectForKey:@"relID"] intValue];
                    
                }
                
                if(cambiaLista){
                
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [listaCheckin reloadData];
                    });
                }
            }
        }
        
    }];
    
    [uploadTask resume];
    
}

-(void)getCheckins{

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getallcheckins.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"id=%@&latitud=%f&longitud=%f&p=1",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"],miLocation.coordinate.latitude, miLocation.coordinate.longitude] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){
            
            if(data != nil){
                
                NSDictionary *checkinsNuevos = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                if(segmentedControl.selectedSegmentIndex == 0 && [[[[checkinsNuevos objectForKey:@"todosOtrosPost"] objectAtIndex:0] objectForKey:@"valido"] intValue] == 0 && [[[[checkinsNuevos objectForKey:@"todosUltimosPosts"] objectAtIndex:0] objectForKey:@"valido"] intValue] == 0){
                    
                    if(!seMuestraMapa){
                        [listaCheckin setHidden:YES];
                        [vistaEmptyState setHidden:NO];
                    }
                }else if(segmentedControl.selectedSegmentIndex == 0 && [[[[checkinsNuevos objectForKey:@"todosOtrosPost"] objectAtIndex:0] objectForKey:@"valido"] intValue] == 1 && [[[[checkinsNuevos objectForKey:@"todosUltimosPosts"] objectAtIndex:0] objectForKey:@"valido"] intValue] == 1){
                    
                    if(!seMuestraMapa){
                        [listaCheckin setHidden:NO];
                        [vistaEmptyState setHidden:YES];
                    }
                }
                
                if(segmentedControl.selectedSegmentIndex == 1 && [[[[checkinsNuevos objectForKey:@"amigosOtrosPosts"] objectAtIndex:0] objectForKey:@"valido"] intValue] == 0 && [[[[checkinsNuevos objectForKey:@"amigosUltimosPosts"] objectAtIndex:0] objectForKey:@"valido"] intValue] == 0){
                    
                    if(!seMuestraMapa){
                        [listaCheckin setHidden:YES];
                        [vistaEmptyState setHidden:NO];
                    }
                }else if(segmentedControl.selectedSegmentIndex == 1 && [[[[checkinsNuevos objectForKey:@"amigosOtrosPosts"] objectAtIndex:0] objectForKey:@"valido"] intValue] == 1 && [[[[checkinsNuevos objectForKey:@"amigosUltimosPosts"] objectAtIndex:0] objectForKey:@"valido"] intValue] == 1){
                    
                    if(!seMuestraMapa){
                        [listaCheckin setHidden:NO];
                        [vistaEmptyState setHidden:YES];
                    }
                }
                
                if(error == nil){
                    
                    if([[[[checkinsNuevos objectForKey:@"todosUltimosPosts"] objectAtIndex:0] objectForKey:@"valido"] intValue] != 0){
                    
                        ultimoRelIDTodos = [[[[checkinsNuevos objectForKey:@"todosUltimosPosts"] objectAtIndex:0] objectForKey:@"relID"] intValue];
                    }else if([[[[checkinsNuevos objectForKey:@"todosOtrosPost"] objectAtIndex:0] objectForKey:@"valido"] intValue] != 0){
                        
                        ultimoRelIDTodos = [[[[checkinsNuevos objectForKey:@"todosOtrosPost"] objectAtIndex:0] objectForKey:@"relID"] intValue];
                    }

                    if([[[[checkinsNuevos objectForKey:@"amigosUltimosPosts"] objectAtIndex:0] objectForKey:@"valido"] intValue] != 0){
                        
                        ultimoRelIDAmigos = [[[[checkinsNuevos objectForKey:@"amigosUltimosPosts"] objectAtIndex:0] objectForKey:@"relID"] intValue];
                    }else if([[[[checkinsNuevos objectForKey:@"amigosOtrosPosts"] objectAtIndex:0] objectForKey:@"valido"] intValue] != 0){
                        
                        ultimoRelIDAmigos = [[[[checkinsNuevos objectForKey:@"amigosOtrosPosts"] objectAtIndex:0] objectForKey:@"relID"] intValue];
                    }
                    
                    if(![[checkinsNuevos objectForKey:@"todosOtrosPost"] isEqual:checkinsTodosOtros]){
                        
                        checkinsTodosOtros = [checkinsNuevos objectForKey:@"todosOtrosPost"];
                    }
                    
                    if(![[checkinsNuevos objectForKey:@"todosUltimosPosts"] isEqual:checkinsTodosUltimos]){
                        
                        checkinsTodosUltimos = [checkinsNuevos objectForKey:@"todosUltimosPosts"];
                    }
                    
                    if(![[checkinsNuevos objectForKey:@"amigosOtrosPosts"] isEqual:checkinsTodosAmigos]){
                        
                        checkinsTodosAmigos = [checkinsNuevos objectForKey:@"amigosOtrosPosts"];
                        
                    }
                    
                    
                    if(![[checkinsNuevos objectForKey:@"amigosUltimosPosts"] isEqual:checkinsUltimosAmigos]){
                        
                        checkinsUltimosAmigos = [checkinsNuevos objectForKey:@"amigosUltimosPosts"];
                        
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                            
                        [listaCheckin reloadData];
                    });
                    
                   // intervaloActualizarLista = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(actualizarCheckins) userInfo:nil repeats:YES];
                    
                }
                
                if([[[checkinsNuevos objectForKey:@"tutorial"] objectForKey:@"hacercheckin1"] intValue] == 0){
                
                    [backgroundTutorial setHidden:NO];
                }
            }
        }
        
    }];
    
    [uploadTask resume];
}

- (IBAction)cerrarTutorial:(id)sender {
    
    [backgroundTutorial setHidden:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarhacercheckintuto.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){}];
    
    [uploadTask resume];

}

-(void)altOpcionMeGusta:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"Me gusta este lugar";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionEstaLleno:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"Aún hay lugares disponibles";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionMeGustaMusica:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"Me gusta la música";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionServicioRapido:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"El servicio es rápido";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionServicioBueno:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"El servicio es bueno";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionMeGustaNegativo:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"No me gusta este lugar";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionEstaLlenoNegativo:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"El lugar está lleno";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionMeGustaMusicaNegativo:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"No me gusta la música";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionServicioRapidoNegativo:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"El servicio es lento";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionServicioBuenoNegativo:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"El servicio es malo";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
