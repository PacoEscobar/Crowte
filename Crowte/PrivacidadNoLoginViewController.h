//
//  PrivacidadNoLoginViewController.h
//  Crowte
//
//  Created by Paco Escobar on 28/12/15.
//  Copyright © 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacidadNoLoginViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextView *privacidadContainer;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;


@end
