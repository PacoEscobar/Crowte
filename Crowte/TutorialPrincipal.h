//
//  TutorialPrincipal.h
//  Crowte
//
//  Created by Paco Escobar on 19/01/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialPrincipal : UIViewController

@property NSUInteger index;
@property NSMutableDictionary *datosUsuario;
@property (weak, nonatomic) IBOutlet UIImageView *crowte;
@property (weak, nonatomic) IBOutlet UILabel *bienvenido;

@property (weak, nonatomic) IBOutlet UILabel *texto1;
@property (weak, nonatomic) IBOutlet UILabel *texto2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *texto1_crowte;
@property (weak, nonatomic) IBOutlet UIButton *botonComenzar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botonComenzarConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTexto2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTexto3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintT1_T2;


@end
