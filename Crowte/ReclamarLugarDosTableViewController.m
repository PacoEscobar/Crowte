//
//  ReclamarLugarDosTableViewController.m
//  Crowte
//
//  Created by Paco Escobar on 10/04/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "ReclamarLugarDosTableViewController.h"

@interface ReclamarLugarDosTableViewController ()

@end

@implementation ReclamarLugarDosTableViewController

@synthesize nombre, telefono, de, a, error, idlugar;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [de setTimeZone:[NSTimeZone localTimeZone]];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelar) name:@"dismissreclamarone" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    return 4;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    CGFloat size;
    
    if(indexPath.row == 0){
    
        size = 114;
    }else if(indexPath.row == 1){
    
        size = 114;
    }else if(indexPath.row == 2){
    
        size = 525;
    }else if(indexPath.row == 3){
    
        size = 201;
    }
    
    return size;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return 150;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UIView *view = [[UIView alloc] init];
    
    [view setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *texto = [[UILabel alloc] initWithFrame:CGRectMake(16, 28, 288, 67)];
    
    [texto setText:@"La cuenta de usuario que estés usando en éste momento, será la cuenta que se utilizará para acceder a la administración del sitio."];
    
    [texto setNumberOfLines:3];
    [texto setFont:[UIFont systemFontOfSize:11]];
    [texto setTextColor:[UIColor lightGrayColor]];
    
    [view addSubview:texto];
    
    UILabel *titulo = [[UILabel alloc] initWithFrame:CGRectMake(16, 100, 288, 50)];
    
    [titulo setText:@"Información de contacto"];
    
    [titulo setTextColor:[UIColor blackColor]];
    
    [view addSubview:titulo];
    
    return view;

}

#pragma mark - textfield

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    if([textField isEqual:nombre]){
    
        [nombre resignFirstResponder];
        [telefono becomeFirstResponder];
    }else{
    
        [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - mios

-(IBAction)cancelar{

    [self dismissViewControllerAnimated:YES completion:^{
    
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissreclamarone" object:[NSNumber numberWithInt:1]];
    }];
}

-(IBAction)enviar{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [error setHidden:YES];
    
    if([[nombre text] length] == 0 || [[telefono text] length] == 0){

        [error setHidden:NO];
    }else{
    
        [error setHidden:YES];
        
        NSDateFormatter *formto = [[NSDateFormatter alloc] init];
        
        [formto setDateFormat:@"h:mm a"];
        
        NSString *desde = [NSString stringWithFormat:@"%f",[de.date timeIntervalSince1970]];
        NSString *hasta = [NSString stringWithFormat:@"%f",[a.date timeIntervalSince1970]];

        NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/pedirreclamolugar.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%@&lugar=%d&nombre=%@&tele=%@&desde=%@&hasta=%@&token=%@",[defaults objectForKey:@"identifier"], idlugar, [nombre text], [telefono text],desde,hasta,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *errorEnvio){
            
            if(errorEnvio.code == NSURLErrorTimedOut || errorEnvio.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                [self enviar];
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    UIViewController *gracias = [storyboard instantiateViewControllerWithIdentifier:@"graciasReclamarLugar"];
                    
                    [self presentViewController:gracias animated:YES completion:nil];
                });
            }
        }];
    }    
}

@end
