//
//  ResponderMensajeViewController.h
//  Crowte
//
//  Created by Paco Escobar on 12/02/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResponderMensajeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate>

@property int esVideo;
@property NSString *nombre;
@property NSData *fotoUsuario;
@property NSString *publicacion;
@property NSString *fecha;
@property NSString *numDeAmor;
@property NSDictionary *datosPublicacion;
@property NSMutableArray *comentarios;
@property UIImage *imagenComentario;
@property NSData *dataImagenComentario;
@property NSURL *urlVideo;
@property AVPlayer *reproductor;
@property AVPlayerLayer *layer;
@property (weak, nonatomic) IBOutlet UITextField *ComentarTextField;
@property (weak, nonatomic) IBOutlet UIButton *BotonEnviar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintResponderBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBotonEnviarBottom;
@property (weak, nonatomic) IBOutlet UITableView *tabla;



- (IBAction)enviarComentario:(id)sender;
-(void)expandirImagen:(UIGestureRecognizer *)gesto;
-(void)getComentarios;

#pragma mark - video control

-(void)pausarVideo;
-(void)loopVideo:(NSNotification *)notification;

@end
