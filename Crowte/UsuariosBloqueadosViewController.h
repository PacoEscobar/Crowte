//
//  UsuariosBloqueadosViewController.h
//  Crowte
//
//  Created by Paco Escobar on 10/07/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsuariosBloqueadosViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@property NSMutableArray *listaBloqueados;
@property NSUserDefaults *defaults;
@property (weak, nonatomic) IBOutlet UITableView *tabla;

-(IBAction)desbloquear:(id)sender;

@end
