//
//  PerfilUsuarioViewController.h
//  Crowte
//
//  Created by Paco Escobar on 13/02/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface PerfilUsuarioViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSURLSessionDelegate, NSURLSessionDownloadDelegate, CLLocationManagerDelegate>

@property int idUsuario;
@property BOOL locationContador;
@property float navBarYInicial;

@property NSString *nombreUsuario;
@property NSString *fotoUsuario;

@property CLLocationManager *locationManager;
@property CLGeocoder *geocoder;

@property NSDictionary *datosUsuario;
@property NSMutableDictionary *checkins;
@property (strong, nonatomic) IBOutlet UITableView *tabla;

@property (strong, nonatomic) IBOutlet UIView *fondoTutorialAgregar;
@property (strong, nonatomic) IBOutlet UILabel *textoTutorialAgregar;


-(void)getCheckins;
-(IBAction)opcionesPerfil:(id)sender;
-(IBAction)join:(id)sender;
-(IBAction)opcionesCheckin:(id)sender;
- (IBAction)cerrarTutorialAgregar:(id)sender;

@end
