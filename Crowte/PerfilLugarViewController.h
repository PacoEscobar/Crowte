//
//  PerfilLugarViewController.h
//  Crowte
//
//  Created by Paco Escobar on 18/02/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface PerfilLugarViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSURLSessionDelegate, NSURLSessionDownloadDelegate>


@property int idLugar;
@property float navBarYInicial;

@property NSString *fotoPrincipalLugar;
@property NSString *fotoSecundariaLugar;
@property NSString *nombreLugar;

@property (strong, nonatomic) IBOutlet UIView *backgroundTutoCheckinVotos;
@property (strong, nonatomic) IBOutlet UILabel *tituloTutoCheckinVotos;


@property NSDictionary *mensajes;

@property CLLocation *location;
@property CLGeocoder *geocoder;

@property NSMutableDictionary *opcionesAmbienteLugar;

@property (strong, nonatomic) IBOutlet UITableView *tabla;

-(void)getMensajes;
-(IBAction)meGustaComentario:(id)sender;
-(IBAction)irAVotar:(id)sender;
-(void)abrirImagen:(UIPanGestureRecognizer*)gesto;
-(IBAction)mostrarOpcionesMensaje:(id)sender;
-(IBAction)mostrarOpcionesLugar:(id)sender;
- (IBAction)cerrarTutoCheckinVotos:(id)sender;
-(void)playMicro:(UITapGestureRecognizer *)sender;
- (void)getOpcionesAmbiente;

@end
