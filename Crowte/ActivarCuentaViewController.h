//
//  ActivarCuentaViewController.h
//  Crowte
//
//  Created by Paco Escobar on 23/05/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivarCuentaViewController : UIViewController <UITextViewDelegate>

@property NSMutableDictionary *datosUsuario;
@property (weak, nonatomic) IBOutlet UITextView *campocodigo;


- (IBAction)enviarcodigo:(id)sender;
- (IBAction)omitir:(id)sender;

@end
