//
//  controladorTabControllerViewController.m
//  Crowte
//
//  Created by Paco Escobar on 23/04/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import "controladorTabControllerViewController.h"
#import "NavegacionPrincipalController.h"
#import "PageViewController.h"
#import "ActivarCuentaViewController.h"

@interface controladorTabControllerViewController ()

@end

@implementation controladorTabControllerViewController

@synthesize nombre, datosUsuario, configuracionBandera, fotoDeData, enLugar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
    
    NSUserDefaults *usuario = [NSUserDefaults standardUserDefaults];
    
    [usuario setObject:[[datosUsuario objectForKey:@"datos"] objectForKey:@"sesion"] forKey:@"token"];
    
    [usuario setObject:[[datosUsuario objectForKey:@"datos"] objectForKey:@"verificado"] forKey:@"verificado"];
    
    [usuario synchronize];
    
    if([[[datosUsuario objectForKey:@"datos"] objectForKey:@"iosNotifToken"] isEqualToString:@"0"]){
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/notificaciones/updateiosnotif.php"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1.0];
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%@&token=%@&tokena=%@",[usuario objectForKey:@"identifier"], [usuario objectForKey:@"notifToken"], [usuario objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){}];
    }

    if([[[datosUsuario objectForKey:@"datos"] objectForKey:@"verificado"] isEqualToString:@"0"]){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *codigo = [UIAlertController alertControllerWithTitle:@"Verifica tu cuenta" message:@"Verifica tu cuenta para poder utilizar todas las funciones de Crowte sin problemas" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *insertarCodigo = [UIAlertAction actionWithTitle:@"Insertar código" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                ActivarCuentaViewController *activarCuenta = [storyboard instantiateViewControllerWithIdentifier:@"insertacodigo"];
                
                activarCuenta.datosUsuario = datosUsuario;
                
                [self presentViewController:activarCuenta animated:YES completion:nil];
            }];
            
            UIAlertAction *omitir = [UIAlertAction actionWithTitle:@"Omitir" style:UIAlertActionStyleCancel handler:nil];
            
            [codigo addAction:insertarCodigo];
            [codigo addAction:omitir];
            
            [self presentViewController:codigo animated:YES completion:nil];
        });
        
        NSURL *fotoURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/%@",[[datosUsuario objectForKey:@"datos"] objectForKey:@"ID"],[[datosUsuario objectForKey:@"datos"] objectForKey:@"foto"]]];
        
        NSData *imagen = [NSData dataWithContentsOfURL:fotoURL];
        
        fotoDeData = [UIImage imageWithData:imagen];
        
        configuracionBandera = 1;
        
        if([[datosUsuario objectForKey:@"lugar"] objectForKey:@"status"] != nil){
            
            enLugar = NO;
        }else{
            
            enLugar = YES;
        }
        
        
    }else{
        
        NSURL *fotoURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/%@",[[datosUsuario objectForKey:@"datos"] objectForKey:@"ID"],[[datosUsuario objectForKey:@"datos"] objectForKey:@"foto"]]];
        
        NSData *imagen = [NSData dataWithContentsOfURL:fotoURL];
        
        fotoDeData = [UIImage imageWithData:imagen];
        
        configuracionBandera = 1;
        
        if([[datosUsuario objectForKey:@"lugar"] objectForKey:@"status"] != nil){
            
            enLugar = NO;
        }else{
            
            enLugar = YES;
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [usuario synchronize];
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{

    

}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{

    
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
