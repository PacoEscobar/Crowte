//
//  NavPerfilMioViewController.h
//  Crowte
//
//  Created by Paco Escobar on 10/02/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavPerfilMioViewController : UINavigationController

@property NSDictionary *datosMios;

@end
