//
//  ActivarCuentaViewController.m
//  Crowte
//
//  Created by Paco Escobar on 23/05/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "ActivarCuentaViewController.h"
#import "PageViewController.h"
#import "controladorTabControllerViewController.h"

@interface ActivarCuentaViewController ()

@end

@implementation ActivarCuentaViewController

@synthesize campocodigo, datosUsuario;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [campocodigo.layer setBorderWidth:1];
    [campocodigo.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [campocodigo.layer setCornerRadius:15];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - touches began

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *toque = [[event allTouches] anyObject];
    
    if([campocodigo isFirstResponder] && [toque view] != campocodigo){
        
        [campocodigo resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - hechos por mi

- (IBAction)enviarcodigo:(id)sender {
    
    [sender setEnabled:NO];
    
    __block UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/activacioncodigo.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%@&codigo=%@&token=%@",[defaults objectForKey:@"identifier"],[campocodigo text],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                app.networkActivityIndicatorVisible = NO;
                
                UIAlertController *error = [UIAlertController alertControllerWithTitle:@"Hubo un error" message:@"Por favor inténtalo nuevamente" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *muybien = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    [sender setEnabled:YES];
                }];
                
                [error addAction:muybien];
                
                [self presentViewController:error animated:YES completion:nil];
             
            });
        }else{
        
            NSString *strrespuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            if([strrespuesta isEqualToString:@"1"]){
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

                    if([defaults objectForKey:@"verificado"]){
                        
                        UIStoryboard *storyboard;
                        controladorTabControllerViewController *cambio;
                        
                        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        cambio= [storyboard instantiateViewControllerWithIdentifier:@"tabController"];
                        
                        [[datosUsuario objectForKey:@"datos"] setValue:@"1" forKey:@"verificado"];
                        
                        cambio.datosUsuario = datosUsuario;
                        
                        [self presentViewController:cambio animated:NO completion:nil];
                        
                    }else{
                        
                        UIStoryboard *storyboard;
                        PageViewController *tabcontroller;
                        
                        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        tabcontroller= [storyboard instantiateViewControllerWithIdentifier:@"tutorialPrincipalComienzo"];
                        
                        tabcontroller.datosUsuario = datosUsuario;
                        
                        [self presentViewController:tabcontroller animated:NO completion:nil];
                    }
                });
            }else{
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController *error = [UIAlertController alertControllerWithTitle:nil message:@"Por favor verifica el código" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *muybien = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [sender setEnabled:YES];
                    }];
                    
                    [error addAction:muybien];
                    
                    [self presentViewController:error animated:YES completion:nil];
                });
            }
        }
    }];
}

- (IBAction)omitir:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if([defaults objectForKey:@"verificado"]){
            
            UIStoryboard *storyboard;
            controladorTabControllerViewController *cambio;
            
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            cambio= [storyboard instantiateViewControllerWithIdentifier:@"tabController"];
            
            cambio.datosUsuario = datosUsuario;
            
            [self presentViewController:cambio animated:NO completion:nil];
            
        }else{
            
            UIStoryboard *storyboard;
            PageViewController *tabcontroller;
            
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            tabcontroller= [storyboard instantiateViewControllerWithIdentifier:@"tutorialPrincipalComienzo"];
            
            tabcontroller.datosUsuario = datosUsuario;
            
            [self presentViewController:tabcontroller animated:NO completion:nil];
        }
    });
    
}
@end
