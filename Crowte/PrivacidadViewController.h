//
//  PrivacidadViewController.h
//  Crowte
//
//  Created by Paco Escobar on 29/07/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacidadViewController : UIViewController



@property (weak, nonatomic) IBOutlet UITextView *privacidadContainer;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@end
