//
//  PrivacidadNoLoginViewController.m
//  Crowte
//
//  Created by Paco Escobar on 28/12/15.
//  Copyright © 2015 Crowte Inc. All rights reserved.
//

#import "PrivacidadNoLoginViewController.h"

@interface PrivacidadNoLoginViewController ()

@end

@implementation PrivacidadNoLoginViewController

@synthesize privacidadContainer, activity;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/legal/getprivacidad.php"]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut){
            
            UIAlertController *noconexion = [UIAlertController alertControllerWithTitle:nil message:@"Estamos teniendo algunos problemas, por favor intenta reiniciando la app nuevamente o verificando tu conexión a Internet." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
            
            [noconexion addAction:dismiss];
            
            [self presentViewController:noconexion animated:YES completion:nil];
            
        }else if(error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            UIAlertController *noconexion = [UIAlertController alertControllerWithTitle:nil message:@"Ha ocurrido un error, por favor verifica tu conexión a Internet." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
            
            [noconexion addAction:dismiss];
            
            [self presentViewController:noconexion animated:YES completion:nil];
            
        }else{
            
            NSString *privacidad = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [activity removeFromSuperview];
                [privacidadContainer setHidden:NO];
                [privacidadContainer setText:privacidad];
            });
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
