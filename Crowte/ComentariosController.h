//
//  ComentariosController.h
//  Crowte
//
//  Created by Paco Escobar on 09/05/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComentariosController : UIViewController <UITextViewDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property int postID;
@property int posicionScroll;
@property int amigoID;
@property BOOL joinFilled;
@property NSString *nombrePersona;
@property NSString *textoDePublicacion;
@property NSString *lugar;
@property NSString *joinsDePublicacion;
@property NSData *foto;
@property NSMutableArray *modeloComentarios;
@property NSMutableDictionary *modeloFotos;
@property NSMutableDictionary *json;
@property (weak, nonatomic) IBOutlet UILabel *labelNumeroComentarios;
@property (weak, nonatomic) IBOutlet UIButton *botonEnviar;
@property (weak, nonatomic) IBOutlet UIButton *botonCancelar;
@property (weak, nonatomic) IBOutlet UITextView *publicacion;
@property (weak, nonatomic) IBOutlet UIButton *imagenPerfil;
@property (weak, nonatomic) IBOutlet UIButton *nombrePerfil;
@property (weak, nonatomic) IBOutlet UITextView *textoPublicacion;
@property (weak, nonatomic) IBOutlet UILabel *labelEn;
@property (weak, nonatomic) IBOutlet UIButton *nombreLugar;
@property (weak, nonatomic) IBOutlet UITextField *textCampo;
@property (weak, nonatomic) IBOutlet UIView *textFieldContenedor;
@property (weak, nonatomic) IBOutlet UITableView *tabla;
@property (strong, nonatomic) UIApplication *app;
@property (weak, nonatomic) IBOutlet UILabel *joins;

-(IBAction)enviarComentario:(id)sender;
-(void)pedirComentarios;
-(void)descargarImagen:(NSURL *)url completionBlock:(void (^)(BOOL, UIImage *))completionBlock;

@end
