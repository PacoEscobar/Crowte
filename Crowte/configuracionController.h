//
//  configuracionController.h
//  Crowte
//
//  Created by Paco Escobar on 18/05/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ConfiguracionDelegate.h"

@interface configuracionController : UITableViewController<UITextViewDelegate, ConfiguracionDelegate>

@property int bandera;
@property int nuevaFoto;

@property float navBarYInicial;

@property NSString *fotoActual;

@property UIImage *fotoCambiada;
@property (strong, nonatomic) NSMutableDictionary *datosUsuario;
@property NSData *foto;
@property NSUserDefaults *defaults;
@property (weak, nonatomic) IBOutlet UIImageView *fotoView;
@property (strong, nonatomic) UIButton *fotoPerfil;
@property (strong, nonatomic) IBOutlet UIButton *fotoBotonPerfil;



@property NSString *nombreUsuario;
@property NSString *apellidoUsuario;

@property (retain, nonatomic) IBOutlet UITextView *nombre;
@property (retain, nonatomic) IBOutlet UITextView *apellido;


-(IBAction)cambiarFotoPerfil:(id)sender;
- (IBAction)cerrarsesion:(id)sender;
-(void)checkInfo;
-(void)cancelar;
-(void)enviar;

@end
