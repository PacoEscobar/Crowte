//
//  ModificarSecretodebusquedaController.h
//  Crowte
//
//  Created by Paco Escobar on 24/08/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModificarSecretodebusquedaController : UIViewController < UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *secreto;
@property (weak, nonatomic) IBOutlet UISwitch *tiene;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *cargador;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTop;

-(void)enviar;
-(IBAction)atras:(id)sender;

@end
