//
//  AmigosView.m
//  Crowte
//
//  Created by Paco Escobar on 24/04/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import "AmigosView.h"
#import "Perfil.h"
#import "CuentasVinculadasTableViewController.h"

@interface AmigosView ()

@end

@implementation AmigosView

@synthesize tablaView, arrayDictionarios, indi, ninguno, tieneAmigos;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    tieneAmigos = YES;
    
    ninguno = NO;
    
    arrayDictionarios = [[NSMutableArray alloc] init];
   // [self getFriends];
    
    UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStyleDone target:self action:nil];
    
    self.navigationItem.backBarButtonItem = atras;
    
    [self getFriends];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [arrayDictionarios count]+1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if([arrayDictionarios count] == 0 && tieneAmigos == NO){
        return 150;
    }else if(indexPath.row==[arrayDictionarios count]){
        return 49.0;
    }else{
        return 93;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *celda;

    
    if([arrayDictionarios count] != 0){
        NSInteger rowCount = [self tableView:tablaView numberOfRowsInSection:0];
        
        if(indexPath.row != rowCount-1)
        {
            NSString *identificadorCelda = @"Celda";
            celda = [tablaView dequeueReusableCellWithIdentifier:identificadorCelda];
            
            if(celda == nil)
            {
                
                celda = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identificadorCelda];
                
            }
            UIImageView *backgroundI = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"middleCell.png"]];
            
            celda.backgroundView = backgroundI;
            UILabel *nombre = (UILabel *)[celda viewWithTag:1];
            UILabel *ultimoLugar = (UILabel *)[celda viewWithTag:2];
            UIImageView *foto = (UIImageView *)[celda viewWithTag:3];
            nombre.text = [NSString stringWithFormat:@"%@ %@",[[arrayDictionarios objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[arrayDictionarios objectAtIndex:indexPath.row] objectForKey:@"apeido"]];
            
            if(![[[arrayDictionarios objectAtIndex:indexPath.row] objectForKey:@"lugar"] isEqual: [NSNull null]]){
                
                ultimoLugar.text = [NSString stringWithFormat:@"Ultimo lugar visto: %@",[[arrayDictionarios objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
            }else{
            
                ultimoLugar.text = @"";
            }
            foto.image = [UIImage imageWithData:[[arrayDictionarios objectAtIndex:indexPath.row] objectForKey:@"foto"]];
            [ultimoLugar sizeToFit];
            [nombre sizeToFit];

            [indi stopAnimating];
            [indi removeFromSuperview];
            
        }else{
            
            celda = [tablaView dequeueReusableCellWithIdentifier:@"UltimaCelda"];
            if(celda == nil)
            {
                
                celda = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:@"bottomCell"];
            }
            
        }
    }else if([arrayDictionarios count] == 0 && tieneAmigos == NO){
    
        celda = [tablaView dequeueReusableCellWithIdentifier:@"bottomCell"];

        
        UIImageView *backgroundI = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"middleCell.png"]];
        
        celda.backgroundView = backgroundI;
        
        UILabel *noAmigos = [[UILabel alloc] initWithFrame:CGRectMake(0, 6, [UIScreen mainScreen].bounds.size.width, 36)];
        
        [noAmigos setFont:[UIFont fontWithName:[noAmigos.font fontName] size:14]];
        
        [noAmigos setTextAlignment:NSTextAlignmentCenter];
        noAmigos.text = @"Aún no has agregado amigos";
        
        UIButton *botonAddFbFrend = [[UIButton alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width/2)-100, 80, 200, 25)];
        
        [botonAddFbFrend setBackgroundColor:[UIColor colorWithRed:59/255.0 green:89/255.0 blue:152/255.0 alpha:1]];
        [botonAddFbFrend.titleLabel setFont:[UIFont fontWithName:[botonAddFbFrend.titleLabel.font fontName] size:13]];
        [botonAddFbFrend setTitle:@"Busca amigos en Facebook" forState:UIControlStateNormal];
        [botonAddFbFrend addTarget:self action:@selector(amigosDeFacebook) forControlEvents:UIControlEventTouchUpInside];
        
        celda.selectionStyle = UITableViewCellEditingStyleNone;
        
        [celda addSubview:noAmigos];
        [celda addSubview:botonAddFbFrend];
        
    }else{
        
        NSString *identificadorCelda = @"bottomCell";
        celda = [tablaView dequeueReusableCellWithIdentifier:identificadorCelda];
        
        UIImageView *backgroundI = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"middleCell.png"]];
        
        celda.backgroundView = backgroundI;
        
        if(celda == nil)
        {
            
            celda = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identificadorCelda];
            
        }
        
        indi = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indi.frame = CGRectMake(136, 6, 48, 36);
        [indi startAnimating];
        [celda addSubview:indi];
        
    }
    return  celda;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if([segue.destinationViewController isKindOfClass:[Perfil class]]){
    
        Perfil *controlador = (Perfil *)segue.destinationViewController;
        
        UITableViewCell *celda = [tablaView cellForRowAtIndexPath:[tablaView indexPathForSelectedRow]];

        controlador.stringNombre = [(UILabel *)[celda viewWithTag:1] text];

        controlador.dataFoto = UIImageJPEGRepresentation([(UIImageView *)[celda viewWithTag:3] image], 1);
        
        controlador.amigoID = [[[arrayDictionarios objectAtIndex:[[tablaView indexPathForSelectedRow] row]] objectForKey:@"ID"] intValue];
        
        [tablaView deselectRowAtIndexPath:[tablaView indexPathForSelectedRow] animated:YES];
        
    }
}

-(void)getFriends
{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getfriends.php"]];
   
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[NSString stringWithFormat:@"id=%@&token=%@",[userDefaults objectForKey:@"identifier"],[userDefaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            [[[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil] show];
        }else{
            
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
           
            [arrayDictionarios removeAllObjects];
            
            if(json != NULL){
            
                tieneAmigos = YES;
                NSURL *foto;
                NSData *dataDeFoto;
        
                for(NSMutableDictionary *amigo in json){
            
                    foto = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/%@",[amigo objectForKey:@"ID"],[amigo objectForKey:@"foto"]]];
            
                    dataDeFoto = [NSData dataWithContentsOfURL:foto];

                    [amigo setValue:dataDeFoto forKey:@"foto"];
            
                    [arrayDictionarios insertObject:amigo atIndex:0];

                }
                
                dispatch_async(dispatch_get_main_queue(), ^{

                    [tablaView reloadData];
                });
                
            }else{
            
                tieneAmigos = NO;
                [arrayDictionarios removeAllObjects];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tablaView reloadData];
                });
            }
        }
    }];
    
}

-(void)amigosDeFacebook{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    CuentasVinculadasTableViewController *cuentas = [storyboard instantiateViewControllerWithIdentifier:@"cuentasvinculadas"];
    
    [self.navigationController.navigationBar setTintColor:[UIColor blueColor]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    [self.navigationController pushViewController:cuentas animated:YES];
}

@end
