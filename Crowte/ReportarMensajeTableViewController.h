//
//  ReportarMensajeTableViewController.h
//  Crowte
//
//  Created by Paco Escobar on 10/03/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportarMensajeTableViewController : UITableViewController <UITextViewDelegate, NSURLSessionDelegate, NSURLSessionDownloadDelegate>

@property NSString *idUsuarioABloquear;
@property NSString *idRelMensajeLugar;

@property (strong, nonatomic) IBOutlet UITextView *campoTextoReporte;



- (IBAction)cancelarReporte:(id)sender;
- (IBAction)enviarReporte:(id)sender;

@end
