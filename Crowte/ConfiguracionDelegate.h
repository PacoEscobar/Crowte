
#import <Foundation/Foundation.h>

@protocol ConfiguracionDelegate <NSObject>

@optional

-(void)pasarFoto:(UIImage *)fotoNueva;

@end