//
//  PublicaCheckinViewController.m
//  Crowte
//
//  Created by Paco Escobar on 20/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <UIImageView+WebCache.h>
#import "PublicaCheckinViewController.h"
#import "CheckinViewController.h"

@interface PublicaCheckinViewController ()

@end

@implementation PublicaCheckinViewController

@synthesize nombreLugar,constraintBottomBoton, botonContinuar, stringFotoTraseraLugar, stringFotoPrincipalLugar, fotoTraseraLugar, fotoPrincipalLugar, idLugar, labelNombreLugar, labelDireccionLugar, direccionLugar, locacion, geocoder, campoMensaje;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[UINavigationBar appearance] setTranslucent:YES];
    
    [self.navigationItem setTitle:nombreLugar];
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"transparente.png"]];

    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [fotoPrincipalLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
    
    [[fotoPrincipalLugar layer] setBorderWidth:2];
    [[fotoPrincipalLugar layer] setBorderColor:[[UIColor colorWithWhite:1 alpha:1] CGColor]];
    [[fotoPrincipalLugar layer] setCornerRadius:50];
    [[fotoPrincipalLugar layer] setMasksToBounds:YES];
    
    
    if(![stringFotoPrincipalLugar isEqualToString:@"tierra.png"]){
            
        [fotoPrincipalLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%d/%@",idLugar, stringFotoPrincipalLugar]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
        
            if(error == nil){
            
                [fotoPrincipalLugar.layer setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                [fotoPrincipalLugar setContentMode:UIViewContentModeScaleToFill];
            }
        }];
    }else{
    
        [fotoPrincipalLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
        [fotoPrincipalLugar setImage:[UIImage imageNamed:@"cblancav3.png"]];
    }
        
    [fotoTraseraLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%d/%@",idLugar,stringFotoTraseraLugar]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
    
    [labelNombreLugar setText:nombreLugar];
    
    if([direccionLugar isEqualToString:@"Label"]){
        
        geocoder = [[CLGeocoder alloc] init];
        
        [geocoder reverseGeocodeLocation:locacion completionHandler:^(NSArray *placemarks, NSError *error){
            
            CLPlacemark *placemark = [placemarks lastObject];
            
            [labelDireccionLugar setText:[NSString stringWithFormat:@"%@, %@, %@",[[placemark addressDictionary] objectForKey:@"Name"],[[placemark addressDictionary] objectForKey:@"City"],[[placemark addressDictionary] objectForKey:@"State"]]];
        }];
    }else{
    
        [labelDireccionLugar setText:direccionLugar];
    }
    
    [[UITextField appearance] setTintColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    if([campoMensaje isFirstResponder]){
        [campoMensaje resignFirstResponder];
    }
    
    
}

-(IBAction)enviarCheckin:(id)sender{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *dataComentarioLugar =[[campoMensaje text] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    
    NSString *comentario = [[NSString alloc] initWithData:dataComentarioLugar encoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/publicaracontesimiento.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@&word=%@&lugar=%d",[defaults objectForKey:@"identifier"],comentario,idLugar] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                
        if(error == nil){
            
            NSDictionary *datosRegreso = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            
            CheckinViewController *checkinVC = (CheckinViewController*)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
                       
            [checkinVC setDatosEnLugar:[@{@"lugar":@{@"ID": [NSNumber numberWithInt:[[datosRegreso objectForKey:@"ID"] intValue]], @"fecha":[NSNumber numberWithFloat:[[NSDate date] timeIntervalSince1970]], @"foto":[datosRegreso objectForKey:@"foto"], @"fotoSecundaria":[datosRegreso objectForKey:@"fotoSecundaria"],@"latitud":[NSNumber numberWithFloat:[locacion coordinate].latitude],@"longitud":[NSNumber numberWithFloat:[locacion coordinate].longitude],@"lugar":nombreLugar,@"objID":[NSNumber numberWithInt:idLugar],@"postOrigen":[NSNumber numberWithInt:[[datosRegreso objectForKey:@"postOrigen"] intValue]]}, @"voto":[[datosRegreso objectForKey:@"votos"] mutableCopy], @"tiempoRestante":[NSNumber numberWithInt:0]} mutableCopy]];
            
            [checkinVC setCheckinDone];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }];
    
    [uploadTask resume];
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
