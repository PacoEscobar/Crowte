//
//  Scroller.m
//  Crowte
//
//  Created by Paco Escobar on 04/01/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "Scroller.h"

@implementation Scroller

@synthesize scroll;

-(void)viewDidLoad{

    [super viewDidLoad];
    
    [scroll setContentSize:CGSizeMake(1500, 400)];
    
    [scroll setScrollEnabled:YES];
    
    
}

-(void)viewDidLayoutSubviews{

    [scroll setContentSize:CGSizeMake(1500, 300)];
    
    NSLog(@"tamaño %f y es %f", scroll.bounds.size.height, scroll.contentSize.height);
}

@end
