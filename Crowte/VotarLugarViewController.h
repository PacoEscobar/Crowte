//
//  VotarLugarViewController.h
//  Crowte
//
//  Created by Paco Escobar on 24/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CamaraOverlayViewController.h"
#import "CamaraOverlayView.h"

@interface VotarLugarViewController : UIViewController <UITextViewDelegate, CamaraOverlayViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NSURLSessionTaskDelegate, UITableViewDelegate, UITableViewDataSource>

@property int idLugar;
@property int fotoVideo;
@property unsigned long totalBytesArchivoAsubir;
@property NSData *dataFotoVideo;
@property NSMutableDictionary *votos;
@property NSString *tutoTomarFotoVideo;

@property NSMutableDictionary *opcionesSeleccionadas;

@property (strong, nonatomic) IBOutlet UIButton *botonVotar;
@property (strong, nonatomic) IBOutlet UITextView *campoComentario;
@property (strong, nonatomic) IBOutlet UIButton *votoPositivo;
@property (strong, nonatomic) IBOutlet UIButton *votoNegativo;
@property (strong, nonatomic) IBOutlet UIView *divisionVotos;
@property (strong, nonatomic) IBOutlet UIView *fondoBotonesVotos;
@property (strong, nonatomic) IBOutlet UILabel *numeroDeVotos;

@property (weak, nonatomic) IBOutlet UIView *fondoGeneral;

@property (strong, nonatomic) IBOutlet UIView *fondoFotoTomada;

#pragma mark - botones ambiente
@property (weak, nonatomic) IBOutlet UIButton *botonAunlugares;
@property (weak, nonatomic) IBOutlet UIButton *botonLleno;
@property (weak, nonatomic) IBOutlet UIButton *botonMusica;
@property (weak, nonatomic) IBOutlet UIButton *botonNoMusica;
@property (weak, nonatomic) IBOutlet UITableView *tablaOpcionesMood;

#pragma mark - camara
@property UIImagePickerController *picker;
@property CamaraOverlayView *camara;



- (IBAction)votarPositivo:(id)sender;
- (IBAction)votarNegativo:(id)sender;
- (IBAction)enviarVoto:(id)sender;
- (IBAction)tomarFotoVideo:(id)sender;
- (IBAction)cerrarFotoVideoTomado:(id)sender;
- (IBAction)cerrarFondoCerrarFotoVideoTomado:(id)sender;
- (IBAction)mostrarCerrarFotoVideoTomado:(id)sender;
- (void)getTomarFotoTuto;
- (void)mostrarContadorVotos:(float)promVotos;
- (void)dibujarProgresoCircular:(UIImageView*)imagenView progreso:(float)progreso;

#pragma mark - metodos botones ambiente

- (IBAction)botonAFavor:(id)sender;
- (IBAction)botonEnContra:(id)sender;


@end
