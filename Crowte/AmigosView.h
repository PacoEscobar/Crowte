//
//  AmigosView.h
//  Crowte
//
//  Created by Paco Escobar on 24/04/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AmigosView : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property BOOL tieneAmigos;
@property (strong, nonatomic) IBOutlet UITableView *tablaView;
@property (retain, nonatomic) NSMutableArray *arrayDictionarios;
@property (strong, nonatomic) UIActivityIndicatorView *indi;
@property BOOL ninguno;

-(void)getFriends;
-(void)amigosDeFacebook;

@end
