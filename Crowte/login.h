//
//  login.h
//  crowte
//
//  Created by Paco Escobar on 07/05/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "NSString+MD5.h"
#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonDigest.h>

@interface login : UIViewController <UITextFieldDelegate>

@property int loginfails;
@property (weak, nonatomic) IBOutlet UITextField *correo;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UIButton *iniciarS;
@property (weak, nonatomic) IBOutlet UIButton *recordarpass;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLogo;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintFormularioSuperior;

-(IBAction)ocultarTeclado:(id)sender;
-(IBAction)clickFondo:(id)sender;
-(IBAction)checkUsuario:(id)sender;
-(IBAction)cerrarSesion:(id)sender;

@end
