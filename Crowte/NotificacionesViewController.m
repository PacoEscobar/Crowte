//
//  NotificacionesViewController.m
//  Crowte
//
//  Created by Paco Escobar on 22/02/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIImageView+WebCache.h>
#import "NotificacionesViewController.h"
#import "PerfilUsuarioViewController.h"

@interface NotificacionesViewController ()

@end

@implementation NotificacionesViewController

@synthesize notificaciones, tabla, segmentedControl, intervaloGetNotificaciones;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[UINavigationBar appearance] setTranslucent:YES];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"transparente.png"]];
    
    UIView *division = [[UIView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.width, 0.5)];
    
    [division setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.3]];
    
    [division setTag:1];
    
    [self.navigationController.navigationBar addSubview:division];
    
    self.navigationItem.title = NSLocalizedString(@"Notificaciones", nil);
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [[UISegmentedControl appearance] setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [[UISegmentedControl appearance] setDividerImage:[UIImage imageNamed:@"transparente.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithWhite:1 alpha:0.5],NSForegroundColorAttributeName,[UIFont fontWithName:@"Hind-Light" size:15.0], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Hind-Medium" size:15.0], NSFontAttributeName, nil] forState:UIControlStateSelected];
    
    [self getNotificaciones];
    
   // intervaloGetNotificaciones = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(intervaloNotificaciones) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if(segmentedControl.selectedSegmentIndex == 0){
        
        unsigned long numCeldas;
        
        if([[notificaciones objectForKey:@"notificaciones"] isEqual:[NSNull null]]){
        
            numCeldas = 0;
        }else{
            
            numCeldas = [[notificaciones objectForKey:@"notificaciones"] count];
        }
        
        return numCeldas;
    }else{
    
        unsigned long numCeldas;
        
        if([[notificaciones objectForKey:@"solicitudesAmistad"] isEqual:[NSNull null]]){
        
            numCeldas = 0;
        }else{
        
            numCeldas = [[notificaciones objectForKey:@"solicitudesAmistad"] count];
        }
        
        return numCeldas;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if(segmentedControl.selectedSegmentIndex == 0){
        
        NSError *error;
        
        NSString *palabra = [[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"codigo"];
        
        NSRange rango = NSMakeRange(0, [palabra length]);
        
        NSRegularExpression *comentario = [[NSRegularExpression alloc] initWithPattern:@"^(\\d)+(COM)$" options:NSRegularExpressionCaseInsensitive error:&error];
        
        NSRegularExpression *likes = [[NSRegularExpression alloc] initWithPattern:@"^(\\d)+(LIKE)$" options:NSRegularExpressionCaseInsensitive error:&error];
        
        NSRegularExpression *aceptado = [[NSRegularExpression alloc] initWithPattern:@"^(\\d)+(ADD)$" options:NSRegularExpressionCaseInsensitive error:&error];
        
        NSArray *comentarios = [comentario matchesInString:palabra options:0 range:rango];
        
        NSArray *like = [likes matchesInString:palabra options:0 range:rango];
        
        NSArray *aceptados = [aceptado matchesInString:palabra options:0 range:rango];
        
        if([comentarios count] > 0){
            
            return 87;
        }else if([like count] > 0){
            
            if(![[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]]){
                
                if(![[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"mensaje"] isEqualToString:@""]){
                    
                    return 389;
                }else{
                    
                    return 319;
                }
                
            }else{
                
                return 209;
            }
            
        }else if([aceptados count] > 0){
            
            return 87;
        }
        
        return 0;
    }else{
    
        return 145;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda;
    
    if(segmentedControl.selectedSegmentIndex == 0){
        
        NSError *error;
        
        NSString *palabra = [[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"codigo"];
        
        NSRange rango = NSMakeRange(0, [palabra length]);
        
        NSRegularExpression *comentario = [[NSRegularExpression alloc] initWithPattern:@"^(\\d)+(COM)$" options:NSRegularExpressionCaseInsensitive error:&error];
        
        NSRegularExpression *likes = [[NSRegularExpression alloc] initWithPattern:@"^(\\d)+(LIKE)$" options:NSRegularExpressionCaseInsensitive error:&error];
        
        NSRegularExpression *aceptado = [[NSRegularExpression alloc] initWithPattern:@"^(\\d)+(ADD)$" options:NSRegularExpressionCaseInsensitive error:&error];
        
        NSArray *comentarios = [comentario matchesInString:palabra options:0 range:rango];
        
        NSArray *like = [likes matchesInString:palabra options:0 range:rango];
        
        NSArray *aceptados = [aceptado matchesInString:palabra options:0 range:rango];
        
        if([comentarios count] > 0){
            
        }else if([like count] > 0){
            
            celda = [tableView dequeueReusableCellWithIdentifier:@"celdaNotificacionLike"];
            
            UILabel *notificacion = (UILabel*)[celda viewWithTag:3];
            
            [notificacion setText:@"Le ha gustado tu comentario"];
            
            if(![[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]]){
                
                UIImageView *fotoPerfil = (UIImageView*)[celda viewWithTag:4];
                
                [fotoPerfil setHidden:NO];
                
                if([[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"esVideo"] intValue] == 0){
                    
                    [[celda viewWithTag:6] setHidden:YES];
                    
                    [fotoPerfil sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"],[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"]]]];
                    
                }else{
                
                    NSArray *nombreVideoA = [[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] componentsSeparatedByString:@"."];
                    
                    NSString *nombreVideo = [NSString stringWithFormat:@"%@.jpg",[nombreVideoA objectAtIndex:0]];
                    
                    [[celda viewWithTag:6] setHidden:NO];
                    
                    [fotoPerfil sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/thumbnail/%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"],nombreVideo]]];
                }
                
                if(![[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"mensaje"] isEqualToString:@""]){
                    
                    UITextView *texto = (UITextView*)[celda viewWithTag:5];
                    
                    [texto setHidden:NO];
                    
                    [texto setTextContainerInset:UIEdgeInsetsMake(20, 20, 20, 20)];
                    
                    [texto setTranslatesAutoresizingMaskIntoConstraints:NO];
                    
                    NSString *mensaje = [[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"mensaje"];
                    
                    NSData *data = [mensaje dataUsingEncoding:NSUTF8StringEncoding];
                    
                    mensaje = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                    
                    mensaje = [mensaje stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    [texto setText:mensaje];
                    
                    [texto setContentOffset:CGPointZero];
                }else{
                    
                    UITextView *texto = (UITextView*)[celda viewWithTag:5];
                    
                    [texto setHidden:YES];
                }
                
            }else{
                
                UIImageView *fotoPerfil = (UIImageView*)[celda viewWithTag:4];
                
                [fotoPerfil setHidden:YES];
                
                NSArray *constraints = [[celda contentView] constraints];
                
                for (NSLayoutConstraint *constraint in constraints) {
                    
                    if([[constraint identifier] isEqualToString:@"constraintTextoSuperior"]){
                        
                        [constraint setConstant:19];
                    }
                }
                
                UITextView *texto = (UITextView*)[celda viewWithTag:5];
                
                [texto setTextContainerInset:UIEdgeInsetsMake(20, 20, 20, 20)];
                
                [texto setTranslatesAutoresizingMaskIntoConstraints:NO];
                
                NSString *mensaje = [[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"mensaje"];
                
                NSData *data = [mensaje dataUsingEncoding:NSUTF8StringEncoding];
                
                mensaje = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                
                mensaje = [mensaje stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                [texto setText:mensaje];
                
                [texto setContentOffset:CGPointZero];
                
            }
            
        }else if([aceptados count] > 0){
            
            celda = [tableView dequeueReusableCellWithIdentifier:@"celdaNotificacionAceptado"];
            
            UILabel *notificacion = (UILabel*)[celda viewWithTag:3];
            
            [notificacion setText:@"Ha aceptado tu solicitud de amistad"];
        }
        
        UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldapequeñanuevo.png"]];
        
        [celda setBackgroundView:fondo];
        [celda setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *fotoPrincipal = (UIImageView*)[celda viewWithTag:1];
        
        [[fotoPrincipal layer] setBorderWidth:1];
        [[fotoPrincipal layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
        [[fotoPrincipal layer] setCornerRadius:25];
        [[fotoPrincipal layer] setMasksToBounds:YES];
        
        [fotoPrincipal sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue],[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"] completed:^(UIImage *imag, NSError *error, SDImageCacheType tipo, NSURL *url){
            
            if(error != nil){
                [fotoPrincipal setContentMode:UIViewContentModeCenter];
                [[fotoPrincipal layer] setBorderColor:[[UIColor whiteColor] CGColor]];
            }else{
                
                [fotoPrincipal setContentMode:UIViewContentModeScaleToFill];
                [[fotoPrincipal layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
            }
        }];
        
        UILabel *nombre = (UILabel*)[celda viewWithTag:2];
        
        NSString *nombreyapellido;
        
        if([[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"apellido"] isEqualToString:@"(null)"]){
            
            nombreyapellido = [[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"nombre"];
        }else{
            
            nombreyapellido = [NSString stringWithFormat:@"%@ %@",[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"apellido"]];
        }
        
        [nombre setText:nombreyapellido];
        
    }else{
    
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaSolicitudAmistad"];
        
        UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldapequeñanuevo.png"]];
        
        [celda setBackgroundView:fondo];
        [celda setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *fotoPrincipal = (UIImageView*)[celda viewWithTag:1];
        
        [[fotoPrincipal layer] setBorderWidth:1];
        [[fotoPrincipal layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
        [[fotoPrincipal layer] setCornerRadius:25];
        [[fotoPrincipal layer] setMasksToBounds:YES];
        
        [fotoPrincipal sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[[[notificaciones objectForKey:@"solicitudesAmistad"] objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue],[[[notificaciones objectForKey:@"solicitudesAmistad"] objectAtIndex:indexPath.row] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"] completed:^(UIImage *imag, NSError *error, SDImageCacheType tipo, NSURL *url){
            
            if(error != nil){
                [fotoPrincipal setContentMode:UIViewContentModeCenter];
                [[fotoPrincipal layer] setBorderColor:[[UIColor whiteColor] CGColor]];
            }else{
                
                [fotoPrincipal setContentMode:UIViewContentModeScaleToFill];
                [[fotoPrincipal layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
            }
        }];
        
        UILabel *nombre = (UILabel*)[celda viewWithTag:2];
        
        NSString *nombreyapellido;
        
        if([[[[notificaciones objectForKey:@"solicitudesAmistad"] objectAtIndex:indexPath.row] objectForKey:@"apellido"] isEqualToString:@"(null)"]){
            
            nombreyapellido = [[[notificaciones objectForKey:@"solicitudesAmistad"] objectAtIndex:indexPath.row] objectForKey:@"nombre"];
        }else{
            
            nombreyapellido = [NSString stringWithFormat:@"%@ %@",[[[notificaciones objectForKey:@"solicitudesAmistad"] objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[[notificaciones objectForKey:@"solicitudesAmistad"] objectAtIndex:indexPath.row] objectForKey:@"apellido"]];
        }
        
        [nombre setText:nombreyapellido];
        
        UIButton *botonAceptar = (UIButton*)[celda viewWithTag:4];
        
        [[botonAceptar layer] setBorderWidth:1];
        [[botonAceptar layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
        [[botonAceptar layer] setCornerRadius:5];
        [[botonAceptar layer] setMasksToBounds:YES];
        
        [botonAceptar addTarget:self action:@selector(aceptarAmigo:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *botonCancelar = (UIButton*)[celda viewWithTag:5];
        
        [[botonCancelar layer] setBorderWidth:1];
        [[botonCancelar layer] setBorderColor:[[UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1] CGColor]];
        [[botonCancelar layer] setCornerRadius:5];
        [[botonCancelar layer] setMasksToBounds:YES];
        
        [botonCancelar addTarget:self action:@selector(cancelarAmigo:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return celda;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda = [tableView cellForRowAtIndexPath:indexPath];
    
    if([[celda reuseIdentifier] isEqualToString:@"celdaNotificacionAceptado"]){
            
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PerfilUsuarioViewController *perfil = [storyboard instantiateViewControllerWithIdentifier:@"perfilUsuario"];
        
        [perfil setIdUsuario:[[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue]];
        
        [perfil setNombreUsuario:[NSString stringWithFormat:@"%@ %@",[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"nombre"] ,[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"apellido"]]];
        
        [perfil setFotoUsuario:[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"foto"]];
        
        [tabla deselectRowAtIndexPath:indexPath animated:YES];
        
        UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
        self.navigationItem.backBarButtonItem = atras;
        
        [self.navigationController pushViewController:perfil animated:YES];
        
    }
}

#pragma mark - hechos por mi

- (IBAction)cambiarNotificacion:(id)sender {
    
    [tabla reloadData];
}

-(void)getNotificaciones{
    
    __block NSDictionary *notificacionesNuevas;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getnotificaciones.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"id=%@&max=%d",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"],10] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){
            
            if(data != nil){
                
                NSError *error;
                
                notificacionesNuevas = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                if(![notificaciones isEqual:notificacionesNuevas]){

                    dispatch_async(dispatch_get_main_queue(), ^{
                        notificaciones = notificacionesNuevas;
                        [tabla reloadData];
                    });
                }

            }
        }
        
    }];
    
    [uploadTask resume];
}

-(IBAction)aceptarAmigo:(id)sender{

    UIButton *botonAceptar = (UIButton*)sender;
    
    CGPoint posicionBoton = [botonAceptar convertPoint:CGPointZero toView:tabla];
    
    NSIndexPath *indexPath = [tabla indexPathForRowAtPoint:posicionBoton];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/aceptarsolicitudamistad.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"es=si&solnte=%@&solado=%d",[[[notificaciones objectForKey:@"solicitudesAmistad"] objectAtIndex:indexPath.row] objectForKey:@"ID"],[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
    }];
    
    [uploadTask resume];
    
    [[notificaciones objectForKey:@"solicitudesAmistad"] removeObjectAtIndex:indexPath.row];
    
    [tabla beginUpdates];
    [tabla deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    [tabla endUpdates];
    
}

-(IBAction)cancelarAmigo:(id)sender{

    UIButton *botonCancelar = (UIButton*)sender;
    
    CGPoint posicionBoton = [botonCancelar convertPoint:CGPointZero toView:tabla];
    
    NSIndexPath *indexPath = [tabla indexPathForRowAtPoint:posicionBoton];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/aceptarsolicitudamistad.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"es=no&solnte=%@&solado=%d",[[[notificaciones objectForKey:@"solicitudesAmistad"] objectAtIndex:indexPath.row] objectForKey:@"ID"],[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
    }];
    
    [uploadTask resume];
    
    [[notificaciones objectForKey:@"solicitudesAmistad"] removeObjectAtIndex:indexPath.row];
    
    [tabla beginUpdates];
    [tabla deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
    [tabla endUpdates];
}

-(void)intervaloNotificaciones{

    [self getNotificaciones];
}

#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
