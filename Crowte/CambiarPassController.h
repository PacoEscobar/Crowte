//
//  CambiarPassController.h
//  Crowte
//
//  Created by Paco Escobar on 20/05/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CambiarPassController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nuevaContra;
@property (weak, nonatomic) IBOutlet UITextField *viejaContra;



-(void)cambiar;

@end
