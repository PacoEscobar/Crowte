//
//  PerfilMioViewController.h
//  Crowte
//
//  Created by Paco Escobar on 10/02/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface PerfilMioViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSURLSessionDelegate, NSURLSessionDownloadDelegate, CLLocationManagerDelegate>

@property BOOL locationContador;
@property float navBarYInicial;

@property CLLocationManager *locationManager;
@property CLGeocoder *geocoder;

@property NSDictionary *datosMios;
@property NSDictionary *checkins;
@property (strong, nonatomic) IBOutlet UITableView *tabla;



-(void)getCheckins;
-(IBAction)irAConfiguracion:(id)sender;
-(IBAction)join:(id)sender;
-(IBAction)opciones:(id)sender;

@end
