//
//  principal.h
//  crowte
//
//  Created by Paco Escobar on 16/05/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface principal : UIViewController<FBSDKLoginButtonDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *logo;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopLogo;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintLogoCentrado;


-(void)hacer: (BOOL) animated;

@property (strong,nonatomic) UIApplication *app;

@end
