//
//  CustomPinAnnotation.m
//  Crowte
//
//  Created by Paco Escobar on 15/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import "CustomPinAnnotation.h"

@implementation CustomPinAnnotation

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation type:(BOOL)tipo idUsuario:(int)idUsuario lugar:(NSString *)lugar persona:(NSString *)persona fotoPersona:(NSString *)fotoPersona fotoLugar:(NSString *)fotoLugar fondoLugar:(NSString *)fondoLugar{
    
    self = [super initWithAnnotation:annotation reuseIdentifier:nil];
    
    self.idUsuario = idUsuario;
    self.enVivo = tipo;
    self.lugar = lugar;
    self.persona = persona;
    self.fotoPersona = fotoPersona;
    self.fotoLugar = fotoLugar;
    self.fondoLugar = fondoLugar;
    
    
    UIView *accesorio = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [accesorio setBackgroundColor:[UIColor blackColor]];
    
    
    self.canShowCallout = YES;
    self.detailCalloutAccessoryView = accesorio;
    
    if(tipo){
    
        [self setImage:[UIImage imageNamed:@"pin.png"]];
    }else{
    
        [self setImage:[UIImage imageNamed:@"pinv.png"]];
    }
    
    return self;
}

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent*)event
{
    UIView* hitView = [super hitTest:point withEvent:event];
    if (hitView != nil)
    {
        [self.superview bringSubviewToFront:self];
    }
    return hitView;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent*)event
{
    CGRect rect = self.bounds;
    BOOL isInside = CGRectContainsPoint(rect, point);
    if(!isInside)
    {
        for (UIView *view in self.subviews)
        {
            isInside = CGRectContainsPoint(view.frame, point);
            if(isInside)
                break;
        }
    }
    return isInside;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
