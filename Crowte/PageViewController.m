//
//  PageViewController.m
//  Crowte
//
//  Created by Paco Escobar on 19/01/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "PageViewController.h"
#import "TutorialPrincipal.h"

@implementation PageViewController

@synthesize datosUsuario;

-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    TutorialPrincipal *startingViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContentPageController"];
    
    [self.pageViewController setViewControllers:@[startingViewController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    
    NSUInteger index = ((TutorialPrincipal *) viewController).index;
    
    if((index == 0) || (index == NSNotFound)){
    
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{

    NSUInteger index = ((TutorialPrincipal *) viewController).index;
    
    if(index == NSNotFound){
    
        return nil;
    }
    
    index++;
    
    if(index == 4){
    
        return nil;
    }
    
   return [self viewControllerAtIndex:index];
}

-(TutorialPrincipal *)viewControllerAtIndex:(NSUInteger)index{

    if(index >= 5){
    
        return nil;
    }

    TutorialPrincipal *tutorial;
    
    if(index == 0){
        tutorial = [self.storyboard instantiateViewControllerWithIdentifier:@"ContentPageController"];
        
        tutorial.index = index;
        
    }
    
    if(index == 1){
    
        tutorial = [self.storyboard instantiateViewControllerWithIdentifier:@"tutouno"];
        
        tutorial.index = index;
    }
    
    if(index == 2){
        
        tutorial = [self.storyboard instantiateViewControllerWithIdentifier:@"tutodos"];
        
        tutorial.index = index;
    }
    
    if(index == 3){
        
        tutorial = [self.storyboard instantiateViewControllerWithIdentifier:@"tutotres"];
        
        tutorial.index = index;
        tutorial.datosUsuario = datosUsuario;
    }
    
    return tutorial;
}

-(NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController{

    return 4;
}

-(NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController{

    return 0;
}

@end
