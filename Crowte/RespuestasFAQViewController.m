//
//  RespuestasFAQViewController.m
//  Crowte
//
//  Created by Paco Escobar on 14/07/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "RespuestasFAQViewController.h"

@interface RespuestasFAQViewController ()

@end

@implementation RespuestasFAQViewController

@synthesize idpregunta, carga, web;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/pgrespuesta.php?id=%d",idpregunta]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [web loadRequest:request];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - webview

-(void)webViewDidFinishLoad:(UIWebView *)webView{

    [carga stopAnimating];
    [carga removeFromSuperview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
