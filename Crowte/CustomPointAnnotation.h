//
//  CustomPointAnnotation.h
//  Crowte
//
//  Created by Paco Escobar on 15/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface CustomPointAnnotation : MKPointAnnotation

@property BOOL enVivo;
@property int idUsuario;
@property NSString *lugar;
@property NSString *persona;
@property NSString *fotoPersona;
@property NSString *fondoLugar;
@property NSString *fotoLugar;

@end
