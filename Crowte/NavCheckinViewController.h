//
//  NavCheckinViewController.h
//  Crowte
//
//  Created by Paco Escobar on 23/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavCheckinViewController : UINavigationController

@property NSMutableDictionary *datosCheckin;

@end
