//
//  CondicionesLugaresViewController.h
//  Crowte
//
//  Created by Paco Escobar on 27/09/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CondicionesLugaresViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *condicionesContainer;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;

- (IBAction)regresar:(id)sender;

@end
