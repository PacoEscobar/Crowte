//
//  CambiarFotoPerfil.h
//  Crowte
//
//  Created by Paco Escobar on 20/01/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConfiguracionDelegate.h"

@interface CambiarFotoPerfil : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, ConfiguracionDelegate>


@property UIImage *foto;
@property NSData *dataDeFoto;
@property (weak, nonatomic) IBOutlet UIButton *tomaFoto;
@property (weak, nonatomic) IBOutlet UIButton *seleccionaFoto;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopSeparador;


@property (nonatomic, assign) id<ConfiguracionDelegate> delegate;

-(IBAction)tomarfoto:(id)sender;
-(IBAction)seleccionarFoto:(id)sender;

@end
