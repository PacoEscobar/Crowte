//
//  AyudaYSoporteViewController.m
//  Crowte
//
//  Created by Paco Escobar on 12/07/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "AyudaYSoporteViewController.h"
#import "RespuestasFAQViewController.h"

@interface AyudaYSoporteViewController ()

@end

@implementation AyudaYSoporteViewController

@synthesize textField, preguntas, tabla;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    preguntas = [NSMutableArray array];
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getfaq.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@""] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut){
            
            UIAlertController *noconexion = [UIAlertController alertControllerWithTitle:nil message:@"Estamos teniendo algunos problemas, por favor intenta reiniciando la app nuevamente." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
            
            [noconexion addAction:dismiss];
            
            [self presentViewController:noconexion animated:YES completion:nil];
            
        }else if(error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            UIAlertController *noconexion = [UIAlertController alertControllerWithTitle:nil message:@"Ha ocurrido un error, por favor verifica tu conexión a Internet." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
            
            [noconexion addAction:dismiss];
            
            [self presentViewController:noconexion animated:YES completion:nil];
            
        }else{
        
            NSDictionary *listapreguntas = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            for (NSDictionary *iteracion in listapreguntas){
                
                [preguntas addObject:iteracion];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [tabla reloadData];
            });

        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    NSInteger numero;
    
    if([preguntas count] == 0){
    
        numero = 1;
    }else{
    
        numero = [preguntas count];
    }
    
    return numero;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    float size;
    
    if([preguntas count] == 0){
    
        size = 53.0;
    }else{
    
        float numeroCaracteres = [[[preguntas objectAtIndex:indexPath.row] objectForKey:@"pregunta"] length];
        
        if(numeroCaracteres == 0){
            
            numeroCaracteres = 30;
        }
            
        numeroCaracteres = numeroCaracteres/30;
            
        numeroCaracteres = floorf(numeroCaracteres);
            
        numeroCaracteres *= 21;
        
        
        size = 53.0+numeroCaracteres;
    }
    
    return size;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda;
    
    if([preguntas count] == 0){
        celda = [tableView dequeueReusableCellWithIdentifier:@"cargando"];
        [tabla setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }else{
        
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaPregunta"];
        
        UILabel *labelPregunta = (UILabel*)[celda viewWithTag:1];
        
        labelPregunta.text = [[preguntas objectAtIndex:indexPath.row] objectForKey:@"pregunta"];
        
        [labelPregunta sizeToFit];
        [tabla setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
   
    }
    
    return celda;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
   
    UITableViewCell *celda = (UITableViewCell*)sender;
    
    NSIndexPath *indexPath = [tabla indexPathForCell:celda];
    
    RespuestasFAQViewController *respuesta = (RespuestasFAQViewController *)segue.destinationViewController;
    
    respuesta.idpregunta = [[[preguntas objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue];
    
    [tabla deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - por mi

- (IBAction)enviarPregunta:(id)sender {
    
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    [textField setEnabled:NO];
    
    if(!([[[textField text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)){
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/enviarpregunta.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%@&pregunta=%@&token=%@",[defaults objectForKey:@"identifier"], [textField text],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if(error.code == NSURLErrorTimedOut){
            
                UIAlertController *noconexion = [UIAlertController alertControllerWithTitle:nil message:@"Estamos teniendo algunos problemas, por favor intenta reiniciando la app nuevamente." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
                
                [noconexion addAction:dismiss];
                
                [self presentViewController:noconexion animated:YES completion:nil];
                
            }else if(error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
                UIAlertController *noconexion = [UIAlertController alertControllerWithTitle:nil message:@"Ha ocurrido un error, por favor verifica tu conexión a Internet." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
                
                [noconexion addAction:dismiss];
                
                [self presentViewController:noconexion animated:YES completion:nil];
                
            }else{
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    app.networkActivityIndicatorVisible = NO;
                    [textField setEnabled:YES];
                    [textField setText:@""];
                });
            }
        }];
    }
}

#pragma mark - textfield

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [self enviarPregunta:nil];
    
    return YES;
}
@end
