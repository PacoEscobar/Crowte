//
//  UsuariosBloqueadosViewController.m
//  Crowte
//
//  Created by Paco Escobar on 10/07/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "UsuariosBloqueadosViewController.h"

@interface UsuariosBloqueadosViewController ()

@end

@implementation UsuariosBloqueadosViewController

@synthesize listaBloqueados, defaults, tabla;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    listaBloqueados = [NSMutableArray array];
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *url = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/bloqueos/getlistabloqueados.php"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    
    [url setHTTPMethod:@"POST"];
    [url setHTTPBody:[[NSString stringWithFormat:@"id=%@&token=%@",[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:url queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
           
        }else{
            
            NSDictionary *listaUsuariosBloqueados = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            for (NSDictionary *iteracion in listaUsuariosBloqueados){
                
                [listaBloqueados addObject:iteracion];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [tabla reloadData];
            });
            
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    NSInteger numero;
    
    if([listaBloqueados count] == 0 || [[listaBloqueados objectAtIndex:0] objectForKey:@"existen"]){
    
        numero = 1;
    }else{
    
        numero = [listaBloqueados count];
    }
    
    return numero;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    float size;
    
    if([listaBloqueados count] == 0){
    
        size = 79;
    }else if([[listaBloqueados objectAtIndex:0] objectForKey:@"existen"]){
    
        size = 57;
    }else{
    
        size = 92;
    }
    
    return size;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda;
    
    if([listaBloqueados count] == 0){
        
        celda = [tableView dequeueReusableCellWithIdentifier:@"cargando"];
        
    }else if([[listaBloqueados objectAtIndex:0] objectForKey:@"existen"]){
    
        celda = [tableView dequeueReusableCellWithIdentifier:@"nohay"];
    }else{

        celda = [tableView dequeueReusableCellWithIdentifier:@"usuariobloqueado"];
        
        UILabel *nombre = (UILabel*)[celda viewWithTag:1];
        
        nombre.text = [NSString stringWithFormat:@"%@ %@",[[listaBloqueados objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[listaBloqueados objectAtIndex:indexPath.row] objectForKey:@"apeido"]];
        
        UIButton *desbloquear = (UIButton*)[celda viewWithTag:2];
        
        [desbloquear addTarget:self action:@selector(desbloquear:) forControlEvents:UIControlEventTouchUpInside];
    }
    return celda;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - hechos por mi

-(IBAction)desbloquear:(id)sender{

    UITableViewCell *celda = (UITableViewCell*)[[sender superview] superview];
    
    NSIndexPath *indexpath = [tabla indexPathForCell:celda];
    
    NSMutableURLRequest *url = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/bloqueos/desbloquear.php"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    
    [url setHTTPMethod:@"POST"];
    [url setHTTPBody:[[NSString stringWithFormat:@"bloqueador=%@&usuario=%@&token=%@",[defaults objectForKey:@"identifier"],[[listaBloqueados objectAtIndex:indexpath.row] objectForKey:@"ID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:url queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet){
            
            
        }else{
        
            dispatch_async(dispatch_get_main_queue(), ^{
        
                [listaBloqueados removeObjectAtIndex:indexpath.row];
                
                if([listaBloqueados count] == 0){
                
                    [listaBloqueados addObject:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:0] forKey:@"existen"]];
                    
                    [tabla reloadData];
                }else{
                
                    [tabla deleteRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
                }
                
            });
        }
    }];
}

@end
