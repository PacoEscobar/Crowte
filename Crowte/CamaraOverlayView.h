//
//  CamaraOverlayView.h
//  Crowte
//
//  Created by Paco Escobar on 24/07/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CamaraOverlayView : UIView

@property UIButton *trigger;
@property UIButton *cambiarCamara;
@property UIButton *flash;
@property UIButton *tacha;

@end
