//
//  ReclamarLugarV1ViewController.m
//  Crowte
//
//  Created by Paco Escobar on 23/03/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import "ReclamarLugarV1ViewController.h"
#import "ReclamarLugarV2ViewController.h"
#import "CondicionesLugaresViewController.h"

@interface ReclamarLugarV1ViewController ()

@end

@implementation ReclamarLugarV1ViewController

@synthesize id_lugar, textViewCondiciones;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSMutableAttributedString *stringCondiciones = textViewCondiciones.attributedText.mutableCopy;
    
    [stringCondiciones setAttributes:@{NSLinkAttributeName:@"condiciones"} range:NSMakeRange(33, 33)];
    
    textViewCondiciones.attributedText = stringCondiciones;
    textViewCondiciones.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)reclamarLugar:(id)sender {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/pedirreclamolugar.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"id=%@&lugar=%d",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"],id_lugar] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){
            
            if(data != nil){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

                    ReclamarLugarV2ViewController *reclamarLugar = [storyboard instantiateViewControllerWithIdentifier:@"reclamarLugarV2"];
                    
                    [self presentViewController:reclamarLugar animated:YES completion:nil];
                    
                });
                
            }
        }
        
    }];
    
    [uploadTask resume];
}

- (IBAction)cancelar:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - textview 

-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    NSLog(@"interact");
    if([URL.absoluteString isEqualToString:@"condiciones"]){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        CondicionesLugaresViewController *condicionesLugar = [storyboard instantiateViewControllerWithIdentifier:@"condicionesLugares"];
        
        [self presentViewController:condicionesLugar animated:YES completion:nil];
        
    }
    
    return NO;
}

#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}
@end
