//
//  CheckinViewController.m
//  Crowte
//
//  Created by Paco Escobar on 18/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>
#import <Lottie/Lottie.h>
#import "CheckinViewController.h"
#import "PublicaCheckinViewController.h"
#import "CheckinDoneViewController.h"
#import "VotarLugarViewController.h"
#import "AgregaNuevoLugarController.h"
#import "ImagenPubLugarViewController.h"
#import "ReportarMensajeTableViewController.h"

@interface CheckinViewController ()

@end

@implementation CheckinViewController

@synthesize buscarLugar, lugaresCercanos, locationManager, locationContador, geocoder, tablaLugares, tablaCheckinDone, datosEnLugar, vistaCheckinDone, intervaloTiempoCheckin, mensajes, direccionLugarString, lugaresCercanosSearchPlaceHolder, fondoEmptyState, lupaEmptyState, textoAgregaUno, textoNoSeEncontraron,ultimoMensajeID, intervaloMensajesNuevos, vistaTutorial2;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    ultimoMensajeID = 0;
    
    lugaresCercanos = [NSDictionary dictionary];
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    geocoder = [[CLGeocoder alloc] init];
    locationContador = NO;
    
    UIView *globoDialogo = [self.view viewWithTag:100];
    [[globoDialogo layer] setCornerRadius:20];
    [[globoDialogo layer] setMasksToBounds:YES];
    
    UITapGestureRecognizer *cerrarTuto2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cerrarLugarTutorial:)];
    
    [vistaTutorial2 addGestureRecognizer:cerrarTuto2];
    
    lugaresCercanosSearchPlaceHolder = [NSArray array];
    
    mensajes = [NSMutableArray array];
    
    if([[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"status"] isEqualToString:@"no"]){
    
        [locationManager startUpdatingLocation];
    }
    
    [[UINavigationBar appearance] setTranslucent:YES];
    
    self.navigationItem.title = @"Check-in";
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"transparente.png"]];
    
    UIImage *imageAdd = [UIImage imageNamed:@"registrarlugar.png"];
    
    UIButton *addbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imageAdd.size.width, imageAdd.size.height)];
    
    [addbtn setImage:imageAdd forState:UIControlStateNormal];
    
    [addbtn addTarget:self action:@selector(agregarLugar:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barbtnadd = [[UIBarButtonItem alloc] initWithCustomView:addbtn];
    
    self.navigationItem.rightBarButtonItem = barbtnadd;
    
    [buscarLugar setBackgroundImage:[UIImage imageNamed:@"transparente.png"]];
    
    if([[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"status"] isEqualToString:@"no"]){
        
        [vistaCheckinDone setHidden:YES];
    }else{
    
        [self.navigationController setNavigationBarHidden:YES];
        
        intervaloTiempoCheckin = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(checarTiempoCheckin) userInfo:nil repeats:YES];
        
        [intervaloTiempoCheckin fire];
        
        [self getMensajes];
        [self getTutorialPublicarAmbiente];
    }
    
    intervaloMensajesNuevos = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getMensajesNuevos) userInfo:nil repeats:YES];
    
}

-(void)viewDidAppear:(BOOL)animated{

    if([[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"status"] isEqualToString:@"no"]){
        locationContador = NO;
        [locationManager startUpdatingLocation];
    }else{
    
        intervaloMensajesNuevos = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getMensajesNuevos) userInfo:nil repeats:YES];
    }
    
    if([self.view viewWithTag:1000]){
    
        LOTAnimationView *fuegosArtificiales = (LOTAnimationView*) [[self.view viewWithTag:1000] viewWithTag:1];
        
        if(![fuegosArtificiales isAnimationPlaying]){
        
            [fuegosArtificiales play];
        }
    }
}

-(void)viewDidDisappear:(BOOL)animated{

    [intervaloMensajesNuevos invalidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview metodos

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    if(tableView == tablaLugares){
    
        return 50;
    }else{
    
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    if(tableView == tablaLugares){
        UIView *vista = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        
        [vista setBackgroundColor:[[tableView backgroundColor] colorWithAlphaComponent:0.8]];
        
        UILabel *titulo = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
        
        [titulo setTextColor:[UIColor whiteColor]];
        [titulo setFont:[UIFont fontWithName:@"Hind-Light" size:18]];
        
        [titulo setText:NSLocalizedString(@"Elige el lugar en donde estás", nil) ];
        
        [titulo setTextAlignment:NSTextAlignmentCenter];
        
        [vista addSubview:titulo];
        
        return vista;
    }else{
        
        return nil;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if(tableView == tablaLugares){
        if(lugaresCercanos == nil || [lugaresCercanos count] == 0){
            return 0;
        }else{
            
            return [[lugaresCercanos objectForKey:@"cantidad"] intValue];
        }
    }else{

        return [mensajes count]+1;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if(tableView == tablaLugares){
        return 134;
    }else{

        if(indexPath.row == 0){
        
            //return [tablaCheckinDone frame].size.height;
            return vistaCheckinDone.frame.size.height-20;
            
        }else{
        
            NSString *texto = [[NSString alloc] initWithData:[[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensaje"] dataUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
            
            texto = [texto stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            float adicional = 0;
            
            if(![texto isEqualToString:@""]){
                
                if([texto length] > 44){
                    
                    adicional = ceil([texto length]/44.0);
                    adicional = adicional*55;
                }else{
                    
                    adicional = 55;
                }
            }
            
            if([[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]]){

                return 166+adicional;
            }else{

                return 375+adicional;
            }
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *celda;

    if(tableView == tablaLugares){
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaLugar"];
        
        if(lugaresCercanos != nil && [lugaresCercanos count] > 0){
            
            UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldanuevo.png"]];
            
            
            UIImageView *imagenPrincipalLugar = (UIImageView*)[celda viewWithTag:1];
            
            [imagenPrincipalLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
            
            [[imagenPrincipalLugar layer] setBorderWidth:1];
            [[imagenPrincipalLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
            [[imagenPrincipalLugar layer] setCornerRadius:50];
            [[imagenPrincipalLugar layer] setMasksToBounds:YES];
            
            if([[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"foto"] isEqualToString:@"tierra.png"]){
                
                [imagenPrincipalLugar setContentMode:UIViewContentModeCenter];
                [imagenPrincipalLugar setImage:[UIImage imageNamed:@"cblancav3.png"]];
            }else{
                
                [imagenPrincipalLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"ID"],[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){

                    if(error.code == 0){
                    
                        [imagenPrincipalLugar setContentMode:UIViewContentModeScaleToFill];
                        [imagenPrincipalLugar.layer setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                    }else{
                    
                        [imagenPrincipalLugar setContentMode:UIViewContentModeCenter];
                        [imagenPrincipalLugar.layer setBorderColor:[[UIColor whiteColor] CGColor]];
                    }
                }];
            }
            
            UIImageView *imagenSecundariaLugar = (UIImageView*)[celda viewWithTag:2];
            
            [imagenSecundariaLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"ID"],[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
            
            UILabel *nombreLugar = (UILabel*)[celda viewWithTag:3];
            [nombreLugar setText:[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
            
            
            UILabel *direccionLugar = (UILabel*)[celda viewWithTag:4];
            
            CLLocation *locacion = [[CLLocation alloc] initWithLatitude:[[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"latitud"] floatValue] longitude:[[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"longitud"] floatValue]];
            
            [geocoder reverseGeocodeLocation:locacion completionHandler:^(NSArray *placemarks, NSError *error){
                
                CLPlacemark *placemark = [placemarks lastObject];
                
                [direccionLugar setText:[NSString stringWithFormat:@"%@, %@, %@",[[placemark addressDictionary] objectForKey:@"Name"],[[placemark addressDictionary] objectForKey:@"City"],[[placemark addressDictionary] objectForKey:@"State"]]];
            }];
            
            [celda setBackgroundView:fondo];
            [celda setBackgroundColor:[UIColor clearColor]];
        }
    }else if([tableView isEqual: tablaCheckinDone]){
        
        if(indexPath.row == 0){
            
            celda = [tableView dequeueReusableCellWithIdentifier:@"celdaCheckinDone"];
            
            UIImageView *p1Tutorial = (UIImageView*)[celda viewWithTag:100];
            UIImageView *p2Tutorial = (UIImageView*)[celda viewWithTag:101];
            UIImageView *p3Tutorial = (UIImageView*)[celda viewWithTag:102];
            
            if([[[datosEnLugar objectForKey:@"tutoriales"] objectForKey:@"botonDejarVoto"] intValue] == 0){
                
                [p1Tutorial setHidden:NO];
                [p2Tutorial setHidden:NO];
                [p3Tutorial setHidden:NO];
            }else{
                
                [p1Tutorial removeFromSuperview];
                [p2Tutorial removeFromSuperview];
                [p3Tutorial removeFromSuperview];
            }
            
            
            UIImageView *imagenLugar = (UIImageView*)[celda viewWithTag:1];
            
            [[imagenLugar layer] setBorderWidth:2];
            [[imagenLugar layer] setCornerRadius:60];
            [[imagenLugar layer] setMasksToBounds:YES];
            
            if([[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"foto"] isEqualToString:@"tierra.png"]){
                
                [imagenLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
                
                [[imagenLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
                
                [imagenLugar setImage:[UIImage imageNamed:@"cblancav3.png"]];
            }else{
                
                [[imagenLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
                
                [imagenLugar setContentMode:UIViewContentModeCenter];
                
                
                [imagenLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"objID"],[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){

                    if(error.code != 0){
                    
                        [imagenLugar setContentMode:UIViewContentModeCenter];
                        [imagenLugar.layer setBorderColor:[[UIColor whiteColor] CGColor]];
                    }else{
                    
                        [imagenLugar setContentMode:UIViewContentModeScaleToFill];
                        [[imagenLugar layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                    }
                }];
            }
            
            UIImageView *imagenLugarSecundaria = (UIImageView*)[celda viewWithTag:7];
            
            [imagenLugarSecundaria sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"objID"],[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"fotoSecundaria"]]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];

            UILabel *nombreLugar = (UILabel*)[celda viewWithTag:3];
            
            [nombreLugar setText:[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"lugar"]];
            
            UILabel *direccionLugar = (UILabel*)[celda viewWithTag:4];
            
            [direccionLugar setText:@""];
            
            CLLocation *locacion = [[CLLocation alloc] initWithLatitude:[[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"latitud"] floatValue] longitude:[[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"longitud"] floatValue]];
            
            if(![[direccionLugar text] isEqualToString:@""]){
                
                [direccionLugar setText:direccionLugarString];
            }else{
                
                [geocoder reverseGeocodeLocation:locacion completionHandler:^(NSArray *placemarks, NSError *error){
                    
                    CLPlacemark *placemark = [placemarks lastObject];
                    
                    [direccionLugar setText:[NSString stringWithFormat:@"%@, %@, %@",[[placemark addressDictionary] objectForKey:@"Name"],[[placemark addressDictionary] objectForKey:@"City"],[[placemark addressDictionary] objectForKey:@"State"]]];
                    
                    direccionLugarString = [NSString stringWithFormat:@"%@, %@, %@",[[placemark addressDictionary] objectForKey:@"Name"],[[placemark addressDictionary] objectForKey:@"City"],[[placemark addressDictionary] objectForKey:@"State"]];
                }];
            }
            
            UIButton *votarLugar = (UIButton*)[celda viewWithTag:5];
            
            [[votarLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
            [[votarLugar layer] setBorderWidth:1];
            [[votarLugar layer] setCornerRadius:21.5];
            [[votarLugar layer] setMasksToBounds:YES];
            
            [votarLugar addTarget:self action:@selector(dejarVoto:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *noEstoyAhi = (UIButton*)[celda viewWithTag:6];
            
            [[noEstoyAhi layer] setBorderColor:[[UIColor whiteColor] CGColor]];
            [[noEstoyAhi layer] setBorderWidth:1];
            [[noEstoyAhi layer] setCornerRadius:21.5];
            [[noEstoyAhi layer] setMasksToBounds:YES];
            
            [noEstoyAhi addTarget:self action:@selector(noahi:) forControlEvents:UIControlEventTouchUpInside];
            
            UIView *globoDialogo = [celda viewWithTag:100];
            
            [[globoDialogo layer] setCornerRadius:15];
            [[globoDialogo layer] setMasksToBounds:YES];
            
        }else{
        
            celda = [tablaCheckinDone dequeueReusableCellWithIdentifier:@"celdaMensaje"];
            
            if([celda viewWithTag:11]){
            
                [[celda viewWithTag:11] removeFromSuperview];
            }
                        
            UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldanuevo.png"]];
            
            [celda setBackgroundView:fondo];
            [celda setBackgroundColor:[UIColor clearColor]];
            
            UIImageView *fotovideo = (UIImageView*)[celda viewWithTag:4];
            
            if([[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]]){
                
                [fotovideo setHidden:YES];
                [[celda viewWithTag:5] setHidden:YES];
            }else{
            
                [fotovideo setHidden:NO];
            }
            
            UIImageView *fotoUsuario = (UIImageView*)[celda viewWithTag:1];
            
            [fotoUsuario.layer setBorderWidth:0];
            [fotoUsuario.layer setCornerRadius:22.5];
            [fotoUsuario.layer setMasksToBounds:YES];
            
            [fotoUsuario sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioID"] intValue],[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"]];
            
            UILabel *nombreylugar = (UILabel*)[celda viewWithTag:2];

            
            if([[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioApellido"] isEqualToString:@"(null)"]){
                
                [[mensajes objectAtIndex:indexPath.row-1] setObject:@"" forKey:@"mensajeUsuarioApellido"];
            }

            [nombreylugar setText:[NSString stringWithFormat:@"%@ %@ estuvo en %@",[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioNombre"],[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioApellido"],[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"lugar"]]];
            
            unsigned long sizenombre = [[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioNombre"] length]+[[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioApellido"] length]+1;
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:nombreylugar.attributedText];
            
            [attributedString addAttribute:NSForegroundColorAttributeName value:[tableView backgroundColor] range:NSMakeRange(0, sizenombre)];
            [nombreylugar setAttributedText:attributedString];

            [attributedString addAttribute:NSForegroundColorAttributeName value:[tableView backgroundColor] range:NSMakeRange(sizenombre+11, [[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"lugar"] length])];
            [nombreylugar setAttributedText:attributedString];

            
            UILabel *fecha = (UILabel*)[celda viewWithTag:3];
            
            [fecha setText:[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"fechaFormateada"]];
            
            NSArray *constraints = [[celda contentView] constraints];
            NSLayoutConstraint *constraintInferior;
            
            NSString *mensaje = [[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensaje"];
            
            NSData *data = [mensaje dataUsingEncoding:NSUTF8StringEncoding];
            
            mensaje = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
            
            mensaje = [mensaje stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            float adicional = 0;
            
            if(![[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]]){
                
                if([[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"esVideo"] intValue] == 0){
                
                    [fotovideo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/publicaciones/%@",[[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioID"] intValue], [[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"fotoPublicacion"]]]];
                    
                    [[celda viewWithTag:5] setHidden:YES];
                    
                }else{
                
                    NSArray *nombreVideoA = [[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"fotoPublicacion"] componentsSeparatedByString:@"."];
                    
                    NSString *nombreVideo = [NSString stringWithFormat:@"%@.jpg",[nombreVideoA objectAtIndex:0]];
                    
                    NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/thumbnail/%@",[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioID"],nombreVideo]];
                    
                    [[celda viewWithTag:5] setHidden:NO];
                    
                    [fotovideo sd_setImageWithURL:videoURL];
                }
                
                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(abrirImagen:)];
                
                [fotovideo addGestureRecognizer:tapGesture];
                [fotovideo setUserInteractionEnabled:YES];
                
                for (NSLayoutConstraint *constraint in constraints) {
                    
                    if([[constraint identifier] isEqualToString:@"constraintImagenBottom"]){
                        
                        constraintInferior = constraint;
                    }
                }
                
                if(constraintInferior != nil){
                    
                    if(![mensaje isEqualToString:@""]){
                        
                        if([mensaje length] > 44){
                            
                            adicional = ceil([mensaje length]/44.0);
                            adicional = adicional*55;
                        }else{
                            
                            adicional = 55;
                        }
                    }
                    
                    [constraintInferior setConstant:62+adicional];
                }
                
                
            }
            
            if(![mensaje isEqualToString:@""]){

                UITextView *texto = [[UITextView alloc] initWithFrame:CGRectMake(9, 96, celda.frame.size.width-18, 100)];
                
                [texto setText:mensaje];
                
                [texto setFont:[UIFont fontWithName:@"Raleway-Regular" size:13]];
                [texto setTextColor:[UIColor colorWithRed:48.0/255.0 green:52.0/255.0 blue:63.0/255.0 alpha:1]];
                
                [texto setBackgroundColor:[UIColor colorWithRed:246.0/255.0 green:244.0/255.0 blue:243.0/255.0 alpha:1]];

                [texto setTextContainerInset:UIEdgeInsetsMake(20, 20, 20, 20)];
                
                [texto setTag:11];

                [texto setUserInteractionEnabled:NO];
                [texto setScrollEnabled:NO];
                [texto setEditable:NO];
                
                [texto setContentMode:UIViewContentModeCenter];
                
                texto.translatesAutoresizingMaskIntoConstraints = NO;
                
                [celda addSubview:texto];
                
                if([[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]]){
                
                    [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:[texto superview] attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:96]];
                }else{
                
                    [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:fotovideo attribute:NSLayoutAttributeBottom multiplier:1.0 constant:8]];
                }
                
                [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:[texto superview] attribute:NSLayoutAttributeLeadingMargin multiplier:1.0 constant:1]];
                
                [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:[texto superview] attribute:NSLayoutAttributeTrailingMargin relatedBy:NSLayoutRelationEqual toItem:texto attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:1]];
                
                [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:[texto superview] attribute:NSLayoutAttributeBottomMargin relatedBy:NSLayoutRelationEqual toItem:texto attribute:NSLayoutAttributeBottom multiplier:1.0 constant:50]];
                
            }else{
                
                UITextView *texto = (UITextView*)[celda viewWithTag:11];

                [texto removeFromSuperview];
            }
            
            UIButton *corazon = (UIButton*)[celda viewWithTag:6];
            
            if([[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"likeMio"] intValue] == 0){
            
                [corazon setSelected:NO];
            }else{
            
                [corazon setSelected:YES];
            }
            
            [corazon removeTarget:self action:@selector(meGustaComentario:) forControlEvents:UIControlEventTouchUpInside];
            
            [corazon addTarget:self action:@selector(meGustaComentario:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *numCorazon = (UIButton*)[celda viewWithTag:7];
            
            if([[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"likes"] intValue] == 0){
            
                [numCorazon setTitle:@"" forState:UIControlStateNormal];
            }else{
            
                [numCorazon setTitle:[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"likes"] forState:UIControlStateNormal];
            }
            
            UIButton *opciones = (UIButton*)[celda viewWithTag:10];
            
            [opciones addTarget:self action:@selector(mostrarOpcionesMensaje:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    return celda;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == tablaLugares){
        
        [self cerrarLugarTutorial:nil];
        
        UITableViewCell *celda = [tableView cellForRowAtIndexPath:indexPath];
        
        UILabel *labelDireccion = (UILabel*)[celda viewWithTag:4];
        
        CLLocation *locacion = [[CLLocation alloc] initWithLatitude:[[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"latitud"] floatValue] longitude:[[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"longitud"] floatValue]];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PublicaCheckinViewController *publicahrCheckin = [storyboard instantiateViewControllerWithIdentifier:@"publicarCheckin"];
        
        [publicahrCheckin setIdLugar:[[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue]];
        
        [publicahrCheckin setLocacion:locacion];
        
        [publicahrCheckin setNombreLugar:[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
        
        [publicahrCheckin setStringFotoPrincipalLugar:[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"foto"]];
        
        [publicahrCheckin setStringFotoTraseraLugar:[[[lugaresCercanos objectForKey:@"lugares"] objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]];
        
        [publicahrCheckin setDireccionLugar:[labelDireccion text]];
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        
        [self.navigationController pushViewController:publicahrCheckin animated:YES];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if([buscarLugar isFirstResponder]){
    
        [buscarLugar resignFirstResponder];
    }
    
}

#pragma mark - hechos por mi

-(void)setCheckinDone{

    [vistaCheckinDone setHidden:NO];
    [self.navigationController setNavigationBarHidden:YES];
    [tablaCheckinDone reloadData];
    intervaloTiempoCheckin = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(checarTiempoCheckin) userInfo:nil repeats:YES];
    [intervaloTiempoCheckin fire];
    [self getTutorialPublicarAmbiente];
    [self getMensajes];
    
   /* UITableViewCell *celda = [tablaCheckinDone cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    UIButton *boton = (UIButton*)[celda viewWithTag:5];
    
    UIImageView *pinicito = [[UIImageView alloc] initWithFrame:CGRectMake(boton.frame.origin.x, boton.frame.origin.y-60, 60, 60)];
    
    [pinicito setImage:[UIImage imageNamed:@"pinicito-flash.png"]];
    
    [celda addSubview:pinicito];*/
    
}

-(void)setFotoPrincipal:(NSString *)fotoPrincipal fotoSecundaria:(NSString *)fotoSecundaria nombreLugar:(NSString *)nombre latitud:(double)latitud longitud:(double)longitud id:(int)identificador{

    [[datosEnLugar objectForKey:@"lugar"] setValue:@"si" forKey:@"status"];
    [[datosEnLugar objectForKey:@"lugar"] setValue:nombre forKey:@"lugar"];
    [[datosEnLugar objectForKey:@"lugar"] setValue:[NSNumber numberWithDouble:latitud] forKey:@"latitud"];
    [[datosEnLugar objectForKey:@"lugar"] setValue:[NSNumber numberWithDouble:longitud] forKey:@"longitud"];
    [[datosEnLugar objectForKey:@"lugar"] setValue:fotoPrincipal forKey:@"foto"];
    [[datosEnLugar objectForKey:@"lugar"] setValue:[NSNumber numberWithInt:identificador] forKey:@"ID"];
}

-(void)checarTiempoCheckin{
    
    double fechaActual = [[NSDate date] timeIntervalSince1970];
    
    if(fechaActual > [[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"fecha"] doubleValue]+7200){
        
        [self terminaCheckin];
        [intervaloTiempoCheckin invalidate];
        intervaloTiempoCheckin = nil;
    }
    
    if(fechaActual > [[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"fecha"] doubleValue]+6300){
    
        UITableViewCell *celda = [tablaCheckinDone cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        UIButton *aunestoyahi = (UIButton*)[celda viewWithTag:6];
        
        [aunestoyahi setTitle:@"Aún estoy aquí" forState:UIControlStateNormal];
        [aunestoyahi setImage:[UIImage imageNamed:@"palomita.png"] forState:UIControlStateNormal];
        
        [aunestoyahi removeTarget:self action:@selector(noahi:) forControlEvents:UIControlEventTouchUpInside];
        
        [aunestoyahi addTarget:self action:@selector(aunahi:) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)terminaCheckin{

    [locationManager startUpdatingLocation];
    [vistaCheckinDone setHidden:YES];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)dejarVoto:(id)sender{

    UITableViewCell *celda = [tablaCheckinDone cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    UIImageView *p1Tutorial = (UIImageView*)[celda viewWithTag:100];
    UIImageView *p2Tutorial = (UIImageView*)[celda viewWithTag:101];
    UIImageView *p3Tutorial = (UIImageView*)[celda viewWithTag:102];
    
    [p1Tutorial removeFromSuperview];
    [p2Tutorial removeFromSuperview];
    [p3Tutorial removeFromSuperview];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarbotonvotartuto.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){}];
    
    [uploadTask resume];
    
    [[datosEnLugar objectForKey:@"tutoriales"] setObject:[NSNumber numberWithInt:1] forKey:@"comentarioEnLugar"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    VotarLugarViewController *votarLugarVC = [storyboard instantiateViewControllerWithIdentifier:@"votarLugar"];

    [votarLugarVC setVotos: [[datosEnLugar objectForKey:@"voto"] mutableCopy]];
    
    [votarLugarVC setIdLugar:[[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"objID"] intValue]];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.navigationController pushViewController:votarLugarVC animated:YES];
}

-(void)getMensajes{

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getmensajeslugar.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"id=%@&usuario=%@&ultimo=0",[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"objID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){

            if(data != nil){

                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSError *error;

                    mensajes = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                    if(error == nil){

                        ultimoMensajeID = [[[mensajes objectAtIndex:0] objectForKey:@"mensajeID"] intValue];
                                        
                        [tablaCheckinDone reloadData];
                        
                        [UIView animateWithDuration:0.5 animations:^{
                            
                            [tablaCheckinDone setContentOffset:CGPointMake(tablaCheckinDone.contentOffset.x, tablaCheckinDone.contentOffset.y+50)];
                        } completion:^(BOOL finished){
                            
                            if(finished){
                                
                                [UIView animateWithDuration:0.2 animations:^{
                                    
                                    [tablaCheckinDone setContentOffset:CGPointMake(tablaCheckinDone.contentOffset.x, tablaCheckinDone.contentOffset.y-50)];
                                }];
                            }
                        }];
                    }
                    
                    [intervaloMensajesNuevos fire];
                });
                
            }
        }
        
    }];
    
    [uploadTask resume];
}

-(void)getMensajesNuevos{
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getmensajeslugar.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"id=%@&usuario=%@&ultimo=%d",[[datosEnLugar objectForKey:@"lugar"] objectForKey:@"objID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"],ultimoMensajeID] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){
            
            if(data != nil){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSError *error;
                    
                    NSArray *nuevos = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                    
                    if(error == nil){
                    
                        for(NSArray *iteracion in nuevos){
                        
                            [mensajes insertObject:iteracion atIndex:0];
                            [tablaCheckinDone beginUpdates];
                            [tablaCheckinDone insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
                            [tablaCheckinDone endUpdates];
                        }
                        
                        ultimoMensajeID = [[[nuevos objectAtIndex:0] objectForKey:@"mensajeID"] intValue];
                    }
                });
                
            }
        }
        
    }];
    
    [uploadTask resume];
}

-(IBAction)noahi:(id)sender{
        
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/removejoin.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"recurso=2&usuario=%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){
            
            if(data != nil){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    app.networkActivityIndicatorVisible = NO;
                    [self terminaCheckin];
                });
                
            }
        }
        
    }];
    
    [uploadTask resume];

}

-(IBAction)aunahi:(id)sender{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/addjoin.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%d&origen=%d&estado=1",[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue], 0] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){
            
            if(data != nil){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                    UITableViewCell *celda = [tablaCheckinDone cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    
                    UIButton *noEstoyAhi = (UIButton*)[celda viewWithTag:6];
                    
                    [noEstoyAhi setTitle:@"No estoy más aquí" forState:UIControlStateNormal];
                    
                    [noEstoyAhi setImage:[UIImage imageNamed:@"closeicon.png"] forState:UIControlStateNormal];
                    
                    [noEstoyAhi removeTarget:self action:@selector(aunahi:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [noEstoyAhi addTarget:self action:@selector(noahi:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [[datosEnLugar objectForKey:@"lugar"] setObject:[NSNumber numberWithFloat:[[NSDate date] timeIntervalSince1970]] forKey:@"fecha"];
                });
                
            }
        }
        
    }];
    
    [uploadTask resume];
    
}

-(IBAction)meGustaComentario:(id)sender{

    UIButton *corazon = (UIButton*)sender;
    
    CGPoint posicionBoton = [corazon convertPoint:CGPointZero toView:tablaCheckinDone];
    
    NSIndexPath *indexPath = [tablaCheckinDone indexPathForRowAtPoint:posicionBoton];
    
    UITableViewCell *celda = [tablaCheckinDone cellForRowAtIndexPath:indexPath];
    
    if(![corazon isSelected]){
        
        LOTAnimationView *corazonLot = [LOTAnimationView animationNamed:@"data"];
        
        [corazonLot setFrame:corazon.frame];
        [corazonLot setTag:12];
        
        [celda addSubview:corazonLot];
        
        [corazon setAlpha:0];
        
        [corazonLot playWithCompletion:^(BOOL animationFinished){
            
            if(animationFinished){
                
                [corazonLot removeFromSuperview];
                [corazon setAlpha:1];
                
                [corazon setSelected:YES];
                
                [[mensajes objectAtIndex:indexPath.row-1] setValue:@"1" forKey:@"likeMio"];
                
                int numMegusta = [[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"likes"] intValue];
                
                numMegusta++;
                
                UIButton *numCorazones = (UIButton*)[celda viewWithTag:7];
                
                [numCorazones setTitle:[NSString stringWithFormat:@"%d",numMegusta] forState:UIControlStateNormal];
                
                [[mensajes objectAtIndex:indexPath.row-1] setValue:[NSString stringWithFormat:@"%d",numMegusta] forKey:@"likes"];
                
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
                
                [request setHTTPMethod:@"POST"];
                
                NSData *cuerpo = [[NSString stringWithFormat:@"id=%d&mensajeID=%@&act=%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue], [[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensajeID"], 1] dataUsingEncoding:NSUTF8StringEncoding];
                
                NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                
                NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                
                NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                    
                    if(error == nil){
                        
                        if(data != nil){
                            
                            NSError *error = nil;
                            
                            NSDictionary *numMeGusta = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                            
                            if(error == nil){
                                
                                if([[numMeGusta objectForKey:@"status"] isEqualToString:@"si"]){
                                    
                                    UITableViewCell *celda = [tablaCheckinDone cellForRowAtIndexPath:indexPath];
                                    
                                    UIButton *numCorazones = (UIButton*)[celda viewWithTag:7];
                                    
                                    if([[numMeGusta objectForKey:@"numero"] intValue] == 0){
                                        
                                        [numCorazones setTitle:@"" forState:UIControlStateNormal];
                                    }else{
                                        
                                        [numCorazones setTitle:[numMeGusta objectForKey:@"numero"] forState:UIControlStateNormal];
                                    }
                                }
                            }
                        }
                    }
                    
                }];
                
                [uploadTask resume];
                
            }
        }];
    }else{
        
        LOTAnimationView *corazonLot = [LOTAnimationView animationNamed:@"corazon_roto"];
        
        [corazonLot setFrame:corazon.frame];
        [corazonLot setTag:12];
        
        [celda addSubview:corazonLot];
        
        [corazon setAlpha:0];
        
        [corazonLot playWithCompletion:^(BOOL animationFinished){
            
            if(animationFinished){
                
                [corazonLot removeFromSuperview];
                [corazon setAlpha:1];
                
                [corazon setSelected:NO];
                
                [[mensajes objectAtIndex:indexPath.row-1] setValue:@"0" forKey:@"likeMio"];
                
                int numMegusta = [[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"likes"] intValue];
                
                numMegusta--;
                
                UIButton *numCorazones = (UIButton*)[celda viewWithTag:7];
                
                if(numMegusta == 0){
                    
                    
                    [numCorazones setTitle:@"" forState:UIControlStateNormal];
                }else{
                    
                    
                    [numCorazones setTitle:[NSString stringWithFormat:@"%d",numMegusta] forState:UIControlStateNormal];
                }
                
                [[mensajes objectAtIndex:indexPath.row-1] setValue:[NSString stringWithFormat:@"%d",numMegusta] forKey:@"likes"];
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
                
                [request setHTTPMethod:@"POST"];
                
                NSData *cuerpo = [[NSString stringWithFormat:@"id=%d&mensajeID=%@&act=%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue], [[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensajeID"], 2] dataUsingEncoding:NSUTF8StringEncoding];
                
                NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                
                NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                
                NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                    
                    if(error == nil){
                        
                        if(data != nil){
                            
                            NSError *error = nil;
                            
                            NSDictionary *numMeGusta = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                            
                            if(error == nil){
                                
                                if([[numMeGusta objectForKey:@"status"] isEqualToString:@"si"]){
                                    
                                    UITableViewCell *celda = [tablaCheckinDone cellForRowAtIndexPath:indexPath];
                                    
                                    UIButton *numCorazones = (UIButton*)[celda viewWithTag:7];
                                    
                                    if([[numMeGusta objectForKey:@"numero"] intValue] == 0){
                                        
                                        [numCorazones setTitle:@"" forState:UIControlStateNormal];
                                    }else{
                                        
                                        [numCorazones setTitle:[numMeGusta objectForKey:@"numero"] forState:UIControlStateNormal];
                                    }
                                }
                            }
                        }
                    }
                    
                }];
                
                [uploadTask resume];
            }
        }];
    }
    
}

-(IBAction)agregarLugar:(id)sender{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    AgregaNuevoLugarController *agregarLugar = [storyboard instantiateViewControllerWithIdentifier:@"agregarLugar"];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.navigationController pushViewController:agregarLugar animated:YES];    
}

-(void)actualizarCheckins{}

-(void)abrirImagen:(UIPanGestureRecognizer*)gesto{

    UIImageView *imagenPresionada = (UIImageView*)[gesto view];
    
    CGPoint posicionBoton = [imagenPresionada convertPoint:CGPointZero toView:tablaCheckinDone];
    
    NSIndexPath *indexPath = [tablaCheckinDone indexPathForRowAtPoint:posicionBoton];
    
    UITableViewCell *celda = [tablaCheckinDone cellForRowAtIndexPath:indexPath];
    
    UIImageView *fotoUsuario = (UIImageView*)[celda viewWithTag:1];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [self.view.layer renderInContext:context];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    
    ImagenPubLugarViewController *imgPub = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"imgPubLugarView"];
    
    imgPub.fotoAnterior = img;
    imgPub.fotoAmostrar = [imagenPresionada image];
    imgPub.imagenUsuario = UIImageJPEGRepresentation([fotoUsuario image], 90);
    imgPub.json = [mensajes objectAtIndex:indexPath.row-1];
    [self presentViewController:imgPub animated:NO completion:^{}];

}

-(IBAction)mostrarOpcionesMensaje:(id)sender{

    UIButton *botonOpciones = (UIButton*)sender;
    
    CGPoint posicionBoton = [botonOpciones convertPoint:CGPointZero toView:tablaCheckinDone];
    
    NSIndexPath *indexPath = [tablaCheckinDone indexPathForRowAtPoint:posicionBoton];
    
    UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *reportarCheckin = [UIAlertAction actionWithTitle:@"Reportar mensaje" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ReportarMensajeTableViewController *reporte = [storyboard instantiateViewControllerWithIdentifier:@"ReportarMensaje"];
                
        [reporte setIdRelMensajeLugar:[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensajeID"]];
        
        [reporte setIdUsuarioABloquear:[[mensajes objectAtIndex:indexPath.row-1] objectForKey:@"mensajeUsuarioID"]];
        
        [self presentViewController:reporte animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
        
        [opciones dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [opciones addAction:reportarCheckin];
    [opciones addAction:cancelar];
    
    [self presentViewController:opciones animated:YES completion:nil];
}

-(void)lugarTutorial{
    
    [vistaTutorial2 setHidden:NO];
}

- (IBAction)cerrarLugarTutorial:(id)sender {
    
    [vistaTutorial2 removeFromSuperview];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarhacercheckintutodos.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){}];
    
    [uploadTask resume];
}

-(IBAction)cerrarTutorialPublicarAmbiente:(id)sender{

    UITableViewCell *celda = [tablaCheckinDone cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    UIView *vistaTutorial = [celda viewWithTag:8];
    
    [vistaTutorial removeFromSuperview];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarpublicarambientetuto.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){}];
    
    [uploadTask resume];
}

- (void)getTutorialPublicarAmbiente{

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/gettutorialpublicarambiente.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){
            
            if(data != nil){
                
                NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                if([respuesta isEqualToString:@"0"]){
                
                    UIView *vistaFelicidades = [[UIView alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width-10, self.view.frame.size.height-10)];
                    
                    [vistaFelicidades setBackgroundColor:[UIColor whiteColor]];
                    
                    [vistaFelicidades layer].masksToBounds = YES;
                    [[vistaFelicidades layer] setCornerRadius:15];
                    
                    [vistaFelicidades setTag:1000];
                    
                    [self.view addSubview:vistaFelicidades];
                    
                    LOTAnimationView *fuegosArtificiales = [LOTAnimationView animationNamed:@"fuegos_artificiales"];
                    
                    [fuegosArtificiales setFrame:CGRectMake((vistaFelicidades.frame.size.width/2)-100, 20, 200, 112.485)];
                    
                    [[vistaFelicidades layer] setBorderColor:[[UIColor colorWithRed:84.0/255.0 green:159.0/255.0 blue:220.0/255.0 alpha:1] CGColor]];
                    
                    [[vistaFelicidades layer] setBorderWidth:4];
                    
                    [vistaFelicidades addSubview:fuegosArtificiales];
                    
                    [fuegosArtificiales setLoopAnimation:YES];
                    [fuegosArtificiales play];
                    [fuegosArtificiales setTag:1];
                    
                    UIImageView *textoFelicidades = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"texto_felicidades.png"]];
                    
                    [textoFelicidades setFrame:CGRectMake((vistaFelicidades.frame.size.width/2)-95, (fuegosArtificiales.frame.size.height/2), 200, 33.444)];

                    [vistaFelicidades addSubview:textoFelicidades];
                    
                    
                    UIButton *cerrarFelicidades = [[UIButton alloc] initWithFrame:CGRectMake(20, vistaFelicidades.frame.size.height-70, vistaFelicidades.frame.size.width-50, 50)];
                    
                    [cerrarFelicidades setBackgroundColor:[UIColor colorWithRed:84.0/255.0 green:159.0/255.0 blue:220.0/255.0 alpha:1]];
                    
                    [cerrarFelicidades setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                    [cerrarFelicidades setTitle:@"¡Entiendo!" forState:UIControlStateNormal];
                    
                    [cerrarFelicidades addTarget:self action:@selector(cerrarMensajeCerveza) forControlEvents:UIControlEventTouchUpInside];
                    
                    UITextView *textoDetalle = [[UITextView alloc] initWithFrame:CGRectMake(20, fuegosArtificiales.frame.origin.y+fuegosArtificiales.frame.size.height+20, vistaFelicidades.frame.size.width-40, cerrarFelicidades.frame.origin.y-150)];
                    
                    
                    [textoDetalle setTextColor:[UIColor colorWithRed:84.0/255.0 green:159.0/255.0 blue:220.0/255.0 alpha:1]];
                    
                    [textoDetalle setText:[NSString stringWithFormat:@"Para que funcione Crowte, todos deben poner su granito de arena. \n\nCada que hagas un check-in te recomendamos publicar una foto, video o comentario del ambiente del lugar en donde estás, así, otra persona que esté en su casa pensando a dónde ir, podrá ver cómo está el ambiente en tiempo real y decidir a dónde ir de acuerdo a su mood del momento. \n\nTambién puedes compartir detalles como si el lugar está muy lleno, si la música está muy fuerte, y cualquier cosa que pueda ayudar a una persona a decidir ir o no a ese lugar. Recuerda, algún día tú puedes ser esa persona %C\n\n¡Bienvenido y que te diviertas crowteando!",0xe415]];
                    
                    
                    
                    UIFont *textoEnNegrita = [UIFont boldSystemFontOfSize:textoDetalle.font.pointSize];
                    UIFont *textoEnCursiva = [UIFont italicSystemFontOfSize:textoDetalle.font.pointSize];
                    NSDictionary *dictBoldText = [NSDictionary dictionaryWithObjectsAndKeys:textoEnNegrita, NSFontAttributeName, nil];
                    
                    NSDictionary *dictCursivaText = [NSDictionary dictionaryWithObjectsAndKeys:textoEnCursiva, NSFontAttributeName, nil];
                    
                    
                    NSMutableAttributedString *textoFormateadoNegrita = [[NSMutableAttributedString alloc] initWithString:textoDetalle.text];
                    [textoFormateadoNegrita setAttributes:dictBoldText range:NSMakeRange(152, 8)];
                    
                    [textoFormateadoNegrita setAttributes:dictCursivaText range:NSMakeRange(387, 59)];
                    
                    [textoDetalle setAttributedText:textoFormateadoNegrita];
                    
                    
                    [textoDetalle setTextColor:[UIColor colorWithRed:0.0/255.0 green:115.0/255.0 blue:176.0/255.0 alpha:1]];
                    
                    [textoDetalle setTextAlignment:NSTextAlignmentJustified];
                    
                    [textoDetalle setSelectable:NO];
                    [textoDetalle setEditable:NO];
                    
                    [vistaFelicidades addSubview:cerrarFelicidades];
                    [vistaFelicidades addSubview:textoDetalle];
                   
                    UITableViewCell *celda = [tablaCheckinDone cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    
                    UIButton *botonDejarComentario = (UIButton*)[celda viewWithTag:5];
                    
                    LOTAnimationView *toucher = [LOTAnimationView animationNamed:@"touch"];
                    
                    [toucher setFrame:CGRectMake((botonDejarComentario.frame.origin.x+botonDejarComentario.frame.size.width/2)-25, (botonDejarComentario.frame.origin.y+botonDejarComentario.frame.size.height/2)-25, 50, 50)];
                    
                    [toucher setTag:8];
                    [toucher setLoopAnimation:YES];
                    [toucher play];
                
                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dejarVoto:)];
                
                [toucher addGestureRecognizer:tapGesture];
                
                    [celda addSubview:toucher];
                    
                }
                
            }
        }
        
    }];
    
    [uploadTask resume];

}

-(void)cerrarMensajeCerveza{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarpublicarambientetuto.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){}];
    
    [uploadTask resume];
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
        [[self.view viewWithTag:1000] removeFromSuperview];
    });
    
    
}

#pragma mark - location

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{

    if(locationContador){
        return;
    }
    
    locationContador = YES;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getlugarcheckinrap.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"latitud=%f&longitud=%f&usuario=%@",manager.location.coordinate.latitude, manager.location.coordinate.longitude, [[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];

    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){

        if(error == nil){
        
            lugaresCercanos = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"lugars cercanos %@",lugaresCercanos);
            if(lugaresCercanos == nil || [lugaresCercanos count] == 0 || [[lugaresCercanos objectForKey:@"cantidad"] isEqual:[NSNumber numberWithInt:0]]){

                [tablaLugares setHidden:YES];
                [fondoEmptyState setHidden:NO];
                [lupaEmptyState setHidden:NO];
                [textoNoSeEncontraron setHidden:NO];
                [textoAgregaUno setHidden:NO];
            }else{

                [tablaLugares setHidden:NO];
                [fondoEmptyState setHidden:YES];
                [lupaEmptyState setHidden:YES];
                [textoNoSeEncontraron setHidden:YES];
                [textoAgregaUno setHidden:YES];
                
                lugaresCercanosSearchPlaceHolder = [lugaresCercanos objectForKey:@"lugares"];
            
                [tablaLugares reloadData];
             
                if([[lugaresCercanos objectForKey:@"hacercheckin2"] intValue] == 0){
                    
                    [self lugarTutorial];
                }else{
                    
                    [self cerrarLugarTutorial:nil];
                }
            }
        }
        
    }];
    
    [uploadTask resume];
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{}


#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}

#pragma mark - search bar

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

    NSMutableArray *arregloAcambiar = [NSMutableArray array];
    
    if([searchText isEqualToString:@""]){
        
        [lugaresCercanos setValue:[NSNumber numberWithUnsignedInteger:[lugaresCercanosSearchPlaceHolder count]] forKey:@"cantidad"];
        [lugaresCercanos setValue:lugaresCercanosSearchPlaceHolder forKey:@"lugares"];
        
        [tablaLugares reloadData];
        
        [tablaLugares setHidden:NO];
        [fondoEmptyState setHidden:YES];
        [lupaEmptyState setHidden:YES];
        [textoNoSeEncontraron setHidden:YES];
        [textoAgregaUno setHidden:YES];
        
    }else{
        
        for(NSDictionary *iteracion in lugaresCercanosSearchPlaceHolder){
            
            if([[[iteracion objectForKey:@"lugar"] lowercaseString] rangeOfString:[searchText lowercaseString]].location != NSNotFound){
                
                [arregloAcambiar addObject:iteracion];
                
            }
        }
        
        if([arregloAcambiar count] == 0){
        
            [tablaLugares setHidden:YES];
            [fondoEmptyState setHidden:NO];
            [lupaEmptyState setHidden:NO];
            [textoNoSeEncontraron setHidden:NO];
            [textoAgregaUno setHidden:NO];
            
           // [searchBar resignFirstResponder];
        }else{
        
            [tablaLugares setHidden:NO];
            [fondoEmptyState setHidden:YES];
            [lupaEmptyState setHidden:YES];
            [textoNoSeEncontraron setHidden:YES];
            [textoAgregaUno setHidden:YES];
        }
        
        [lugaresCercanos setValue:[NSNumber numberWithUnsignedInteger:[arregloAcambiar count]] forKey:@"cantidad"];
        [lugaresCercanos setValue:arregloAcambiar forKey:@"lugares"];
        
        [tablaLugares reloadData];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
