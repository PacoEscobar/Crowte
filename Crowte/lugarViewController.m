//
//  lugarViewController.m
//  Crowte
//
//  Created by Paco Escobar on 03/07/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "KLCPopup.h"
#import "lugarViewController.h"
#import "Perfil.h"
#import "denunciar_lugar.h"
#import "ReclamarLugarUno.h"
#import "ImagenPubLugarViewController.h"
#import "ResponderMensajeViewController.h"
#import "CamaraOverlayViewController.h"

@interface lugarViewController ()

@end

@implementation lugarViewController

@synthesize idLugar, activity, nombreLugar, fotoComentario, lugar, vista, tabla, comentariosLugar, dataFoto, imagenLugar, numeroAmigos, numeroPersonas, numeroVotos, topPlace, barraVotos, votoPositivo, votoNegativo, mensajes, datos, ultimoMensaje, fotoLugar, etiquetaMensajes, comentarLugar, segmentControl, enlugar, votado, textoNoEstasAqui, enviarMensajeBoton,terminaCarga, propietario, constraintEnviaMensajeB, constraintRightEnviaM,constraintWidthEnviar,constraintWidthTextField, privado, seTomaFoto, scrollView, numeroDeFotos, imagenCargada, dataFotoNueva, timerMensajesAlready, timerDatosAlready, ocultarTutoMensajeDirecto, picker, camara, alreadyVideo, reproductor, layer, controlesReproductor, urlVideoApasar, videofoto, popupFullSVideo, totalBytesArchivoAsubir, bytesSubidosArchivoAsubir;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    privado = NO;
    seTomaFoto = YES;
    imagenCargada = NO;
    timerMensajesAlready = NO;
    timerDatosAlready = NO;
    ocultarTutoMensajeDirecto = NO;
    
    [enviarMensajeBoton setTitle:@"" forState:UIControlStateNormal];
    [enviarMensajeBoton setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
    [constraintWidthEnviar setConstant:30];
    
    mensajes = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getNuevosMensajes) userInfo:nil repeats:YES];
    
    datos = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getDatos) userInfo:nil repeats:YES];
    
    [constraintWidthTextField setConstant:220];
        
    self.navigationController.navigationBar.topItem.title = @"";
    
    if([[UIScreen mainScreen] bounds].size.height <= 480){
    
        [constraintRightEnviaM setConstant:10];
    }
    
    terminaCarga = 0;
    
    tabla.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    comentarLugar.layer.borderColor = [[UIColor colorWithRed:0 green:180.0/255.0 blue:180.0/255.0 alpha:1] CGColor];
    comentarLugar.layer.borderWidth = 1.0;
    comentarLugar.layer.cornerRadius = 0;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    
        NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/checarexistencia.php"]];
    
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"lugar=%d&rec=2",idLugar] dataUsingEncoding:NSUTF8StringEncoding]];
    
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

            if(data != NULL){
                char resultado[3];
                [data getBytes:resultado length:2];
                resultado[2] = '\0';
                
                NSInteger iresultado = atoi(resultado);
                
                if(iresultado == 0){}
            }
       
        }];
    
    
        if(dataFoto == nil){

            if(fotoLugar == nil){

                NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getfotolugar.php"]];
                
                [peticion setHTTPMethod:@"POST"];
                [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d",idLugar] dataUsingEncoding:NSUTF8StringEncoding]];
                
                NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                
                [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
                    if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                        
                    
                    }else{
                    
                        NSString *foto = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        
                        [imagenLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%d/%@",idLugar,foto]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                        
                            fotoLugar = nil;
                        }];
                    }

                }];
                
            }else{
    
                UIActivityIndicatorView *activityFoto = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(imagenLugar.frame.origin.x, imagenLugar.frame.origin.y, imagenLugar.frame.size.width, imagenLugar.frame.size.height)];
            
                [activityFoto setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
            
                [activityFoto startAnimating];
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    [vista addSubview:activityFoto];
                });
            
                __block NSData *foto = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat: @"https://www.crowte.com/lugares/%d/%@",idLugar,fotoLugar]]];
        
                dispatch_async(dispatch_get_main_queue(), ^{
                    [activityFoto stopAnimating];
                    [activityFoto removeFromSuperview];
                    imagenLugar.image = [[UIImage alloc] initWithData:foto];

                });
            }
        }
    });
    
    ultimoMensaje = 0;
    
    comentariosLugar = [NSMutableArray array];
    fotoComentario = [NSMutableDictionary dictionary];
    
    
    lugar.text = nombreLugar;
    
    imagenLugar.image = [[UIImage alloc] initWithData:dataFoto];
    
    
    activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activity.frame = CGRectMake(150, 250, activity.frame.size.width, activity.frame.size.height);
    
    votoPositivo.layer.cornerRadius = 15;
    votoNegativo.layer.cornerRadius = 15;
    
    if([[UIScreen mainScreen] bounds].size.height <= 480){
    
        votoPositivo.frame = CGRectMake(votoPositivo.frame.origin.x, votoPositivo.frame.origin.y-100, votoPositivo.frame.size.width, votoPositivo.frame.size.height);
        votoNegativo.frame = CGRectMake(votoNegativo.frame.origin.x, votoNegativo.frame.origin.y-100, votoNegativo.frame.size.width, votoNegativo.frame.size.height);
        
    }
    
    [vista addSubview:activity];
    
    [activity startAnimating];
    
    
    [self getDatos];
    [self getMensajes];
    
    UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStyleDone target:self action:nil];
    self.navigationItem.backBarButtonItem = atras;
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if(timerMensajesAlready){
    
        mensajes = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getNuevosMensajes) userInfo:nil repeats:YES];
    }
    
    if(timerDatosAlready){
    
        datos = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getDatos) userInfo:nil repeats:YES];
    }
}

-(void)viewDidDisappear:(BOOL)animated{

    [mensajes invalidate];
    mensajes = nil;
    [datos invalidate];
    datos = nil;
    
    if(reproductor){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [reproductor pause];
            /*[reproductor removeObserver:self forKeyPath:@"status"];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
            layer = nil;
            reproductor = nil;*/
        });
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - metodos de la tabla

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    int size;
    
    if([comentariosLugar count] == 0 || indexPath.row == [comentariosLugar count]){
    
        size = 20.0;
    }else if(![[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]] && ![[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqualToString:@"null"]){
    
        float numeroCaracteres = [[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensaje"] length];
        
        if(numeroCaracteres == 0){
            
            numeroCaracteres = 0;
        }else{
            
            numeroCaracteres = numeroCaracteres/25;
            
            numeroCaracteres = floorf(numeroCaracteres);
            
            numeroCaracteres *= 15;
        }
        
        size = 350.0+numeroCaracteres;
    }else{
    
        float numeroCaracteres = [[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensaje"] length];
        
        if(numeroCaracteres == 0){
            
            numeroCaracteres = 0;
        }else{
            
            numeroCaracteres = numeroCaracteres/25;
            
            numeroCaracteres = floorf(numeroCaracteres);
            
            numeroCaracteres *= 15;
        }
        
        size = 123.0+numeroCaracteres;
    }
    return size;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger cantidad = 0;
    
    if([comentariosLugar count] > 0){

        cantidad = [comentariosLugar count];
    }else if([comentariosLugar count] == 0){
    
        cantidad = 1;
    }
    
    return cantidad;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *celda;
    
    if([comentariosLugar count] != 0 && indexPath.row < [comentariosLugar count]){
        
        if([[[comentariosLugar objectAtIndex:0] objectForKey:@"mensajeID"] intValue] > 0){
            
            celda = [tabla dequeueReusableCellWithIdentifier:@"celdaComentario"];
            
            UIImageView *foto = (UIImageView *)[celda viewWithTag:1];
            
            NSURL *urlFoto = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/%@",[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioID"],[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"foto"]]];
            
            [foto sd_setImageWithURL:urlFoto];
            
            
            UILabel *nombre = (UILabel *)[celda viewWithTag:2];
            nombre.text = [NSString stringWithFormat:@"%@ %@",[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioNombre"], [[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioApellido"]];
            
            NSData *dataPublicacion = [[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensaje"] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSString *publicacionFormateada = [[NSString alloc] initWithData:dataPublicacion encoding:NSNonLossyASCIIStringEncoding];
            
            UILabel *texto = (UILabel *)[celda viewWithTag:3];
            texto.text = publicacionFormateada;
            
            float numeroCaracteres = [publicacionFormateada length];

            if(numeroCaracteres == 0){
                
                numeroCaracteres = 0;
            }else if(numeroCaracteres < 25){
                
                numeroCaracteres = 25.0;
            }else{
                
                numeroCaracteres = numeroCaracteres/25;
                
                numeroCaracteres = floorf(numeroCaracteres);
                
                numeroCaracteres *= 18;
                
            }
            
            [[[texto constraints] objectAtIndex:1] setConstant:numeroCaracteres];

            NSDate *fechacomentario = [NSDate date];
            
            NSDateComponents *componentes = [[NSDateComponents alloc] init];
            [componentes setSecond:([[NSDate date] timeIntervalSince1970] - [[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fecha"] doubleValue])];

            NSCalendar *calendario = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            
            fechacomentario = [calendario dateFromComponents:componentes];
            
            NSDateComponents *fechaComponentes = [calendario components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:fechacomentario];
            
            UILabel *fecha = (UILabel *)[celda viewWithTag:4];
            
            if([fechaComponentes hour] != 0){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    fecha.text = [NSString stringWithFormat:@"Hace %ld hora",(long)[fechaComponentes hour]];
                });
            }else if([fechaComponentes minute] != 0){
                
                if([fechaComponentes minute] == 1){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        fecha.text = [NSString stringWithFormat:@"Hace %ld minuto",(long)[fechaComponentes minute]];
                    });
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        fecha.text = [NSString stringWithFormat:@"Hace %ld minutos",(long)[fechaComponentes minute]];
                    });
                }
                
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    fecha.text = @"Hace unos segundos";
                });
            }
            
            
            if(![[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]] && ![[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqualToString:@"null"]){
                
                UIImageView *fotoPublicacion = [[UIImageView alloc] initWithFrame:CGRectMake(50, 100, 180, 200)];
                
                fotoPublicacion.layer.cornerRadius = 5;
                fotoPublicacion.layer.borderWidth = 0.5;
                fotoPublicacion.layer.masksToBounds = YES;
                fotoPublicacion.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4] CGColor];
                fotoPublicacion.contentMode = UIViewContentModeScaleAspectFill;
                fotoPublicacion.clipsToBounds = YES;
                [fotoPublicacion setCenter:CGPointMake(celda.center.x, fotoPublicacion.center.y+numeroCaracteres-20)];
                [fotoPublicacion setUserInteractionEnabled:YES];
                [fotoPublicacion setTag:9];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(expandirImagen:)];
                
                [tap setNumberOfTapsRequired:1];
                [fotoPublicacion addGestureRecognizer:tap];

                if([[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"esVideo"] intValue] == 0){
                    
                    [fotoPublicacion sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/%@",[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioID"],[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"]]]];
                    
                }else if([[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"esVideo"] intValue] == 1){
                    
                    if(reproductor == nil && layer == nil){
                        
                        NSArray *nombreVideoA = [[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] componentsSeparatedByString:@"."];
                        
                        NSString *nombreVideo = [NSString stringWithFormat:@"%@.jpg",[nombreVideoA objectAtIndex:0]];
                        
                        NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/thumbnail/%@",[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioID"],nombreVideo]];
                        
                        CALayer *playIconLayer = [[CALayer alloc] init];
                        
                        [playIconLayer setFrame:CGRectMake((fotoPublicacion.frame.size.width/2)-25,(fotoPublicacion.frame.size.height/2)-25, 50, 50)];
                        
                        [playIconLayer setContents:(id)[UIImage imageNamed:@"playicon.png"].CGImage];
                        
                        [fotoPublicacion.layer addSublayer:playIconLayer];
                        
                        [fotoPublicacion sd_setImageWithURL:videoURL];
                    }
                    
                }
                
                [celda addSubview:fotoPublicacion];
            }else{
            
                UIImageView *fotoPublicacion = (UIImageView*)[celda viewWithTag:9];
                
                if(![fotoPublicacion isEqual:[NSNull null]]){
                
                    [fotoPublicacion removeFromSuperview];
                }
            }
            
            
            UIButton *heart = (UIButton *)[celda viewWithTag:5];
            UIButton *numAmor = (UIButton *)[celda viewWithTag:6];
            
            if([[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"likeMio"] isEqualToString:@"1"]){
            
                [heart setBackgroundImage:[UIImage imageNamed:@"redheart.png"] forState:UIControlStateNormal];
                [numAmor setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            }else{
            
                [numAmor setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            }
            
            [heart addTarget:self action:@selector(likeComentario:) forControlEvents:UIControlEventTouchUpInside];
           
            
            
            if([[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"likes"] isEqualToString:@"0"]){
                
                [numAmor setTitle:@"" forState:UIControlStateNormal];
            }else{
            
                [numAmor setTitle:[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"likes"] forState:UIControlStateNormal];
            }
            [numAmor.titleLabel setFont:[UIFont systemFontOfSize:12]];
            [numAmor addTarget:self action:@selector(likeComentario:) forControlEvents:UIControlEventTouchUpInside];
            
            
        }else{
        
            celda = [tabla dequeueReusableCellWithIdentifier:@"sinMensajes"];
        }
    
    }else if([comentariosLugar count] == 0){
    
        celda = [tabla dequeueReusableCellWithIdentifier:@"nomensajes"];
        
        
    }else{
    
        celda = [tabla dequeueReusableCellWithIdentifier:@"ultimaCelda"];
        
        
    }
    

    tabla.contentSize = [tabla sizeThatFits:CGSizeMake(CGRectGetWidth(tabla.bounds), CGFLOAT_MAX)];
    
    return celda;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    ResponderMensajeViewController *responderMensaje = [storyboard instantiateViewControllerWithIdentifier:@"responderMensaje"];
    
    UITableViewCell *celda = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    UIImageView *fotoImagenView = (UIImageView*)[celda viewWithTag:1];
    
    responderMensaje.nombre = [NSString stringWithFormat:@"%@ %@",[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioNombre"],[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioApellido"]];
    
    responderMensaje.fotoUsuario = UIImagePNGRepresentation([fotoImagenView image]);
    responderMensaje.publicacion = [[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensaje"];
    responderMensaje.fecha = [[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fecha"];
    responderMensaje.numDeAmor = [NSString stringWithFormat:@"%@",[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"likes"] ];
    responderMensaje.datosPublicacion = [comentariosLugar objectAtIndex:indexPath.row];
    responderMensaje.esVideo = [[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"esVideo"] intValue];
    
    if(![[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]] && ![[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqualToString:@"(null)"]){
        
        if([[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"esVideo"] intValue] == 0){
            
            responderMensaje.dataImagenComentario = UIImageJPEGRepresentation([(UIImageView*)[celda viewWithTag:9] image], 1.0);
        
        }else if([[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"esVideo"] intValue] == 1){
            
            if(urlVideoApasar == nil){
                responderMensaje.urlVideo = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/%@",[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioID"],[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"]]];
            }else{
                
                responderMensaje.urlVideo = urlVideoApasar;
            }
        }
    }
    
    [self.navigationController pushViewController:responderMensaje animated:YES];
}

#pragma mark - getdatos

-(void)getDatos{

    @try{
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
        NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getdatoslugar.php"]];
    
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&usuario=%@&token=%@",idLugar,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self getDatos];
                });
            }else{
        
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                if(json == NULL){
                
                    [self getDatos];
                }else{
                    
                    votado = [[json objectForKey:@"votado"] intValue];
                    enlugar = [[json objectForKey:@"ahi"] intValue];
                    propietario = [[json objectForKey:@"propietario"] isEqualToString:@"0"];
                    
                    if(![[json objectForKey:@"propietario"] isEqualToString:@"0"]){
                    
                        dispatch_async(dispatch_get_main_queue(), ^{
                        
                            /*if([segmentControl numberOfSegments] < 3){
                            
                                segmentControl.frame = CGRectMake(8, 199, 304, 29);
                                [segmentControl insertSegmentWithTitle:@"Mensaje" atIndex:2 animated:YES];
                            }*/

                            if([[json objectForKey:@"mensajePrivado"] intValue] == 0 && !ocultarTutoMensajeDirecto){
                                
                                if([self.view viewWithTag:10000] == NULL){
                                    
                                    UIView *vistaTuto;
                                    
                                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                                    
                                        vistaTuto = [[UIView alloc] initWithFrame:CGRectMake(0, 247, 250, 150)];
                                    }else{
                                    
                                        vistaTuto = [[UIView alloc] initWithFrame:CGRectMake(0, 340, 250, 150)];
                                    }
                                    
                                    UIImageView *tuto = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 250, 150)];
                                    
                                    [tuto setImage:[UIImage imageNamed:@"globomorado.png"]];
                                    
                                    [vistaTuto setTag:10000];
                                    
                                    
                                    UILabel *texto = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 220, 70)];
                                    
                                    [texto setText:@"Envía un mensaje privado al encargado del lugar, solo él podra verlo."];
                                    [texto setTextColor:[UIColor whiteColor]];
                                    [texto setFont:[UIFont systemFontOfSize:13]];
                                    [texto setNumberOfLines:0];
                                    
                                    UIButton *entiendoMensajeDirecto = [[UIButton alloc] initWithFrame:CGRectMake(tuto.frame.size.width-80, 90, 60, 20)];
                                    
                                    [entiendoMensajeDirecto setTitle:@"Entiendo" forState:UIControlStateNormal];
                                    [entiendoMensajeDirecto.titleLabel setFont:[UIFont systemFontOfSize:12]];
                                    [entiendoMensajeDirecto setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                                    [entiendoMensajeDirecto addTarget:self action:@selector(cerrarTutoMensajeDirecto:) forControlEvents:UIControlEventTouchUpInside];
                                    [vistaTuto addSubview:tuto];
                                    [vistaTuto addSubview:texto];
                                    [vistaTuto addSubview:entiendoMensajeDirecto];
                                    
                                    [self.view addSubview:vistaTuto];
                                }
                            }
                            
                            if([self.view viewWithTag:10] == NULL){
                                
                                UIButton *candado;
                                
                                if([[UIScreen mainScreen] bounds].size.height <= 480){
                                
                                    candado = [[UIButton alloc] initWithFrame:CGRectMake(241, 394, 25, 30)];
                                }else{
                                
                                    candado = [[UIButton alloc] initWithFrame:CGRectMake(240, 482, 25, 30)];
                                }
                                
                                [candado setBackgroundImage:[UIImage imageNamed:@"candado.png"] forState:UIControlStateNormal];
                                
                                candado.layer.borderColor = [[UIColor colorWithRed:0 green:180.0/255.0 blue:180.0/255.0 alpha:1] CGColor];
                                candado.layer.borderWidth = 1.0;
                                
                                [candado setTag:10];
                                
                                [candado setAlpha:1.0];
                                
                                [candado addTarget:self action:@selector(privatizarMensaje:) forControlEvents:UIControlEventTouchUpInside];
                                
                                [self.view addSubview:candado];
                            }
                            
                        });
                        
                        
                    }
                    
                    if([[[json objectForKey:@"personas"] objectForKey:@"cantidad"] intValue] != 0){
            
                        dispatch_async(dispatch_get_main_queue(), ^{
                            numeroPersonas.text = [NSString stringWithFormat:@"%@ personas",[[json objectForKey:@"personas"] objectForKey:@"cantidad"]];
                        });
                    }
                
                    if([[[json objectForKey:@"amigos"] objectForKey:@"cantidad"] intValue] != 0){
            
                        dispatch_async(dispatch_get_main_queue(), ^{
                            numeroAmigos.text = [NSString stringWithFormat:@"%@ amigos",[[json objectForKey:@"amigos"] objectForKey:@"cantidad"]];
                        });
                    }
            
                    if([json objectForKey:@"votos"] != NULL && [[[json objectForKey:@"votos"] objectForKey:@"numeroDeVotos"] intValue] != 0){
                    
                        dispatch_async(dispatch_get_main_queue(), ^{
            
                            numeroVotos.text = [NSString stringWithFormat:@"%@ votos",[[json objectForKey:@"votos"] objectForKey:@"numeroDeVotos"]];

                            [UIView animateWithDuration:0.9 animations:^{
                    
                                barraVotos.frame = CGRectMake(barraVotos.frame.origin.x, barraVotos.frame.origin.y, ([[[json objectForKey:@"votos"] objectForKey:@"votos"] doubleValue]*16400)/100, barraVotos.frame.size.height);
                            }];
                        });
                    }
                    
                    if([[json objectForKey:@"ahi"] intValue] == 1){
            
                        
                        if(segmentControl.selectedSegmentIndex == 0){
                            //se muestra boton escribir mensaje
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [comentarLugar setAlpha:1];
                                [enviarMensajeBoton setAlpha:1];
                                [textoNoEstasAqui setHidden:YES];
                                
                            });
                            
                            
                            if([[json objectForKey:@"votado"] intValue] == 0){
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    votoPositivo.alpha = 1.0;
                                    votoNegativo.alpha = 1.0;
                                });
                                
                            }else{
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [votoPositivo removeFromSuperview];
                                    [votoNegativo removeFromSuperview];
                                });
                            }
                        }
                    }else{
            
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            votoPositivo = nil;
                            votoNegativo = nil;
                            [votoPositivo removeFromSuperview];
                            [votoNegativo removeFromSuperview];
                            if([segmentControl selectedSegmentIndex] == 0){
                                [textoNoEstasAqui setHidden:NO];
                            }
                        });
                    }
            
                    topPlace.text = @"";
                    
                }
            
                dispatch_async(dispatch_get_main_queue(), ^{
        
                    [activity stopAnimating];
                    [activity removeFromSuperview];
                    terminaCarga = 1;
                });
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [datos fire];
                    timerDatosAlready = YES;
                });
            }
        }];

    }@catch(NSException *e){
        
        [self getDatos];
    }
}

-(void)getMensajes{
    
    @try{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            for(int i =0; i< [comentariosLugar count]; i++){
                
                NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:0];
                UITableViewCell *celda = (UITableViewCell*)[tabla cellForRowAtIndexPath:index];
                                
                NSDate *fechacomentario = [NSDate date];
                
                NSDateComponents *componentes = [[NSDateComponents alloc] init];
                [componentes setSecond:([[NSDate date] timeIntervalSince1970] - [[[comentariosLugar objectAtIndex:index.row] objectForKey:@"fecha"] doubleValue])];
                
                NSCalendar *calendario = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                
                fechacomentario = [calendario dateFromComponents:componentes];
                
                NSDateComponents *fechaComponentes = [calendario components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:fechacomentario];
                
                UILabel *tiemporestante = (UILabel*)[celda viewWithTag:4];
                
                if([fechaComponentes hour] != 0){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        tiemporestante.text = [NSString stringWithFormat:@"Hace %ld hora",(long)[fechaComponentes hour]];
                    });
                }else if([fechaComponentes minute] != 0){
                    
                    if([fechaComponentes minute] == 1){
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            tiemporestante.text = [NSString stringWithFormat:@"Hace %ld minuto",(long)[fechaComponentes minute]];
                        });
                    }else{
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            tiemporestante.text = [NSString stringWithFormat:@"Hace %ld minutos",(long)[fechaComponentes minute]];
                        });
                    }
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        tiemporestante.text = @"Hace unos segundos";
                        [tiemporestante sizeToFit];
                    });
                }
            }
            
        });
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getmensajeslugar.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&usuario=%@&ultimo=%d&token=%@",idLugar,[defaults objectForKey:@"identifier"], ultimoMensaje,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                
                [self getMensajes];
            }else{

                NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                if(![json isEqual:[NSNull null]]){
                    
                    if(json == NULL){
                        
                        NSMutableArray *elementosAEliminar = [NSMutableArray array];
                        for(NSDictionary *elemento in comentariosLugar){
                            
                            if(([[elemento objectForKey:@"currenttime"] intValue] - [[elemento objectForKey:@"fecha"] intValue]) > 86400){
                                
                                [elementosAEliminar addObject:elemento];
                            }
                        }
                        
                        if([elementosAEliminar count] > 0){
                            
                            [comentariosLugar removeObjectsInArray:elementosAEliminar];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [tabla reloadData];
                            });
                        }
                        
                    }else{
                        
                        if([[[json objectAtIndex:0] objectForKey:@"mensajeID"] intValue] > 0){
                            ultimoMensaje = [[[json objectAtIndex:[json count]-1] objectForKey:@"mensajeID"] intValue];


                            for(NSDictionary *intervalo in json){
                                NSLog(@"%@",intervalo);
                                [comentariosLugar insertObject:intervalo atIndex:0];

                            }
                            
                            NSMutableArray *elementosAEliminar = [NSMutableArray array];
                            for(NSDictionary *elemento in comentariosLugar){
                                
                                if(([[elemento objectForKey:@"currenttime"] intValue] - [[elemento objectForKey:@"fecha"] intValue]) > 86400){
                                    
                                    [elementosAEliminar addObject:elemento];
                                }
                            }
                            
                            if([elementosAEliminar count] > 0){
                                
                                [comentariosLugar removeObjectsInArray:elementosAEliminar];
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [tabla reloadData];
                            });
                            
                        }
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [mensajes fire];
                    timerMensajesAlready = YES;
                });
            }
        }];
        
    }@catch(NSException *e){

        [self getMensajes];
    }
}

-(void)getNuevosMensajes{
    
    @try{
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        
        
        NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getnuevosmensajes.php"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:40];
        
        [peticion setHTTPMethod:@"POST"];
        
        if([comentariosLugar count] > 0){
                        
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&usuario=%@&token=%@&ultimoID=%d",idLugar,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"],[[[comentariosLugar objectAtIndex:0] objectForKey:@"mensajeID"] intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
        }else{
        
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&usuario=%@&token=%@&ultimoID=%d",idLugar,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"],0] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
            }else if(data == nil){
                
            }else{
                
                NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                if(json == NULL){
                    
                }else{
                    
                    
                    if([[[json objectAtIndex:0] objectForKey:@"numeroDeVotos"] intValue] > 0){
                        
                        numeroVotos.text = [NSString stringWithFormat:@"%@ votos",[[json objectAtIndex:0] objectForKey:@"numeroDeVotos"]];
                        
                        [UIView beginAnimations:nil context:NULL];
                        [UIView setAnimationDelegate:self];
                        [UIView setAnimationDuration:0.9];
                        [UIView setAnimationBeginsFromCurrentState:YES];
                        
                        barraVotos.frame = CGRectMake(barraVotos.frame.origin.x, barraVotos.frame.origin.y, ([[[json objectAtIndex:0] objectForKey:@"votos"] intValue]*164)/100, barraVotos.frame.size.height);
                        
                        [UIView commitAnimations];
                    }else{
                        
                        numeroVotos.text = @"0 votos";
                        
                        [UIView beginAnimations:nil context:NULL];
                        [UIView setAnimationDelegate:self];
                        [UIView setAnimationDuration:0.9];
                        [UIView setAnimationBeginsFromCurrentState:YES];
                        
                        barraVotos.frame = CGRectMake(barraVotos.frame.origin.x, barraVotos.frame.origin.y, 164, barraVotos.frame.size.height);
                        
                        [UIView commitAnimations];
                    }
                    
                    if([[[json objectAtIndex:0] objectForKey:@"mensajeID"] intValue] > 0){
                        ultimoMensaje = [[[json objectAtIndex:[json count]-1] objectForKey:@"mensajeID"] intValue];
                        
                        if([comentariosLugar count] > 0){
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                for(NSDictionary *intervalo in json){
                                    
                                    [comentariosLugar insertObject:intervalo atIndex:0];
                                    [tabla beginUpdates];
                                    [tabla insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
                                    [tabla endUpdates];
                                    
                                }
                            });
                        }else{
                        
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                for(NSDictionary *intervalo in json){
                                    
                                    [comentariosLugar insertObject:intervalo atIndex:0];
                                    [tabla reloadData];
                                }
                            });
                        }
                    }
                }
            }
        }];
        
        tabla.contentSize = [tabla sizeThatFits:CGSizeMake(CGRectGetWidth(tabla.bounds), CGFLOAT_MAX)];
    }@catch(NSException *e){
        
        [self getNuevosMensajes];
    }

}

#pragma mark eventos votos

-(IBAction)votoPositivo:(id)sender{
    
    [UIView animateWithDuration:0.4 animations:^{
        
        votoPositivo.alpha = 0.0;
        votoNegativo.alpha = 0.0;
    }];
    
    votoPositivo = nil;
    votoNegativo = nil;
    [votoPositivo removeFromSuperview];
    [votoNegativo removeFromSuperview];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/addvoto.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"voto=positivo&lugar=%d&usuario=%@&token=%@",idLugar,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut){
        
        }else if(error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            
            [[[UIAlertView alloc] initWithTitle:@"No estas conectado a Internet" message:@"Verifica por favor tu conexión a Internet e inténtalo de nuevo." delegate:self cancelButtonTitle:@"Muy bien" otherButtonTitles: nil] show];
        }else{
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

            if(json == NULL || json == 0){
           
            }else{
                
                if([[json objectForKey:@"numeroDeVotos"] intValue] > 0){

                    numeroVotos.text = [NSString stringWithFormat:@"%@ votos",[json objectForKey:@"numeroDeVotos"]];
                
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UIView animateWithDuration:0.4 animations:^{

                            barraVotos.frame = CGRectMake(barraVotos.frame.origin.x, barraVotos.frame.origin.y, ([[json objectForKey:@"votos"] intValue]*164)/100, barraVotos.frame.size.height);
                            
                          /*  comentarLugar.frame = CGRectMake(comentarLugar.frame.origin.x, 270, comentarLugar.frame.size.width, comentarLugar.frame.size.height);
                            enviarComentario.frame = CGRectMake(enviarComentario.frame.origin.x, 275, enviarComentario.frame.size.width, enviarComentario.frame.size.height);*/
                            
                        }];
                    });
                }
            }
        }
    }];
}

-(IBAction)votoNegativo:(id)sender{

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:2];
    [UIView setAnimationBeginsFromCurrentState:YES];
    votoPositivo.alpha = 0.0;
    votoNegativo.alpha = 0.0;
    [UIView commitAnimations];
    votoPositivo = nil;
    votoNegativo = nil;
    [votoPositivo removeFromSuperview];
    [votoNegativo removeFromSuperview];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/addvoto.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"voto=negativo&lugar=%d&usuario=%@&token=%@",idLugar,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut){
            
        }else if(error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            
            [[[UIAlertView alloc] initWithTitle:@"No estas conectado a Internet" message:@"Verifica por favor tu conexión a Internet e inténtalo de nuevo." delegate:self cancelButtonTitle:@"Muy bien" otherButtonTitles: nil] show];
        }else{
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            if(json == NULL || json == 0){
                
            }else{
                
                if([[json objectForKey:@"numeroDeVotos"] intValue] > 0){
                    
                    numeroVotos.text = [NSString stringWithFormat:@"%@ votos",[json objectForKey:@"numeroDeVotos"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UIView animateWithDuration:0.4 animations:^{
                            
                            barraVotos.frame = CGRectMake(barraVotos.frame.origin.x, barraVotos.frame.origin.y, ([[json objectForKey:@"votos"] intValue]*164)/100, barraVotos.frame.size.height);
                            
                            /*comentarLugar.frame = CGRectMake(comentarLugar.frame.origin.x, 270, comentarLugar.frame.size.width, comentarLugar.frame.size.height);
                            enviarComentario.frame = CGRectMake(enviarComentario.frame.origin.x, 275, enviarComentario.frame.size.width, enviarComentario.frame.size.height);*/
                            
                        }];
                    });
                }
            }
        }
    }];
}

#pragma mark - text view 

-(void)textFieldDidBeginEditing:(UITextField *)textField{

    if([textField isEqual:comentarLugar]){
        
        [UIView animateWithDuration:0.4 animations:^{
            
            
            self.view.frame = CGRectMake(self.view.frame.origin.x, -225, self.view.frame.size.width, self.view.frame.size.height);
            self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y-100, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
        }];
    }
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

    UITouch *toque = [[event allTouches] anyObject];
    
    if([comentarLugar isFirstResponder] && [toque view] != comentarLugar){
        
        
        if([[comentarLugar text] isEqualToString:@""] && !imagenCargada){
        
            [enviarMensajeBoton setTitle:@"" forState:UIControlStateNormal];
            [enviarMensajeBoton setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
            [constraintWidthEnviar setConstant:30];
        }
        
        [comentarLugar resignFirstResponder];
        
        [UIView animateWithDuration:0.6 animations:^{
            
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
            self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y+100, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
        }];
    }
    
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - hechos por mi

-(IBAction)enviarMensaje:(id)sender{
    
    [enviarMensajeBoton setEnabled:NO];
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    if(privado == NO){
        
        if(!seTomaFoto){
            
            if(!imagenCargada){
                
                [comentarLugar resignFirstResponder];
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    
                    NSData *data =[[comentarLugar text] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
                    NSString *mensaje = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/nuevomensajelugar.php"]];
                    
                    [peticion setHTTPMethod:@"POST"];
                    [peticion setHTTPBody:[[NSString stringWithFormat:@"msj=%@&idLugar=%d&usuario=%@&token=%@",mensaje,idLugar,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                    
                    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                        
                        if(error.code == NSURLErrorTimedOut){
                            
                            UIAlertController *noconection = [UIAlertController alertControllerWithTitle:nil message:@"Verifica por favor tu conexión a Internet e inténtalo de nuevo." preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
                            
                            [noconection addAction:dismiss];
                            [self presentViewController:noconection animated:YES completion:nil];
                            
                        }else if(error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                            
                            
                            [[[UIAlertView alloc] initWithTitle:@"No estas conectado a Internet" message:@"Verifica por favor tu conexión a Internet e inténtalo de nuevo." delegate:self cancelButtonTitle:@"Muy bien" otherButtonTitles: nil] show];
                        }else{
                            
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [comentarLugar setText:@""];
                                [enviarMensajeBoton setEnabled:YES];
                                app.networkActivityIndicatorVisible = NO;
                            });
                        }
                    }];
                    
                });
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
                    self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y+100, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
                }];
                
                UIButton *candado = [self.view viewWithTag:10];
                
                [enviarMensajeBoton setTitle:@"" forState:UIControlStateNormal];
                [enviarMensajeBoton setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                [constraintWidthEnviar setConstant:30];
                [constraintWidthTextField setConstant:220];
                
                if(candado != nil){
                    [candado setFrame:CGRectMake(240, 482, 25, 30)];
                }
                seTomaFoto = YES;
                
            }else{
                
                NSData *data =[[comentarLugar text] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
                NSString *textoFoto = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                [comentarLugar setText:@""];
                
                [enviarMensajeBoton setTitle:@"" forState:UIControlStateNormal];
                [enviarMensajeBoton setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                [constraintWidthEnviar setConstant:30];
                
                if([comentarLugar isFirstResponder]){
                    
                    [UIView animateWithDuration:0.6 animations:^{
                        
                        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
                        self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y+100, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
                    }];
                    
                    [comentarLugar resignFirstResponder];
                }

                UIImageView *foto = (UIImageView *)[self.view viewWithTag:101];
                UIButton *cerrar = (UIButton *)[self.view viewWithTag:102];
                
                [cerrar removeFromSuperview];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                
                
                UIImageView *progressView = [[UIImageView alloc] initWithFrame:foto.frame];
                
                [progressView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
                
                [progressView setTag:104];
                
                [self.view addSubview:progressView];
                
                UIApplication *app = [UIApplication sharedApplication];
                app.networkActivityIndicatorVisible = YES;
                
                bytesSubidosArchivoAsubir = 0;
                totalBytesArchivoAsubir = [dataFotoNueva length];
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/subirfotoenlugar.php"]];
                    
                    [request setHTTPMethod:@"POST"];
                    
                    NSMutableData *cuerpo = [NSMutableData data];
                    
                    NSString *boundary = @"---------------------------14737809831466499882746641449";
                    
                    NSString *contenido = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                    
                    NSString *linea;
                    
                    [request addValue:contenido forHTTPHeaderField:@"Content-Type"];
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    if(videofoto == 1){
                        
                        linea = @"foto";
                        [cuerpo appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"foto\"; filename=\"%@.jpg\"\r\n",linea] dataUsingEncoding:NSUTF8StringEncoding]];
                    }else if(videofoto == 2){
                        
                        linea = @"video";
                        [cuerpo appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"foto\"; filename=\"%@.mp4\"\r\n",linea] dataUsingEncoding:NSUTF8StringEncoding]];
                    }
                    
                    
                    linea = @"Content-Type: application/octet-stream\r\n\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    [cuerpo appendData:dataFotoNueva];
                    linea = @"\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"Content-Disposition: form-data; name=\"id\"\r\n\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    [cuerpo appendData:[[NSString stringWithFormat:@"%@",[defaults objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"Content-Disposition: form-data; name=\"lugar\"\r\n\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    [cuerpo appendData:[[NSString stringWithFormat:@"%d",idLugar] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"Content-Disposition: form-data; name=\"mensaje\"\r\n\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    [cuerpo appendData:[[NSString stringWithFormat:@"%@",textoFoto] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                   /* [request setHTTPBody:cuerpo];
                    
                    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                    
                    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                        
                        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                            
                            
                        }else{
                            
                            NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                            
                            if([respuesta isEqualToString:@"listo"]){
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    app.networkActivityIndicatorVisible = NO;
                                    [foto removeFromSuperview];
                                    [progressView removeFromSuperview];
                                });
                            }else{
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [[[UIAlertView alloc] initWithTitle:@"Hubo un error" message:@"La foto no pudo subirse al servidor, por favor intentalo nuevamente" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                                });
                            }
                        }
                        
                    }];*/
                    
                    
                    
                    
                    [request setHTTPBody:cuerpo];
                    
                    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                    
                    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                    
                    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                                
                                
                            }else{
                                
                                NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                
                                if([respuesta isEqualToString:@"listo"]){
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        app.networkActivityIndicatorVisible = NO;
                                        [foto removeFromSuperview];
                                        [progressView removeFromSuperview];
                                        seTomaFoto = YES;
                                        imagenCargada = NO;
                                        
                                        UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:dataFotoNueva], nil, nil, nil);
                                    });
                                }else{
                                    
                                    [[[UIAlertView alloc] initWithTitle:@"Hubo un error" message:@"La foto no pudo subirse al servidor, por favor intentalo nuevamente" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                                    
                                    app.networkActivityIndicatorVisible = NO;
                                    [progressView removeFromSuperview];
                                    
                                    UIButton *cerrar = [[UIButton alloc] initWithFrame:CGRectMake((foto.frame.origin.x+foto.frame.size.width)-5, foto.frame.origin.y-5, 15, 15)];
                                    
                                    [cerrar setTag:102];
                                    [cerrar setBackgroundImage:[UIImage imageNamed:@"tachafnegro.png"] forState:UIControlStateNormal];
                                    
                                    [cerrar addTarget:self action:@selector(cerrarImagen:) forControlEvents:UIControlEventTouchUpInside];
                                    
                                    [self.view addSubview:cerrar];
                                    
                                }
                            }
                        });
                    }];
                    
                    [uploadTask resume];
                    
                    
                    
                });
                
                seTomaFoto = YES;
                imagenCargada = NO;
            }
            
        }else{
            
            
            if([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied || [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined){
                
                UIAlertController *noPermisoCamara = [UIAlertController alertControllerWithTitle:nil message:@" Crowte no tiene permiso para acceder a tu cámara." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){}];
                
                UIAlertAction *cambiar = [UIAlertAction actionWithTitle:@"Ir a la configuración" style:UIAlertActionStyleDefault handler:^(UIAlertAction *accion){
                    
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }];
                
                [noPermisoCamara addAction:cambiar];
                [noPermisoCamara addAction:cancel];
                
                [self presentViewController:noPermisoCamara animated:YES completion:nil];
            }else{
                
              /*  UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = NO;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                
                [self presentViewController:picker animated:YES completion:^{}];*/
                
                camara = [[CamaraOverlayView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
                
                picker = [[UIImagePickerController alloc] init];
                
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                
                picker.showsCameraControls = NO;
                picker.toolbarHidden = YES;
                picker.navigationBarHidden = YES;
                picker.cameraOverlayView = self.view;
                
                picker.edgesForExtendedLayout = UIRectEdgeAll;
                CGAffineTransform translate = CGAffineTransformMakeTranslation(0.0, 25.0);
                picker.cameraViewTransform = CGAffineTransformScale(translate, 480.0/430.0, 680.0/430.0);
                //picker.cameraViewTransform = CGAffineTransformScale(picker.cameraViewTransform, 1, 1.12412);
                
                [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
                
                picker.cameraOverlayView = camara;
                
                [picker setDelegate:self];
                
                UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tomarVideo:)];
                
                [camara.trigger addGestureRecognizer:longTap];
                [camara.trigger addTarget:self action:@selector(tomarFoto:) forControlEvents:UIControlEventTouchUpInside];
                
                [camara.cambiarCamara addTarget:self action:@selector(cambiarCamara) forControlEvents:UIControlEventTouchUpInside];
                
                [camara.flash addTarget:self action:@selector(flash) forControlEvents:UIControlEventTouchUpInside];
                
                [camara.tacha addTarget:self action:@selector(cerrarCamara) forControlEvents:UIControlEventTouchUpInside];
                
                CamaraOverlayViewController *cm = [[CamaraOverlayViewController alloc] init];
                
                [cm setDelegate:self];
                
                [self presentViewController:cm animated:YES completion:nil];
            }
        }
        
    }else{
        
        
        ////////////////////////////////////////////
        
        
        //mensaje privado
        
        /////////////////////////////////////////////
        
        
        if(!seTomaFoto){
            
            if(!imagenCargada){
                
                [comentarLugar resignFirstResponder];
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSData *data =[[comentarLugar text] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
                    NSString *mensajeLugar = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/mensajelugar.php"]];
                    
                    [peticion setHTTPMethod:@"POST"];
                    [peticion setHTTPBody:[[NSString stringWithFormat:@"lugar=%d&usuario=%@&mensaje=%@&token=%@",idLugar,[defaults objectForKey:@"identifier"], mensajeLugar,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                    
                    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                        
                        if(error.code == NSURLErrorTimedOut){
                            
                            UIAlertController *noconection = [UIAlertController alertControllerWithTitle:nil message:@"Verifica por favor tu conexión a Internet e inténtalo de nuevo." preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
                            
                            [noconection addAction:dismiss];
                            [self presentViewController:noconection animated:YES completion:nil];
                            
                        }else if(error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                            
                            
                            [[[UIAlertView alloc] initWithTitle:@"No estas conectado a Internet" message:@"Verifica por favor tu conexión a Internet e inténtalo de nuevo." delegate:self cancelButtonTitle:@"Muy bien" otherButtonTitles: nil] show];
                        }else{
                            
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [comentarLugar setText:@""];
                                [enviarMensajeBoton setEnabled:YES];
                                app.networkActivityIndicatorVisible = NO;
                            });
                        }
                        
                    }];
                });
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
                    self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y+100, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
                }];
                
                UIButton *candado = [self.view viewWithTag:10];
                
                [enviarMensajeBoton setTitle:@"" forState:UIControlStateNormal];
                [enviarMensajeBoton setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                [constraintWidthEnviar setConstant:30];
                [constraintWidthTextField setConstant:220];
                
                if(candado != nil){
                [candado setFrame:CGRectMake(240, 482, 25, 30)];
                }
                seTomaFoto = YES;
                
            }else{
            
                NSString *textoFoto = [comentarLugar text];
                
                [comentarLugar setText:@""];
                
                [enviarMensajeBoton setTitle:@"" forState:UIControlStateNormal];
                [enviarMensajeBoton setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                [constraintWidthEnviar setConstant:30];
                
                if([comentarLugar isFirstResponder]){
                    
                    [UIView animateWithDuration:0.6 animations:^{
                        
                        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
                        self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y+100, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
                    }];
                    
                    [comentarLugar resignFirstResponder];
                }
                
                UIImageView *foto = (UIImageView *)[self.view viewWithTag:101];
                UIButton *cerrar = (UIButton *)[self.view viewWithTag:102];
                
                [cerrar removeFromSuperview];
                
                UIActivityIndicatorView *activityFoto = [[UIActivityIndicatorView alloc] initWithFrame:foto.frame];
                
                [activityFoto setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
                
                [activityFoto startAnimating];
                
                [self.view addSubview:activityFoto];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                
                
                UIApplication *app = [UIApplication sharedApplication];
                app.networkActivityIndicatorVisible = YES;
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/subirfotoenlugarpriv.php"]];
                    
                    [request setHTTPMethod:@"POST"];
                    
                    NSMutableData *cuerpo = [NSMutableData data];
                    
                    NSString *boundary = @"---------------------------14737809831466499882746641449";
                    
                    NSString *contenido = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                    
                    NSString *linea = @"foto";
                    
                    [request addValue:contenido forHTTPHeaderField:@"Content-Type"];
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    [cuerpo appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"foto\"; filename=\"%@.jpg\"\r\n",linea] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"Content-Type: application/octet-stream\r\n\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    [cuerpo appendData:dataFotoNueva];
                    linea = @"\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"Content-Disposition: form-data; name=\"id\"\r\n\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    [cuerpo appendData:[[NSString stringWithFormat:@"%@",[defaults objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"Content-Disposition: form-data; name=\"lugar\"\r\n\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    [cuerpo appendData:[[NSString stringWithFormat:@"%d",idLugar] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"Content-Disposition: form-data; name=\"mensaje\"\r\n\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    [cuerpo appendData:[[NSString stringWithFormat:@"%@",textoFoto] dataUsingEncoding:NSUTF8StringEncoding]];
                    linea = @"\r\n";
                    [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    [request setHTTPBody:cuerpo];
                    
                    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                    
                    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                        
                        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                            
                            
                        }else{
                            
                            NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                            
                            if([respuesta isEqualToString:@"listo"]){
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    app.networkActivityIndicatorVisible = NO;
                                    [foto removeFromSuperview];
                                    [activityFoto removeFromSuperview];
                                });
                            }else{
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [[[UIAlertView alloc] initWithTitle:@"Hubo un error" message:@"La foto no pudo subirse al servidor, por favor intentalo nuevamente" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                                });
                            }
                        }
                        
                    }];
                    
                    
                    
                });
            }
        }else{
        
            if([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied || [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined){
                
                UIAlertController *noPermisoCamara = [UIAlertController alertControllerWithTitle:nil message:@"Crowte no tiene permiso para acceder a tu cámara." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){}];
                
                UIAlertAction *cambiar = [UIAlertAction actionWithTitle:@"Ir a la configuración" style:UIAlertActionStyleDefault handler:^(UIAlertAction *accion){
                    
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }];
                
                [noPermisoCamara addAction:cambiar];
                [noPermisoCamara addAction:cancel];
                
                [self presentViewController:noPermisoCamara animated:YES completion:nil];
            }else{
                
                camara = [[CamaraOverlayView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
                
                picker = [[UIImagePickerController alloc] init];
                
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                
                picker.showsCameraControls = NO;
                picker.toolbarHidden = YES;
                picker.navigationBarHidden = YES;
                picker.cameraOverlayView = self.view;
                
                picker.edgesForExtendedLayout = UIRectEdgeAll;
                CGAffineTransform translate = CGAffineTransformMakeTranslation(0.0, 25.0);
                picker.cameraViewTransform = CGAffineTransformScale(translate, 480.0/430.0, 680.0/430.0);
                
                [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
                
                picker.cameraOverlayView = camara;
                
                [picker setDelegate:self];
                
                UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tomarVideo:)];
                
                [camara.trigger addGestureRecognizer:longTap];
                [camara.trigger addTarget:self action:@selector(tomarFoto:) forControlEvents:UIControlEventTouchUpInside];
                
                [camara.cambiarCamara addTarget:self action:@selector(cambiarCamara) forControlEvents:UIControlEventTouchUpInside];
                
                [camara.flash addTarget:self action:@selector(flash) forControlEvents:UIControlEventTouchUpInside];
                
                [camara.tacha addTarget:self action:@selector(cerrarCamara) forControlEvents:UIControlEventTouchUpInside];
                
                CamaraOverlayViewController *cm = [[CamaraOverlayViewController alloc] init];
                
                [cm setDelegate:self];
                
                [self presentViewController:cm animated:YES completion:nil];
            }
        }
    }
    
    [enviarMensajeBoton setEnabled:YES];
    
}

-(IBAction)opcionesLugar:(id)sender{
    
    UIAlertController *menu = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
    UIAlertAction *reportar = [UIAlertAction actionWithTitle:@"Reportar lugar" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
    
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        denunciar_lugar *denuncia = [storyboard instantiateViewControllerWithIdentifier:@"denunciar_lugar"];
        
        denuncia.nombre = nombreLugar;
        denuncia.idLugar = idLugar;
        denuncia.nombreFoto = fotoLugar;
        
        if([[fotoLugar componentsSeparatedByString:@"."][1] isEqualToString:@".jpg"]){
            
            denuncia.dataFoto = UIImageJPEGRepresentation(imagenLugar.image, 1);
        }else if([[fotoLugar componentsSeparatedByString:@"."][1] isEqualToString:@".png"]){
            
            denuncia.dataFoto = UIImagePNGRepresentation(imagenLugar.image);
        }else{
            
            denuncia.dataFoto = UIImagePNGRepresentation(imagenLugar.image);
        }
        
        [self presentViewController:denuncia animated:YES completion:nil];
        
    }];
    
    [menu addAction:reportar];
    
    if(propietario == YES){
        
        UIAlertAction *reclamar = [UIAlertAction actionWithTitle:@"Verificar lugar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *accion){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            ReclamarLugarUno *reclamarlugar = [storyboard instantiateViewControllerWithIdentifier:@"reclamarLugarInicio"];
            
            reclamarlugar.idlugar = idLugar;
            
            [self presentViewController:reclamarlugar animated:YES completion:nil];
        }];
        
        [menu addAction:reclamar];
    }
    
    UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){}];
    
    
    
    [menu addAction:cancelar];
    
    [self presentViewController:menu animated:YES completion:nil];
    
    
}

- (IBAction)seleccionDeControl:(UISegmentedControl *)sender {
    
    UIButton *candado = (UIButton *)[self.view viewWithTag:10];
    
    UIImageView *fotoTomada = (UIImageView*)[self.view viewWithTag:101];
    UIButton *cerrarFotoTomada = (UIButton*)[self.view viewWithTag:102];
    
    if(terminaCarga == 1){
        
        switch(segmentControl.selectedSegmentIndex){
                
            case 0:{
                
                [tabla setAlpha:0];
                [etiquetaMensajes setAlpha:0];
                [enviarMensajeBoton setHidden:NO];
                
                
                if(enlugar != 0 && votado == 0){
                    
                    [votoPositivo setAlpha:1];
                    [votoNegativo setAlpha:1];
                }
                
                if(fotoTomada != nil && cerrarFotoTomada != nil){
                    
                    [fotoTomada setAlpha:1];
                    [cerrarFotoTomada setAlpha:1];
                }
                
                if(enlugar != 0){
                    
                    [comentarLugar setAlpha:1];
                    if(candado != nil){
                        [candado setAlpha:1];
                    }
                    [textoNoEstasAqui setHidden:YES];
                }if(enlugar == 0){
                    
                    [textoNoEstasAqui setHidden:NO];
                    [comentarLugar setAlpha:0];
                    if(candado != nil){
                        [candado setAlpha:0];
                    }
                }

                [numeroPersonas setAlpha:1];
                [numeroAmigos setAlpha:1];
                [numeroVotos setAlpha:1];
                
                break;
            }
                
            case 1:{
                
                
                if(fotoTomada != nil && cerrarFotoTomada != nil){
                    
                    [fotoTomada setAlpha:0];
                    [cerrarFotoTomada setAlpha:0];
                }
                
                [enviarMensajeBoton setAlpha:0];
                [votoPositivo setAlpha:0];
                [votoNegativo setAlpha:0];
                [comentarLugar setAlpha:0];
                if(candado != nil){
                    [candado setAlpha:0];
                }
                [numeroPersonas setAlpha:0];
                [numeroAmigos setAlpha:0];
                [numeroVotos setAlpha:0];
                [enviarMensajeBoton setHidden:YES];
                [textoNoEstasAqui setHidden:YES];
                
                [tabla setAlpha:1];
                [etiquetaMensajes setAlpha:1];
                
                break;
            }
                
            case 2:{
                
                [tabla setAlpha:0];
                [etiquetaMensajes setAlpha:0];
                [votoPositivo setAlpha:0];
                [votoNegativo setAlpha:0];
                [comentarLugar setAlpha:0];

                [numeroPersonas setAlpha:0];
                [numeroAmigos setAlpha:0];
                [numeroVotos setAlpha:0];
                [textoNoEstasAqui setHidden:YES];
                
                if(enlugar != 0){
                    
                    [enviarMensajeBoton setHidden:NO];
                }else{
                    
                    [enviarMensajeBoton setHidden:YES];
                }
                
                break;
            }
                
            default:{
                
                break;
            }
        }
    }
}


-(void)likeComentario:(id)sender{
    
    
    if([sender tag] == 5){
        UIButton *corazon = (UIButton *)sender;
        UIButton *numDeAmor = (UIButton *) [[sender superview] viewWithTag:6];
        UITableViewCell *celda = (UITableViewCell *)[[sender superview] superview];
        NSIndexPath *indexPath = [(UITableView*)[[[[sender superview] superview] superview] superview] indexPathForCell:celda];
        
            
        if([[corazon backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"grayheart.png"]]){
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&mensajeID=%@&token=%@&act=1",[[defaults objectForKey:@"identifier"] intValue], [[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
            }];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [corazon setBackgroundImage:[UIImage imageNamed:@"redheart.png"] forState:UIControlStateNormal];
                [numDeAmor setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                int num = [[[numDeAmor titleLabel]text] intValue];
                [numDeAmor setTitle:[NSString stringWithFormat:@"%d",num++] forState:UIControlStateNormal];
            });
        }else{
            
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&mensajeID=%@&token=%@&act=2",[[defaults objectForKey:@"identifier"] intValue], [[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [corazon setBackgroundImage:[UIImage imageNamed:@"grayheart.png"] forState:UIControlStateNormal];
                [numDeAmor setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                int num = [[[numDeAmor titleLabel]text] intValue];
                if(num == 0){
                    [numDeAmor setTitle:@"" forState:UIControlStateNormal];
                }else{
                    [numDeAmor setTitle:[NSString stringWithFormat:@"%d",--num] forState:UIControlStateNormal];
                }
            });
        }
        
    }else if([sender tag] == 6){
        UIButton *numDeAmor = (UIButton *)sender;
        UIButton *corazon = (UIButton *) [[sender superview] viewWithTag:5];
        UITableViewCell *celda = (UITableViewCell *)[[sender superview] superview];
        NSIndexPath *indexPath = [(UITableView*)[[[[sender superview] superview] superview] superview] indexPathForCell:celda];
        
        
        if([[corazon backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"grayheart.png"]]){
            
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&mensajeID=%@&token=%@&act=1",[[defaults objectForKey:@"identifier"] intValue], [[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
            }];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [corazon setBackgroundImage:[UIImage imageNamed:@"redheart.png"] forState:UIControlStateNormal];
                [numDeAmor setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                int num = [[[numDeAmor titleLabel]text] intValue];
                [numDeAmor setTitle:[NSString stringWithFormat:@"%d",++num] forState:UIControlStateNormal];
            });
        }else{
            
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&mensajeID=%@&token=%@&act=2",[[defaults objectForKey:@"identifier"] intValue], [[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [corazon setBackgroundImage:[UIImage imageNamed:@"grayheart.png"] forState:UIControlStateNormal];
                [numDeAmor setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                int num = [[[numDeAmor titleLabel]text] intValue];
                if(num == 0){
                    [numDeAmor setTitle:@"" forState:UIControlStateNormal];
                }else{
                    [numDeAmor setTitle:[NSString stringWithFormat:@"%d",--num] forState:UIControlStateNormal];
                }
            });
        }
        
    }
}

-(IBAction)privatizarMensaje:(id)sender{

    UIButton *candado = (UIButton*)[self.view viewWithTag:10];
    
    if(privado == YES){
    
        [candado setBackgroundColor:[UIColor colorWithRed:0 green:100.0/255.0 blue:100.0/255.0 alpha:0]];
        [comentarLugar setPlaceholder:@"Envía un comentario"];
        privado = NO;
    }else{
    
        [candado setBackgroundColor:[UIColor colorWithRed:0 green:100.0/255.0 blue:100.0/255.0 alpha:1]];
        [comentarLugar setPlaceholder:@"Envía un mensaje privado"];
        privado = YES;
    }
}

- (IBAction)textoCambio:(id)sender {
    
    UIButton *candado = [self.view viewWithTag:10];
    
    if([[comentarLugar text] isEqualToString:@""] && !imagenCargada){
        
        [enviarMensajeBoton setTitle:@"" forState:UIControlStateNormal];
        [enviarMensajeBoton setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
        [constraintWidthEnviar setConstant:30];
        [constraintWidthTextField setConstant:220];
        if(candado != nil){
            [candado setFrame:CGRectMake(240, 482, 25, 30)];
        }
        seTomaFoto = YES;
    }else{
        
        [UIView animateWithDuration:0.2 animations:^{
            [enviarMensajeBoton setTitle:@"Enviar" forState:UIControlStateNormal];
            [enviarMensajeBoton setBackgroundImage:nil forState:UIControlStateNormal];
            [constraintWidthEnviar setConstant:45];
            [constraintWidthTextField setConstant:210];
            if(candado != nil){
                [candado setFrame:CGRectMake(230, 482, 25, 30)];
            }
        }];
        seTomaFoto = NO;
    }
}


-(IBAction)cerrarImagen:(id)sender{

    UIImageView *imagen = (UIImageView *)[self.view viewWithTag:101];
    UIButton *cerrar = (UIButton *)[self.view viewWithTag:102];
    
    [imagen removeFromSuperview];
    [cerrar removeFromSuperview];
    
    
    imagenCargada = false;
    
    UIButton *candado = [self.view viewWithTag:10];
    
    if([[comentarLugar text] isEqualToString:@""] && !imagenCargada){
        
        [enviarMensajeBoton setTitle:@"" forState:UIControlStateNormal];
        [enviarMensajeBoton setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
        [constraintWidthEnviar setConstant:30];
        [constraintWidthTextField setConstant:220];
        if(candado != nil){
            [candado setFrame:CGRectMake(240, 482, 25, 30)];
        }
        seTomaFoto = YES;
    }
    
}


-(void)expandirImagen:(UIGestureRecognizer *)gesto{
   
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UITableViewCell *celda = (UITableViewCell*)[gesto.view superview];
    NSIndexPath *indexpath = [tabla indexPathForCell:celda];
    UIImageView *imagenView = (UIImageView*)[celda viewWithTag:1];
    UIImageView *fotoPublicacion = (UIImageView*)gesto.view;
    
    
    if([[[comentariosLugar objectAtIndex:indexpath.row] objectForKey:@"esVideo"] intValue] == 0){
        UIGraphicsBeginImageContext(self.view.frame.size);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        [self.view.layer renderInContext:context];
        
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        ImagenPubLugarViewController *imgPub = [storyboard instantiateViewControllerWithIdentifier:@"imgPubLugarView"];
        
        imgPub.fotoAnterior = img;
        imgPub.fotoAmostrar = [(UIImageView*)gesto.view image];
        imgPub.imagenUsuario = UIImageJPEGRepresentation([imagenView image], 90);
        imgPub.json = [comentariosLugar objectAtIndex:indexpath.row];
        [self presentViewController:imgPub animated:NO completion:^{}];
        
        
    }else if([[[comentariosLugar objectAtIndex:indexpath.row] objectForKey:@"esVideo"] intValue] == 1){
    
        if(![[layer superlayer] isEqual:celda.layer]){
            [layer removeFromSuperlayer];
        }
        
        if(![[controlesReproductor superview] isEqual:celda]){
            [controlesReproductor removeFromSuperview];
        }
        
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"crowtevideo%@.mp4",[[comentariosLugar objectAtIndex:indexpath.row] objectForKey:@"idMensaje"]]];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if([fileManager fileExistsAtPath:filePath]){
            
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            
            if(reproductor){
                
                [reproductor removeObserver:self forKeyPath:@"status"];
                [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
            }
            
            reproductor = [[AVPlayer alloc] init];
            
            layer = [AVPlayerLayer playerLayerWithPlayer:reproductor];
            [layer setBackgroundColor:[[UIColor whiteColor] CGColor]];
            
            
            
            if([celda viewWithTag:10]){
                
                controlesReproductor = [celda viewWithTag:10];
                controlesReproductor.frame = fotoPublicacion.frame;
            }else{
                controlesReproductor = [[UIView alloc] init];
                controlesReproductor.frame = fotoPublicacion.frame;
                [controlesReproductor setTag:10];
            }
            
            AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:filePath]];
            
            AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
            
            reproductor = [AVPlayer playerWithPlayerItem:item];
            
            reproductor.actionAtItemEnd = AVPlayerActionAtItemEndNone;
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loopVideo:) name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
            
            [layer setPlayer:reproductor];
            
            [reproductor addObserver:self forKeyPath:@"status" options:0 context:nil];
            
            UITapGestureRecognizer *pausaVideo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pausarVideo)];
            
            UIPanGestureRecognizer *cerrarVideo = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(cerrarVideoFullScreen:)];
            
            UIView *fullScreenVideo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
            
            layer.frame = fullScreenVideo.frame;
            [fullScreenVideo.layer addSublayer:layer];
            [fullScreenVideo addGestureRecognizer:pausaVideo];
            [fullScreenVideo addGestureRecognizer:cerrarVideo];
            popupFullSVideo = [KLCPopup popupWithContentView:fullScreenVideo showType:KLCPopupShowTypeBounceIn dismissType:KLCPopupDismissTypeBounceOut maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:YES dismissOnContentTouch:NO];
            
            [popupFullSVideo show];
            
            [reproductor play];
            
            urlVideoApasar = [NSURL fileURLWithPath:filePath];
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            
        }else{
        
            
            NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/%@",[[comentariosLugar objectAtIndex:indexpath.row] objectForKey:@"mensajeUsuarioID"],[[comentariosLugar objectAtIndex:indexpath.row] objectForKey:@"fotoPublicacion"]]];
            
            
            NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
            
            NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
            
            NSURLSessionDownloadTask *uploadTask = [urlSession downloadTaskWithURL:videoURL];
            
            [uploadTask resume];
            
            if(reproductor){
                
                [reproductor removeObserver:self forKeyPath:@"status"];
                [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
            }
            reproductor = [[AVPlayer alloc] init];
            
            layer = [AVPlayerLayer playerLayerWithPlayer:reproductor];
            [layer setBackgroundColor:[[UIColor blackColor] CGColor]];
            
            if([celda viewWithTag:10]){
                
                controlesReproductor = [celda viewWithTag:10];
                controlesReproductor.frame = fotoPublicacion.frame;
            }else{
                controlesReproductor = [[UIView alloc] init];
                controlesReproductor.frame = fotoPublicacion.frame;
                [controlesReproductor setTag:10];
            }
            
             UITapGestureRecognizer *pausaVideo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pausarVideo)];
            
            [controlesReproductor setBackgroundColor:[UIColor blackColor]];
            
            [controlesReproductor addGestureRecognizer:pausaVideo];
            
            UIProgressView *progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
            
            [progress setTrackTintColor:[UIColor grayColor]];
            [progress setProgressTintColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
            [progress setFrame:CGRectMake(controlesReproductor.frame.origin.x, controlesReproductor.frame.origin.y+(controlesReproductor.frame.size.height/2), controlesReproductor.frame.size.width, controlesReproductor.frame.size.height)];
            [progress setTag:100];
            
            [celda addSubview:controlesReproductor];
            [celda addSubview:progress];
        }
        
    }
    
}

-(IBAction)cerrarTutoMensajeDirecto:(id)sender{
    
    if([self.view viewWithTag:10000] != NULL){
    
        [[self.view viewWithTag:10000] removeFromSuperview];
        
        ocultarTutoMensajeDirecto = YES;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/tutorialvisto.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&recurso=2",[[defaults objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
        }];
    }
}

-(void)tomarVideo:(UILongPressGestureRecognizer *)gesto{
    
    if(gesto.state == UIGestureRecognizerStateBegan){
        
        if(!alreadyVideo){
            
            // 1 foto, 2 video
            videofoto = 2;
            
            picker.mediaTypes = [NSArray arrayWithObject:(NSString*)kUTTypeMovie];
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [picker startVideoCapture];
            
            alreadyVideo = YES;
        }
    }
    
    if(gesto.state == UIGestureRecognizerStateEnded){
        
        [picker stopVideoCapture];
        alreadyVideo = NO;
    }
    
}

-(void)tomarFoto:(UILongPressGestureRecognizer *)gesto{
    
    videofoto = 1;
    [picker setCameraCaptureMode:UIImagePickerControllerCameraCaptureModePhoto];
    [picker takePicture];
    
}

-(void)cambiarCamara{
    
    if([picker cameraDevice] == UIImagePickerControllerCameraDeviceRear){
        
        [picker setCameraDevice:UIImagePickerControllerCameraDeviceFront];
    }else if([picker cameraDevice] == UIImagePickerControllerCameraDeviceFront){
        
        [picker setCameraDevice:UIImagePickerControllerCameraDeviceRear];
    }
}

-(void)flash{
    
    if([picker cameraFlashMode] == UIImagePickerControllerCameraFlashModeOn){
        
        [camara.flash setBackgroundImage:[UIImage imageNamed:@"flashno.png"] forState:UIControlStateNormal];
        picker.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
    }else if([picker cameraFlashMode] == UIImagePickerControllerCameraFlashModeOff){
        
        [camara.flash setBackgroundImage:[UIImage imageNamed:@"flashsi.png"] forState:UIControlStateNormal];
        picker.cameraFlashMode = UIImagePickerControllerCameraFlashModeOn;
    }
}

-(void)cerrarCamara{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)loopVideo:(NSNotification *)notification{
    
    AVPlayerItem *item = [notification object];
    
    [item seekToTime:kCMTimeZero];
}

-(void)pausarVideo{
    
    if([reproductor rate] == 0.0){
        [reproductor play];
    }else{
        
        [reproductor pause];
    }
}

-(void)cerrarVideoFullScreen:(UIPanGestureRecognizer *)gesto{
    
    [popupFullSVideo dismiss:YES];
    [reproductor pause];
    [controlesReproductor removeFromSuperview];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

#pragma mark - observer

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
    if(object == reproductor && [keyPath isEqualToString:@"status"]){
        
        if(reproductor.status == AVPlayerStatusReadyToPlay){
            
            [reproductor play];
        }else if(reproductor.status == AVPlayerStatusFailed){
            
            NSLog(@"Error: %@",reproductor.error);
        }
    }
}

#pragma mark - camaraoverlayviewcontrollerdelegate

-(void)getImageVideoTomado:(NSData *)data videoImagen:(int)videoimagen{
    
    /* UITextField *comentarioVistaVotar = (UITextField *)[vistaVotar viewWithTag:1];
     UIButton *candado = [self.view viewWithTag:10];*/
    
    videofoto = videoimagen;
    dataFotoNueva = data;
    
    if(videofoto == 1){
        
        UIImageView *imagen = [[UIImageView alloc] initWithFrame:CGRectMake(50, 370, 100, 100)];
        
        if([[UIScreen mainScreen] bounds].size.height < 560){
            
            imagen.frame = CGRectMake(50, 280, 100, 100);
        }
        
        [imagen setCenter:CGPointMake(self.view.center.x, imagen.center.y)];
        
        imagen.layer.cornerRadius = 5;
        imagen.layer.borderWidth = 0.5;
        imagen.layer.masksToBounds = YES;
        imagen.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4] CGColor];
        [imagen setImage:[[UIImage alloc] initWithData:data]];
        [imagen setTag:101];
        
        [self.view addSubview:imagen];
        
        UIButton *cerrar = [[UIButton alloc] initWithFrame:CGRectMake((imagen.frame.origin.x+imagen.frame.size.width)-5, imagen.frame.origin.y-5, 15, 15)];
        
        [cerrar setTag:102];
        [cerrar setBackgroundImage:[UIImage imageNamed:@"tachafnegro.png"] forState:UIControlStateNormal];
        
        [cerrar addTarget:self action:@selector(cerrarImagen:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:cerrar];
        
        UIButton *candado = (UIButton*)[self.view viewWithTag:10];
        
        [UIView animateWithDuration:0.1 animations:^{
            [enviarMensajeBoton setTitle:@"Enviar" forState:UIControlStateNormal];
            [enviarMensajeBoton setBackgroundImage:nil forState:UIControlStateNormal];
            [constraintWidthEnviar setConstant:45];
            [constraintWidthTextField setConstant:210];
            
            if(candado != nil){
                [candado setFrame:CGRectMake(230, 482, 25, 30)];
                
                if([[UIScreen mainScreen] bounds].size.height < 560){
                    
                    imagen.frame = CGRectMake(230, 382, 25, 30);
                }
            }
        }];
        
        seTomaFoto = NO;
        imagenCargada = YES;
        
        
        
    }else if(videofoto == 2){
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"thefile.mov"];
        
        NSURL *urlvd = [NSURL URLWithString:filePath];
        
        [[NSFileManager defaultManager] createFileAtPath:[urlvd path] contents:data attributes:nil];
        
        NSURL *videoUrl = [NSURL fileURLWithPath:filePath];
        
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoUrl options:nil];
        
        AVAssetImageGenerator *imgGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        
        imgGenerator.appliesPreferredTrackTransform = YES;
        
        NSError *error = NULL;
        
        CMTime time = CMTimeMake(1, 60);
        CGImageRef imgRef = [imgGenerator copyCGImageAtTime:time actualTime:NULL error:&error];
        
        UIImageView *imagen = [[UIImageView alloc] initWithFrame:CGRectMake(50, 370, 100, 100)];
        UIImage *imgCG = [[UIImage alloc] initWithCGImage:imgRef];
        
        if([[UIScreen mainScreen] bounds].size.height < 560){
            
            imagen.frame = CGRectMake(50, 280, 100, 100);
        }
        
        [imagen setCenter:CGPointMake(self.view.center.x, imagen.center.y)];
        
        imagen.layer.cornerRadius = 5;
        imagen.layer.borderWidth = 0.5;
        imagen.layer.masksToBounds = YES;
        imagen.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4] CGColor];
        [imagen setImage:imgCG];
        [imagen setTag:101];
        
        [self.view addSubview:imagen];
        
        UIButton *cerrar = [[UIButton alloc] initWithFrame:CGRectMake((imagen.frame.origin.x+imagen.frame.size.width)-5, imagen.frame.origin.y-5, 15, 15)];
        
        [cerrar setTag:102];
        [cerrar setBackgroundImage:[UIImage imageNamed:@"tachafnegro.png"] forState:UIControlStateNormal];
        
        [cerrar addTarget:self action:@selector(cerrarImagen:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:cerrar];
        
        UIButton *candado = (UIButton*)[self.view viewWithTag:10];
        
        [UIView animateWithDuration:0.1 animations:^{
            [enviarMensajeBoton setTitle:@"Enviar" forState:UIControlStateNormal];
            [enviarMensajeBoton setBackgroundImage:nil forState:UIControlStateNormal];
            [constraintWidthEnviar setConstant:45];
            [constraintWidthTextField setConstant:210];
            
            if(candado != nil){
                [candado setFrame:CGRectMake(230, 482, 25, 30)];
                
                if([[UIScreen mainScreen] bounds].size.height < 560){
                    
                    imagen.frame = CGRectMake(230, 382, 25, 30);
                }
            }
        }];
        
        seTomaFoto = NO;
        imagenCargada = YES;
        
    }
}


#pragma mark - circular progress

-(void)dibujarProgresoCircular:(UIImageView*)imagenView progreso:(float)progreso{
    
    [self.view addSubview:imagenView];
    
    UIGraphicsBeginImageContextWithOptions(imagenView.frame.size, NO, 0);
    
    UIBezierPath *bazierPath = [UIBezierPath bezierPath];
    
    [bazierPath moveToPoint:CGPointMake(50, 50)];
    [bazierPath addArcWithCenter:CGPointMake(50, 50) radius:50 startAngle:(0*M_PI)/2 endAngle:((progreso*0.04)*M_PI)/2  clockwise:YES];
    [bazierPath closePath];
    
    [bazierPath setLineWidth:2];
    [[UIColor colorWithWhite:1 alpha:0.7] setStroke];
    [bazierPath stroke];
    [[UIColor colorWithWhite:1 alpha:0.7] setFill];
    [bazierPath fill];
    
    [imagenView setImage:UIGraphicsGetImageFromCurrentImageContext()];
    
    UIGraphicsEndImageContext();
    
    
    imagenView.transform = CGAffineTransformMakeRotation(4.71239);
    
}

#pragma mark - image picker controller

-(void)imagePickerController:(UIImagePickerController *)picker2 didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    [picker2 dismissViewControllerAnimated:YES completion:^{
    
        
        /**************************************
         *                                    *
         * añade las fotos a un scrollview    *
         * para poder subir muchas a la vez   *
         
         
         
        numeroDeFotos++;

        UIImageView *imagen = [[UIImageView alloc] initWithFrame:CGRectMake(((numeroDeFotos-1)*100)+(numeroDeFotos*10), 0, 100, 100)];
        
        [imagen setImage:info[UIImagePickerControllerOriginalImage]];
        
        [scrollView setScrollEnabled:YES];
        [scrollView setContentSize:CGSizeMake(((numeroDeFotos+2)*100), 100)];
        
        [scrollView addSubview:imagen];
        
        if(numeroDeFotos > 3){
            
            [scrollView setContentOffset:CGPointMake(scrollView.contentSize.width - scrollView.bounds.size.width, 0) animated:YES];
        }
         
         ***************************************************/
        
        dataFotoNueva = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], 0.8);
        
        UIImageView *imagen = [[UIImageView alloc] initWithFrame:CGRectMake(50, 370, 100, 100)];
        
        if([[UIScreen mainScreen] bounds].size.height <= 480){
            
            imagen.frame = CGRectMake(50, 280, 100, 100);
        }
        
        [imagen setCenter:CGPointMake(self.view.center.x, imagen.center.y)];
        
        imagen.layer.cornerRadius = 5;
        imagen.layer.borderWidth = 0.5;
        imagen.layer.masksToBounds = YES;
        imagen.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4] CGColor];
        [imagen setImage:info[UIImagePickerControllerOriginalImage]];
        [imagen setTag:101];
        
        [self.view addSubview:imagen];
        
        UIButton *cerrar = [[UIButton alloc] initWithFrame:CGRectMake((imagen.frame.origin.x+imagen.frame.size.width)-5, imagen.frame.origin.y-5, 15, 15)];
        
        [cerrar setTag:102];
        [cerrar setBackgroundImage:[UIImage imageNamed:@"tachafnegro.png"] forState:UIControlStateNormal];
        
        [cerrar addTarget:self action:@selector(cerrarImagen:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:cerrar];
        
        UIButton *candado = (UIButton*)[self.view viewWithTag:10];
        
        [UIView animateWithDuration:0.2 animations:^{
            [enviarMensajeBoton setTitle:@"Enviar" forState:UIControlStateNormal];
            [enviarMensajeBoton setBackgroundImage:nil forState:UIControlStateNormal];
            [constraintWidthEnviar setConstant:45];
            [constraintWidthTextField setConstant:210];
            if(candado != nil){
                [candado setFrame:CGRectMake(230, 482, 25, 30)];
            }
        }];
        
        seTomaFoto = NO;
        imagenCargada = YES;
        
        
    }];

}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker2{
    
    [picker2 dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - nsurlsession

-(void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
    
    
    NSLog(@"se envió data %ld esperados %ld de %ld",(long)bytesWritten, (long)totalBytesWritten, (long)totalBytesExpectedToWrite);
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    
   /* UITableViewCell *celda = (UITableViewCell*)[controlesReproductor superview];
    
    UIProgressView *progressView = (UIProgressView*)[celda viewWithTag:100];
    
    NSError *errormv;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"crowtevideo%@.mov",[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"idMensaje"]]];
    
    [fileManager copyItemAtPath:[location path] toPath:filePath error:&errormv];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:filePath]];
        
        AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
        
        reproductor = [AVPlayer playerWithPlayerItem:item];
        
        reproductor.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loopVideo:) name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
        
        [layer setPlayer:reproductor];
        
        [reproductor addObserver:self forKeyPath:@"status" options:0 context:nil];
        
        [progressView removeFromSuperview];
        
        [reproductor play];
        
        urlVideoApasar = [NSURL fileURLWithPath:filePath];
        
    });*/
    
    UITableViewCell *celda = (UITableViewCell*)[controlesReproductor superview];
    
    UITableView *tablaComentarios = (UITableView*)[[celda superview] superview];
    NSIndexPath *indexPath = [tablaComentarios indexPathForCell:celda];
    
    UIView *fullScreenVideo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    UIProgressView *progressView = (UIProgressView*)[celda viewWithTag:100];
    
    NSError *errormv;
    
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"crowtevideo%@.mov",[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"idMensaje"]]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    [fileManager copyItemAtPath:[location path] toPath:filePath error:&errormv];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:filePath]];
        
        AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
        
        reproductor = [AVPlayer playerWithPlayerItem:item];
        
        reproductor.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loopVideo:) name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
        
        [layer setPlayer:reproductor];
        
        [reproductor addObserver:self forKeyPath:@"status" options:0 context:nil];
        
        [progressView removeFromSuperview];
        
        UITapGestureRecognizer *pausaVideo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pausarVideo)];
        
        UIPanGestureRecognizer *cerrarVideo = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(cerrarVideoFullScreen:)];
        
        layer.frame = fullScreenVideo.frame;
        [fullScreenVideo.layer addSublayer:layer];
        [fullScreenVideo addGestureRecognizer:pausaVideo];
        [fullScreenVideo addGestureRecognizer:cerrarVideo];
        popupFullSVideo = [KLCPopup popupWithContentView:fullScreenVideo showType:KLCPopupShowTypeBounceIn dismissType:KLCPopupDismissTypeBounceOut maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:YES dismissOnContentTouch:NO];
        
        [popupFullSVideo show];
        
        [reproductor play];
        
        urlVideoApasar = [NSURL fileURLWithPath:filePath];
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        
    });
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        float porcentaje = ((totalBytesWritten*100)/totalBytesExpectedToWrite);
        
        porcentaje /= 100;
        
        UITableViewCell *celda = (UITableViewCell*)[controlesReproductor superview];
        
        UIProgressView *progressView = (UIProgressView*)[celda viewWithTag:100];
        
        [progressView setProgress:porcentaje];
    });
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes{}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    
}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend{
    
    NSLog(@"subio %llu%%",(totalBytesSent*100)/totalBytesArchivoAsubir);
    
    UIImageView *progressView = (UIImageView*)[self.view viewWithTag:104];
    
    [self dibujarProgresoCircular:progressView progreso:(totalBytesSent*100)/totalBytesArchivoAsubir];
}

#pragma mark - segue


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if([segue.destinationViewController isKindOfClass:[ResponderMensajeViewController class]]){
    
        ResponderMensajeViewController *responderMensaje = (ResponderMensajeViewController*)segue.destinationViewController;
        
        UITableViewCell *celda = (UITableViewCell *)[[sender superview] superview];
        NSIndexPath *indexPath = [(UITableView*)[[[[sender superview] superview] superview] superview] indexPathForCell:celda];
        
        UIImageView *fotoImagenView = (UIImageView*)[celda viewWithTag:1];
        
        responderMensaje.nombre = [NSString stringWithFormat:@"%@ %@",[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioNombre"],[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioApellido"]];
        
        responderMensaje.fotoUsuario = UIImagePNGRepresentation([fotoImagenView image]);
        responderMensaje.publicacion = [[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"mensaje"];
        responderMensaje.fecha = [[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fecha"];
        responderMensaje.numDeAmor = [NSString stringWithFormat:@"%@",[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"likes"] ];
        responderMensaje.datosPublicacion = [comentariosLugar objectAtIndex:indexPath.row];

        if(![[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]] && ![[[comentariosLugar objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqualToString:@"(null)"]){

            responderMensaje.dataImagenComentario = UIImageJPEGRepresentation([(UIImageView*)[celda viewWithTag:9] image], 1.0);
        }
    }
}

@end
