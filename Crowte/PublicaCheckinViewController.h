//
//  PublicaCheckinViewController.h
//  Crowte
//
//  Created by Paco Escobar on 20/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublicaCheckinViewController : UIViewController <NSURLSessionDelegate, NSURLSessionDownloadDelegate>

@property int idLugar;

@property NSString *nombreLugar;

@property NSString *direccionLugar;

@property NSString *stringFotoPrincipalLugar;
@property NSString *stringFotoTraseraLugar;

@property CLGeocoder *geocoder;
@property CLLocation *locacion;

@property (strong, nonatomic) IBOutlet UIButton *botonContinuar;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomBoton;
@property (strong, nonatomic) IBOutlet UIImageView *fotoPrincipalLugar;
@property (strong, nonatomic) IBOutlet UIImageView *fotoTraseraLugar;

@property (strong, nonatomic) IBOutlet UILabel *labelNombreLugar;

@property (strong, nonatomic) IBOutlet UILabel *labelDireccionLugar;
@property (strong, nonatomic) IBOutlet UITextField *campoMensaje;


-(IBAction)enviarCheckin:(id)sender;

@end
