//
//  NavActividadViewController.m
//  Crowte
//
//  Created by Paco Escobar on 12/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import "NavActividadViewController.h"
#import "ActividadViewController.h"

@interface NavActividadViewController ()

@end

@implementation NavActividadViewController

@synthesize checkinsTodosOtros,checkinsTodosUltimos;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        
    ActividadViewController *actividadVC = (ActividadViewController*)[self topViewController];
    
    [actividadVC setCheckinsTodosOtros:checkinsTodosOtros];
    [actividadVC setCheckinsTodosUltimos:checkinsTodosUltimos];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
