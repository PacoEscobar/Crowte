//
//  ReclamarLugarDosTableViewController.h
//  Crowte
//
//  Created by Paco Escobar on 10/04/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReclamarLugarDosTableViewController : UITableViewController <UITextFieldDelegate>

@property int idlugar;
@property (strong, nonatomic) IBOutlet UITableView *tabla;
@property (weak, nonatomic) IBOutlet UITextField *nombre;
@property (weak, nonatomic) IBOutlet UITextField *telefono;
@property (weak, nonatomic) IBOutlet UIDatePicker *de;
@property (weak, nonatomic) IBOutlet UIDatePicker *a;
@property (weak, nonatomic) IBOutlet UILabel *error;

-(IBAction)cancelar;
-(IBAction)enviar;

@end
