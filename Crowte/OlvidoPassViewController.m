//
//  OlvidoPassViewController.m
//  Crowte
//
//  Created by Paco Escobar on 29/07/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "OlvidoPassViewController.h"
#import "login.h"

@interface OlvidoPassViewController ()

@end

@implementation OlvidoPassViewController

@synthesize correo, botonEnviar;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ocultarTeclado)];
    
    [self.view addGestureRecognizer:tap];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)enviar:(id)sender {
    
    UIButton *boton = (UIButton*)sender;
    
    [boton setEnabled:NO];
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    if([self checarCorreo:[correo text]] == YES){
        
        NSURL *url = [NSURL URLWithString:@"https://www.crowte.com/app/asincronos/recuperapw.php"];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[[NSString stringWithFormat:@"correo=%@",[correo text]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController *seenvio = [UIAlertController alertControllerWithTitle:nil message:@"Recuerda revisar tu correo no deseado, en caso de que no esté en tu bandeja de entrada." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
                
                [seenvio addAction:dismiss];
                
                [self presentViewController:seenvio animated:YES completion:nil];
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                login *log = [storyboard instantiateViewControllerWithIdentifier:@"login"];
                
                [self presentViewController:log animated:YES completion:nil];
            });
            
        }];
    }else{
    
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [boton setEnabled:YES];
            
            UIAlertController *formato = [UIAlertController alertControllerWithTitle:nil message:@"El correo está mal escrito" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
            
            [formato addAction:dismiss];
            
            [self presentViewController:formato animated:YES completion:nil];
        });
    }
}

-(BOOL)checarCorreo:(NSString *) correotxt
{
    NSString *expresion = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *probarCorreo = [NSPredicate predicateWithFormat:@"Self matches %@", expresion];
    
    return [probarCorreo evaluateWithObject:[correotxt stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [correo resignFirstResponder];
    [self enviar:botonEnviar];
    
    return YES;
}

-(void)ocultarTeclado{

    if([correo isFirstResponder]){
    
        [correo resignFirstResponder];
    }
}

@end
