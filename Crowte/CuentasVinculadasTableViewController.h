//
//  CuentasVinculadasTableViewController.h
//  Crowte
//
//  Created by Paco Escobar on 29/05/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface CuentasVinculadasTableViewController : UITableViewController <FBSDKAppInviteDialogDelegate>


@property (weak, nonatomic) IBOutlet UISwitch *publicarFacebookSwitch;

-(void)actualizarCompartirFacebook:(id) sender;

@end
