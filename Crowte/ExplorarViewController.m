//
//  ExplorarViewController.m
//  Crowte
//
//  Created by Paco Escobar on 24/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIImageView+WebCache.h>
#import "ExplorarViewController.h"
#import "PerfilLugarViewController.h"

@interface ExplorarViewController ()

@end

@implementation ExplorarViewController

@synthesize lugares, tabla, geocoder, fondoExplore, textoNoSeEncontraron, intervaloLugaresMas, tablaFiltroAmbiente, flechaMenu, iconOpcionFiltro, nombreOpcionFiltro, heightOpcionesFiltro, menuAbierto, vistaOpcionesFiltro, lugaresDisponibles, lugaresMasGustados, lugaresMasVisitados, lugaresConMejorMusica, lugaresConBuenServicio, lugaresConServicioMasRapido, lugaresSeleccionados, numeroDeOpcionSeleccionada, lugaresDisponiblesViejos, lugaresMasGustadosViejos, lugaresMasVisitadosViejos, lugaresSeleccionadosViejos, lugaresConMejorMusicaViejos, lugaresConBuenServicioViejos, lugaresConServicioMasRapidoViejos;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    lugares = [NSDictionary dictionary];
    geocoder = [[CLGeocoder alloc] init];
    
    menuAbierto = NO;
    
    numeroDeOpcionSeleccionada = 0;
    
    [[UINavigationBar appearance] setTranslucent:YES];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"transparente.png"]];
    
    UIView *division = [[UIView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.width, 0.5)];
    
    [division setTag:1];
    
    [division setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.3]];
    
    [self.navigationController.navigationBar addSubview:division];
    
    self.navigationItem.title = NSLocalizedString(@"Explora", nil);
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
    
    [[vistaOpcionesFiltro layer] setCornerRadius:5];
    
    [[tablaFiltroAmbiente layer] setBorderWidth:0.5];
    [[tablaFiltroAmbiente layer] setBorderColor:[[UIColor colorWithWhite:0.7 alpha:1] CGColor]];
    
    intervaloLugaresMas = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getLugaresMas) userInfo:nil repeats:YES];
    
   // [intervaloLugaresMas fire];
    
    [self getLugaresMas];
    
}

-(void)viewDidAppear:(BOOL)animated{

    intervaloLugaresMas = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getLugaresMas) userInfo:nil repeats:YES];
    
    [intervaloLugaresMas fire];
    
    if(![self.navigationController.navigationBar viewWithTag:1]){
        UIView *division = [[UIView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.width, 0.5)];
        
        [division setTag:1];
        
        [division setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.3]];
        
        [self.navigationController.navigationBar addSubview:division];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [intervaloLugaresMas invalidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)mostrarEsconderMenu:(id)sender {
    
    if(menuAbierto){
        
        [flechaMenu setImage:[UIImage imageNamed:@"flechanegra.png"]];
        [UIView animateWithDuration:0.5 animations:^{
            
            [tablaFiltroAmbiente setFrame:CGRectMake(tablaFiltroAmbiente.frame.origin.x, tablaFiltroAmbiente.frame.origin.y, tablaFiltroAmbiente.frame.size.width, 0)];
            
            [heightOpcionesFiltro setConstant:0];
        }];
        
    }else{
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [tablaFiltroAmbiente setFrame:CGRectMake(tablaFiltroAmbiente.frame.origin.x, tablaFiltroAmbiente.frame.origin.y, tablaFiltroAmbiente.frame.size.width, 300)];
            
            [heightOpcionesFiltro setConstant:300];
        }];
    }
    
    menuAbierto = !menuAbierto;
    
}

-(void)getLugaresMas{

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getdatoslugaresmas.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){

        if(error == nil){
            
            if(data != nil){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSError *error;
                    
                    NSDictionary *lugaresNuevos;
                    
                    lugaresNuevos = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];

                    if(![lugares isEqual:lugaresNuevos]){
                        
                        lugares = lugaresNuevos;
                        
                        if(![[lugares objectForKey:@"visitados"] isEqual:[NSNull null]] || ![[lugares objectForKey:@"visitadosPasados"] isEqual:[NSNull null]]){
                            
                            
                            if(numeroDeOpcionSeleccionada == 0){
                                
                                if(![[lugares objectForKey:@"visitados"] isEqual:[NSNull null]]){
                                    
                                    lugaresMasVisitados = [NSMutableArray arrayWithArray:[lugares objectForKey:@"visitados"]];
                                    
                                    lugaresSeleccionados = [NSMutableArray arrayWithArray:[lugares objectForKey:@"visitados"]];
                                    
                                }else{
                                    
                                    lugaresSeleccionados = nil;
                                    lugaresMasVisitados = nil;
                                }
                                
                                if(![[lugares objectForKey:@"visitadosPasados"] isEqual:[NSNull null]]){
                                    
                                    lugaresMasVisitadosViejos = [NSMutableArray arrayWithArray:[lugares objectForKey:@"visitadosPasados"]];
                                    
                                    lugaresSeleccionadosViejos = [NSMutableArray arrayWithArray:[lugares objectForKey:@"visitadosPasados"]];
                                    
                                }else{
                                    
                                    lugaresSeleccionadosViejos = nil;
                                    lugaresMasVisitadosViejos = nil;
                                }
                            }
                            
                            if(![[lugares objectForKey:@"meGustaAhora"] isEqual:[NSNull null]]){
                                
                                lugaresMasGustados = [NSMutableArray arrayWithArray:[lugares objectForKey:@"meGustaAhora"]];
                                
                            }else{
                                
                                lugaresMasGustados = nil;
                            }

                            if(![[lugares objectForKey:@"meGustaPasados"] isEqual:[NSNull null]]){
                                
                                lugaresMasGustadosViejos = [NSMutableArray arrayWithArray:[lugares objectForKey:@"meGustaPasados"]];
                                
                            }else{
                                
                                lugaresMasGustadosViejos = nil;
                            }
                            
                            if(![[lugares objectForKey:@"estaLlenoAhora"] isEqual:[NSNull null]]){
                                
                                lugaresDisponibles = [NSMutableArray arrayWithArray:[lugares objectForKey:@"estaLlenoAhora"]];
                                
                            }else{
                                
                                lugaresDisponibles = nil;
                            }
                            
                            if(![[lugares objectForKey:@"estaLlenoPasados"] isEqual:[NSNull null]]){
                                
                                lugaresDisponiblesViejos = [NSMutableArray arrayWithArray:[lugares objectForKey:@"estaLlenoPasados"]];
                                
                            }else{
                                
                                lugaresDisponiblesViejos = nil;
                            }
                            
                            
                            if(![[lugares objectForKey:@"meGustaMusicaAhora"] isEqual:[NSNull null]]){
                                
                                lugaresConMejorMusica = [NSMutableArray arrayWithArray:[lugares objectForKey:@"meGustaMusicaAhora"]];
                                
                            }else{
                                
                                lugaresConMejorMusica = nil;
                            }
                            
                            if(![[lugares objectForKey:@"meGustaMusicaPasados"] isEqual:[NSNull null]]){
                                
                                lugaresConMejorMusicaViejos = [NSMutableArray arrayWithArray:[lugares objectForKey:@"meGustaMusicaPasados"]];
                                
                            }else{
                                
                                lugaresConMejorMusicaViejos = nil;
                            }
                            
                            if(![[lugares objectForKey:@"servicioRapidoAhora"] isEqual:[NSNull null]]){
                                
                                lugaresConServicioMasRapido = [NSMutableArray arrayWithArray:[lugares objectForKey:@"servicioRapidoAhora"]];
                                
                            }else{
                                
                                lugaresConServicioMasRapido = nil;
                            }
                            
                            if(![[lugares objectForKey:@"servicioRapidoPasados"] isEqual:[NSNull null]]){
                                
                                lugaresConServicioMasRapidoViejos = [NSMutableArray arrayWithArray:[lugares objectForKey:@"servicioRapidoPasados"]];
                                
                            }else{
                                
                                lugaresConServicioMasRapidoViejos = nil;
                            }
                            
                            
                            if(![[lugares objectForKey:@"servicioBuenoAhora"] isEqual:[NSNull null]]){
                                
                                lugaresConBuenServicio = [NSMutableArray arrayWithArray:[lugares objectForKey:@"servicioBuenoAhora"]];
                                
                            }else{
                                
                                lugaresConBuenServicio = nil;
                            }
                            
                            if(![[lugares objectForKey:@"servicioBuenoPasados"] isEqual:[NSNull null]]){
                                
                                lugaresConBuenServicioViejos = [NSMutableArray arrayWithArray:[lugares objectForKey:@"servicioBuenoPasados"]];
                                
                            }else{
                                
                                lugaresConBuenServicioViejos = nil;
                            }
                            
                            [fondoExplore setAlpha:0];
                        }else{
                            
                            [tabla setAlpha:0];
                            [fondoExplore setAlpha:1];
                        }
                        
                        [tabla reloadData];
                    }
                });
                
            }
        }
        
    }];
    
    [uploadTask resume];
}

#pragma mark - table view

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if([tableView isEqual:tabla]){
    
        unsigned long visitadosCount;
        unsigned long visitadosPasadosCount;

        if([lugaresSeleccionados isEqual:[NSNull null]]){

            visitadosCount = 0;
        }else{

            visitadosCount = [lugaresSeleccionados count];
        }
        
        if([lugaresSeleccionadosViejos isEqual:[NSNull null]]){
            
            visitadosPasadosCount = 0;
        }else{
            
            visitadosPasadosCount = [lugaresSeleccionadosViejos count];
        }
        
        return visitadosPasadosCount+visitadosCount;
    }else{
        
        return 6;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if([tableView isEqual:tabla]){
        
        return 315;
    }else{
        
        return 50;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if([tableView isEqual:tabla]){
        UITableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"celdaLugar"];
        
        UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldanuevo.png"]];
        
        [celda setBackgroundView:fondo];
        [celda setBackgroundColor:[UIColor clearColor]];
        
        ///// parte que quitamos de mas visitados
        
        if(!((lugaresSeleccionados == nil) && (lugaresSeleccionadosViejos == nil))){
            
            [tabla setAlpha:1];
            [fondoExplore setAlpha:0];
            
            unsigned long visitadosCount;
            
            if([lugaresSeleccionados isEqual:[NSNull null]]){
                
                visitadosCount = 0;
            }else{
                
                visitadosCount = [lugaresSeleccionados count];
            }
            
            if(!(lugaresSeleccionados == nil) && indexPath.row < visitadosCount){
                
                UILabel *nombre = (UILabel*)[celda viewWithTag:1];
                [nombre setText:[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
                
                UILabel *direccionLugar = (UILabel*)[celda viewWithTag:2];
                
                CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"latitud"] floatValue] longitude:[[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"longitud"] floatValue]];
                
                [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error){
                    
                    CLPlacemark *placemark = [placemarks lastObject];
                    
                    [direccionLugar setText:[NSString stringWithFormat:@"%@, %@, %@",[[placemark addressDictionary] objectForKey:@"Name"],[[placemark addressDictionary] objectForKey:@"City"],[[placemark addressDictionary] objectForKey:@"State"]]];
                    
                    [direccionLugar setText:[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"dire"]];
                }];
                
                UILabel *fecha = (UILabel*)[celda viewWithTag:3];
                
                [fecha setText:@"En vivo"];
                [fecha setTextColor:[UIColor redColor]];
                
                UIImageView *fotoSecundariaLugar = (UIImageView*)[celda viewWithTag:4];
                [fotoSecundariaLugar setImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
                
                UIImageView *fotoPrincipalLugar = (UIImageView*)[celda viewWithTag:5];
                [fotoPrincipalLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
                
                [fotoPrincipalLugar setContentMode:UIViewContentModeCenter];
                
                [fotoPrincipalLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"ID"],[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
                    
                    if(error == nil){
                        
                        [fotoPrincipalLugar setContentMode:UIViewContentModeScaleToFill];
                        [[fotoPrincipalLugar layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                    }
                }];
                
                
                [[fotoPrincipalLugar layer] setBorderWidth:1];
                [[fotoPrincipalLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
                [[fotoPrincipalLugar layer] setCornerRadius:50];
                [[fotoPrincipalLugar layer] setMasksToBounds:YES];
                
                [fotoSecundariaLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"ID"],[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
                
            }else if(!(lugaresSeleccionadosViejos == nil)){
                
                UILabel *nombre = (UILabel*)[celda viewWithTag:1];
                [nombre setText:[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"lugar"]];
                
                UILabel *direccionLugar = (UILabel*)[celda viewWithTag:2];
                
                CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"latitud"] floatValue] longitude:[[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"longitud"] floatValue]];
                
                [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error){
                    
                    CLPlacemark *placemark = [placemarks lastObject];
                    
                    [direccionLugar setText:[NSString stringWithFormat:@"%@, %@, %@",[[placemark addressDictionary] objectForKey:@"Name"],[[placemark addressDictionary] objectForKey:@"City"],[[placemark addressDictionary] objectForKey:@"State"]]];
                }];
                
                UILabel *fecha = (UILabel*)[celda viewWithTag:3];
                [fecha setText:[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"fechaDescripcion"]];
                
                [fecha setTextColor:[UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1]];
                
                UIImageView *fotoSecundariaLugar = (UIImageView*)[celda viewWithTag:4];
                
                [fotoSecundariaLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"ID"],[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"fotoSecundaria"]]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
                
                UIImageView *fotoPrincipalLugar = (UIImageView*)[celda viewWithTag:5];
                [fotoPrincipalLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
                
                [fotoPrincipalLugar setContentMode:UIViewContentModeCenter];
                
                [[fotoPrincipalLugar layer] setBorderWidth:1];
                [[fotoPrincipalLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
                [[fotoPrincipalLugar layer] setCornerRadius:50];
                [[fotoPrincipalLugar layer] setMasksToBounds:YES];
                
                [fotoPrincipalLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"ID"],[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
                    
                    if(error == nil){
                        
                        [fotoPrincipalLugar setContentMode:UIViewContentModeScaleToFill];
                        [[fotoPrincipalLugar layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                    }
                }];
                
            }else{
                
                celda = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            }
        }else{
            
            [tabla setAlpha:0];
            [fondoExplore setAlpha:1];
        }
        
        
        
        
        
        /////////
        
        return celda;
    }else{
        
        UITableViewCell *celda = [tablaFiltroAmbiente dequeueReusableCellWithIdentifier:@"opcion"];
        
        UIImageView *icono = (UIImageView*)[celda viewWithTag:1];
        UILabel *nombre = (UILabel*)[celda viewWithTag:2];
        
        switch (indexPath.row) {
            case 0:
                [icono setImage:[UIImage imageNamed:@"corona.png"]];
                [nombre setText:@"More visited"];
                break;
                
            case 1:
                [icono setImage:[UIImage imageNamed:@"me_gusta_icon.png"]];
                [nombre setText:@"More liked"];
                break;
            
            case 2:
                [icono setImage:[UIImage imageNamed:@"aun_lugares_icon.png"]];
                [nombre setText:@"With seats available"];
                break;
                
            case 3:
                [icono setImage:[UIImage imageNamed:@"me_gusta_musica_icon.png"]];
                [nombre setText:@"With best music"];
                break;
                
            case 4:
                [icono setImage:[UIImage imageNamed:@"buen_servicio_icon.png"]];
                [nombre setText:@"With best service"];
                break;
            default:
                [icono setImage:[UIImage imageNamed:@"servicio_rapido_icon.png"]];
                [nombre setText:@"With quickest service"];
                break;
        }
        
        return celda;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([tableView isEqual:tabla]){
        
        UIStoryboard *storybard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PerfilLugarViewController *lugar = (PerfilLugarViewController*)[storybard instantiateViewControllerWithIdentifier:@"perfilLugar"];
        
        
        
        //////////// parte que quitamos de más visitados
        
        
        
        unsigned long visitadosCount;
        
        if([lugaresSeleccionados isEqual:[NSNull null]]){
            
            visitadosCount = 0;
        }else{
            
            visitadosCount = [lugaresSeleccionados count];
        }
        
        if(![lugaresSeleccionados isEqual:[NSNull null]] && indexPath.row < visitadosCount){

            [lugar setIdLugar:[[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"objeto"] intValue]];
            
            [lugar setNombreLugar:[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
            
            [lugar setFotoPrincipalLugar:[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"foto"]];
            
            [lugar setFotoSecundariaLugar:[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]];
            
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"latitud"] floatValue] longitude:[[[lugaresSeleccionados objectAtIndex:indexPath.row] objectForKey:@"longitud"] floatValue]];
            
            [lugar setLocation:location];
        }else{
            
            [lugar setIdLugar:[[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"ID"] intValue]];
            
            [lugar setNombreLugar:[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"lugar"]];
            
            [lugar setFotoPrincipalLugar:[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"foto"]];
            
            [lugar setFotoSecundariaLugar:[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"fotoSecundaria"]];
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"latitud"] floatValue] longitude:[[[lugaresSeleccionadosViejos objectAtIndex:indexPath.row-visitadosCount] objectForKey:@"longitud"] floatValue]];
            
            [lugar setLocation:location];
        }
        
        
        
        
        
        
        //////////////////////////
        
        UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
        self.navigationItem.backBarButtonItem = atras;
        
        [self.navigationController pushViewController:lugar animated:YES];
    }else{
        
        
        switch (indexPath.row) {
            case 0:
                [iconOpcionFiltro setImage:[UIImage imageNamed:@"corona.png"]];
                [nombreOpcionFiltro setTitle:@"More visited" forState:UIControlStateNormal];
                
                numeroDeOpcionSeleccionada = 0;
                
                if(lugaresMasVisitados != nil){
                    
                    if(lugaresSeleccionados != nil){
                        
                        [lugaresSeleccionados removeAllObjects];
                        [lugaresSeleccionados addObjectsFromArray:lugaresMasVisitados];
                    }else{
                        
                        lugaresSeleccionados = [NSMutableArray arrayWithArray:lugaresMasVisitados];
                    }
                }else{
                    
                    lugaresSeleccionados = nil;
                }
                
                if(lugaresMasVisitadosViejos != nil){
                    
                    if(lugaresSeleccionadosViejos != nil){
                        
                        [lugaresSeleccionadosViejos removeAllObjects];
                        [lugaresSeleccionadosViejos addObjectsFromArray:lugaresMasVisitadosViejos];
                    }else{
                        
                        lugaresSeleccionadosViejos = [NSMutableArray arrayWithArray:lugaresMasVisitadosViejos];
                    }
                }else{
                    
                    lugaresSeleccionadosViejos = nil;
                }
                
                break;
                
            case 1:
                [iconOpcionFiltro setImage:[UIImage imageNamed:@"me_gusta_icon.png"]];
                [nombreOpcionFiltro setTitle:@"More liked" forState:UIControlStateNormal];
                
                numeroDeOpcionSeleccionada = 1;
                
                if(lugaresMasGustados != nil){
                    
                    if(lugaresSeleccionados != nil){
                        
                        [lugaresSeleccionados removeAllObjects];
                        [lugaresSeleccionados addObjectsFromArray:lugaresMasGustados];
                    }else{
                        
                        lugaresSeleccionados = [NSMutableArray arrayWithArray:lugaresMasGustados];
                    }
                }else{
                    
                    lugaresSeleccionados = nil;
                }
                
                if(lugaresMasGustadosViejos != nil){
                    
                    if(lugaresSeleccionadosViejos != nil){
                        
                        [lugaresSeleccionadosViejos removeAllObjects];
                        [lugaresSeleccionadosViejos addObjectsFromArray:lugaresMasGustadosViejos];
                    }else{
                        
                        lugaresSeleccionadosViejos = [NSMutableArray arrayWithArray:lugaresMasGustadosViejos];
                    }
                }else{
                    
                    lugaresSeleccionadosViejos = nil;
                }
                
                break;
                
            case 2:
                [iconOpcionFiltro setImage:[UIImage imageNamed:@"aun_lugares_icon.png"]];
                [nombreOpcionFiltro setTitle:@"With seats available" forState:UIControlStateNormal];
                
                numeroDeOpcionSeleccionada = 2;
                
                if(lugaresDisponibles != nil){
                    
                    if(lugaresSeleccionados != nil){
                        
                        [lugaresSeleccionados removeAllObjects];
                        [lugaresSeleccionados addObjectsFromArray:lugaresDisponibles];
                    }else{
                        
                        lugaresSeleccionados = [NSMutableArray arrayWithArray:lugaresDisponibles];
                    }
                }else{
                    
                    lugaresSeleccionados = nil;
                }

                if(lugaresDisponiblesViejos != nil){
                    
                    if(lugaresSeleccionadosViejos != nil){
                        
                        [lugaresSeleccionadosViejos removeAllObjects];
                        [lugaresSeleccionadosViejos addObjectsFromArray:lugaresDisponiblesViejos];
                    }else{
                        
                        lugaresSeleccionadosViejos = [NSMutableArray arrayWithArray:lugaresDisponiblesViejos];
                    }
                }else{
                    
                    lugaresSeleccionadosViejos = nil;
                }
                
                break;
                
            case 3:
                [iconOpcionFiltro setImage:[UIImage imageNamed:@"me_gusta_musica_icon.png"]];
                [nombreOpcionFiltro setTitle:@"With best music" forState:UIControlStateNormal];
                
                numeroDeOpcionSeleccionada = 3;
                
                if(lugaresConMejorMusica != nil){
                    
                    if(lugaresSeleccionados != nil){
                        
                        [lugaresSeleccionados removeAllObjects];
                        [lugaresSeleccionados addObjectsFromArray:lugaresConMejorMusica];
                    }else{
                        
                        lugaresSeleccionados = [NSMutableArray arrayWithArray:lugaresConMejorMusica];
                    }
                }else{
                    
                    lugaresSeleccionados = nil;
                }
                
                if(lugaresConMejorMusicaViejos != nil){
                    
                    if(lugaresSeleccionadosViejos != nil){
                        
                        [lugaresSeleccionadosViejos removeAllObjects];
                        [lugaresSeleccionadosViejos addObjectsFromArray:lugaresConMejorMusicaViejos];
                    }else{
                        
                        lugaresSeleccionadosViejos = [NSMutableArray arrayWithArray:lugaresConMejorMusicaViejos];
                    }
                }else{
                    
                    lugaresSeleccionadosViejos = nil;
                }
                
                break;
                
            case 4:
                [iconOpcionFiltro setImage:[UIImage imageNamed:@"buen_servicio_icon.png"]];
                [nombreOpcionFiltro setTitle:@"With best service" forState:UIControlStateNormal];
                
                numeroDeOpcionSeleccionada = 4;
                
                if(lugaresConBuenServicio != nil){
                    
                    if(lugaresSeleccionados != nil){
                        
                        [lugaresSeleccionados removeAllObjects];
                        [lugaresSeleccionados addObjectsFromArray:lugaresConBuenServicio];
                    }else{
                        
                        lugaresSeleccionados = [NSMutableArray arrayWithArray:lugaresConBuenServicio];
                    }
                }else{
                    
                    lugaresSeleccionados = nil;
                }
                
                if(lugaresConBuenServicioViejos != nil){
                    
                    if(lugaresSeleccionadosViejos != nil){

                        [lugaresSeleccionadosViejos removeAllObjects];
                        [lugaresSeleccionadosViejos addObjectsFromArray:lugaresConBuenServicioViejos];
                    }else{
                        
                        lugaresSeleccionadosViejos = [NSMutableArray arrayWithArray:lugaresConBuenServicioViejos];
                    }
                }else{
                    
                    lugaresSeleccionadosViejos = nil;
                }
                
                break;
            default:
                [iconOpcionFiltro setImage:[UIImage imageNamed:@"servicio_rapido_icon.png"]];
                [nombreOpcionFiltro setTitle:@"With quickest service" forState:UIControlStateNormal];
                
                numeroDeOpcionSeleccionada = 5;
                
                if(lugaresConServicioMasRapido != nil){
                    
                    if(lugaresSeleccionados != nil){
                        
                        [lugaresSeleccionados removeAllObjects];
                        [lugaresSeleccionados addObjectsFromArray:lugaresConServicioMasRapido];
                    }else{
                        
                        lugaresSeleccionados = [NSMutableArray arrayWithArray:lugaresConServicioMasRapido];
                    }
                }else{
                    
                    lugaresSeleccionados = nil;
                }
                
                if(lugaresConServicioMasRapidoViejos != nil){
                    
                    if(lugaresSeleccionadosViejos != nil){
                        
                        [lugaresSeleccionadosViejos removeAllObjects];
                        [lugaresSeleccionadosViejos addObjectsFromArray:lugaresConServicioMasRapidoViejos];
                    }else{
                        
                        lugaresSeleccionadosViejos = [NSMutableArray arrayWithArray:lugaresConServicioMasRapidoViejos];
                    }
                }else{
                    
                    lugaresSeleccionadosViejos = nil;
                }
                
                break;
        }
        
        [tabla reloadData];
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [tablaFiltroAmbiente setFrame:CGRectMake(tablaFiltroAmbiente.frame.origin.x, tablaFiltroAmbiente.frame.origin.y, tablaFiltroAmbiente.frame.size.width, 0)];
            
            [heightOpcionesFiltro setConstant:0];
        }];
        
        [tablaFiltroAmbiente deselectRowAtIndexPath:indexPath animated:NO];
        
        menuAbierto = !menuAbierto;
    }
}

#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
