//
//  AppDelegate.m
//  Crowte
//
//  Created by Paco Escobar on 22/04/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "AppDelegate.h"
#import "NavegacionPrincipalController.h"
#import "NavegacionAmigosController.h"
#import "DeEjemplo.h"

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [self initializeStoryBoardBasedOnScreenSize];

    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:0.0/255.0 green:160.0/255.0 blue:193.0/255.0 alpha:1.0];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.backgroundColor = [UIColor colorWithRed:22.0/255.0 green:179.0/255.0 blue:173.0/255.0 alpha:1.0];

    [[UINavigationBar appearanceWhenContainedIn:[NavegacionPrincipalController class], nil] setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearanceWhenContainedIn:[NavegacionPrincipalController class], nil] setShadowImage:[UIImage new]];
    [[UINavigationBar appearanceWhenContainedIn:[NavegacionPrincipalController class], nil] setTintColor:[UIColor colorWithRed:227.0/255.0 green:220.0/255.0 blue:215.0/255.0 alpha:1.0]];
    
    [[UINavigationBar appearanceWhenContainedIn:[NavegacionAmigosController class], nil] setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearanceWhenContainedIn:[NavegacionAmigosController class], nil] setShadowImage:[UIImage new]];
    [[UINavigationBar appearanceWhenContainedIn:[NavegacionAmigosController class], nil] setTintColor:[UIColor colorWithRed:227.0/255.0 green:220.0/255.0 blue:215.0/255.0 alpha:1.0]];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    
    if(launchOptions != nil){
    
        NSDictionary *notificaciones = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        
        if(notificaciones != nil){
        
            [self handleNotificacion: notificaciones actualizarUI:NO];
        }
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        
        NSArray *archivos = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil];
        
        for (NSString *archivo in archivos) {
            
            NSError *error;
            
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^crowtevideo\\d+\\.mov$" options:0 error:&error];
            
            NSArray *matchados = [regex matchesInString:archivo options:0 range:NSMakeRange(0, [archivo length])];
            
            if([matchados count] == 1){
                
                NSDate *fileDate;
                
                NSError *errorDate;
                
                NSURL *urlVideo = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:archivo]];
                
                [urlVideo getResourceValue:&fileDate forKey:NSURLContentModificationDateKey error:&errorDate];
                
                NSTimeInterval tiempo = [fileDate timeIntervalSinceNow];
                                
                if(fabs(tiempo) > 7200){
                    
                    [[NSFileManager defaultManager] removeItemAtPath:[urlVideo path] error:&error];
                }
                
            }
        }
    });
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{

    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{

    [self handleNotificacion: userInfo actualizarUI:NO];
}

-(void)handleNotificacion:(NSDictionary*)userInfo actualizarUI:(BOOL)actualizarUI{

    [[NSNotificationCenter defaultCenter] postNotificationName:@"numeroNotificaciones" object:[[userInfo objectForKey:@"aps"] objectForKey:@"badge"]];
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

-(void)updateNotifToken{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/notificaciones/updateiosnotif.php"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1.0];
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%@&token=%@",[defaults objectForKey:@"identifier"], [defaults objectForKey:@"notifToken"]] dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];

    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){}];
    
    defaults = nil;
    peticion = nil;
    queue = nil;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [FBSDKAppEvents activateApp];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)initializeStoryBoardBasedOnScreenSize {
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {    // The iOS device = iPhone or iPod Touch
        
        
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 480)
        {   // iPhone 3GS, 4, and 4S and iPod Touch 3rd and 4th generation: 3.5 inch screen (diagonally measured)
        
            // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone35
            UIStoryboard *iPhone35Storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            // Instantiate the initial view controller object from the storyboard
            UIViewController *initialViewController = [iPhone35Storyboard instantiateInitialViewController];
            
            // Instantiate a UIWindow object and initialize it with the screen size of the iOS device
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            
            // Set the initial view controller to be the root view controller of the window object
            self.window.rootViewController  = initialViewController;
            
            // Set the window object to be the key window and show it
            [self.window makeKeyAndVisible];
        }else{
        
            // iPhone 5 and iPod Touch 5th generation: 4 inch screen (diagonally measured)
            
            // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone4
            UIStoryboard *iPhone4Storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            // Instantiate the initial view controller object from the storyboard
            UIViewController *initialViewController = [iPhone4Storyboard instantiateInitialViewController];
            
            // Instantiate a UIWindow object and initialize it with the screen size of the iOS device
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            
            // Set the initial view controller to be the root view controller of the window object
            self.window.rootViewController  = initialViewController;
            
            // Set the window object to be the key window and show it
            [self.window makeKeyAndVisible];
        }
        
        /*if (iOSDeviceScreenSize.height == 568)
        {   // iPhone 5 and iPod Touch 5th generation: 4 inch screen (diagonally measured)
            
            // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone4
            UIStoryboard *iPhone4Storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            // Instantiate the initial view controller object from the storyboard
            UIViewController *initialViewController = [iPhone4Storyboard instantiateInitialViewController];
            
            // Instantiate a UIWindow object and initialize it with the screen size of the iOS device
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            
            // Set the initial view controller to be the root view controller of the window object
            self.window.rootViewController  = initialViewController;
            
            // Set the window object to be the key window and show it
            [self.window makeKeyAndVisible];
        }*/
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        
    {   // The iOS device = iPad
        
        UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
        UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
        splitViewController.delegate = (id)navigationController.topViewController;
        
    }
}

-(void)saveContext{

    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    
    if(managedObjectContext != nil){
    
        if([managedObjectContext hasChanges] && ![managedObjectContext save:&error]){
        
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{

    [application registerForRemoteNotifications];
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{

    NSString *token = [deviceToken description];
    token = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
    [defaults setObject:token forKey:@"notifToken"];
    [defaults synchronize];
    [self updateNotifToken];

    defaults = nil;
    token = nil;
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{

    NSLog(@"Falló el registro con error: %@",error);
}

@end
