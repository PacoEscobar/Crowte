//
//  denunciar_lugar.h
//  Crowte
//
//  Created by Paco Escobar on 09/04/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface denunciar_lugar : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>

@property int idLugar;
@property NSString *nombre;
@property NSData *dataFoto;
@property NSString *nombreFoto;
@property NSMutableDictionary *arrayFotos;
@property (weak, nonatomic) IBOutlet UITableView *tabla;

-(IBAction)cancelar:(id)sender;
-(IBAction)enviar;

@end
