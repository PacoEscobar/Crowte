//
//  NotificacionesController.h
//  Crowte
//
//  Created by Paco Escobar on 07/01/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificacionesController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property int maximo;
@property NSMutableDictionary *notificaciones;
@property NSMutableDictionary *arregloFotos;
@property (strong, nonatomic) IBOutlet UITableView *tabla;

-(IBAction)aceptarAmigo:(id)sender;
-(IBAction)ignorarAmigo:(id)sender;
-(void)pedirNotificaciones;

@end
