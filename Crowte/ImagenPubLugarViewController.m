//
//  ImagenPubLugarViewController.m
//  Crowte
//
//  Created by Paco Escobar on 06/02/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "ImagenPubLugarViewController.h"

@interface ImagenPubLugarViewController ()

@end

@implementation ImagenPubLugarViewController

@synthesize fondoImgAnterior, fotoAnterior,fotoQueSeMuestra, fotoAmostrar,scrollview,constraints, ContentView, zoomed, panGesture, json, panel, cerrar, imagenUsuario, centroInicial, primerToqueMover, reproductor, layer, controlesReproductor, urlVideoApasar, progress, fullScreenVideo;



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    zoomed = NO;
    primerToqueMover = 0.0;
    centroInicial = fotoQueSeMuestra.center;
    
    [fondoImgAnterior setImage:fotoAnterior];
    
    if([[json objectForKey:@"esVideo"] intValue] == 0){
    
        [fullScreenVideo setHidden:YES];
        [fullScreenVideo setUserInteractionEnabled:NO];
        [fotoQueSeMuestra setImage:fotoAmostrar];
    }else{
    
        [fotoQueSeMuestra setHidden:YES];
        [fotoQueSeMuestra setUserInteractionEnabled:NO];
        [self cargarVideo];
    }
    
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    
    UITapGestureRecognizer *unTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(unTapeo:)];
    unTap.numberOfTouchesRequired = 1;
    
    panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(imagenMovida:)];
    
    [scrollview addGestureRecognizer:doubleTapRecognizer];
    [scrollview setMultipleTouchEnabled:YES];
    [scrollview addGestureRecognizer:panGesture];
    [scrollview addGestureRecognizer:unTap];
    
    panel = [[UIView alloc] initWithFrame:CGRectMake(0, 465, 320, 100)];
    
    if([[UIScreen mainScreen] bounds].size.height <= 480){
    
        panel.frame = CGRectMake(0, 375, 320, 100);
    }
    
    [panel setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.4]];
    
    UIImageView *imagenViewUsuario = [[UIImageView alloc] initWithFrame:CGRectMake(5, 20, 30, 30)];
    [imagenViewUsuario setImage:[UIImage imageWithData:imagenUsuario]];
    imagenViewUsuario.layer.borderWidth = 1;
    imagenViewUsuario.layer.borderColor = [[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor];
    imagenViewUsuario.layer.cornerRadius = 15;
    imagenViewUsuario.layer.masksToBounds = YES;
    
    UILabel *nombre = [[UILabel alloc] initWithFrame:CGRectMake(40,15, 300, 20)];
    [nombre setText:[NSString stringWithFormat:@"%@ %@",[json objectForKey:@"mensajeUsuarioNombre"],[json objectForKey:@"mensajeUsuarioApellido"]]];
    [nombre setTextColor:[UIColor whiteColor]];
    [nombre setFont:[UIFont systemFontOfSize:13]];
    
    UILabel *comentario = [[UILabel alloc] initWithFrame:CGRectMake(55, 20, 250, 50)];
    
    NSData *data = [[json objectForKey:@"mensaje"] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *publicacionFormateada;
    
    publicacionFormateada = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    
    [comentario setText: publicacionFormateada];
    [comentario setTextColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
    [comentario setFont:[UIFont systemFontOfSize:13]];
    
    
    UIButton *heart = [[UIButton alloc] initWithFrame:CGRectMake(50, panel.frame.size.height-20, 15, 15)];
    UIButton *numDeAmor = [[UIButton alloc] initWithFrame:CGRectMake(70, panel.frame.size.height-20, 50, 15)];
    
    if([[json objectForKey:@"likeMio"] intValue] == 1){
    
        [heart setBackgroundImage:[UIImage imageNamed:@"redheart.png"] forState:UIControlStateNormal];
        [numDeAmor setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    }else{
        
        [heart setBackgroundImage:[UIImage imageNamed:@"whiteheart.png"] forState:UIControlStateNormal];
        [numDeAmor setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    [heart setTag:5];
    [numDeAmor setTag:6];
    [numDeAmor.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [numDeAmor setTitle:[json objectForKey:@"likes"] forState:UIControlStateNormal];
    
    [heart addTarget:self action:@selector(likeComentario:) forControlEvents:UIControlEventTouchUpInside];
    [numDeAmor addTarget:self action:@selector(likeComentario:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cerrar = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-35, 20, 20, 20)];
    [cerrar setBackgroundImage:[UIImage imageNamed:@"tachafnegro.png"] forState:UIControlStateNormal];
    [cerrar addTarget:self action:@selector(cerrarImagen) forControlEvents:UIControlEventTouchUpInside];
    
    [panel addSubview:imagenViewUsuario];
    [panel addSubview:nombre];
    [panel addSubview:comentario];
    [panel addSubview:heart];
    [panel addSubview:numDeAmor];
    [self.view addSubview:panel];
    [self.view addSubview:cerrar];

}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
}

-(void)viewWillDisappear:(BOOL)animated{

    if(reproductor){
        
        [reproductor pause];
        [reproductor removeObserver:self forKeyPath:@"status"];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{

    if([[json objectForKey:@"esVideo"] intValue] == 0){
        
        if(fotoQueSeMuestra.frame.size.width > 320){
            
            zoomed = YES;
            [scrollview removeGestureRecognizer:panGesture];
        }else{
            
            zoomed = NO;
            [scrollview addGestureRecognizer:panGesture];
        }
        
        return fotoQueSeMuestra;
    }else{
    
        if(fullScreenVideo.frame.size.width > 320){
            
            zoomed = YES;
            [scrollview removeGestureRecognizer:panGesture];
        }else{
            
            zoomed = NO;
            [scrollview addGestureRecognizer:panGesture];
        }
        
        return fullScreenVideo;
    }
}


#pragma mark - hechos por mi

-(void)scrollViewDoubleTapped:(UITapGestureRecognizer *)recognizer{

    if([[json objectForKey:@"esVideo"] intValue] == 0){

        if(!zoomed){
            
            zoomed = YES;
            CGPoint pointInView = [recognizer locationInView:fotoQueSeMuestra];
            
            CGFloat newZoomScale = scrollview.zoomScale * 3.0f;
            newZoomScale = MIN(newZoomScale, scrollview.maximumZoomScale);
            
            CGSize scrollViewSize = scrollview.bounds.size;
            
            CGFloat w = scrollViewSize.width / newZoomScale;
            CGFloat h = scrollViewSize.height / newZoomScale;
            CGFloat x = pointInView.x - (w / 3.0f);
            CGFloat y = pointInView.y - (h / 3.0f);
            
            CGRect rectToZoomTo = CGRectMake(x, y, w, h);
            
            [scrollview zoomToRect:rectToZoomTo animated:YES];
            
            if(panel.alpha == 1.0){
                
                [UIView animateWithDuration:0.2 animations:^{
                    
                    panel.center = CGPointMake(panel.center.x, panel.center.y+100);
                    [panel setAlpha:0.0];
                    cerrar.center = CGPointMake(cerrar.center.x, cerrar.center.y-100);
                    [cerrar setAlpha:0.0];
                    
                }];
            }
        }else{
            
            zoomed = NO;
            CGFloat newZoomScale = scrollview.zoomScale / 3.0f;
            newZoomScale = MAX(newZoomScale, scrollview.minimumZoomScale);
            [scrollview setZoomScale:newZoomScale animated:YES];
        }
    }else{

        if(!zoomed){
            
            zoomed = YES;
            CGPoint pointInView = [recognizer locationInView:fullScreenVideo];
            
            CGFloat newZoomScale = scrollview.zoomScale * 3.0f;
            newZoomScale = MIN(newZoomScale, scrollview.maximumZoomScale);
            
            CGSize scrollViewSize = scrollview.bounds.size;
            
            CGFloat w = scrollViewSize.width / newZoomScale;
            CGFloat h = scrollViewSize.height / newZoomScale;
            CGFloat x = pointInView.x - (w / 3.0f);
            CGFloat y = pointInView.y - (h / 3.0f);
            
            CGRect rectToZoomTo = CGRectMake(x, y, w, h);
            
            [scrollview zoomToRect:rectToZoomTo animated:YES];
            
            if(panel.alpha == 1.0){
                
                [UIView animateWithDuration:0.2 animations:^{
                    
                    panel.center = CGPointMake(panel.center.x, panel.center.y+100);
                    [panel setAlpha:0.0];
                    cerrar.center = CGPointMake(cerrar.center.x, cerrar.center.y-100);
                    [cerrar setAlpha:0.0];
                    
                }];
            }
        }else{
            
            zoomed = NO;
            CGFloat newZoomScale = scrollview.zoomScale / 3.0f;
            newZoomScale = MAX(newZoomScale, scrollview.minimumZoomScale);
            [scrollview setZoomScale:newZoomScale animated:YES];
        }
    }
}

-(void)imagenMovida:(UIPanGestureRecognizer *)recognizer{

    float diferencia;
    
    CGPoint punto = [recognizer locationInView:scrollview];
    CGPoint velocidad = [recognizer velocityInView:scrollview];
    
    
    if(recognizer.state == UIGestureRecognizerStateBegan){

        primerToqueMover = punto.y;
        diferencia = fotoQueSeMuestra.center.y - primerToqueMover;
    }
    
    fotoQueSeMuestra.center = CGPointMake(fotoQueSeMuestra.center.x, ((fotoQueSeMuestra.center.y+(punto.y-primerToqueMover))/2)+130);
    

    if(recognizer.state == UIGestureRecognizerStateEnded){
    
        
        if(velocidad.y < 1500 && velocidad.y > -1500){
        
            
            [UIView animateWithDuration:0.4 animations:^{
                
                fotoQueSeMuestra.center = CGPointMake(fotoQueSeMuestra.center.x, 256.0);
                fotoQueSeMuestra.center = CGPointMake(fotoQueSeMuestra.center.x, 256.0);
            }];
        }else{
        
            if(velocidad.y < 0){
                
                [UIView animateWithDuration:0.2 animations:^{
                    
                    panel.center = CGPointMake(panel.center.x, panel.center.y+100);
                    [panel setAlpha:0.0];
                    cerrar.center = CGPointMake(cerrar.center.x, cerrar.center.y-100);
                    [cerrar setAlpha:0.0];
                    
                }];
                
                [UIView animateWithDuration:0.2 animations:^{
                    
                    fotoQueSeMuestra.center = CGPointMake(fotoQueSeMuestra.center.x, fotoQueSeMuestra.center.y - 500.0);
                    [scrollview setAlpha:0.5];
                    [fotoQueSeMuestra setAlpha:0.5];
                    [fotoQueSeMuestra setAlpha:0.5];
                } completion:^(BOOL finished){
                    
                    if(finished){
                        
                        [[UIApplication sharedApplication] setStatusBarHidden:NO];
                        [self dismissViewControllerAnimated:NO completion:nil];
                    }
                }];
                
            }else{
                
                [UIView animateWithDuration:0.2 animations:^{
                    
                    panel.center = CGPointMake(panel.center.x, panel.center.y+100);
                    [panel setAlpha:0.0];
                    cerrar.center = CGPointMake(cerrar.center.x, cerrar.center.y-100);
                    [cerrar setAlpha:0.0];
                    
                }];
                
                [UIView animateWithDuration:0.2 animations:^{
                    
                    fotoQueSeMuestra.center = CGPointMake(fotoQueSeMuestra.center.x, fotoQueSeMuestra.center.y +500.0);
                    [scrollview setAlpha:0.5];
                    [fotoQueSeMuestra setAlpha:0.5];
                    [fotoQueSeMuestra setAlpha:0.5];
                } completion:^(BOOL finished){
                    
                    if(finished){
                        
                        [[UIApplication sharedApplication] setStatusBarHidden:NO];
                        [self dismissViewControllerAnimated:NO completion:nil];
                    }
                }];
                
            }
        }
        
    }
    
}

-(void)unTapeo:(UITapGestureRecognizer *)recognizer{
    
    if(!zoomed){
        
        if(panel.alpha == 1.0){
            
            [UIView animateWithDuration:0.2 animations:^{
                
                panel.center = CGPointMake(panel.center.x, panel.center.y+100);
                [panel setAlpha:0.0];
                cerrar.center = CGPointMake(cerrar.center.x, cerrar.center.y-100);
                [cerrar setAlpha:0.0];
                
            }];
        }else{
            
            [UIView animateWithDuration:0.2 animations:^{
                
                panel.center = CGPointMake(panel.center.x, panel.center.y-100);
                [panel setAlpha:1.0];
                cerrar.center = CGPointMake(cerrar.center.x, cerrar.center.y+100);
                [cerrar setAlpha:1.0];
            }];
        }
    }
}

-(void)cerrarImagen{

    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction)likeComentario:(id)sender{

    if([sender tag] == 5){
        UIButton *corazon = (UIButton *)sender;
        UIButton *numDeAmor = (UIButton *) [super.view viewWithTag:6];
        
        if([[corazon backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"grayheart.png"]]){
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&mensajeID=%@&token=%@&act=1",[[defaults objectForKey:@"identifier"] intValue], [json objectForKey:@"mensajeID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
            }];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [corazon setBackgroundImage:[UIImage imageNamed:@"redheart.png"] forState:UIControlStateNormal];
                [numDeAmor setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                int num = [[[numDeAmor titleLabel]text] intValue];
                [numDeAmor setTitle:[NSString stringWithFormat:@"%d",num++] forState:UIControlStateNormal];
            });
        }else{
            
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&mensajeID=%@&token=%@&act=2",[[defaults objectForKey:@"identifier"] intValue], [json objectForKey:@"mensajeID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [corazon setBackgroundImage:[UIImage imageNamed:@"grayheart.png"] forState:UIControlStateNormal];
                [numDeAmor setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                int num = [[[numDeAmor titleLabel]text] intValue];
                if(num == 0){
                    [numDeAmor setTitle:@"" forState:UIControlStateNormal];
                }else{
                    [numDeAmor setTitle:[NSString stringWithFormat:@"%d",--num] forState:UIControlStateNormal];
                }
            });
        }
        
    }else if([sender tag] == 6){
        UIButton *numDeAmor = (UIButton *)sender;
        UIButton *corazon = (UIButton *) [self.view viewWithTag:5];
        
        
        if([[corazon backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"grayheart.png"]]){
            
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&mensajeID=%@&token=%@&act=1",[[defaults objectForKey:@"identifier"] intValue], [json objectForKey:@"mensajeID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
            }];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [corazon setBackgroundImage:[UIImage imageNamed:@"redheart.png"] forState:UIControlStateNormal];
                [numDeAmor setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                int num = [[[numDeAmor titleLabel]text] intValue];
                [numDeAmor setTitle:[NSString stringWithFormat:@"%d",++num] forState:UIControlStateNormal];
            });
        }else{
            
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&mensajeID=%@&token=%@&act=2",[[defaults objectForKey:@"identifier"] intValue], [json objectForKey:@"mensajeID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [corazon setBackgroundImage:[UIImage imageNamed:@"grayheart.png"] forState:UIControlStateNormal];
                [numDeAmor setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                int num = [[[numDeAmor titleLabel]text] intValue];
                if(num == 0){
                    [numDeAmor setTitle:@"" forState:UIControlStateNormal];
                }else{
                    [numDeAmor setTitle:[NSString stringWithFormat:@"%d",--num] forState:UIControlStateNormal];
                }
            });
        }
        
    }
}

-(void)cargarVideo{
    
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"crowtevideo%@.mp4",[json objectForKey:@"idMensaje"]]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if([fileManager fileExistsAtPath:filePath]){

        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        
        if(reproductor){
            
            [reproductor removeObserver:self forKeyPath:@"status"];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
        }
        
        reproductor = [[AVPlayer alloc] init];
        
        layer = [AVPlayerLayer playerLayerWithPlayer:reproductor];
        [layer setBackgroundColor:[[UIColor blackColor] CGColor]];
        
        
       
        controlesReproductor = [[UIView alloc] init];
        controlesReproductor.frame = fotoQueSeMuestra.frame;
        [controlesReproductor setTag:10];
        [scrollview addSubview:controlesReproductor];
        
        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:filePath]];
        
        AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
        
        reproductor = [AVPlayer playerWithPlayerItem:item];
        
        reproductor.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loopVideo:) name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
        
        [layer setPlayer:reproductor];
        
        [reproductor addObserver:self forKeyPath:@"status" options:0 context:nil];
                
        layer.frame = fullScreenVideo.frame;
        [fullScreenVideo.layer addSublayer:layer];
        
        [scrollview addSubview:fullScreenVideo];
        
        [reproductor play];
        
        urlVideoApasar = [NSURL fileURLWithPath:filePath];
        
    }else{

        NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/%@",[json objectForKey:@"mensajeUsuarioID"],[json objectForKey:@"fotoPublicacion"]]];
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
        
        NSURLSessionDownloadTask *uploadTask = [urlSession downloadTaskWithURL:videoURL];
        
        [uploadTask resume];
        
        if(reproductor){
            
            [reproductor removeObserver:self forKeyPath:@"status"];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
        }
        reproductor = [[AVPlayer alloc] init];
        
        layer = [AVPlayerLayer playerLayerWithPlayer:reproductor];
        [layer setBackgroundColor:[[UIColor blackColor] CGColor]];
        
        
        controlesReproductor = [[UIView alloc] init];
        controlesReproductor.frame = fotoQueSeMuestra.frame;
        [controlesReproductor setTag:10];
        
        
       // UITapGestureRecognizer *pausaVideo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pausarVideo)];
        
        //[controlesReproductor addGestureRecognizer:pausaVideo];
        
        progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        
        [progress setTrackTintColor:[UIColor grayColor]];
        [progress setProgressTintColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
        [progress setFrame:CGRectMake(controlesReproductor.frame.origin.x, controlesReproductor.frame.origin.y+(controlesReproductor.frame.size.height/2), controlesReproductor.frame.size.width, controlesReproductor.frame.size.height)];
        
        [controlesReproductor setBackgroundColor:[UIColor blackColor]];
        [scrollview addSubview:controlesReproductor];
        [scrollview addSubview:progress];
        
    }
}

-(void)loopVideo:(NSNotification *)notification{
    
    AVPlayerItem *item = [notification object];
    
    [item seekToTime:kCMTimeZero];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
    if(object == reproductor && [keyPath isEqualToString:@"status"]){
        
        if(reproductor.status == AVPlayerStatusReadyToPlay){
            
            [reproductor play];
        }else if(reproductor.status == AVPlayerStatusFailed){
            
            NSLog(@"Error: %@",reproductor.error);
        }
    }
}

#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{

    NSError *errormv;
    
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"crowtevideo%@.mp4",[json objectForKey:@"idMensaje"]]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    [fileManager copyItemAtPath:[location path] toPath:filePath error:&errormv];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:filePath]];
        
        AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
        
        reproductor = [AVPlayer playerWithPlayerItem:item];
        
        reproductor.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loopVideo:) name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
        
        [layer setPlayer:reproductor];
        
        [reproductor addObserver:self forKeyPath:@"status" options:0 context:nil];
        
        [progress removeFromSuperview];
        
        layer.frame = fotoQueSeMuestra.frame;
        
        layer.frame = fullScreenVideo.frame;
        [fullScreenVideo.layer addSublayer:layer];
        
        [scrollview addSubview:fullScreenVideo];
        
        [reproductor play];
        
        urlVideoApasar = [NSURL fileURLWithPath:filePath];
        
    });
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        float porcentaje = ((totalBytesWritten*100)/totalBytesExpectedToWrite);
        
        porcentaje /= 100;
        
        [progress setProgress:porcentaje];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
