//
//  ExplorarViewController.h
//  Crowte
//
//  Created by Paco Escobar on 24/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ExplorarViewController : UIViewController<NSURLSessionDelegate, NSURLSessionDownloadDelegate, UITableViewDelegate, UITableViewDataSource>


@property BOOL menuAbierto;

@property (strong, nonatomic) IBOutlet UILabel *textoNoSeEncontraron;
@property (strong, nonatomic) IBOutlet UITableView *tabla;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;


@property NSDictionary *lugares;
@property CLGeocoder *geocoder;

@property NSTimer *intervaloLugaresMas;

@property (strong, nonatomic) IBOutlet UIImageView *fondoExplore;

#pragma mark - menu drop down

@property (weak, nonatomic) IBOutlet UITableView *tablaFiltroAmbiente;
@property (weak, nonatomic) IBOutlet UIImageView *flechaMenu;
@property (weak, nonatomic) IBOutlet UIImageView *iconOpcionFiltro;
@property (weak, nonatomic) IBOutlet UIButton *nombreOpcionFiltro;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightOpcionesFiltro;
@property (weak, nonatomic) IBOutlet UIView *vistaOpcionesFiltro;

#pragma mark - lugares mas

@property int numeroDeOpcionSeleccionada;

@property NSMutableArray *lugaresMasVisitados;
@property NSMutableArray *lugaresMasGustados;
@property NSMutableArray *lugaresDisponibles;
@property NSMutableArray *lugaresConMejorMusica;
@property NSMutableArray *lugaresConBuenServicio;
@property NSMutableArray *lugaresConServicioMasRapido;
@property NSMutableArray *lugaresSeleccionados;

@property NSMutableArray *lugaresMasVisitadosViejos;
@property NSMutableArray *lugaresMasGustadosViejos;
@property NSMutableArray *lugaresDisponiblesViejos;
@property NSMutableArray *lugaresConMejorMusicaViejos;
@property NSMutableArray *lugaresConBuenServicioViejos;
@property NSMutableArray *lugaresConServicioMasRapidoViejos;
@property NSMutableArray *lugaresSeleccionadosViejos;

- (IBAction)mostrarEsconderMenu:(id)sender;

-(void)getLugaresMas;

@end
