//
//  denunciar_lugar.m
//  Crowte
//
//  Created by Paco Escobar on 09/04/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "denunciar_lugar.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface denunciar_lugar ()

@end

@implementation denunciar_lugar

@synthesize idLugar, nombre, dataFoto, nombreFoto, arrayFotos, tabla;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda;
    
    if(indexPath.row == 0){

        celda = [tableView dequeueReusableCellWithIdentifier:@"lugar"];
        
        UIImageView *imagen = (UIImageView*)[celda viewWithTag:1];
        
        
        
        if(dataFoto != nil){
        
            [imagen setImage:[UIImage imageWithData:dataFoto]];
        }else{
        
            if([arrayFotos objectForKey:nombreFoto]){
            
                [imagen setImage:[arrayFotos objectForKey:nombreFoto]];
            }else{
            
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%d/%@",idLugar,nombreFoto]];
                
                [imagen sd_setImageWithURL:url];
            }
        }
        
        if(![nombre isEqual:[NSNull null]] && ![nombre isEqualToString:@""] && ![nombre isEqualToString:@"(null)"]){
            
            UILabel *nombreLugar = (UILabel*)[celda viewWithTag:2];
            [nombreLugar setText:nombre];
        }
        
        
    }else if(indexPath.row == 1){
    
        celda = [tableView dequeueReusableCellWithIdentifier:@"comentario"];
        
        UITextView *textview = (UITextView*)[celda viewWithTag:1];
        
        [[textview layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [[textview layer] setBorderWidth:0.5];
    }
    
    return celda;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    int tam = 0;
    
    if(indexPath.row == 0){
    
        tam = 130;
    }else if(indexPath.row == 1){
    
        tam = 351;
    }
    
    return tam;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 2;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];

    UITextView *textview = (UITextView *)[celda viewWithTag:1];
    [textview resignFirstResponder];
}

#pragma mark - textview

-(void)textViewDidBeginEditing:(UITextView *)textView{

    [UIView animateWithDuration:0.4 animations:^{
    
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-150, self.view.frame.size.width, self.view.frame.size.height);
    }];
    
    if([[textView text] isEqualToString:@"Escribe un comentario..."]){
    
        [textView setText:@""];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView{

    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+150, self.view.frame.size.width, self.view.frame.size.height);
    }];
    
    if([[textView text] isEqualToString:@""]){
    
        [textView setText:@"Escribe un comentario..."];
    }
}

#pragma mark - mios

-(IBAction)cancelar:(id)sender{

    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)enviar{

    __block UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    UITextView *comentario = (UITextView*)[celda viewWithTag:1];
    __block UIButton *boton = (UIButton*)[celda viewWithTag:2];
    
    [boton setEnabled:NO];
    NSString *texto = [[NSString alloc] init];
    if(![[comentario text] isEqualToString:@""] && ![[comentario text] isEqualToString:@"Escribe un comentario..."]){
        
        texto = [comentario text];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/denuncia/denunciarlugar.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"ID=%d&lugar=%d&denuncia=%@&token=%@",[[defaults objectForKey:@"identifier"] intValue],idLugar,texto,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            NSLog(@"error");
        }else{

            dispatch_async(dispatch_get_main_queue(), ^{

                [UIView animateWithDuration:0.5 animations:^{
                
                
                    [boton setBackgroundColor:[UIColor colorWithRed:0 green:0.8 blue:0 alpha:1]];
                    [boton setTitle:@"Gracias!" forState:UIControlStateNormal];
                    app.networkActivityIndicatorVisible = NO;

                    
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
            });
        }
        
    }];

}

@end
