//
//  VotarLugarViewController.m
//  Crowte
//
//  Created by Paco Escobar on 24/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import "VotarLugarViewController.h"
#import "TabBarPrincipalViewController.h"
#import "CamaraOverlayViewController.h"
#import "CheckinViewController.h"
#import "CamaraOverlayView.h"
#import <Lottie/Lottie.h>

@interface VotarLugarViewController ()

@end

@implementation VotarLugarViewController

@synthesize votos, botonVotar, campoComentario, votoPositivo, votoNegativo, picker, camara, fondoFotoTomada, divisionVotos, fondoBotonesVotos, numeroDeVotos, idLugar, fotoVideo, dataFotoVideo, totalBytesArchivoAsubir, tutoTomarFotoVideo, botonMusica, botonLleno, tablaOpcionesMood, opcionesSeleccionadas;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    fotoVideo = 0;
    dataFotoVideo = nil;
    
    [votos removeObjectForKey:@"voto"];
    [votos removeObjectForKey:@"votos"];
    
    opcionesSeleccionadas = [NSMutableDictionary dictionary];

    if([votos count] > 0){
        
        if(![[votos objectForKey:@"meGusta"] isEqual:[NSNull null]]){
        
            [opcionesSeleccionadas setObject:[NSNumber numberWithInt:[[votos objectForKey:@"meGusta"] intValue]] forKey:@"meGusta"];
        }
        
        if(![[votos objectForKey:@"estaLleno"] isEqual:[NSNull null]]){
            
            [opcionesSeleccionadas setObject:[NSNumber numberWithInt:[[votos objectForKey:@"estaLleno"] intValue]] forKey:@"estaLleno"];
        }
        
        if(![[votos objectForKey:@"meGustaMusica"] isEqual:[NSNull null]]){
            
            [opcionesSeleccionadas setObject:[NSNumber numberWithInt:[[votos objectForKey:@"meGustaMusica"] intValue]] forKey:@"meGustaMusica"];
        }
        
        if(![[votos objectForKey:@"servicioRapido"] isEqual:[NSNull null]]){
            
            [opcionesSeleccionadas setObject:[NSNumber numberWithInt:[[votos objectForKey:@"servicioRapido"] intValue]] forKey:@"servicioRapido"];
        }
        
        if(![[votos objectForKey:@"servicioBueno"] isEqual:[NSNull null]]){
            
            [opcionesSeleccionadas setObject:[NSNumber numberWithInt:[[votos objectForKey:@"servicioBueno"] intValue]] forKey:@"servicioBueno"];
        }
    }

    [self.navigationController setNavigationBarHidden:NO];
    
    [[UINavigationBar appearance] setTranslucent:YES];
    
    self.navigationItem.title = @"Deja tu voto";
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"transparente.png"]];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [[UITextView appearance] setTintColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
    
    [self getTomarFotoTuto];

    
    NSLog(@"nav %@",[self.navigationController viewControllers]);
    
    for(id viewController in [self.navigationController viewControllers]){
        
        if([viewController isKindOfClass:[CheckinViewController class]]){
            
            NSLog(@"si es %@",viewController);
        }
    }
    
    
}

-(void)viewWillDisappear:(BOOL)animated{

    if(self.isMovingFromParentViewController){
    
        [self.navigationController setNavigationBarHidden:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 81;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"celdaOpcionesMood"];
    
    UIButton *botonAFavor = (UIButton*)[celda viewWithTag:1];
    
    [botonAFavor setFrame:CGRectMake(botonAFavor.frame.origin.x, botonAFavor.frame.origin.y, 163.0, botonAFavor.frame.size.height)];
    
    UIButton *botonEnContra = (UIButton*)[celda viewWithTag:2];
    
    [botonEnContra setFrame:CGRectMake(170, botonEnContra.frame.origin.y, 163.0, botonEnContra.frame.size.height)];
    
    if(indexPath.row == 0){
        
        if([celda viewWithTag:3]){
            
            [[celda viewWithTag:3] removeFromSuperview];
        }
        
        if([celda viewWithTag:4]){
            
            [[celda viewWithTag:4] removeFromSuperview];
        }
        
        [botonAFavor setTitle:NSLocalizedString(@"Me gusta el ambiente", nil) forState:UIControlStateNormal];
        
        [botonEnContra setTitle:NSLocalizedString(@"No me gusta el ambiente", nil) forState:UIControlStateNormal];
        
        
        if(![celda viewWithTag:3]){
            LOTAnimationView *nota = [LOTAnimationView animationNamed:@"me_gusta"];

            [nota setFrame:CGRectMake(botonAFavor.frame.origin.x+botonAFavor.frame.size.width-50, botonAFavor.frame.origin.y+(botonAFavor.frame.size.height/2)-15, 30, 30)];
            
            if([[opcionesSeleccionadas objectForKey:@"meGusta"] isEqual:[NSNumber numberWithInt:1]]){

                [nota setAnimationProgress:1];
                [[nota layer] setBorderWidth:1];
                
                [botonAFavor setTitleColor:[UIColor colorWithRed:39.0/255.0 green:140.0/255.0 blue:233.0/255.0 alpha:1] forState:UIControlStateNormal];
                [[nota layer] setBorderColor:[[UIColor colorWithRed:39.0/255.0 green:140.0/255.0 blue:233.0/255.0 alpha:1] CGColor]];
                
            }else{
                
                [nota setProgressWithFrame:[NSNumber numberWithInt:1]];
                [[nota layer] setBorderWidth:0];
                [botonAFavor setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            [nota setTag:3];
            
            [nota setBackgroundColor:[UIColor whiteColor]];
            
            [nota layer].borderWidth = 0;
            [[nota layer] setCornerRadius:15];
            
            [celda addSubview:nota];
            
            [celda sendSubviewToBack:nota];
        }
        
        if(![celda viewWithTag:4]){
            LOTAnimationView *notaNegativa = [LOTAnimationView animationNamed:@"no_me_gusta"];
            
            [notaNegativa setFrame:CGRectMake(botonEnContra.frame.origin.x+botonEnContra.frame.size.width-50, botonEnContra.frame.origin.y+(botonEnContra.frame.size.height/2)-15, 30, 30)];
            
            if([[opcionesSeleccionadas objectForKey:@"meGusta"] isEqual:[NSNumber numberWithInt:0]]){
                
                [notaNegativa setAnimationProgress:1];
                
                [[notaNegativa layer] setBorderWidth:1];
                [botonEnContra setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                [[notaNegativa layer] setBorderColor:[[UIColor redColor] CGColor]];
            }else{
                
                [notaNegativa setProgressWithFrame:[NSNumber numberWithInt:1]];
                
                [[notaNegativa layer] setBorderWidth:0];
                [botonEnContra setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            [notaNegativa setTag:4];
            
            [notaNegativa setBackgroundColor:[UIColor whiteColor]];
            
            [notaNegativa layer].borderWidth = 0;
            [[notaNegativa layer] setCornerRadius:15];
            
            [celda addSubview:notaNegativa];
            
            [celda sendSubviewToBack:notaNegativa];
        }
    }else if(indexPath.row == 1){
        
        if([celda viewWithTag:3]){
            
            [[celda viewWithTag:3] removeFromSuperview];
        }
        
        if([celda viewWithTag:4]){
            
            [[celda viewWithTag:4] removeFromSuperview];
        }
        
        [botonAFavor setTitle:NSLocalizedString(@"Aún hay lugares", nil) forState:UIControlStateNormal];
        
        [botonEnContra setTitle:NSLocalizedString(@"Está lleno", nil) forState:UIControlStateNormal];
        
        if(![celda viewWithTag:3]){
            
            
            LOTAnimationView *nota = [LOTAnimationView animationNamed:@"aun_lugares"];
            
            [nota setFrame:CGRectMake(botonAFavor.frame.origin.x+botonAFavor.frame.size.width-50, botonAFavor.frame.origin.y+(botonAFavor.frame.size.height/2)-15, 30, 30)];
            
            if([[opcionesSeleccionadas objectForKey:@"estaLleno"] isEqual:[NSNumber numberWithInt:1]]){
                
                [[nota layer] setBorderWidth:1];
                [botonAFavor setTitleColor:[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] forState:UIControlStateNormal];
                [[nota layer] setBorderColor:[[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] CGColor]];
                
                [nota setAnimationProgress:1];
                
            }else{
                
                [nota setProgressWithFrame:[NSNumber numberWithInt:1]];
                [[nota layer] setBorderWidth:0];
                [botonAFavor setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            [nota setTag:3];
            
            [nota setBackgroundColor:[UIColor whiteColor]];
            
            [nota layer].borderWidth = 0;
            [[nota layer] setCornerRadius:15];
            
            [celda addSubview:nota];
            
            [celda sendSubviewToBack:nota];
        }
        
        if(![celda viewWithTag:4]){
            
            LOTAnimationView *notaNegativa = [LOTAnimationView animationNamed:@"esta_lleno"];
            
            [notaNegativa setFrame:CGRectMake(botonEnContra.frame.origin.x+botonEnContra.frame.size.width-50, botonEnContra.frame.origin.y+(botonEnContra.frame.size.height/2)-15, 30, 30)];
            
            
            if([[opcionesSeleccionadas objectForKey:@"estaLleno"] isEqual:[NSNumber numberWithInt:0]]){
                
                [notaNegativa setAnimationProgress:1];
                
                [[notaNegativa layer] setBorderWidth:1];
                [botonEnContra setTitleColor:[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] forState:UIControlStateNormal];
                [[notaNegativa layer] setBorderColor:[[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] CGColor]];
            }else{
                
                [notaNegativa setProgressWithFrame:[NSNumber numberWithInt:1]];
                
                [[notaNegativa layer] setBorderWidth:0];
                [botonEnContra setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            [notaNegativa setTag:4];
            
            [notaNegativa setBackgroundColor:[UIColor whiteColor]];
            
            [notaNegativa layer].borderWidth = 0;
            [[notaNegativa layer] setCornerRadius:15];
            
            [celda addSubview:notaNegativa];
            
            [celda sendSubviewToBack:notaNegativa];
        }
        
    }else if(indexPath.row == 2){
        
        if([celda viewWithTag:3]){
            
            [[celda viewWithTag:3] removeFromSuperview];
        }
        
        if([celda viewWithTag:4]){
            
            [[celda viewWithTag:4] removeFromSuperview];
        }
        
        [botonAFavor setTitle: NSLocalizedString(@"Me gusta la música", nil) forState:UIControlStateNormal];
        
        [botonEnContra setTitle:NSLocalizedString(@"No me gusta la música", nil) forState:UIControlStateNormal];
        
        
        if(![celda viewWithTag:3]){
            LOTAnimationView *nota = [LOTAnimationView animationNamed:@"check_nota"];
            
            [nota setFrame:CGRectMake(botonAFavor.frame.origin.x+botonAFavor.frame.size.width-50, botonAFavor.frame.origin.y+(botonAFavor.frame.size.height/2)-15, 30, 30)];
            
            if([[opcionesSeleccionadas objectForKey:@"meGustaMusica"] isEqual:[NSNumber numberWithInt:1]]){
                
                [nota setAnimationProgress:1];
                
                [[nota layer] setBorderWidth:1];
                [botonAFavor setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
                [[nota layer] setBorderColor:[[UIColor purpleColor] CGColor]];
                
            }else{
                
                [nota setProgressWithFrame:[NSNumber numberWithInt:1]];
                [[nota layer] setBorderWidth:0];
                [botonAFavor setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            [nota setTag:3];
            
            [nota setBackgroundColor:[UIColor whiteColor]];
            
            [nota layer].borderWidth = 0;
            [[nota layer] setCornerRadius:15];
            
            [celda addSubview:nota];
            
            [celda sendSubviewToBack:nota];
            
        }
        
        
        if(![celda viewWithTag:4]){
            
            LOTAnimationView *notaNegativa = [LOTAnimationView animationNamed:@"check_nota_nogusta"];
            
            [notaNegativa setFrame:CGRectMake(botonEnContra.frame.origin.x+botonEnContra.frame.size.width-50, botonEnContra.frame.origin.y+(botonEnContra.frame.size.height/2)-15, 30, 30)];
            
            if([[opcionesSeleccionadas objectForKey:@"meGustaMusica"] isEqual:[NSNumber numberWithInt:0]]){
                
                [notaNegativa setAnimationProgress:1];
                
                [[notaNegativa layer] setBorderWidth:1];
                [botonEnContra setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                [[notaNegativa layer] setBorderColor:[[UIColor grayColor] CGColor]];
            }else{
                
                [notaNegativa setProgressWithFrame:[NSNumber numberWithInt:1]];
                
                [[notaNegativa layer] setBorderWidth:0];
                [botonEnContra setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            [notaNegativa setTag:4];
            
            [notaNegativa setBackgroundColor:[UIColor whiteColor]];
            
            [notaNegativa layer].borderWidth = 0;
            [[notaNegativa layer] setCornerRadius:15];
            
            [celda addSubview:notaNegativa];
            
            [celda sendSubviewToBack:notaNegativa];
        }
    }else if(indexPath.row == 3){
        
        if([celda viewWithTag:3]){
            
            [[celda viewWithTag:3] removeFromSuperview];
        }
        
        if([celda viewWithTag:4]){
            
            [[celda viewWithTag:4] removeFromSuperview];
        }
        
        [botonAFavor setTitle:NSLocalizedString(@"Servicio rápido", nil) forState:UIControlStateNormal];
        
        [botonEnContra setTitle:NSLocalizedString(@"Servicio lento", nil) forState:UIControlStateNormal];
        
        
        if(![celda viewWithTag:3]){
            LOTAnimationView *nota = [LOTAnimationView animationNamed:@"servicio_rapido"];
            
            [nota setFrame:CGRectMake(botonAFavor.frame.origin.x+botonAFavor.frame.size.width-50, botonAFavor.frame.origin.y+(botonAFavor.frame.size.height/2)-15, 30, 30)];
            
            [nota setBackgroundColor:[UIColor greenColor]];
            
            if([[opcionesSeleccionadas objectForKey:@"servicioRapido"] isEqual:[NSNumber numberWithInt:1]]){
                
                [nota setAnimationProgress:1];
                
                [[nota layer] setBorderWidth:1];
                [botonAFavor setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                [[nota layer] setBorderColor:[[UIColor blueColor] CGColor]];
            }else{
                
                [nota setProgressWithFrame:[NSNumber numberWithInt:1]];
                [[nota layer] setBorderWidth:0];
                [botonAFavor setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            [nota setTag:3];
            
            [nota setBackgroundColor:[UIColor whiteColor]];
            
            [nota layer].borderWidth = 0;
            [[nota layer] setCornerRadius:15];
            
            [celda addSubview:nota];
            
            [celda sendSubviewToBack:nota];
            
        }
        
        
        if(![celda viewWithTag:4]){
            
            LOTAnimationView *notaNegativa = [LOTAnimationView animationNamed:@"servicio_lento"];
            
            [notaNegativa setFrame:CGRectMake(170+botonEnContra.frame.size.width-50, botonEnContra.frame.origin.y+(botonEnContra.frame.size.height/2)-15, 30, 30)];
            
            if([[opcionesSeleccionadas objectForKey:@"servicioRapido"] isEqual:[NSNumber numberWithInt:0]]){
                
                [notaNegativa setAnimationProgress:1];
                
                [[notaNegativa layer] setBorderWidth:1];
                [botonEnContra setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                [[notaNegativa layer] setBorderColor:[[UIColor grayColor] CGColor]];
            }else{
                
                [notaNegativa setProgressWithFrame:[NSNumber numberWithInt:1]];
                
                [[notaNegativa layer] setBorderWidth:0];
                [botonEnContra setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            [notaNegativa setTag:4];
            
            [notaNegativa setBackgroundColor:[UIColor whiteColor]];
            
            [notaNegativa layer].borderWidth = 0;
            [[notaNegativa layer] setCornerRadius:15];
            
            [celda addSubview:notaNegativa];
            
            [celda sendSubviewToBack:notaNegativa];
        }
    }else if(indexPath.row == 4){
        
        if([celda viewWithTag:3]){
            
            [[celda viewWithTag:3] removeFromSuperview];
        }
        
        if([celda viewWithTag:4]){
            
            [[celda viewWithTag:4] removeFromSuperview];
        }
        
        [botonAFavor setTitle:NSLocalizedString(@"Buen servicio", nil) forState:UIControlStateNormal];
        
        [botonEnContra setTitle:NSLocalizedString(@"Mal servicio", nil) forState:UIControlStateNormal];
        
        
        if(![celda viewWithTag:3]){
            LOTAnimationView *nota = [LOTAnimationView animationNamed:@"buen_servicio"];
            
            [nota setFrame:CGRectMake(botonAFavor.frame.origin.x+botonAFavor.frame.size.width-50, botonAFavor.frame.origin.y+(botonAFavor.frame.size.height/2)-15, 30, 30)];
            
            if([[opcionesSeleccionadas objectForKey:@"servicioBueno"] isEqual:[NSNumber numberWithInt:1]]){
                
                [nota setAnimationProgress:1];
                
                [[nota layer] setBorderWidth:1];
                [botonAFavor setTitleColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:0 alpha:1] forState:UIControlStateNormal];
                [[nota layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:0 alpha:1] CGColor]];
                
            }else{
                
                [nota setProgressWithFrame:[NSNumber numberWithInt:1]];
                [[nota layer] setBorderWidth:0];
                [botonAFavor setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            [nota setTag:3];
            
            [nota setBackgroundColor:[UIColor whiteColor]];
            
            [nota layer].borderWidth = 0;
            [[nota layer] setCornerRadius:15];
            
            [celda addSubview:nota];
            
            [celda sendSubviewToBack:nota];
            
        }
        
        
        if(![celda viewWithTag:4]){
            
            LOTAnimationView *notaNegativa = [LOTAnimationView animationNamed:@"mal_servicio"];
            
            [notaNegativa setFrame:CGRectMake(botonEnContra.frame.origin.x+botonEnContra.frame.size.width-50, botonEnContra.frame.origin.y+(botonEnContra.frame.size.height/2)-15, 30, 30)];
            
            if([[opcionesSeleccionadas objectForKey:@"servicioBueno"] isEqual:[NSNumber numberWithInt:0]]){
                
                [notaNegativa setAnimationProgress:1];
                
                [[notaNegativa layer] setBorderWidth:1];
                [botonEnContra setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                [[notaNegativa layer] setBorderColor:[[UIColor redColor] CGColor]];
            }else{
                [notaNegativa setProgressWithFrame:[NSNumber numberWithInt:1]];
                
                [[notaNegativa layer] setBorderWidth:0];
                [botonEnContra setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            [notaNegativa setTag:4];
            
            [notaNegativa setBackgroundColor:[UIColor whiteColor]];
            
            [notaNegativa layer].borderWidth = 0;
            [[notaNegativa layer] setCornerRadius:15];
            
            [celda addSubview:notaNegativa];
            
            [celda sendSubviewToBack:notaNegativa];
        }
    }
    
    [botonAFavor addTarget:self action:@selector(botonAFavor:) forControlEvents:UIControlEventTouchUpInside];
    
    [botonEnContra addTarget:self action:@selector(botonEnContra:) forControlEvents:UIControlEventTouchUpInside];
    
    return celda;
}

#pragma mark - text view

-(void)textViewDidEndEditing:(UITextView *)textView{

    if([[textView text] isEqualToString:@""]){
        
        [textView setText:@"Deja tu voto y comentario sobre el lugar..."];
        [textView setTextColor:[UIColor colorWithRed:153.0/255 green:153.0/255 blue:153.0/255 alpha:1.0]];
        
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView{

    /*[UIView animateWithDuration:0.4 animations:^{
    
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-200, self.view.frame.size.width, self.view.frame.size.height)];
    }];*/
    
    if([[textView text] isEqualToString:@"Deja tu voto y comentario sobre el lugar..."]){

        [textView setText:@""];
        [textView setTextColor:[UIColor colorWithWhite:0.0 alpha:1.0]];
    }
}

-(void)textViewDidChange:(UITextView *)textView{

    if([[textView text] length] > 0){
        
        NSString *ultimoCaracter = [[textView text] substringFromIndex:[[textView text] length]-1];
        
        if([ultimoCaracter isEqualToString:@"\n"]){
            
            [textView setText:[[textView text] substringToIndex:[[textView text] length]-1]];
            
            [textView resignFirstResponder];
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)votarPositivo:(id)sender {
    
    if(![votoPositivo isSelected]){

        [votoPositivo setSelected:YES];
        [votoNegativo setSelected:NO];
    }else{

        [votoPositivo setSelected:NO];
    }
    
}

- (IBAction)votarNegativo:(id)sender {
    
    if(![votoNegativo isSelected]){
        
        [votoNegativo setSelected:YES];
        [votoPositivo setSelected:NO];
    }else{
        
        [votoNegativo setSelected:NO];
    }
}

- (IBAction)enviarVoto:(id)sender {
    
    if([opcionesSeleccionadas count] > 0){
        
        
        LOTAnimationView *uploader = [LOTAnimationView animationNamed:@"uploader"];
        
        [uploader setFrame:CGRectMake((self.view.frame.size.width/2)-50, self.view.frame.size.height-75, 100, 75)];
        
        [self.view addSubview:uploader];
        
        [botonVotar setAlpha:0];
        
        [uploader playToProgress:0.09 withCompletion:^(BOOL animatinFinished){
        }];
        
        [uploader playWithCompletion:^(BOOL finished){
            
            [uploader playWithCompletion:^(BOOL animationFinished){
                
                if(animationFinished){
                    
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
            
        }];
        
        [uploader setAnimationProgress:0.789];
        
        TabBarPrincipalViewController *tabBar = (TabBarPrincipalViewController*) [self tabBarController];
        
        CheckinViewController *checkinViewController;
        
        for(id viewController in [self.navigationController viewControllers]){
            
            if([viewController isKindOfClass:[CheckinViewController class]]){
                
                checkinViewController = (CheckinViewController*) viewController;
            }
        }
        
        NSMutableDictionary *datosCheckinEnLugar = [tabBar datosCheckinEnLugar];
        NSMutableDictionary *datosCheckinEnLugarViewController = [checkinViewController datosEnLugar];
        
        [[datosCheckinEnLugarViewController objectForKey:@"voto"] setObject:[NSNumber numberWithInt:1] forKey:@"meGusta"];
        
        [botonVotar setEnabled:NO];
        
        NSNumber *meGusta, *estaLleno, *meGustaMusica, *servicioRapido, *servicioBueno;
        
        if([opcionesSeleccionadas objectForKey:@"meGusta"]){
            meGusta = [opcionesSeleccionadas objectForKey:@"meGusta"];
            
            [[datosCheckinEnLugar objectForKey:@"voto"] setObject:[opcionesSeleccionadas objectForKey:@"meGusta"] forKey:@"meGusta"];
            
            [[datosCheckinEnLugarViewController objectForKey:@"voto"] setObject:[opcionesSeleccionadas objectForKey:@"meGusta"] forKey:@"meGusta"];
            
        }else{
            
            meGusta = nil;
            
            [[datosCheckinEnLugar objectForKey:@"voto"] setObject:[NSNull null] forKey:@"meGusta"];
            
            [[datosCheckinEnLugarViewController objectForKey:@"voto"] setObject:[NSNull null] forKey:@"meGusta"];
        }
        
        if([opcionesSeleccionadas objectForKey:@"estaLleno"]){
            
            estaLleno = [opcionesSeleccionadas objectForKey:@"estaLleno"];
            
            [[datosCheckinEnLugar objectForKey:@"voto"] setObject:[opcionesSeleccionadas objectForKey:@"estaLleno"] forKey:@"estaLleno"];
            
            [[datosCheckinEnLugarViewController objectForKey:@"voto"] setObject:[opcionesSeleccionadas objectForKey:@"estaLleno"] forKey:@"estaLleno"];
        }else{
            
            estaLleno = nil;
            [[datosCheckinEnLugar objectForKey:@"voto"] setObject:[NSNull null] forKey:@"estaLleno"];
            [[datosCheckinEnLugarViewController objectForKey:@"voto"] setObject:[NSNull null] forKey:@"estaLleno"];
        }
        
        if([opcionesSeleccionadas objectForKey:@"meGustaMusica"]){
            
            meGustaMusica = [opcionesSeleccionadas objectForKey:@"meGustaMusica"];
            
            [[datosCheckinEnLugar objectForKey:@"voto"] setObject:[opcionesSeleccionadas objectForKey:@"meGustaMusica"] forKey:@"meGustaMusica"];
            
            [[datosCheckinEnLugarViewController objectForKey:@"voto"] setObject:[opcionesSeleccionadas objectForKey:@"meGustaMusica"] forKey:@"meGustaMusica"];
        }else{
            
            meGustaMusica = nil;
            [[datosCheckinEnLugar objectForKey:@"voto"] setObject:[NSNull null] forKey:@"meGustaMusica"];
            [[datosCheckinEnLugarViewController objectForKey:@"voto"] setObject:[NSNull null] forKey:@"meGustaMusica"];
        }
        
        if([opcionesSeleccionadas objectForKey:@"servicioRapido"]){
            
            servicioRapido = [opcionesSeleccionadas objectForKey:@"servicioRapido"];
            
            [[datosCheckinEnLugar objectForKey:@"voto"] setObject:[opcionesSeleccionadas objectForKey:@"servicioRapido"] forKey:@"servicioRapido"];
            
            [[datosCheckinEnLugarViewController objectForKey:@"voto"] setObject:[opcionesSeleccionadas objectForKey:@"servicioRapido"] forKey:@"servicioRapido"];
        }else{
            
            servicioRapido = nil;
            [[datosCheckinEnLugar objectForKey:@"voto"] setObject:[NSNull null] forKey:@"servicioRapido"];
            [[datosCheckinEnLugarViewController objectForKey:@"voto"] setObject:[NSNull null] forKey:@"servicioRapido"];
        }
        
        if([opcionesSeleccionadas objectForKey:@"servicioBueno"]){
            
            servicioBueno = [opcionesSeleccionadas objectForKey:@"servicioBueno"];
            
            [[datosCheckinEnLugar objectForKey:@"voto"] setObject:[opcionesSeleccionadas objectForKey:@"servicioBueno"] forKey:@"servicioBueno"];
            
            [[datosCheckinEnLugarViewController objectForKey:@"voto"] setObject:[opcionesSeleccionadas objectForKey:@"servicioBueno"] forKey:@"servicioBueno"];
        }else{
            
            servicioBueno = nil;
            
            [[datosCheckinEnLugar objectForKey:@"voto"] setObject:[NSNull null] forKey:@"servicioBueno"];
            
            [[datosCheckinEnLugarViewController objectForKey:@"voto"] setObject:[NSNull null] forKey:@"servicioBueno"];
        }
        
        if([opcionesSeleccionadas objectForKey:@"meGusta"]){
            [votos setObject:[opcionesSeleccionadas objectForKey:@"meGusta"] forKey:@"meGusta"];
        }
        if([opcionesSeleccionadas objectForKey:@"estaLleno"]){
            [votos setObject:[opcionesSeleccionadas objectForKey:@"estaLleno"] forKey:@"estaLleno"];
        }
        if([opcionesSeleccionadas objectForKey:@"meGustaMusica"]){
            [votos setObject:[opcionesSeleccionadas objectForKey:@"meGustaMusica"] forKey:@"meGustaMusica"];
        }
        if([opcionesSeleccionadas objectForKey:@"servicioRapido"]){
            [votos setObject:[opcionesSeleccionadas objectForKey:@"servicioRapido"] forKey:@"servicioRapido"];
        }
        if([opcionesSeleccionadas objectForKey:@"servicioBueno"]){
            [votos setObject:[opcionesSeleccionadas objectForKey:@"servicioBueno"] forKey:@"servicioBueno"];
        }
        
        [tabBar setDatosCheckinEnLugar:datosCheckinEnLugar];
        
        [checkinViewController setDatosEnLugar:datosCheckinEnLugarViewController];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/addvoto.php"]];
        
        [request setHTTPMethod:@"POST"];
        
        NSData *cuerpo = [[NSString stringWithFormat:@"meGusta=%@&estaLleno=%@&meGustaMusica=%@&servicioRapido=%@&servicioBueno=%@&lugar=%d&usuario=%d",meGusta, estaLleno, meGustaMusica, servicioRapido, servicioBueno,idLugar,[[defaults objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
        
        NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
            
            [botonVotar setEnabled:YES];
            
            UIView *vistaListo = [[UIView alloc] initWithFrame:CGRectMake(20, 100, self.view.frame.size.width-50, self.view.frame.size.height-200)];
            
            [vistaListo setBackgroundColor:[UIColor colorWithRed:0.00 green:0.67 blue:0.67 alpha:0.9]];
            
            /*LOTAnimationView *animacionListo = [LOTAnimationView animationNamed:@"envio_terminado"];
             
             [animacionListo setFrame:CGRectMake((vistaListo.frame.size.width/2)-(animacionListo.frame.size.width/2), (vistaListo.frame.size.height/2)-(animacionListo.frame.size.height/2), animacionListo.frame.size.width, animacionListo.frame.size.height)];
             
             UILabel *textoListo = [[UILabel alloc] initWithFrame:CGRectMake(animacionListo.frame.origin.x+22.5, animacionListo.frame.origin.y+animacionListo.frame.size.height, 45, 15)];
             
             [textoListo setFont:[UIFont systemFontOfSize:13]];
             
             [textoListo setTextColor:[UIColor whiteColor]];
             [textoListo setText:@"¡Listo!"];
             
             [vistaListo addSubview:animacionListo];
             [vistaListo addSubview:textoListo];
             [self.view addSubview:vistaListo];
             
             [animacionListo play];*/
            
            [uploader playWithCompletion:^(BOOL completition){
                
                if(completition){
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [uploader setAlpha:0];
                        [botonVotar setAlpha:1];
                        
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                }
            }];
            
        }];
        
        [uploadTask resume];
    }
    
    if(![[campoComentario text] isEqualToString:@"Deja tu voto y comentario sobre el lugar..."] && fotoVideo == 0){
        
        NSString *comentario = [[campoComentario text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if([comentario length] > 0){
            
            [botonVotar setEnabled:NO];
            
            NSData *dataComentarioLugar =[comentario dataUsingEncoding:NSNonLossyASCIIStringEncoding];
            
            NSString *palabra = [[NSString alloc] initWithData:dataComentarioLugar encoding:NSUTF8StringEncoding];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/nuevomensajelugar.php"]];
            
            [request setHTTPMethod:@"POST"];
            
            NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%d&msj=%@&idLugar=%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue], palabra,idLugar] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
            
            NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
            
            NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                
                if(error == nil){
                    
                    NSString *resputesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    if([resputesta isEqualToString:@"ok"]){
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [campoComentario setText:@"Deja tu voto y comentario sobre el lugar..."];
                            [campoComentario setTextColor:[UIColor colorWithRed:153.0/255 green:153.0/255 blue:153.0/255 alpha:1.0]];
                            
                            [botonVotar setEnabled:YES];
                            
                        });
                    }
                }
                
            }];
            
            [uploadTask resume];
        }
    }
    
    if(fotoVideo > 0){
        
        [botonVotar setEnabled:NO];
        
        NSString *textoComentario;
        
        if([[campoComentario text] isEqualToString:@"Deja tu voto y comentario sobre el lugar..."]){
            
            textoComentario = @"";
        }else{
            
            textoComentario = [campoComentario text];
        }
        
        NSData *data =[textoComentario dataUsingEncoding:NSNonLossyASCIIStringEncoding];
        NSString *textoFoto = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        
        UIImageView *foto = (UIImageView *)[fondoFotoTomada viewWithTag:1];
        
        LOTAnimationView *uploader = [LOTAnimationView animationNamed:@"uploader"];
        
        [uploader setFrame:CGRectMake((self.view.frame.size.width/2)-50, self.view.frame.size.height-75, 100, 75)];
        
        [uploader setTag:104];
        
        [self.view addSubview:uploader];
        
        [botonVotar setAlpha:0];
        
        [uploader playToProgress:0.09 withCompletion:^(BOOL animatinFinished){
        }];
        
        UIApplication *app = [UIApplication sharedApplication];
        app.networkActivityIndicatorVisible = YES;
        
        
        totalBytesArchivoAsubir = [dataFotoVideo length];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/subirfotoenlugar.php"]];
            
            [request setHTTPMethod:@"POST"];
            
            NSMutableData *cuerpo = [NSMutableData data];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            
            NSString *contenido = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            
            NSString *linea;
            
            [request addValue:contenido forHTTPHeaderField:@"Content-Type"];
            
            [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            if(fotoVideo == 1){
                
                linea = @"foto";
                [cuerpo appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"foto\"; filename=\"%@.jpg\"\r\n",linea] dataUsingEncoding:NSUTF8StringEncoding]];
            }else if(fotoVideo == 2){
                
                linea = @"video";
                [cuerpo appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"foto\"; filename=\"%@.mp4\"\r\n",linea] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            linea = @"Content-Type: application/octet-stream\r\n\r\n";
            [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            [cuerpo appendData:dataFotoVideo];
            linea = @"\r\n";
            [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            linea = @"Content-Disposition: form-data; name=\"id\"\r\n\r\n";
            [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            [cuerpo appendData:[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding]];
            linea = @"\r\n";
            [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            
            [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            
            [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            linea = @"Content-Disposition: form-data; name=\"lugar\"\r\n\r\n";
            [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            [cuerpo appendData:[[NSString stringWithFormat:@"%d",idLugar] dataUsingEncoding:NSUTF8StringEncoding]];
            linea = @"\r\n";
            [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            
            [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            
            [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            linea = @"Content-Disposition: form-data; name=\"mensaje\"\r\n\r\n";
            [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            [cuerpo appendData:[[NSString stringWithFormat:@"%@",textoFoto] dataUsingEncoding:NSUTF8StringEncoding]];
            linea = @"\r\n";
            [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
            
            [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            [request setHTTPBody:cuerpo];
            
            NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
            
            NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
            
            NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                        
                        NSLog(@"error al subir video/foto");
                    }else{
                        
                        NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        
                        if(![respuesta isEqualToString:@"error"]){
                            
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                app.networkActivityIndicatorVisible = NO;
                                [foto removeFromSuperview];
                                
                                
                                LOTAnimationView *uploader = (LOTAnimationView*)[self.view viewWithTag:104];
                                
                                
                                [uploader playWithCompletion:^(BOOL finished){
                                    
                                    [uploader playWithCompletion:^(BOOL animationFinished){
                                        
                                        if(animationFinished){
                                            
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }
                                    }];
                                    
                                }];
                                
                                [uploader setAnimationProgress:0.789];
                                
                                
                                [fondoFotoTomada setAlpha:0];
                                
                                [botonVotar setEnabled:YES];
                                
                                UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:dataFotoVideo], nil, nil, nil);
                            });
                        }else{
                            
                            [[[UIAlertView alloc] initWithTitle:@"Hubo un error" message:@"La foto no pudo subirse al servidor, por favor intentalo nuevamente" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                            
                            app.networkActivityIndicatorVisible = NO;
                            
                            [fondoFotoTomada setAlpha:0];
                            
                            [botonVotar setEnabled:YES];
                        }
                    }
                });
            }];
            
            [uploadTask resume];
            
        });
    }
    
}

-(void)mostrarContadorVotos:(float)promVotos{

    NSTimeInterval duracion = 0.05;
    dispatch_time_t tiempo = DISPATCH_TIME_NOW;
    
    [votoPositivo setHidden:YES];
    [votoNegativo setHidden:YES];
    [divisionVotos setHidden:YES];
    
    for(int i = 0; i< (int)promVotos; i++){
        
        dispatch_after(tiempo, dispatch_get_main_queue(), ^{
            
            [UIView animateWithDuration:1 animations:^{
                
                [numeroDeVotos setText:[NSString stringWithFormat:@"%d votos positivos",i+1]];
            }];
        });
        
        tiempo = dispatch_time(tiempo, duracion*NSEC_PER_SEC);
    }
    
}

- (IBAction)tomarFotoVideo:(id)sender {
    
    
    if([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied){
        
        UIAlertController *noPermisoCamara = [UIAlertController alertControllerWithTitle:nil message:@"Crowte no tiene permiso para acceder a tu cámara." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){}];
        
        UIAlertAction *cambiar = [UIAlertAction actionWithTitle:@"Ir a la configuración" style:UIAlertActionStyleDefault handler:^(UIAlertAction *accion){
            
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        
        [noPermisoCamara addAction:cambiar];
        [noPermisoCamara addAction:cancel];
        
        [self presentViewController:noPermisoCamara animated:YES completion:nil];
    }else if([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined){
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted){
            
            if(granted){
                
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted){
                    
                    
                    if(granted){
                        
                        CamaraOverlayViewController *cm = [[CamaraOverlayViewController alloc] init];
                        
                        if([tutoTomarFotoVideo isEqualToString:@"1"]){
                        
                            [cm setTutoTomarFotoVideo:NO];
                        }else if([tutoTomarFotoVideo isEqualToString:@"0"]){
                        
                            [cm setTutoTomarFotoVideo:YES];
                        }
                        
                        [cm setIdLugar:idLugar];
                        
                        [cm setDelegate:self];
                        
                        [self presentViewController:cm animated:YES completion:nil];
                    }
                }];
            }
        }];
    }else{
        
        CamaraOverlayViewController *cm = [[CamaraOverlayViewController alloc] init];
        
        if([tutoTomarFotoVideo isEqualToString:@"1"]){
            
            [cm setTutoTomarFotoVideo:NO];
        }else if([tutoTomarFotoVideo isEqualToString:@"0"]){
            
            [cm setTutoTomarFotoVideo:YES];
        }
        
        [cm setIdLugar:idLugar];
        
        [cm setDelegate:self];
        
        [self presentViewController:cm animated:YES completion:nil];
    }
    
    
}


-(void)getImageVideoTomado:(NSData *)data videoImagen:(int)videoimagen comentario:(NSString *)comentario compartirFacebook:(BOOL)compartirFacebook{
    
    [self setFotoVideo:videoimagen];
    [self setDataFotoVideo:data];
    
    [fondoFotoTomada setAlpha:1];
    
    if(videoimagen == 1){

        UIImageView *imagen;
        
        if(![fondoFotoTomada viewWithTag:1]){
        
            imagen = [[UIImageView alloc] initWithFrame:CGRectMake((fondoFotoTomada.frame.size.width/2)-((fondoFotoTomada.frame.size.height-10)/2), 5, fondoFotoTomada.frame.size.height-10, fondoFotoTomada.frame.size.height-10)];
        
            [imagen setTag:1];
        }else{
        
            imagen = (UIImageView*)[fondoFotoTomada viewWithTag:1];
        }
        
        [imagen setImage:[[UIImage alloc] initWithData:data]];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mostrarCerrarFotoVideoTomado:)];
        
        [imagen setUserInteractionEnabled:YES];
        [imagen addGestureRecognizer:tapGesture];
        
        [fondoFotoTomada addSubview:imagen];
    }else{
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"thefile.mp4"];
        
        NSURL *urlvd = [NSURL URLWithString:filePath];
        
        [[NSFileManager defaultManager] createFileAtPath:[urlvd path] contents:data attributes:nil];
        
        NSURL *videoUrl = [NSURL fileURLWithPath:filePath];
        
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoUrl options:nil];
        
        AVAssetImageGenerator *imgGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        
        imgGenerator.appliesPreferredTrackTransform = YES;
        
        NSError *error = NULL;
        
        CMTime time = CMTimeMake(1, 60);
        CGImageRef imgRef = [imgGenerator copyCGImageAtTime:time actualTime:NULL error:&error];
        
        
        
        UIImageView *imagen;
        
        if(![fondoFotoTomada viewWithTag:1]){
            
            imagen = [[UIImageView alloc] initWithFrame:CGRectMake((fondoFotoTomada.frame.size.width/2)-((fondoFotoTomada.frame.size.height-10)/2), 5, fondoFotoTomada.frame.size.height-10, fondoFotoTomada.frame.size.height-10)];
            
            [imagen setTag:1];
        }else{
            
            imagen = (UIImageView*)[fondoFotoTomada viewWithTag:1];
        }
        
        UIImage *imgCG = [[UIImage alloc] initWithCGImage:imgRef];
        
        [imagen setImage:imgCG];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mostrarCerrarFotoVideoTomado:)];
        
        [imagen setUserInteractionEnabled:YES];
        [imagen addGestureRecognizer:tapGesture];
        
        [fondoFotoTomada addSubview:imagen];
        
    }
}

-(void)mostrarCerrarFotoVideoTomado:(id)sender{

    UIImageView *fotoTomada = (UIImageView*)[fondoFotoTomada viewWithTag:1];
    
    UIView *fondoCerrar = [[UIView alloc] initWithFrame:fotoTomada.frame];
    
    [fondoCerrar setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.8]];
    
    [fondoCerrar setTag:2];
    
    [fondoCerrar setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapCerrarCerrarImagen = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cerrarFondoCerrarFotoVideoTomado:)];
    
    [fondoCerrar addGestureRecognizer:tapCerrarCerrarImagen];
    
    UIButton *cerrarFoto = [[UIButton alloc] initWithFrame:CGRectMake((fondoCerrar.frame.size.width/2)-((fondoCerrar.frame.size.height/3)/2), (fondoCerrar.frame.size.height/2)-((fondoCerrar.frame.size.height/3)/2), fondoCerrar.frame.size.width/3, fondoCerrar.frame.size.height/3)];
    
    [cerrarFoto setTitle:@"X" forState:UIControlStateNormal];
    
    [cerrarFoto.titleLabel setFont:[UIFont fontWithName:@"Hind-Bold" size:13]];
    
    [cerrarFoto.layer setBorderWidth:1];
    [cerrarFoto.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [cerrarFoto.layer setCornerRadius:cerrarFoto.frame.size.width/2];
    
    [cerrarFoto addTarget:self action:@selector(cerrarFotoVideoTomado:) forControlEvents:UIControlEventTouchUpInside];
    
    [fondoCerrar addSubview:cerrarFoto];
    [fondoFotoTomada addSubview:fondoCerrar];
}

-(void)cerrarFondoCerrarFotoVideoTomado:(id)sender{

    [[fondoFotoTomada viewWithTag:2] removeFromSuperview];
}

-(void)cerrarFotoVideoTomado:(id)sender{

    [[fondoFotoTomada viewWithTag:1] removeFromSuperview];
    [[fondoFotoTomada viewWithTag:2] removeFromSuperview];
    [fondoFotoTomada setAlpha:0];
    fotoVideo = 0;
    dataFotoVideo = nil;
}

#pragma mark - url session

-(void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
    
    
    NSLog(@"1 se envió data %ld esperados %ld de %ld",(long)bytesWritten, (long)totalBytesWritten, (long)totalBytesExpectedToWrite);
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes{}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    
}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        LOTAnimationView *uploader = (LOTAnimationView*)[self.view viewWithTag:104];
        
        
       // [uploader playToProgress:((totalBytesSent*100)/totalBytesArchivoAsubir)*0.00627 withCompletion:^(BOOL animationFinished){}];
        
    });
}

-(void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session{
    
}

-(void)dibujarProgresoCircular:(UIImageView*)imagenView progreso:(float)progreso{
    
    [fondoFotoTomada addSubview:imagenView];
    
    UIImageView *imagen = [fondoFotoTomada viewWithTag:1];
    
    UIGraphicsBeginImageContextWithOptions(imagenView.frame.size, NO, 0);
    
    UIBezierPath *bazierPath = [UIBezierPath bezierPath];
    
    [bazierPath moveToPoint:CGPointMake(imagen.frame.size.height/2, imagen.frame.size.width/2)];
    [bazierPath addArcWithCenter:CGPointMake(imagen.frame.size.height/2, imagen.frame.size.width/2) radius:imagen.frame.size.width/2 startAngle:(0*M_PI)/2 endAngle:((progreso*0.04)*M_PI)/2  clockwise:YES];
    [bazierPath closePath];
    
    [bazierPath setLineWidth:2];
    [[UIColor colorWithWhite:1 alpha:0.7] setStroke];
    [bazierPath stroke];
    [[UIColor colorWithWhite:1 alpha:0.7] setFill];
    [bazierPath fill];
    
    [imagenView setImage:UIGraphicsGetImageFromCurrentImageContext()];
    
    UIGraphicsEndImageContext();
    
    
    imagenView.transform = CGAffineTransformMakeRotation(4.71239);
    
}

#pragma mark - metodos opciones ambiente del lugar

- (IBAction)botonAFavor:(id)sender {
    
    UITableViewCell *celda = (UITableViewCell*)[[sender superview] superview];
    
    NSIndexPath *indexPath = [tablaOpcionesMood indexPathForCell:celda];
    
    UIButton *boton = (UIButton*)sender;
    
    LOTAnimationView *animacionNegativa = (LOTAnimationView*)[celda viewWithTag:4];
    
    [animacionNegativa setProgressWithFrame:[NSNumber numberWithInt:1]];
    
    [[animacionNegativa layer] setBorderWidth:0];
    
    UIButton *botonNegativo = (UIButton*)[celda viewWithTag:2];
    
    [botonNegativo setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    LOTAnimationView *animacionPostiva = (LOTAnimationView*)[celda viewWithTag:3];
    
    if(indexPath.row == 0 && [[opcionesSeleccionadas objectForKey:@"meGusta"] isEqual:[NSNumber numberWithInt:1]]){
        
        [opcionesSeleccionadas setObject:[NSNull null] forKey:@"meGusta"];
        
        [boton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [animacionPostiva setProgressWithFrame:[NSNumber numberWithInt:1]];
        
        [[animacionPostiva layer] setBorderWidth:0];
        
    }else if(indexPath.row == 1 && [[opcionesSeleccionadas objectForKey:@"estaLleno"] isEqual:[NSNumber numberWithInt:1]]){
        
        [opcionesSeleccionadas setObject:[NSNull null] forKey:@"estaLleno"];
        
        [boton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [animacionPostiva setProgressWithFrame:[NSNumber numberWithInt:1]];
        
        [[animacionPostiva layer] setBorderWidth:0];
        
    }else if(indexPath.row == 2 && [[opcionesSeleccionadas objectForKey:@"meGustaMusica"] isEqual:[NSNumber numberWithInt:1]]){
        
        [opcionesSeleccionadas setObject:[NSNull null] forKey:@"meGustaMusica"];
        
        [boton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [animacionPostiva setProgressWithFrame:[NSNumber numberWithInt:1]];
        
        [[animacionPostiva layer] setBorderWidth:0];
        
    }else if(indexPath.row == 3 && [[opcionesSeleccionadas objectForKey:@"servicioRapido"] isEqual:[NSNumber numberWithInt:1]]){
        
        [opcionesSeleccionadas setObject:[NSNull null] forKey:@"servicioRapido"];
        
        [boton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [animacionPostiva setProgressWithFrame:[NSNumber numberWithInt:1]];
        
        [[animacionPostiva layer] setBorderWidth:0];
        
    }else if(indexPath.row == 4 && [[opcionesSeleccionadas objectForKey:@"servicioBueno"] isEqual:[NSNumber numberWithInt:1]]){
        
        [opcionesSeleccionadas setObject:[NSNull null] forKey:@"servicioBueno"];
        
        [boton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [animacionPostiva setProgressWithFrame:[NSNumber numberWithInt:1]];
        
        [[animacionPostiva layer] setBorderWidth:0];
        
    }else{
        
        [animacionPostiva playToProgress:0.15 withCompletion: ^(BOOL animationFinished){
            
            if(animationFinished){
                
                if(indexPath.row == 0){
                    
                    [opcionesSeleccionadas setObject:[NSNumber numberWithInt:1] forKey:@"meGusta"];
                    
                    [[animacionPostiva layer] setBorderWidth:1];
                    [boton setTitleColor:[UIColor colorWithRed:39.0/255.0 green:140.0/255.0 blue:233.0/255.0 alpha:1] forState:UIControlStateNormal];
                    [[animacionPostiva layer] setBorderColor:[[UIColor colorWithRed:39.0/255.0 green:140.0/255.0 blue:233.0/255.0 alpha:1] CGColor]];
                    [animacionPostiva play];
                    
                }else if(indexPath.row == 1){
                    
                    [opcionesSeleccionadas setObject:[NSNumber numberWithInt:1] forKey:@"estaLleno"];
                    
                    [[animacionPostiva layer] setBorderWidth:1];
                    [boton setTitleColor:[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] forState:UIControlStateNormal];
                    [[animacionPostiva layer] setBorderColor:[[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] CGColor]];
                    [animacionPostiva play];
                }else if(indexPath.row == 2){
                    
                    [opcionesSeleccionadas setObject:[NSNumber numberWithInt:1] forKey:@"meGustaMusica"];
                    
                    [[animacionPostiva layer] setBorderWidth:1];
                    [boton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
                    [[animacionPostiva layer] setBorderColor:[[UIColor purpleColor] CGColor]];
                    [animacionPostiva play];
                }else if(indexPath.row == 3){
                    
                    [opcionesSeleccionadas setObject:[NSNumber numberWithInt:1] forKey:@"servicioRapido"];
                    
                    [[animacionPostiva layer] setBorderWidth:1];
                    [boton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                    [[animacionPostiva layer] setBorderColor:[[UIColor blueColor] CGColor]];
                    [animacionPostiva play];
                }else if(indexPath.row == 4){
                    
                    [opcionesSeleccionadas setObject:[NSNumber numberWithInt:1] forKey:@"servicioBueno"];
                    
                    [[animacionPostiva layer] setBorderWidth:1];
                    [boton setTitleColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:0 alpha:1] forState:UIControlStateNormal];
                    [[animacionPostiva layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:0 alpha:1] CGColor]];
                    [animacionPostiva play];
                }
            }
        }];
    }
    
}

- (IBAction)botonEnContra:(id)sender {
    
    UITableViewCell *celda = (UITableViewCell*)[[sender superview] superview];
    
    NSIndexPath *indexPath = [tablaOpcionesMood indexPathForCell:celda];
    
    UIButton *boton = (UIButton*)sender;
    
    LOTAnimationView *animacionPositiva = (LOTAnimationView*)[celda viewWithTag:3];
    
    [animacionPositiva setProgressWithFrame:[NSNumber numberWithInt:1]];
    
    [[animacionPositiva layer] setBorderWidth:0];
    
    UIButton *botonPositivo = (UIButton*)[celda viewWithTag:1];
    
    [botonPositivo setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    LOTAnimationView *animacionNegativa = (LOTAnimationView*)[celda viewWithTag:4];
    
    if(indexPath.row == 0 && [[opcionesSeleccionadas objectForKey:@"meGusta"] isEqual:[NSNumber numberWithInt:0]]){
        
        [opcionesSeleccionadas setObject:[NSNull null] forKey:@"meGusta"];
        
        [boton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [animacionNegativa setProgressWithFrame:[NSNumber numberWithInt:1]];
        
        [[animacionNegativa layer] setBorderWidth:0];
        
    }else if(indexPath.row == 1 && [[opcionesSeleccionadas objectForKey:@"estaLleno"] isEqual:[NSNumber numberWithInt:0]]){
        
        [opcionesSeleccionadas setObject:[NSNull null] forKey:@"estaLleno"];
        
        [boton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [animacionNegativa setProgressWithFrame:[NSNumber numberWithInt:1]];
        
        [[animacionNegativa layer] setBorderWidth:0];
        
    }else if(indexPath.row == 2 && [[opcionesSeleccionadas objectForKey:@"meGustaMusica"] isEqual:[NSNumber numberWithInt:0]]){
        
        [opcionesSeleccionadas setObject:[NSNull null] forKey:@"meGustaMusica"];
        
        [boton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [animacionNegativa setProgressWithFrame:[NSNumber numberWithInt:1]];
        
        [[animacionNegativa layer] setBorderWidth:0];
        
    }else if(indexPath.row == 3 && [[opcionesSeleccionadas objectForKey:@"servicioRapido"] isEqual:[NSNumber numberWithInt:0]]){
        
        [opcionesSeleccionadas setObject:[NSNull null] forKey:@"servicioRapido"];
        
        [boton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [animacionNegativa setProgressWithFrame:[NSNumber numberWithInt:1]];
        
        [[animacionNegativa layer] setBorderWidth:0];
        
    }else if(indexPath.row == 4 && [[opcionesSeleccionadas objectForKey:@"servicioBueno"] isEqual:[NSNumber numberWithInt:0]]){
        
        [opcionesSeleccionadas setObject:[NSNull null] forKey:@"servicioBueno"];
        
        [boton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [animacionNegativa setProgressWithFrame:[NSNumber numberWithInt:1]];
        
        [[animacionNegativa layer] setBorderWidth:0];
        
    }else{
    
    [animacionNegativa playToProgress:0.15 withCompletion: ^(BOOL animationFinished){
        
        if(animationFinished){
            
            if(indexPath.row == 0){
                
                [[animacionNegativa layer] setBorderWidth:1];
                [boton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                [[animacionNegativa layer] setBorderColor:[[UIColor redColor] CGColor]];
                [animacionNegativa play];
                
                [opcionesSeleccionadas setObject:[NSNumber numberWithInt:0] forKey:@"meGusta"];
                
            }else if(indexPath.row == 1){
                
                [[animacionNegativa layer] setBorderWidth:1];
                [boton setTitleColor:[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] forState:UIControlStateNormal];
                [[animacionNegativa layer] setBorderColor:[[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] CGColor]];
                [animacionNegativa play];
                
                [opcionesSeleccionadas setObject:[NSNumber numberWithInt:0] forKey:@"estaLleno"];
                
            }else if(indexPath.row == 2){
                
                [[animacionNegativa layer] setBorderWidth:1];
                [boton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                [[animacionNegativa layer] setBorderColor:[[UIColor grayColor] CGColor]];
                [animacionNegativa play];
                
                [opcionesSeleccionadas setObject:[NSNumber numberWithInt:0] forKey:@"meGustaMusica"];
                
            }else if(indexPath.row == 3){
                [[animacionNegativa layer] setBorderWidth:1];
                [boton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                [[animacionNegativa layer] setBorderColor:[[UIColor grayColor] CGColor]];
                [animacionNegativa play];
                
                [opcionesSeleccionadas setObject:[NSNumber numberWithInt:0] forKey:@"servicioRapido"];
                
            }else if(indexPath.row == 4){
                [[animacionNegativa layer] setBorderWidth:1];
                [boton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                [[animacionNegativa layer] setBorderColor:[[UIColor redColor] CGColor]];
                [animacionNegativa play];
                
                [opcionesSeleccionadas setObject:[NSNumber numberWithInt:0] forKey:@"servicioBueno"];
                
            }
        }
    }];
    }

}

-(void)getTomarFotoTuto{

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/gettutotomarfotovideo.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){
            
            tutoTomarFotoVideo = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        
    }];
    
    [uploadTask resume];
}

@end
