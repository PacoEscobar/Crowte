//
//  ActividadViewController.h
//  Crowte
//
//  Created by Paco Escobar on 09/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface Actividad2ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate, NSURLSessionDelegate, NSURLSessionDownloadDelegate>


@property BOOL seMuestraMapa;
@property int numeroCambiosMapa;

@property long ultimoRelIDTodos;
@property long ultimoRelIDAmigos;

@property NSMutableArray *checkinsTodosOtros;
@property NSMutableArray *checkinsTodosUltimos;

@property NSTimer *intervaloNumSolicitudes;
@property NSTimer *intervaloActualizarLista;

@property CLLocation *miLocation;

@property (strong, nonatomic) IBOutlet UIView *vistaEmptyState;
@property (weak, nonatomic) IBOutlet UIView *vistaNoLocation;


#pragma mark - mapa
@property (strong, nonatomic) IBOutlet UIView *fondoMapa;
@property (strong, nonatomic) IBOutlet MKMapView *mapa;

#pragma mark - lista check-in
@property (strong, nonatomic) IBOutlet UITableView *listaCheckin;
@property CGFloat previousScrollingViewYOffset;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintTablaOriginY;


#pragma mark - tutorial
@property (strong, nonatomic) IBOutlet UIView *backgroundTutorial;
@property NSMutableDictionary *tutoriales;

//opcionesambiente
@property NSMutableDictionary *opcionesSeleccionadas;

#pragma mark - mapa metodos

-(IBAction)mostrarMapa:(id)sender;
-(void)agregarPines:(NSArray*)checkinesNuevos viejos:(NSArray*)checkinesViejos;

#pragma mark - segmented control
- (IBAction)cambiarListaTodosAmigos:(id)sender;

#pragma mark - miselaneos
-(IBAction)buscar:(id)sender;
-(void)getNotificacionesNuevas;
-(void)actualizarCheckins;
-(void)getCheckins;
-(void)setNoLocalizacion;
-(void)setSiLocalizacion;
- (IBAction)activarLocalizacion:(id)sender;

#pragma mark - tutoriales
- (IBAction)cerrarTutorial:(id)sender;

//alt opciones

-(void)altOpcionMeGusta:(UITapGestureRecognizer*)gesture;
-(void)altOpcionEstaLleno:(UITapGestureRecognizer*)gesture;
-(void)altOpcionMeGustaMusica:(UITapGestureRecognizer*)gesture;
-(void)altOpcionServicioRapido:(UITapGestureRecognizer*)gesture;
-(void)altOpcionServicioBueno:(UITapGestureRecognizer*)gesture;

-(void)altOpcionMeGustaNegativo:(UITapGestureRecognizer*)gesture;
-(void)altOpcionEstaLlenoNegativo:(UITapGestureRecognizer*)gesture;
-(void)altOpcionMeGustaMusicaNegativo:(UITapGestureRecognizer*)gesture;
-(void)altOpcionServicioRapidoNegativo:(UITapGestureRecognizer*)gesture;
-(void)altOpcionServicioBuenoNegativo:(UITapGestureRecognizer*)gesture;

@end

