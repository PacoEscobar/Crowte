//
//  ReportarCheckinTableViewController.h
//  Crowte
//
//  Created by Paco Escobar on 12/03/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportarCheckinTableViewController : UITableViewController <UITextViewDelegate, NSURLSessionDelegate, NSURLSessionDownloadDelegate>

@property NSString *idUsuarioABloquear;
@property NSString *idRelObjDia;

@property (strong, nonatomic) IBOutlet UITextView *campoTextoReporte;



- (IBAction)cancelarReporte:(id)sender;
- (IBAction)enviarReporte:(id)sender;

@end
