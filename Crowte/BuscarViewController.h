//
//  BuscarViewController.h
//  Crowte
//
//  Created by Paco Escobar on 18/08/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuscarViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, NSURLConnectionDataDelegate>

@property int envioActual;
@property NSMutableData *datos;
@property NSString *palabra;
@property NSURLConnection *coneccion;
@property (weak, nonatomic) IBOutlet UITextField *campoBusqueda;
@property (weak, nonatomic) IBOutlet UITableView *tabla;
@property NSMutableArray *arregloResultadosBusquedaLugares;

@property NSMutableDictionary *fotosLugares;
@property NSMutableArray *arregloResultadosBusquedaPersonas;
@property NSMutableDictionary *fotosPersonas;
@property BOOL seBusca;
@property NSUInteger palabraABuscar;

-(void)enviarBusqueda;

@end
