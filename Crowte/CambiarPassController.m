//
//  CambiarPassController.m
//  Crowte
//
//  Created by Paco Escobar on 20/05/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import "CambiarPassController.h"
#import "NSString+MD5.h"

@interface CambiarPassController ()

@end

@implementation CambiarPassController

@synthesize nuevaContra, viejaContra;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIBarButtonItem *enviar = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"Cambiar"] style:UIBarButtonItemStylePlain target:self action:@selector(cambiar)];
    
    self.navigationItem.rightBarButtonItem = enviar;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    
    if(textField == viejaContra){
    
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.4];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-100, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }else if(textField == nuevaContra){
    
        if(self.view.frame.origin.y < 0){
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDuration:0.4];
            [UIView setAnimationBeginsFromCurrentState:YES];
            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+100, self.view.frame.size.width, self.view.frame.size.height);
            [UIView commitAnimations];
        }
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    if(textField == viejaContra){
    
        [nuevaContra becomeFirstResponder];
    }else if(textField == nuevaContra){
    
        [viejaContra becomeFirstResponder];
    }
    
    return NO;
}

-(void)cambiar{
    
    UIApplication *app = [UIApplication sharedApplication];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        app.networkActivityIndicatorVisible = YES;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.4];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    });

    NSUserDefaults *datosUsuario = [NSUserDefaults standardUserDefaults];
    
    [nuevaContra resignFirstResponder];
    [viejaContra resignFirstResponder];
    
    NSString *nuevaPass = [nuevaContra text];
    NSString *viejaPass = [viejaContra text];
    
    nuevaPass = [nuevaPass MD5String];
    viejaPass = [viejaPass MD5String];
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/cambiardatos.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"codigo=2&id=%@&passViejo=%@&passNuevo=%@&token=%@",[datosUsuario objectForKey:@"identifier"],viejaPass,nuevaPass, [datosUsuario objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
    
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil];
            
            [error show];
        }else{
            NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            if([respuesta intValue] == 0){
                
                UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Ups ! Algo salió mal, por favor inténtalo más tarde" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Aceptar", nil];
                
                dispatch_async(dispatch_get_main_queue(), ^{
            
                    app.networkActivityIndicatorVisible = NO;
                    [alerta show];
                });
            }else if([respuesta intValue] == 1){
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    app.networkActivityIndicatorVisible = NO;
                
                    [nuevaContra setText:@""];
                    [viejaContra setText:@""];
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
        }
    
    }];
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

    UITouch *toque = [[event allTouches] anyObject];
    
    if([viejaContra isFirstResponder] && [toque view] != viejaContra){
        
        [viejaContra resignFirstResponder];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.2];
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
    
    if([nuevaContra isFirstResponder] && [toque view] != nuevaContra){
        
        [nuevaContra resignFirstResponder];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.2];
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
    
    [super touchesBegan:touches withEvent:event];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
