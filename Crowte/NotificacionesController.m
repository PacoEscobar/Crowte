//
//  NotificacionesController.m
//  Crowte
//
//  Created by Paco Escobar on 07/01/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "NotificacionesController.h"
#import "ComentariosController.h"
#import "Perfil.h"

@implementation NotificacionesController

@synthesize tabla, notificaciones, maximo, arregloFotos;


-(void)viewDidLoad{

    [super viewDidLoad];
    
    maximo = 15;
    notificaciones = [NSMutableDictionary dictionaryWithObject:@"no" forKey:@"status"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"numeroNotificaciones" object:@"0"];
    
    [self pedirNotificaciones];
    
    UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStyleDone target:self action:nil];

    self.navigationItem.backBarButtonItem = atras;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

}


#pragma mark - tabla

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    int numero = 1;
    
    if([[notificaciones objectForKey:@"status"] isEqualToString:@"si"] && [[notificaciones objectForKey:@"cantidad"] intValue] > 0){
        
        numero = [[notificaciones objectForKey:@"cantidad"] intValue];
    }
   
    return numero;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    int numero = 0;
    
   if([[notificaciones objectForKey:@"status"] isEqualToString:@"no"]){
    
        numero = 71;
    }else if([[notificaciones objectForKey:@"status"] isEqualToString:@"si"] && [[notificaciones objectForKey:@"cantidad"] intValue] > 0){
        
        NSError *error = nil;
        
        NSString *palabra = [[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"codigo"];
        
        NSRange rango = NSMakeRange(0, [palabra length]);
        
        NSRegularExpression *comentario = [[NSRegularExpression alloc] initWithPattern:@"^(\\d)+(COM)$" options:NSRegularExpressionCaseInsensitive error:&error];
        
        NSRegularExpression *solicitud = [[NSRegularExpression alloc] initWithPattern:@"^(\\d)+(SOL)$" options:NSRegularExpressionCaseInsensitive error:&error];
        
        NSRegularExpression *aceptado = [[NSRegularExpression alloc] initWithPattern:@"^(\\d)+(ADD)$" options:NSRegularExpressionCaseInsensitive error:&error];
        
        NSArray *comentarios = [comentario matchesInString:palabra options:0 range:rango];
 
        NSArray *solicitudes = [solicitud matchesInString:palabra options:0 range:rango];
 
        NSArray *aceptados = [aceptado matchesInString:palabra options:0 range:rango];
 
        if([comentarios count] > 0){
            
            numero = 133;
        }else if([solicitudes count] > 0){
            
            numero = 230;
        }else if([aceptados count] > 0){
            
            numero = 102;
        }
    }else{
    
        numero = 94;
    }
    
    return numero;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    UITableViewCell *celda;
    
    if([[notificaciones objectForKey:@"status"] isEqualToString:@"no"]){
        
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaCargando"];
    }else if([[notificaciones objectForKey:@"status"] isEqualToString:@"si"]){
        
        if([[notificaciones objectForKey:@"cantidad"] intValue] == 0){
            
            celda = [tableView dequeueReusableCellWithIdentifier:@"nonotificaciones"];
        }else if(![[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"solicitudes"] isEqual:[NSNull null]]){
            
            celda  = [tableView dequeueReusableCellWithIdentifier:@"solicitud"];
            
            UIButton *aceptar = (UIButton*)[celda viewWithTag:3];
            UIButton *ignorar = (UIButton*)[celda viewWithTag:4];
            
            if(![[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"status"] isEqualToString:@"<null>"] && [[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"status"] isEqualToString:@"0"]){
            
                [aceptar addTarget:self action:@selector(aceptarAmigo:) forControlEvents:UIControlEventTouchUpInside];
                
                [ignorar addTarget:self action:@selector(ignorarAmigo:) forControlEvents:UIControlEventTouchUpInside];
            }else{
            
                [aceptar setAlpha:0];
                [ignorar setAlpha:0];
            }
            
            if([[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"visto"] intValue] == 0){
                
                [celda setBackgroundColor:[UIColor colorWithRed:0.901 green:1.0 blue:1.0 alpha:1]];
            }
            
            UIButton *nombre = (UIButton *)[celda viewWithTag:2];
            
            [nombre setTitle:[NSString stringWithFormat:@"%@ %@",[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"apellido"]] forState:UIControlStateNormal];
            
            
            if([arregloFotos objectForKey:[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"foto"]]){
            
                UIButton *foto = (UIButton *)[celda viewWithTag:1];
                [foto setBackgroundImage:[arregloFotos objectForKey:[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"foto"]] forState:UIControlStateNormal];
                
                foto.layer.borderWidth = 0.0;
                foto.layer.cornerRadius = 5;
                foto.layer.masksToBounds = YES;
                
            }else{
                
                NSURL *urlFoto = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue],[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"foto"]]];
                
                
                UIButton *foto = (UIButton*)[celda viewWithTag:1];
                
                [foto sd_setBackgroundImageWithURL:urlFoto forState:UIControlStateNormal];
                
                foto.layer.borderWidth = 0.0;
                foto.layer.cornerRadius = 5;
                foto.layer.masksToBounds = YES;
            
            }
            
            
        }else if(![[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"comentarios"] isEqual:[NSNull null]]){
            
            celda = [tableView dequeueReusableCellWithIdentifier:@"comentario"];
            
            if([[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"visto"] intValue] == 0){
                
                [celda setBackgroundColor:[UIColor colorWithRed:0.901 green:1.0 blue:1.0 alpha:1]];
            }
            
            UILabel *nombre = (UILabel *)[celda viewWithTag:2];
            
            nombre.text = [NSString stringWithFormat:@"%@ %@",[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"apellido"]];
            
            UILabel *comentario = (UILabel *)[celda viewWithTag:3];
            
            NSData *dataComentario = [[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"comentariotexto"] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSString *publicacionFormateada = [[NSString alloc] initWithData:dataComentario encoding:NSNonLossyASCIIStringEncoding];
            
            [comentario setText:[NSString stringWithFormat:@"Comentó: %@",publicacionFormateada]];
            
            UIActivityIndicatorView *cargaFoto = (UIActivityIndicatorView *)[celda viewWithTag:4];
            
            [cargaFoto startAnimating];
            
            NSURL *urlFoto = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue],[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"foto"]]];
            
            UIImageView *foto = (UIImageView *)[celda viewWithTag:1];
            [foto sd_setImageWithURL:urlFoto completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *url){
                
                [cargaFoto stopAnimating];
                [cargaFoto removeFromSuperview];
            }];

            
            foto.layer.borderWidth = 0.0;
            foto.layer.cornerRadius = 5;
            foto.layer.masksToBounds = YES;
            
        }else if([[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"comentarios"] isEqual:[NSNull null]] && [[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"solicitudes"] isEqual:[NSNull null]]){
        
            celda = [tableView dequeueReusableCellWithIdentifier:@"aceptaron"];
            
            if([[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"visto"] intValue] == 0){
                
                [celda setBackgroundColor:[UIColor colorWithRed:0.901 green:1.0 blue:1.0 alpha:1]];
            }
            
            UILabel *nombre = (UILabel *)[celda viewWithTag:2];
            
            [nombre setText:[NSString stringWithFormat:@"%@ %@",[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"apellido"]]];
            
            UIActivityIndicatorView *cargaFoto = (UIActivityIndicatorView *)[celda viewWithTag:3];
            
            [cargaFoto startAnimating];
            
            
            NSURL *urlFoto = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue],[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"foto"]]];
            
            UIImageView *foto = (UIImageView *)[celda viewWithTag:1];
            [foto sd_setImageWithURL:urlFoto completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *url){
                
                [cargaFoto stopAnimating];
                [cargaFoto removeFromSuperview];
            }];
            
            foto.layer.borderWidth = 0.0;
            foto.layer.cornerRadius = 5;
            foto.layer.masksToBounds = YES;
            
        }
    }
    
    if(![[notificaciones objectForKey:@"status"] isEqualToString:@"no"] && indexPath.row == [[notificaciones objectForKey:@"notificaciones"] count]-1){
    
        [self pedirNotificaciones];
    }else if([[notificaciones objectForKey:@"status"] isEqualToString:@"no"]){
        
        [self pedirNotificaciones];
    }
    
    app.networkActivityIndicatorVisible = NO;
    
    return celda;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda = [tabla cellForRowAtIndexPath:indexPath];
    [celda setBackgroundColor:[UIColor whiteColor]];
    
    if([celda.reuseIdentifier isEqualToString:@"comentario"]){
    
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ComentariosController *comentarios = [storyboard instantiateViewControllerWithIdentifier:@"comentarios"];
        
        NSArray *arreglo = [[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"foto"] componentsSeparatedByString:@"."];
        
        if([arreglo[[arreglo count]-1] isEqualToString:@"png"] || [arreglo[[arreglo count]-1] isEqualToString:@"PNG"]){
            
            comentarios.foto = UIImagePNGRepresentation([(UIImageView *)[celda viewWithTag:1] image]);
        }else{
            
            comentarios.foto = UIImageJPEGRepresentation([(UIImageView *)[celda viewWithTag:1] image],1);
        }
        
        comentarios.nombrePersona = [NSString stringWithFormat:@"%@ %@",[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"apellido"]];
        
        comentarios.postID = [[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"aconID"] intValue];
        
        comentarios.textoDePublicacion = @"<nohay>";
        
        [self.navigationController pushViewController:comentarios animated:YES];
        
        
    }else if([celda.reuseIdentifier isEqualToString:@"aceptaron"]){
    
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        Perfil *perfil = [storyboard instantiateViewControllerWithIdentifier:@"Perfil"];
        
        NSArray *arreglo = [[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"foto"] componentsSeparatedByString:@"."];
        
        perfil.stringNombre = [NSString stringWithFormat:@"%@ %@",[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"nombre"],[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"apellido"]];
        
        if([arreglo[[arreglo count]-1] isEqualToString:@"png"] || [arreglo[[arreglo count]-1] isEqualToString:@"PNG"]){
        
            perfil.dataFoto = UIImagePNGRepresentation([(UIImageView *)[celda viewWithTag:1] image]);
        }else{
        
            perfil.dataFoto = UIImageJPEGRepresentation([(UIImageView *)[celda viewWithTag:1] image],1);
        }
        
        perfil.amigoID = [[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexPath.row] objectForKey:@"remitente"] intValue];
        
        UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStyleDone target:self action:nil];
        
        self.navigationItem.backBarButtonItem = atras;
        
        [self.navigationController pushViewController:perfil animated:YES];
        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - mios

-(IBAction)aceptarAmigo:(id)sender{
    
    UITableViewCell *celda = (UITableViewCell*)[[sender superview] superview];
    
    NSIndexPath *indexpath = [tabla indexPathForCell:celda];
    
   NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    __block UIApplication *app = [UIApplication sharedApplication];
    
    app.networkActivityIndicatorVisible = YES;
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/aceptarsolicitudamistad.php"]];
   
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"solado=%d&solnte=%d&es=si&token=%@",[[defaults objectForKey:@"identifier"] intValue],[[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexpath.row] objectForKey:@"solicitante"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet){
                
                NSLog(@"error");
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    app.networkActivityIndicatorVisible = NO;
                });
            }
        });
    }];
    
    [[notificaciones objectForKey:@"notificaciones"] removeObjectAtIndex:indexpath.row];
    
    int notif = [[notificaciones objectForKey:@"cantidad"] intValue];
    notif--;
    
    [notificaciones setObject:[NSString stringWithFormat:@"%d",notif] forKey:@"cantidad"];
 
    if([[notificaciones objectForKey:@"notificaciones"] count] == 0){
        
        [notificaciones removeAllObjects];
        [notificaciones addEntriesFromDictionary:[NSDictionary dictionaryWithObjects:@[@"si",@0] forKeys:@[@"status",@"cantidad"]]];
    }
    
    [tabla beginUpdates];
    [tabla deleteRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationFade];
    [tabla endUpdates];
}

-(IBAction)ignorarAmigo:(id)sender{

    UITableViewCell *celda = (UITableViewCell*)[[sender superview] superview];
    
    NSIndexPath *indexpath = [tabla indexPathForCell:celda];
    
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    __block UIApplication *app = [UIApplication sharedApplication];
    
    app.networkActivityIndicatorVisible = YES;
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/aceptarsolicitudamistad.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"solado=%d&solnte=%d&es=no&token=%@",[[defaults objectForKey:@"identifier"] intValue],[[[[notificaciones objectForKey:@"notificaciones"] objectAtIndex:indexpath.row] objectForKey:@"solicitante"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet){
                
                NSLog(@"error");
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    app.networkActivityIndicatorVisible = NO;
                });
            }
        });
    }];
    
    [[notificaciones objectForKey:@"notificaciones"] removeObjectAtIndex:indexpath.row];
    
    int notif = [[notificaciones objectForKey:@"cantidad"] intValue];
    notif--;
    
    [notificaciones setObject:[NSString stringWithFormat:@"%d",notif] forKey:@"cantidad"];

    
    if([[notificaciones objectForKey:@"notificaciones"] count] == 0){
        
        [notificaciones removeAllObjects];
        [notificaciones addEntriesFromDictionary:[NSDictionary dictionaryWithObjects:@[@"si",@0] forKeys:@[@"status",@"cantidad"]]];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [tabla beginUpdates];
        [tabla deleteRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationFade];
        [tabla endUpdates];
    });
    
}

-(void)pedirNotificaciones{
    
    __block UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSUserDefaults *datosUsuario = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getnotificaciones.php"]];

        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&max=%d&token=%@",[[datosUsuario objectForKey:@"identifier"] intValue],maximo,[datosUsuario objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

            if(data != nil){

                if([[notificaciones objectForKey:@"status"] isEqualToString:@"no"]){
                    
                    [notificaciones removeAllObjects];
                }
                
                NSDictionary *llegan = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [notificaciones addEntriesFromDictionary:llegan];
                    
                    [tabla reloadData];
                    
                    app.networkActivityIndicatorVisible = NO;
                    maximo+= 15;
                });
            }
        }];
    });
}

@end
