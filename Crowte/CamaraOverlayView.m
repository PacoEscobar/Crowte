//
//  CamaraOverlayView.m
//  Crowte
//
//  Created by Paco Escobar on 24/07/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import "CamaraOverlayView.h"
#import <QuartzCore/QuartzCore.h>

@implementation CamaraOverlayView

@synthesize trigger, cambiarCamara, flash,tacha;

-(id)initWithFrame:(CGRect)frame{

    if(self = [super initWithFrame:frame]){
        
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        
        trigger = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
        trigger.layer.cornerRadius = 35;
        trigger.layer.borderWidth = 1;
        trigger.layer.masksToBounds = YES;
        trigger.layer.borderColor = [[UIColor blackColor] CGColor];

        [trigger setBackgroundColor:[UIColor whiteColor]];
        
        trigger.center = self.center;
        trigger.frame = CGRectMake(trigger.frame.origin.x, 450, trigger.frame.size.width, trigger.frame.size.height);
        
        cambiarCamara = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-50, 25, 25, 25)];
        
        [cambiarCamara setBackgroundColor:[UIColor blueColor]];
        
        flash = [[UIButton alloc] initWithFrame:CGRectMake(50, 15, 25, 25)];
        
        [flash setBackgroundImage:[UIImage imageNamed:@"flashno.png"] forState:UIControlStateNormal];
        
        tacha = [[UIButton alloc] initWithFrame:CGRectMake(10, 15, 15, 15)];
        
        [tacha setBackgroundImage:[UIImage imageNamed:@"tachablanca.png"] forState:UIControlStateNormal];
        
        [self bringSubviewToFront:trigger];
        [trigger setUserInteractionEnabled:YES];
        
        [self addSubview:trigger];
        [self addSubview:cambiarCamara];
        [self addSubview:flash];
        [self addSubview:tacha];
        
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
