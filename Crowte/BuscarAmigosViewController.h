//
//  BuscarAmigosViewController.h
//  Crowte
//
//  Created by Paco Escobar on 26/08/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuscarAmigosViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
