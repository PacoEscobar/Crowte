//
//  PerfilMioViewController.m
//  Crowte
//
//  Created by Paco Escobar on 10/02/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "PerfilMioViewController.h"
#import "configuracionController.h"
#import "PerfilLugarViewController.h"

@interface PerfilMioViewController ()

@end

@implementation PerfilMioViewController

@synthesize datosMios, navBarYInicial, checkins, tabla, locationManager, locationContador, geocoder;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    geocoder = [[CLGeocoder alloc] init];
    locationContador = NO;
    
    navBarYInicial = self.navigationController.navigationBar.frame.origin.y;
    
    UIImage *imageConfig = [UIImage imageNamed:@"iconconfig.png"];
    
    UIButton *config = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imageConfig.size.width, imageConfig.size.height)];
    
    [config setImage:imageConfig forState:UIControlStateNormal];
    
    [config addTarget:self action:@selector(irAConfiguracion:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barbtnconfig = [[UIBarButtonItem alloc] initWithCustomView:config];
    
    self.navigationItem.rightBarButtonItem = barbtnconfig;

    
    [[UINavigationBar appearance] setTranslucent:YES];
    
    self.navigationItem.title = NSLocalizedString(@"Perfil", nil);
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"transparente.png"]];
    
    [locationManager startUpdatingLocation];
    
    [self getCheckins];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{

    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"transparente.png"]];
    
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
}

#pragma mark - tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if([[[[checkins objectForKey:@"checkins"] objectAtIndex:0] objectForKey:@"valido"] isEqual:[NSNumber numberWithInt:0]]){
        
        return 1;
    }else{
        return [[checkins objectForKey:@"checkins"] count]+1;
    }    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.row == 0){
        
        return 236;
    }else{
    
        float adicional;
        
        NSString *texto = [[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"acontesimiento"];
        
        texto = [texto stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if(![texto isEqualToString:@""]){
            
            if([texto length] > 44){
                
                adicional = ceil([texto length]/44);
                adicional = adicional*55;
            }else{
                
                adicional = 55;
            }
        }
        
        return 375;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda;
    
    if(indexPath.row == 0){
    
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaInfo"];
        
        UIImageView *fotoPerfil = (UIImageView*)[celda viewWithTag:1];
        
        [fotoPerfil setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
        
        [[fotoPerfil layer] setBorderWidth:1];
        [[fotoPerfil layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
        [[fotoPerfil layer] setCornerRadius:50];
        [[fotoPerfil layer] setMasksToBounds:YES];
        
        [fotoPerfil sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[datosMios objectForKey:@"ID"] intValue],[datosMios objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"imageprofileempty50.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
        
            if(error != nil){
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[fotoPerfil layer] setBorderColor:[[UIColor whiteColor] CGColor]];
                    [fotoPerfil setContentMode:UIViewContentModeCenter];
                });
            }
        }];
        
        NSString *nombreString;
        
        if([[datosMios objectForKey:@"apeido"] isEqualToString:@"(null)"]){
        
            nombreString = [datosMios objectForKey:@"nombre"];
        }else{
        
            nombreString = [NSString stringWithFormat:@"%@ %@",[datosMios objectForKey:@"nombre"],[datosMios objectForKey:@"apeido"]];
        }
        
        UILabel *nombre = (UILabel*)[celda viewWithTag:4];
        [nombre setText:nombreString];

    }else{
    
        
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaCheckin"];
        
        UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldanuevo.png"]];
        
        [celda setBackgroundView:fondo];
        [celda setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *fotoUsuario = (UIImageView*)[celda viewWithTag:1];
        
        [fotoUsuario.layer setBorderWidth:0];
        [fotoUsuario.layer setCornerRadius:22];
        [fotoUsuario.layer setMasksToBounds:YES];
        
        [fotoUsuario sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://crowte.com/usuarios/%@/%@",[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"uID"],[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"]];
        
        
        UILabel *nombreylugar = (UILabel*)[celda viewWithTag:2];
        
        if([[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"apellido"] isEqualToString:@"(null)"]){
            
            [[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] setValue:@"" forKey:@"apellido"];
        }
        
        [nombreylugar setText:[NSString stringWithFormat:@"%@ %@ estuvo en %@",[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"nombre"],[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"apellido"],[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"lugar"]]];
        
        unsigned long sizenombre = [[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"nombre"] length]+[[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"apellido"] length]+1;
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:nombreylugar.attributedText];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:48.0/255.0 green:52.0/255.0 blue:63.0/255.0 alpha:1] range:NSMakeRange(0, sizenombre)];
        [nombreylugar setAttributedText:attributedString];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:48.0/255.0 green:52.0/255.0 blue:63.0/255.0 alpha:1] range:NSMakeRange(sizenombre+11, [[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"lugar"] length])];
        [nombreylugar setAttributedText:attributedString];
        
        
        UILabel *tiempoDeCheckin = (UILabel*)[celda viewWithTag:3];
        [tiempoDeCheckin setText:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"fecha"]];
        
        
        NSArray *constraints = [[celda contentView] constraints];
        NSLayoutConstraint *constraintInferior;
        
        for (NSLayoutConstraint *constraint in constraints) {
            
            if([[constraint identifier] isEqualToString:@"constraintImagenBottom"]){
                
                constraintInferior = constraint;
            }
        }
        
        
        UIImageView *fondoLugar = (UIImageView*)[celda viewWithTag:4];
        [fondoLugar setImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
        
        NSString *mensaje = [[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"acontesimiento"];
        
        NSData *data = [mensaje dataUsingEncoding:NSUTF8StringEncoding];
        
        mensaje = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        
        mensaje = [mensaje stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        float adicional = 0;
        
        if(constraintInferior != nil){
            
            if(![mensaje isEqualToString:@""]){
                
                if([mensaje length] > 44){
                    
                    adicional = ceil([mensaje length]/44);
                    adicional = adicional*55;
                }else{
                    
                    adicional = 55;
                }
            }
            
            [constraintInferior setConstant:62+adicional];
        }
        
        
        if(![mensaje isEqualToString:@""]){
            
            UITextView *texto = [[UITextView alloc] initWithFrame:CGRectMake(fondoLugar.frame.origin.x, fondoLugar.frame.origin.y+fondoLugar.frame.size.height, fondoLugar.frame.size.width-100, 100)];
            
            [texto setText:mensaje];
            
            [texto setFont:[UIFont fontWithName:@"Raleway-Regular" size:13]];
            [texto setTextColor:[UIColor colorWithRed:48.0/255.0 green:52.0/255.0 blue:63.0/255.0 alpha:1]];
            
            [texto setBackgroundColor:[UIColor colorWithRed:246.0/255.0 green:244.0/255.0 blue:243.0/255.0 alpha:1]];
            
            [texto setTextContainerInset:UIEdgeInsetsMake(20, 20, 20, 20)];
            
            [texto setTag:11];
            
            [texto setUserInteractionEnabled:NO];
            [texto setScrollEnabled:NO];
            [texto setEditable:NO];
            
            [texto setContentMode:UIViewContentModeCenter];
            
            texto.translatesAutoresizingMaskIntoConstraints = NO;
            
            [celda addSubview:texto];
            
            [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:fondoLugar attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-302]];
            
            [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:[texto superview] attribute:NSLayoutAttributeTrailingMargin relatedBy:NSLayoutRelationEqual toItem:texto attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:1]];
            
            [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:fondoLugar attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
            
        }else{
            
            UITextView *texto = (UITextView*)[celda viewWithTag:11];
            
            [texto removeFromSuperview];
        }
        
        UIImageView *fotoPrincipalLugar = (UIImageView*)[celda viewWithTag:5];
        [fotoPrincipalLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
                
        [fotoPrincipalLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"lugarID"],[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoLugar"]]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){

            if(error.code == 0){
            
                [fotoPrincipalLugar.layer setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                [fotoPrincipalLugar setContentMode:UIViewContentModeScaleToFill];
            }else{
            
                [fotoPrincipalLugar.layer setBorderColor:[[UIColor whiteColor] CGColor]];
                [fotoPrincipalLugar setContentMode:UIViewContentModeCenter];
            }
        }];
        
        [[fotoPrincipalLugar layer] setBorderWidth:1];
        [[fotoPrincipalLugar layer] setCornerRadius:50];
        [[fotoPrincipalLugar layer] setMasksToBounds:YES];
        
        UIImageView *fotoSecundaria = (UIImageView*)[celda viewWithTag:4];
        
        [fotoSecundaria sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"lugarID"],[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoSecundaria"]]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
        
        UIButton *join = (UIButton*)[celda viewWithTag:6];
        [join addTarget:self action:@selector(join:) forControlEvents:UIControlEventTouchUpInside];
        
        if([[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"finguer"] intValue] == 1){
            
            [join setImage:[UIImage imageNamed:@"joinmanoverde.png"] forState:UIControlStateNormal];
        }else{
            
            [join setImage:[UIImage imageNamed:@"joinmanonegra.png"] forState:UIControlStateNormal];
        }
        
        UIButton *numJoins = (UIButton*)[celda viewWithTag:7];
        [numJoins setTitle:[[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"numDePersonas"] description] forState:UIControlStateNormal];
        
        UIButton *numComentarios = (UIButton*)[celda viewWithTag:9];
        
        if([[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"comentarios"] intValue] == 0){
            
            [numComentarios setTitle:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@""] forState:UIControlStateNormal];
        }else{
            
            [numComentarios setTitle:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"comentarios"] forState:UIControlStateNormal];
        }
        
        UIButton *opciones = (UIButton*)[celda viewWithTag:10];
        
        [opciones addTarget:self action:@selector(opciones:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return celda;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row > 0){
        
        UIStoryboard *storybard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PerfilLugarViewController *lugar = (PerfilLugarViewController*)[storybard instantiateViewControllerWithIdentifier:@"perfilLugar"];
        
        [lugar setIdLugar:[[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"lugarID"] intValue]];
        
        [lugar setNombreLugar:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"lugar"]];
        
        [lugar setFotoPrincipalLugar:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoLugar"]];
        
        [lugar setFotoSecundariaLugar:[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"fotoSecundaria"]];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"localizacionLugar"] objectForKey:@"latitud"] floatValue] longitude:[[[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"localizacionLugar"] objectForKey:@"longitud"] floatValue]];

        [lugar setLocation:location];
        
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
        UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
        self.navigationItem.backBarButtonItem = atras;
        
        [self.navigationController pushViewController:lugar animated:YES];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [self.navigationController.navigationBar setFrame:CGRectMake(self.navigationController.navigationBar.frame.origin.x, ([scrollView contentOffset].y*-1)+navBarYInicial, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
}

#pragma mark - hechos por mi

-(void)getCheckins{

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getfriendposts.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"amigo=0&usuario=%d&m=1",[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){

        if(error == nil){
            
            if(data != nil){
                
                NSError *error = nil;
                
                checkins = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                NSLog(@"checkins %@",checkins);
                if(error == nil){
                    
                    UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    
                    UILabel *numCheckins = (UILabel*)[celda viewWithTag:2];

                    [numCheckins setText:[checkins objectForKey:@"cantidadCheckins"]];
                    
                    UILabel *numAmigos = (UILabel*)[celda viewWithTag:3];
                    
                    [numAmigos setText:[checkins objectForKey:@"cantidadAmigos"]];
                    
                    [tabla reloadData];
                }
            }
        }
        
    }];
    
    [uploadTask resume];
}

-(IBAction)irAConfiguracion:(id)sender{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    configuracionController *config = [storyboard instantiateViewControllerWithIdentifier:@"configuracion"];
    
    NSString *apellidoString;
    
    if([[datosMios objectForKey:@"apeido"] isEqualToString:@"(null)"]){
        
        apellidoString = @"";
    }else{
        
        apellidoString = [datosMios objectForKey:@"apeido"];
    }
    
    [config setNombreUsuario:[datosMios objectForKey:@"nombre"]];
    [config setApellidoUsuario:apellidoString];
    [config setFotoActual:[datosMios objectForKey:@"foto"]];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.navigationController pushViewController:config animated:YES];
}

-(IBAction)join:(id)sender{
    
    NSLog(@"join");
}

-(IBAction)opciones:(id)sender{

    UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *eliminarCheckin = [UIAlertAction actionWithTitle:@"Eliminar" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *accion){
    
        UIAlertController *confirmacion = [UIAlertController alertControllerWithTitle:@"¿Estás seguro?" message:@"Si eliminas este check-in no podrás recuperarlo." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *aceptar = [UIAlertAction actionWithTitle:@"Aceptar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            CGPoint posicionBoton = [sender convertPoint:CGPointZero toView:tabla];
            
            NSIndexPath *indexPath = [tabla indexPathForRowAtPoint:posicionBoton];

            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/eliminarpublicacion.php"]];
            
            [request setHTTPMethod:@"POST"];
            
            NSData *cuerpo = [[NSString stringWithFormat:@"id=%@&u=%d",[[[checkins objectForKey:@"checkins"] objectAtIndex:indexPath.row-1] objectForKey:@"aconID"],[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
            
            NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
            
            NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
            
            NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                
                if(error == nil){
                    
                    if(data != nil){
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                        
                            [[checkins objectForKey:@"checkins"] removeObjectAtIndex:indexPath.row-1];
                            [tabla deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
                        });
                    }
                }
                
            }];
            
            [uploadTask resume];
            
        }];
        
        UIAlertAction *noAceptar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
        
        [confirmacion addAction:aceptar];
        [confirmacion addAction:noAceptar];
        
        [self presentViewController:confirmacion animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){
    
        [opciones dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [opciones addAction:eliminarCheckin];
    [opciones addAction:cancelar];
    
    [self presentViewController:opciones animated:YES completion:nil];
}

#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}

#pragma mark - location

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    if(locationContador){
        return;
    }
    
    locationContador = YES;
    
    [geocoder reverseGeocodeLocation:manager.location completionHandler:^(NSArray *placemarks, NSError *error){
        
        CLPlacemark *placemark = [placemarks lastObject];
        
        UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        UILabel *lugarActual = (UILabel*)[celda viewWithTag:5];
        
        [lugarActual setText:[NSString stringWithFormat:@"%@, %@",[placemark administrativeArea],[placemark country]]];
        
    }];
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
