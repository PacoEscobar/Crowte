//
//  perfil.h
//  Crowte
//
//  Created by Paco Escobar on 11/05/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface Perfil : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>

@property int postMinimo;
@property int offsetImagen;
@property int ultimacelda;
@property int index;
@property float ultimooffset;
@property UIApplication *app;
@property float offset;
@property int posicionScroll;
@property NSString *stringNombre;
@property NSData *dataFoto;
@property int amigoID;
@property (weak, nonatomic) IBOutlet UIImageView *foto;
@property (weak, nonatomic) IBOutlet UIImageView *imagenPMostrar;
@property NSString *publicacion;
@property NSString *lugar;
@property (retain, nonatomic) NSMutableArray *publicaciones;
@property int estatus;
@property (weak, nonatomic) IBOutlet UITableView *tabla;
@property BOOL ninguno;
@property NSString *nombreFoto;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintDisplay;

-(IBAction)accionBoton:(id)boton;
-(void)getPosts;
- (IBAction)opciones:(id)sender;

@end
