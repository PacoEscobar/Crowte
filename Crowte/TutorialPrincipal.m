//
//  TutorialPrincipal.m
//  Crowte
//
//  Created by Paco Escobar on 19/01/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "PageViewController.h"
#import "TabBarPrincipalViewController.h"
#import "TutorialPrincipal.h"

@implementation TutorialPrincipal

@synthesize index, crowte, bienvenido, datosUsuario, texto1, texto2, botonComenzar, botonComenzarConstraint, constraintTexto2, constraintTexto3, texto1_crowte,constraintT1_T2;

-(void)viewDidLoad{

    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{

    dispatch_async(dispatch_get_main_queue(), ^{
        if([[UIScreen mainScreen] bounds].size.height <= 480){
            
            [botonComenzarConstraint setConstant:50];
            [constraintTexto2 setConstant:0];
            [constraintTexto3 setConstant:53];
            [texto1_crowte setConstant:0];
            
            [texto1 setFont:[UIFont systemFontOfSize:15]];
            [texto2 setFont:[UIFont systemFontOfSize:15]];
            [constraintT1_T2 setConstant:0];
            [texto1 sizeToFit];
            [texto2 sizeToFit];
            
        }
    });
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    [sender setEnabled:NO];
    
    __block UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
    
    [[UINavigationBar appearance] setTranslucent:YES];
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
    
    UIStoryboard *storyboard;
    
    TabBarPrincipalViewController *tabBarPrincipalVC;
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    tabBarPrincipalVC= [storyboard instantiateViewControllerWithIdentifier:@"mainTabBar"];

    [tabBarPrincipalVC setCheckinsTodosOtros:[datosUsuario objectForKey:@"todosOtrosPost"]];
    [tabBarPrincipalVC setCheckinsTodosUltimos:[datosUsuario objectForKey:@"todosUltimosPosts"]];
    [tabBarPrincipalVC setDatosCheckinEnLugar:[datosUsuario objectForKey:@"checkinAlready"]];
    [tabBarPrincipalVC setDatosMios:[datosUsuario objectForKey:@"datos"]];
    
    app.networkActivityIndicatorVisible = NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSURL *url = [NSURL URLWithString:@"https://www.crowte.com/app/tutorialvisto.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[NSString stringWithFormat:@"id=%d&recurso=1",[[defaults objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){}];

}

@end
