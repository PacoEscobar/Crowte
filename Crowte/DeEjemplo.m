//
//  DeEjemplo.m
//  Crowte
//
//  Created by Paco Escobar on 27/04/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import "DeEjemplo.h"
#import "ComentariosController.h"
#import "Perfil.h"
#import "lugarViewController.h"
#import "enviarReportePublicacionViewController.h"
#import "controladorTabControllerViewController.h"
#import "NotificacionesController.h"
#import "PageViewController.h"
#import "ActivarCuentaViewController.h"
#import "KLCPopup.h"
#import "ResponderMensajeViewController.h"
#import "ImagenPubLugarViewController.h"
#import "CamaraOverlayView.h"
#import "CamaraOverlayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <AVFoundation/AVFoundation.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <Photos/Photos.h>

@interface DeEjemplo ()

@end

@implementation DeEjemplo

@synthesize vistauno, vistados, vistatres, tabla, arreglo, notificaciones, arregloDeFotos, tablaOffset, seActualiza, dejoDeTocar, offset, loadingPunto, menorIdPost, already, seBusca, indexMenuPublicacion, campoCompartir, palabraABuscar, arregloDeFotosDeLugares,tablaLugares, arregloLugares, nombreMasVisitado, imagenMasVisitado, diccionarioMasVisitado, diccionarioMasVotado, nombreMasVotado, imagenMasVotado, lugarAir, nombreLugarAir, fotolugarair, botonEnviar, yaEnLugar, enLugar, locationManager,scrollview, miID, vistadecarga, activityEstaLugar, lugares, defaults, coneccionPublicar,vistaVotar, datosLugarPublicacion, timerAunAhiBoton, datosLugarYaAhi, fechaPublicacionActual, constraintEstaLugar, constraintVistaVotar, constraintHeightScrollview, constraintCompartirButton, localizacionHab, tutoriales, constraintNombreVotado, constraintEtiqVisitado, constraintEtiqVotado, constraintImbVotado, constraintImgVisitado, constraintNombreVisitado, popup, arregloComentarios,seTomaFoto,imagenCargada, dataFotoNueva, publicarFb, coordenadas, picker, videofoto, alreadyVideo, camara, coneccionSubirFoto, reproductor, layer, controlesReproductor, urlVideoApasar, lugaresCheckinRapido, popupFullSVideo, totalBytesArchivoAsubir, bytesSubidosArchivoAsubir, lugaresMuestra;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    Perfil *perfil = [storyboard instantiateViewControllerWithIdentifier:@"Perfil"];
    
    [self.tabBarController addChildViewController:perfil];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
        
    seTomaFoto = YES;
    imagenCargada = NO;
    enLugar = YES;
    publicarFb = NO;
    alreadyVideo = NO;

    if([[UIScreen mainScreen] bounds].size.height <= 480){
    
        [scrollview setContentSize:CGSizeMake(960, 387)];
        [constraintEtiqVisitado setConstant:10];
        [constraintImgVisitado setConstant:60];
        [constraintNombreVisitado setConstant:140];
        [constraintEtiqVotado setConstant:200];
        [constraintImbVotado setConstant:241];
        [constraintNombreVotado setConstant:322];
    }else{
        [scrollview setContentSize:CGSizeMake(960, 450)];
    }
    
    [scrollview setPagingEnabled:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actualizaNotificaciones:) name:@"numeroNotificaciones" object:nil];
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    miID = [[defaults objectForKey:@"identifier"] intValue];
    
    controladorTabControllerViewController *controlador = (controladorTabControllerViewController *)self.tabBarController;
    
    tutoriales = [controlador.datosUsuario objectForKey:@"tutoriales"];

    [defaults setObject:tutoriales forKey:@"tutoriales"];
    
    [defaults synchronize];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getdatos.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&recurso=2&token=%@",miID,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                NSLog(@"error");
            }else{
                
                lugares = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                if([[lugares objectForKey:@"lugar"] objectForKey:@"status"] != nil){

                    dispatch_async(dispatch_get_main_queue(), ^{
                        [vistadecarga setHidden:YES];
                        [campoCompartir setEditable:YES];
                        enLugar = NO;
                        
                        [self activarBotonCheckinRapido];
                    });
                }else{
                    
                    enLugar = YES;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        timerAunAhiBoton = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(checarSiEstaEnLugar:) userInfo:[[lugares objectForKey:@"lugar"] objectForKey:@"fecha"] repeats:YES];
                        
                        [timerAunAhiBoton fire];
                    });
                    
                    [campoCompartir setEditable:NO];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [activityEstaLugar removeFromSuperview];
                        [vistadecarga setBackgroundColor:[UIColor colorWithRed:22/255.0 green:179/255.0 blue:173/255.0 alpha:1.0]];
                        
                        UILabel *estasen = [[UILabel alloc] init];
                        
                        [estasen setText:NSLocalizedString(@"Ahora estas en:",nil)];
                        
                        if([[UIScreen mainScreen] bounds].size.height <= 480){
                            
                            [estasen setFrame:CGRectMake(50, 10, estasen.frame.size.width, estasen.frame.size.height)];
                        }else{
                            [estasen setFrame:CGRectMake(50, 30, estasen.frame.size.width, estasen.frame.size.height)];
                        }
                        [estasen setTextColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0]];
                        
                        [estasen sizeToFit];
                        
                        [vistadecarga addSubview:estasen];
                        
                        UIActivityIndicatorView *cargaFotoLugar = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(135, 90, 50, 50)];
                        
                        [cargaFotoLugar startAnimating];
                        
                        [vistadecarga addSubview:cargaFotoLugar];
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                            
                            NSURL *urlFotoLugar = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[lugares objectForKey:@"lugar"] objectForKey:@"objID"],[[lugares objectForKey:@"lugar"] objectForKey:@"foto"]]];
                            
                            NSData *dataFotoLugar = [[NSData alloc] initWithContentsOfURL:urlFotoLugar];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                UIImageView *imageFotoLugar = [[UIImageView alloc] initWithImage:[UIImage imageWithData:dataFotoLugar]];
                                
                                if([[UIScreen mainScreen] bounds].size.height <= 480){
                                
                                    [imageFotoLugar setFrame:CGRectMake(110, 50, 100, 100)];
                                }else{
                                    [imageFotoLugar setFrame:CGRectMake(110, 90, 100, 100)];
                                }
                                
                                [cargaFotoLugar stopAnimating];
                                [cargaFotoLugar removeFromSuperview];
                                
                                [vistadecarga addSubview:imageFotoLugar];
                            });
                        });
                        
                        UIButton *lugar = [[UIButton alloc] init];
                        
                        [lugar setTitleColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0] forState:UIControlStateNormal];
                        
                        [lugar setTitle:[[lugares objectForKey:@"lugar"] objectForKey:@"lugar"] forState:UIControlStateNormal];
                        
                        [lugar.titleLabel setFont:[UIFont fontWithName:lugar.titleLabel.font.familyName size:20]];
                        
                        if([[UIScreen mainScreen] bounds].size.height <= 480){
                            
                            [lugar setFrame:CGRectMake(0, 170, 320, lugar.frame.size.height)];
                        }else{
                            [lugar setFrame:CGRectMake(0, 210, 320, lugar.frame.size.height)];
                        }
                        
                        [lugar setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
                        
                        [lugar addTarget:self action:@selector(irALugarYaAhi:) forControlEvents:UIControlEventTouchUpInside];
                        
                        [vistadecarga addSubview:lugar];

                        UIButton *votar = [[UIButton alloc] init];
                        
                        [votar setTitle:NSLocalizedString(@"¡Votar!", nil) forState:UIControlStateNormal];
                        
                        [votar.titleLabel setFont:[UIFont fontWithName:votar.titleLabel.font.familyName size:13.0]];
                        
                        
                        if([[UIScreen mainScreen] bounds].size.height <= 480){
                            
                            [votar setFrame:CGRectMake(110, 170, 100, 30)];
                        }else{
                            [votar setFrame:CGRectMake(110, 250, 100, 30)];
                        }
                        [votar setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                        
                        [votar setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
                        
                        [votar.layer setCornerRadius:15];
                        
                        [votar addTarget:self action:@selector(votarLugar) forControlEvents:UIControlEventTouchUpInside];
                        
                        [vistadecarga addSubview:votar];
                        
                        if([[[lugares objectForKey:@"tutoriales"] objectForKey:@"botonVotar"] intValue] == 0){
                        
                            [self tutorialBotonVotar:votar vista:vistadecarga];
                        }
                        
                        
                        UIButton *aunAhi = [[UIButton alloc] init];
                        
                        [aunAhi setTitle:NSLocalizedString(@"Aún estoy ahí", nil) forState:UIControlStateNormal];
                        
                        [aunAhi.titleLabel setFont:[UIFont fontWithName:aunAhi.titleLabel.font.familyName size:13.0]];
                        
                        if([[UIScreen mainScreen] bounds].size.height <= 480){
                        
                            [aunAhi setFrame:CGRectMake(85, 220, 150, 30)];
                        }else{
                            [aunAhi setFrame:CGRectMake(85, 300, 150, 30)];
                        }
                        
                        if(![[[lugares objectForKey:@"lugar"] objectForKey:@"fecha"] isEqual:[NSNull null]] && ([[NSDate date] timeIntervalSince1970] - [[[lugares objectForKey:@"lugar"] objectForKey:@"fecha"] intValue]) <= 6300){
                            
                            [aunAhi setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:1.0]];
                            
                            [aunAhi addTarget:self action:@selector(aunAhi:) forControlEvents:UIControlEventTouchUpInside];
                        }else{
                            
                            [aunAhi setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                            
                            [aunAhi addTarget:self action:@selector(aunAhi:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        
                        [aunAhi setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
                        
                        [aunAhi.layer setCornerRadius:15];
                        
                        [aunAhi setTag:10];
                        
                        [vistadecarga addSubview:aunAhi];
                        
                        UIButton *yano = [[UIButton alloc] init];
                        
                        [yano setTitle:NSLocalizedString(@"Ya no estoy ahí", nil) forState:UIControlStateNormal];
                        
                        [yano.titleLabel setFont:[UIFont fontWithName:yano.titleLabel.font.familyName size:13.0]];
                        
                        if([[UIScreen mainScreen] bounds].size.height <= 480){
                            
                            [yano setFrame:CGRectMake(60, 270, 200, 30)];
                        }else{
                            [yano setFrame:CGRectMake(60, 350, 200, 30)];
                        }
                        
                        [yano setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                        
                        [yano setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
                        
                        [yano.layer setCornerRadius:15];
                        
                        [yano addTarget:self action:@selector(noAhi:) forControlEvents:UIControlEventTouchUpInside];
                        
                        [vistadecarga addSubview:yano];
                        
                        //--------- textfield
                        
                        UITextField *comentariosLugartxtfield;
                        
                        if([[UIScreen mainScreen] bounds].size.height >= 560){
                            
                            comentariosLugartxtfield = [[UITextField alloc] initWithFrame:CGRectMake(22, 418, 237, 30)];
                        }else{
                            
                            comentariosLugartxtfield = [[UITextField alloc] initWithFrame:CGRectMake(22, 318, 237, 30)];
                        }
                        
                        [comentariosLugartxtfield setDelegate:self];
                        
                        [comentariosLugartxtfield setTag:1];
                        
                        [comentariosLugartxtfield setPlaceholder:NSLocalizedString(@"Envía un comentario", nil)];
                        
                        [comentariosLugartxtfield setBackgroundColor:[UIColor whiteColor]];
                        
                        comentariosLugartxtfield.layer.cornerRadius = 10;
                        
                        [comentariosLugartxtfield addTarget:self action:@selector(textoCambio:) forControlEvents:UIControlEventEditingChanged];
                        
                        [vistadecarga addSubview:comentariosLugartxtfield];
                        
                        UIButton *comentarcamara = [[UIButton alloc] initWithFrame:CGRectMake(270, 418, 45, 30)];
                        
                        [comentarcamara setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                        
                        [comentarcamara setTitle:NSLocalizedString(@"Enviar",nil) forState:UIControlStateNormal];
                        
                        [comentarcamara.titleLabel setFont:[UIFont fontWithName:comentarcamara.titleLabel.font.familyName size:13]];
                        
                        [comentarcamara addTarget:self action:@selector(enviarMensajeLugar:) forControlEvents:UIControlEventTouchUpInside];
                        
                        comentarcamara.layer.cornerRadius = 15;
                        
                        [comentarcamara setTag:2];
                        
                        [comentarcamara setTitle:@"" forState:UIControlStateNormal];
                        [comentarcamara setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                        [comentarcamara setFrame:CGRectMake(comentarcamara.frame.origin.x, comentarcamara.frame.origin.y, 30, comentarcamara.frame.size.height)];
                        comentarcamara.layer.cornerRadius = 5;
                        
                        [vistadecarga addSubview:comentarcamara];

                        
                        //------------------
                        
                        vistaVotar = [[UIView alloc] initWithFrame:CGRectMake(0, 568, 320, 568)];
                        
                        [vistaVotar setTranslatesAutoresizingMaskIntoConstraints:NO];
                        
                        [vistaVotar setBackgroundColor:[UIColor colorWithRed:22/255.0 green:179/255.0 blue:173/255.0 alpha:1]];
                        
                        [self.view addSubview:vistaVotar];
                        
                        
                       // UITextView *comentariosLugar = [[UITextView alloc] initWithFrame:CGRectMake(20, 100, 280, 100)];
                        
                        UITextField *comentariosLugar;
                        
                        if([[UIScreen mainScreen] bounds].size.height >= 560){
                            
                            comentariosLugar = [[UITextField alloc] initWithFrame:CGRectMake(22, 418, 237, 30)];
                        }else{
                            
                            comentariosLugar = [[UITextField alloc] initWithFrame:CGRectMake(22, 318, 237, 30)];
                        }
                        
                        [comentariosLugar setDelegate:self];
                        
                        [comentariosLugar setTag:1];
                        
                        [comentariosLugar setPlaceholder:NSLocalizedString(@"Envía un comentario", nil)];
                        
                        [comentariosLugar setBackgroundColor:[UIColor whiteColor]];
                        
                        comentariosLugar.layer.cornerRadius = 10;
                        
                        [comentariosLugar addTarget:self action:@selector(textoCambio:) forControlEvents:UIControlEventEditingChanged];
                        
                        
                        UIButton *comentar = [[UIButton alloc] initWithFrame:CGRectMake(262, 418, 45, 30)];
                        
                        if([[UIScreen mainScreen] bounds].size.height < 560){
                        
                            comentar.frame = CGRectMake(262, 318, 45, 30);
                        }
                        
                        [comentar setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                        
                        [comentar setTitle:NSLocalizedString(@"Enviar", nil) forState:UIControlStateNormal];
                        
                        [comentar.titleLabel setFont:[UIFont fontWithName:comentar.titleLabel.font.familyName size:13]];
                        
                        [comentar addTarget:self action:@selector(enviarMensajeLugar:) forControlEvents:UIControlEventTouchUpInside];
                        
                        comentar.layer.cornerRadius = 15;
                        
                        [comentar setTag:2];
                        
                        [comentar setTitle:@"" forState:UIControlStateNormal];
                        [comentar setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                        [comentar setFrame:CGRectMake(comentar.frame.origin.x, comentar.frame.origin.y, 30, comentar.frame.size.height)];
                        comentar.layer.cornerRadius = 5;
                        
                        UIButton *verComentarios = [[UIButton alloc] initWithFrame:CGRectMake(65, 200, 200, 30)];
                        
                        if([[UIScreen mainScreen] bounds].size.height < 560){
                            
                            verComentarios.frame = CGRectMake(65, 100, 200, 30);
                        }
                        
                        [verComentarios setCenter:CGPointMake(self.vistaVotar.center.x, verComentarios.center.y)];
                        
                        [verComentarios setBackgroundColor:[UIColor colorWithRed:0 green:111.0/255.0 blue:147.0/255.0 alpha:1]];
                        
                        [verComentarios.titleLabel setFont:[UIFont fontWithName:comentar.titleLabel.font.familyName size:13]];
                        
                        [verComentarios setTitle:NSLocalizedString(@"Ver todos los comentarios", nil) forState:UIControlStateNormal];
                        
                        verComentarios.layer.cornerRadius = 15;
                        
                        [verComentarios setTag:20];
                        
                        [verComentarios addTarget:self action:@selector(mostrarTodosComentarios:) forControlEvents:UIControlEventTouchUpInside];
                        
                        if(![[[lugares objectForKey:@"voto"] objectForKey:@"voto"] isEqual:[NSNull null]] && [[[lugares objectForKey:@"voto"] objectForKey:@"voto"] intValue] == 0){
                            
                            UIButton *votofavor;
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                            
                                votofavor = [[UIButton alloc] initWithFrame:CGRectMake(90, 150, 50, 30)];
                            }else{
                                votofavor = [[UIButton alloc] initWithFrame:CGRectMake(90, 250, 50, 30)];
                            }
                            
                            [votofavor setBackgroundColor:[UIColor colorWithRed:0 green:0.9 blue:0 alpha:1.0]];
                            
                            [votofavor setTitle:NSLocalizedString(@"Cool",nil) forState:UIControlStateNormal];
                            
                            [votofavor.titleLabel setFont:[UIFont fontWithName:votofavor.titleLabel.font.familyName size:12]];
                            
                            votofavor.layer.cornerRadius = 15;
                            
                            [votofavor addTarget:self action:@selector(votar:) forControlEvents:UIControlEventTouchUpInside];
                            
                            [votofavor setTag:3];
                            
                            UIButton *votocontra;
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                            
                                votocontra = [[UIButton alloc] initWithFrame:CGRectMake(190, 150, 50, 30)];
                            }else{
                                votocontra = [[UIButton alloc] initWithFrame:CGRectMake(190, 250, 50, 30)];
                            }
                            
                            [votocontra setBackgroundColor:[UIColor colorWithRed:0.7 green:0 blue:0 alpha:1]];
                            
                            [votocontra setTitle:NSLocalizedString(@"Chafa", nil) forState:UIControlStateNormal];
                            
                            [votocontra.titleLabel setFont:[UIFont fontWithName:votocontra.titleLabel.font.familyName size:11]];
                            
                            votocontra.layer.cornerRadius = 15;
                            
                            [votocontra addTarget:self action:@selector(votar:) forControlEvents:UIControlEventTouchUpInside];
                            
                            [votocontra setTag:4];
                            
                            [vistaVotar addSubview:votofavor];
                            [vistaVotar addSubview:votocontra];
                            
                        }else if(![[[lugares objectForKey:@"voto"] objectForKey:@"voto"] isEqual:[NSNull null]] && [[[lugares objectForKey:@"voto"] objectForKey:@"voto"] intValue] == 1){
                            
                            UIView *votosfavor;
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                            
                                votosfavor = [[UIView alloc] initWithFrame:CGRectMake(90, 150, [[[[lugares objectForKey:@"voto"] objectForKey:@"votos"] objectForKey:@"votos"] intValue]*15000/100, 10)];
                            }else{
                                
                                votosfavor = [[UIView alloc] initWithFrame:CGRectMake(90, 250, [[[[lugares objectForKey:@"voto"] objectForKey:@"votos"] objectForKey:@"votos"] intValue]*15000/100, 10)];
                            }
                            
                            [votosfavor setBackgroundColor:[UIColor colorWithRed:0 green:0.9 blue:0 alpha:1.0]];
                            
                            [vistaVotar addSubview:votosfavor];
                            
                            UIView *votoscontra;
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                            
                                votoscontra = [[UIView alloc] initWithFrame:CGRectMake(votosfavor.frame.origin.x+votosfavor.frame.size.width, 150, 150-votosfavor.frame.size.width, 10)];
                            }else{
                                
                                votoscontra = [[UIView alloc] initWithFrame:CGRectMake(votosfavor.frame.origin.x+votosfavor.frame.size.width, 250, 150-votosfavor.frame.size.width, 10)];
                            }
                            
                            [votoscontra setBackgroundColor:[UIColor colorWithRed:0.7 green:0 blue:0 alpha:1]];
                            
                            [vistaVotar addSubview:votoscontra];
                            
                            UILabel *numVotos;
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                            
                                numVotos = [[UILabel alloc] initWithFrame:CGRectMake(0, 160, 340, 50)];
                            }else{
                                
                                numVotos = [[UILabel alloc] initWithFrame:CGRectMake(0, 260, 340, 50)];
                            }

                            if(![[[[lugares objectForKey:@"voto"] objectForKey:@"votos"] objectForKey:@"numeroDeVotos"] isEqual:[NSNull null]] && [[[[lugares objectForKey:@"voto"] objectForKey:@"votos"] objectForKey:@"numeroDeVotos"] intValue] != 0){
                                [numVotos setText:[NSString stringWithFormat:@"%d votos",[[[[lugares objectForKey:@"voto"] objectForKey:@"votos"] objectForKey:@"numeroDeVotos"] intValue]]];
                            }else{
                                
                                [numVotos setText:NSLocalizedString(@"0 votos",nil)];
                            }
                            
                            numVotos.textAlignment = NSTextAlignmentCenter;
                            
                           // [numVotos setTextColor:[UIColor colorWithWhite:0.5 alpha:1]];
                            [numVotos setTextColor:[UIColor whiteColor]];
                            
                            [numVotos setFont:[UIFont fontWithName:numVotos.font.familyName size:13]];
                            
                            [vistaVotar addSubview:numVotos];
                            
                        }
                        
                        UIButton *esconder = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 30, 30)];
                        
                        [esconder setBackgroundImage:[UIImage imageNamed:@"flechaabajo4.png"] forState:UIControlStateNormal];
                        
                        [esconder addTarget:self action:@selector(esconderVotoLugar) forControlEvents:UIControlEventTouchUpInside];
                        
                        [vistaVotar addSubview:comentariosLugar];
                        [vistaVotar addSubview:verComentarios];
                        [vistaVotar addSubview:comentar];
                        [vistaVotar addSubview:esconder];
                        
                        [self verificarVoto];
                    
                    });
                }
            }
        }];
    });

    [self getMasLugares];
    seBusca = NO;
    
    palabraABuscar = -1;
    
    already = 1;
    
    menorIdPost = 0;
    
    lugarAir = 0;
    
    nombreLugarAir.layer.cornerRadius = 15;
    
    enLugar = NO;
    
    locationManager = [[CLLocationManager alloc] init];
    
    [self cordenadas];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    arreglo = [NSMutableArray array];
    arregloDeFotos = [NSMutableDictionary dictionary];
    arregloLugares = [NSMutableArray array];
    arregloDeFotosDeLugares = [NSMutableDictionary dictionary];
    lugaresCheckinRapido = [NSMutableArray array];
    lugaresMuestra = [NSMutableArray array];
    
    [self getPosts];
    
    if(tablaOffset != 0){
        
        [tabla setContentOffset:CGPointMake(tabla.contentOffset.x, tablaOffset)];
    }
    
    UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStyleDone target:self action:nil];
    
    NSUserDefaults *datosUsuario = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/getnotificaciones.php"]];

    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&token=%@",[[datosUsuario objectForKey:@"identifier"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];

    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(data != NULL){
            
            NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            if(![respuesta isEqual:[NSNull null]] && [respuesta intValue] > 0){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIBarButtonItem *notificacionesBoton = [[UIBarButtonItem alloc] initWithTitle:respuesta style:UIBarButtonItemStylePlain target:self action:@selector(showNotificaciones:)];
                    
                    [notificacionesBoton setBackgroundImage:[UIImage imageNamed:@"fondoNotificaciones.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
                    
                    self.navigationItem.leftBarButtonItem = notificacionesBoton;
                });
            }
            
            [self getNuevasNotificaciones];
        }
    }];
    
    self.navigationItem.backBarButtonItem = atras;
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self getNuevasNotificaciones];
    
}

-(void)viewDidDisappear:(BOOL)animated{

    if(reproductor){
        
        [reproductor removeObserver:self forKeyPath:@"status"];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
        layer = nil;
        reproductor = nil;
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    BOOL regresa;
    
    if([identifier isEqualToString:@"aperfil"] || [identifier isEqualToString:@"aperfili"]){
    
        UITableViewCell *celda = (UITableViewCell *)[[sender superview] superview];
        
        NSInteger index = [[tabla indexPathForCell:celda] row];
        
        if([[defaults objectForKey:@"identifier"] isEqualToString:[[arreglo objectAtIndex:index] objectForKey:@"uID"]]){
            
            controladorTabControllerViewController *ida = (controladorTabControllerViewController *)self.tabBarController;
            
            [ida setSelectedIndex:2];
            
            regresa = NO;
            
        }else{
        
            regresa = YES;
        }
    }else{
    
        regresa = YES;
    }
    
    return regresa;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{


    if(![[arreglo objectAtIndex:0] isEqual:@"valido"]){

        if([segue.destinationViewController isKindOfClass:[ComentariosController class]]){
    
            ComentariosController *controlador = (ComentariosController *)segue.destinationViewController;
        
            controlador.posicionScroll = tabla.contentOffset.y;
        
            UITableViewCell *celda = (UITableViewCell *)[[sender superview] superview];
            
            controlador.foto = UIImageJPEGRepresentation([(UIButton *)[celda viewWithTag:1] imageForState:UIControlStateNormal], 1);
            controlador.nombrePersona = [[(UIButton *)[celda viewWithTag:2] titleLabel] text];
            controlador.textoDePublicacion = [(UILabel *)[celda viewWithTag:3] text];
            controlador.lugar = [[arreglo objectAtIndex:[[tabla indexPathForCell:celda] row]] objectForKey:@"lugar"];
            controlador.postID = [[[arreglo objectAtIndex:[tabla indexPathForCell:celda].row] objectForKey:@"aconID"] intValue];
            controlador.joinsDePublicacion = [[arreglo objectAtIndex:[[tabla indexPathForCell:celda] row]] objectForKey:@"numDePersonas"];
            controlador.joinFilled = [[[arreglo objectAtIndex:[[tabla indexPathForCell:celda] row]] objectForKey:@"finguer"] boolValue];
            
        
        }else if([segue.destinationViewController isKindOfClass:[Perfil class]]){
            
            
            UITableViewCell *celda = (UITableViewCell *)[[sender superview] superview];
            
            NSInteger index = [[tabla indexPathForCell:celda] row];
            
            if([[defaults objectForKey:@"identifier"] isEqualToString:[[arreglo objectAtIndex:index] objectForKey:@"uID"]]){

                controladorTabControllerViewController *ida = (controladorTabControllerViewController *)self.tabBarController;
                
                [ida setSelectedIndex:2];
                
            }else{

                Perfil *controlador = (Perfil *)segue.destinationViewController;
                
                controlador.stringNombre = [[(UIButton *)[celda viewWithTag:2] titleLabel] text];
                controlador.dataFoto = UIImageJPEGRepresentation([(UIButton *)[celda viewWithTag:1] imageForState:UIControlStateNormal], 1);
                
                controlador.amigoID = [[[arreglo objectAtIndex:index] objectForKey:@"uID"] intValue];
                
                controlador.publicacion = [[arreglo objectAtIndex:index] objectForKey:@"acontecimiento"];
            }
            

        }else if([segue.destinationViewController isKindOfClass:[lugarViewController class]]){
            
            
            lugarViewController *controlador = (lugarViewController *)segue.destinationViewController;
            
            if([sender isEqual:imagenMasVotado] || [sender isEqual:nombreMasVotado]){
            
                controlador.idLugar = [[[diccionarioMasVotado objectForKey:@"votados"] objectForKey:@"ID"] intValue];
                controlador.nombreLugar = [[diccionarioMasVotado objectForKey:@"votados"] objectForKey:@"lugar"];
            }else if([sender isEqual:imagenMasVisitado] || [sender isEqual:nombreMasVisitado]){
            
                controlador.idLugar = [[[diccionarioMasVisitado objectForKey:@"visitados"] objectForKey:@"ID"] intValue];
                controlador.nombreLugar = [[diccionarioMasVisitado objectForKey:@"visitados"] objectForKey:@"lugar"];
            }else{
                
                UITableViewCell *celda = (UITableViewCell *)[[sender superview] superview];
            
                NSInteger index = [[tabla indexPathForCell:celda] row];
            
                controlador.idLugar = [[[arreglo objectAtIndex:index] objectForKey:@"lugarID"] intValue];
            
                controlador.nombreLugar = [[(UIButton *)[celda viewWithTag:9] titleLabel] text];
            }
            
        }else if([segue.destinationViewController isKindOfClass:[enviarReportePublicacionViewController class]]){
        
            enviarReportePublicacionViewController *controlador = (enviarReportePublicacionViewController *)segue.destinationViewController;
        
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexMenuPublicacion inSection:0];
        
            UITableViewCell *celda = (UITableViewCell *)[tabla cellForRowAtIndexPath:indexPath];
                
            controlador.nombreUsuario = [[(UIButton *)[celda viewWithTag:2] titleLabel] text];
            
        }
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    __block UITableViewCell *celda;
    long index = 0;
    
    tabla.separatorStyle = UITableViewCellSeparatorStyleNone;

    if([tableView isEqual:tabla]){
        
        index = indexPath.row;

        if([arreglo count] > 0 && arreglo != NULL && ![[arreglo objectAtIndex:0] isEqual:@""] && ![[arreglo objectAtIndex:0] isEqual:@"valido"] && indexPath.row < [arreglo count]){
            
            celda = [tabla dequeueReusableCellWithIdentifier:@"celdaNotificaciones"];
            
            UIButton *nombre = (UIButton *)[celda viewWithTag:2];
            
            NSString *nombreYapellido = [NSString stringWithFormat:@"%@ %@",[[arreglo objectAtIndex:index] objectForKey:@"nombre"], [[arreglo objectAtIndex:index] objectForKey:@"apellido"]];
            
            [nombre setTitle:nombreYapellido forState:UIControlStateNormal];
            
            float numeroCaracteres = [[[arreglo objectAtIndex:index] objectForKey:@"acontesimiento"] length];
            
            if(numeroCaracteres <= 0){
                
                numeroCaracteres = 1;
            }else{
                numeroCaracteres = numeroCaracteres/43;

                numeroCaracteres = ceilf(numeroCaracteres);
            }
            
            
            UILabel *texto = (UILabel *)[celda viewWithTag:3];
            
            [texto setPreferredMaxLayoutWidth:285];

            if(![[[arreglo objectAtIndex:index] objectForKey:@"acontesimiento"] isEqualToString:@"(null)"]){
                
                NSData *data = [[[arreglo objectAtIndex:index] objectForKey:@"acontesimiento"] dataUsingEncoding:NSUTF8StringEncoding];
               
                NSString *publicacionFormateada = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                
                [texto setText:publicacionFormateada];
                
                for(NSLayoutConstraint *constraint in texto.constraints){
                
                    if([constraint.identifier isEqualToString:@"heightAcontesimiento"]){
                        
                        [constraint setConstant:28*numeroCaracteres];
                    }
                }
                
                [texto setFrame:CGRectMake(texto.frame.origin.x, texto.frame.origin.y, texto.frame.size.width, texto.frame.size.height+(28*numeroCaracteres))];
            }else{
                
                [texto setText:@""];
            }
            
            UIButton *fotoPerfil = (UIButton *)[celda viewWithTag:1];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[[arreglo objectAtIndex:index] objectForKey:@"uID"] intValue],[[arreglo objectAtIndex:index] objectForKey:@"foto"]]];
            
            fotoPerfil.layer.borderWidth = 0.0;
            fotoPerfil.layer.cornerRadius = 5;
            fotoPerfil.layer.masksToBounds = YES;
            
            [fotoPerfil sd_setImageWithURL:url forState:UIControlStateNormal];
            
            [fotoPerfil setTitle:@" " forState:UIControlStateNormal];
            
            NSInteger numComentarios = [[[arreglo objectAtIndex:index] objectForKey:@"comentarios"] integerValue];
            
            if(numComentarios == 0){
                
                UILabel *comentarios = (UILabel *)[celda viewWithTag:4];
                comentarios.text = @"";
            }else{
                
                
                UILabel *comentarios = (UILabel *)[celda viewWithTag:4];
                comentarios.text = [NSString stringWithFormat:@"%@",[[arreglo objectAtIndex:index] objectForKey:@"comentarios"]];
            }
            
            if(![[[arreglo objectAtIndex:index] objectForKey:@"lugarID"] isEqual:[NSNull null]] && [[[arreglo objectAtIndex:index] objectForKey:@"lugarID"] intValue] != 0){
                
                NSInteger numDePersonas = [[[arreglo objectAtIndex:index] objectForKey:@"numDePersonas"] integerValue];
                
                if(numDePersonas == 0){
                    
                    UILabel *personas = (UILabel *)[celda viewWithTag:5];
                    personas.text = @"";
                }else{
                    
                    UILabel *personas = (UILabel *)[celda viewWithTag:5];
                    personas.text = [NSString stringWithFormat:@"%@", [[arreglo objectAtIndex:index] objectForKey:@"numDePersonas"]];
                }
                
                UIButton *finguer = (UIButton *)[celda viewWithTag:6];
                NSInteger valorFinguer = [[[arreglo objectAtIndex:index] objectForKey:@"finguer"] integerValue];
                
                if(valorFinguer == 0){
                    
                    [finguer setImage:[UIImage imageNamed:@"joinT.png"] forState:UIControlStateNormal];
                }else if(valorFinguer == 1){
                    
                    [finguer setImage:[UIImage imageNamed:@"joinFilled.png"] forState:UIControlStateNormal];
                }
                
                [finguer setHidden:NO];
                
                if([[arreglo objectAtIndex:index] objectForKey:@"finguerOp"] != nil && [[[arreglo objectAtIndex:index] objectForKey:@"finguerOp"] intValue] == 1){
                    
                    [finguer addTarget:self action:@selector(join:) forControlEvents:UIControlEventTouchUpInside];
                }
            }
            
            double delta = [[NSDate date] timeIntervalSince1970] - [[[arreglo objectAtIndex:index] objectForKey:@"fechaUnix"] doubleValue];
            
            NSString *fechaUnix;

            if(delta < 1*60){
            
                if(delta == 1){
                
                    fechaUnix = NSLocalizedString(@"Hace 1 segundo",nil);
                }else{
                
                    int segundo = (int)delta;
                    
                    fechaUnix = [NSString stringWithFormat:NSLocalizedString(@"Hace %d segundos",nil),segundo];
                }
            }else if(delta < 2*60){
            
                fechaUnix = NSLocalizedString(@"Hace un minuto",nil);
            }else if(delta < 45*60){
            
                int minuto = (int)floorl(delta/60);
                
                fechaUnix = [NSString stringWithFormat:NSLocalizedString(@"Hace %d minutos",nil),minuto];
            }else if(delta < 7200){
                
                fechaUnix = NSLocalizedString(@"Hace una hora",nil);
            }else if(delta < 24*60*60){
            
                int hora = floorl(delta/3600);
                
                fechaUnix = [NSString stringWithFormat:NSLocalizedString(@"Hace %d horas", nil), hora];
            }else if(delta < 48*60*60){
            
                fechaUnix = NSLocalizedString(@"Ayer",nil);
            }else if(delta > 48*60*60){
                
                fechaUnix = [[arreglo objectAtIndex:index] objectForKey:@"fecha"];
            }
            
            UILabel *fecha = (UILabel*)[celda viewWithTag:7];
            [fecha setText:fechaUnix];
            
            
            NSString *lugar = [[arreglo objectAtIndex:index] objectForKey:@"lugar"];
            
            if([lugar isEqual:[NSNull null]]){
                
                UILabel *personas = (UILabel *)[celda viewWithTag:5];
                UIButton *finguer = (UIButton *)[celda viewWithTag:6];
                
                [personas setText:@""];
                [finguer setHidden:YES];
                
                UILabel *en = (UILabel *)[celda viewWithTag:8];
                [en setText:@""];
                UIButton *lugarPublicacion = (UIButton *)[celda viewWithTag:9];
                [lugarPublicacion setTitle:@"" forState:UIControlStateNormal];
                
            }else if(![lugar isEqual:[NSNull null]]){
                
                UILabel *en = (UILabel *)[celda viewWithTag:8];
                en.text = NSLocalizedString(@"En", nil);
                
                UIButton *lugarPublicacion = (UIButton *)[celda viewWithTag:9];
                [lugarPublicacion setTitle:lugar forState:UIControlStateNormal];
            }
            
        }else if([arreglo count] == 0 || [[arreglo objectAtIndex:0] isEqual:@"valido"]){

            /*celda = [tabla dequeueReusableCellWithIdentifier:@"celdaNoPublicaciones"];
            
            UILabel *num1 = (UILabel *)[celda viewWithTag:1];
            num1.layer.cornerRadius = 5;
            
            UILabel *num2 = (UILabel *)[celda viewWithTag:2];
            num2.layer.cornerRadius = 5;
            
            UILabel *num3 = (UILabel *)[celda viewWithTag:3];
            num3.layer.cornerRadius = 5;
            
            UILabel *num4 = (UILabel *)[celda viewWithTag:4];
            num4.layer.cornerRadius = 5;
            
            [celda setSelectionStyle:UITableViewCellSelectionStyleNone];*/
            
            //-------------

            if([lugaresMuestra count] > 0 && [[lugaresMuestra objectAtIndex:0] objectForKey:@"valido"] == nil){

                celda = [tableView dequeueReusableCellWithIdentifier:@"celdaLugarMuestra"];
                
                UIImageView *fotopublicacion = (UIImageView*)[celda viewWithTag:1];
                [fotopublicacion sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/%@",[[lugaresMuestra objectAtIndex:indexPath.row] objectForKey:@"usuario"],[[lugaresMuestra objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"]]]];
                [fotopublicacion.layer setBorderWidth:0.5];
                [fotopublicacion.layer setBorderColor:[[UIColor colorWithWhite:230/255.0 alpha:1] CGColor]];
                [fotopublicacion.layer setCornerRadius:10];
                [fotopublicacion.layer setMasksToBounds:YES];
                
                UILabel *nombre = (UILabel*)[celda viewWithTag:2];
                [nombre setText:[[lugaresMuestra objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
                [nombre.layer setCornerRadius:10];
                [nombre.layer setMasksToBounds:YES];
                
            }else{
            
                celda = [tableView dequeueReusableCellWithIdentifier:@"celdaCheckin"];
            }
            
        }else if(indexPath.row == [arreglo count]){

            celda = [tabla dequeueReusableCellWithIdentifier:@"ultimaCelda"];
            
            UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"C.png"]];
            
            logo.frame = CGRectMake(150, 10, 25, 25);
            
            [celda addSubview:logo];
            
            tabla.contentSize = [tabla sizeThatFits:CGSizeMake(CGRectGetWidth(tabla.bounds), CGFLOAT_MAX)];
            
            menorIdPost += 20;
            
            [self getPostsViejos];
        }
      
    }else if([tableView isEqual:tablaLugares]){
        
        if([arregloLugares count] > 0 && ![[arregloLugares objectAtIndex:0] isKindOfClass:[NSString class]]){
            
            tablaLugares.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            
            celda = [tablaLugares dequeueReusableCellWithIdentifier:@"lugarAEtiquetar"];
            
            
            if([arregloDeFotosDeLugares objectForKey:[[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"foto"]]){
                
                UIImageView *foto = (UIImageView *)[celda viewWithTag:1];
                foto.image = [arregloDeFotosDeLugares objectForKey:[[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"foto"]];
                
            }else{
                
                UIImageView *foto = (UIImageView*)[celda viewWithTag:1];
                
                [foto sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"ID"],[[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"foto"]]]];
              
            }
            
            UILabel *nombre = (UILabel *)[celda viewWithTag:2];
            nombre.text = [[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"nombre"];
            
            [celda setSelectionStyle:UITableViewCellSelectionStyleBlue];
            [celda setUserInteractionEnabled:YES];
        }else if([arregloLugares count] > 0 && [[arregloLugares objectAtIndex:0] isKindOfClass:[NSString class]] && [[arregloLugares objectAtIndex:0] isEqualToString:@"nohay"]){
            
            celda = [tablaLugares dequeueReusableCellWithIdentifier:@"nolugares"];
            tablaLugares.separatorStyle = UITableViewCellSeparatorStyleNone;
        }else{
        
            celda = [[UITableViewCell alloc] init];
            
            tablaLugares.separatorStyle = UITableViewCellSeparatorStyleNone;
            
            [celda setUserInteractionEnabled:NO];
        }
    }else if([tableView tag] == 1000){

        if(arregloComentarios == nil){
            
            celda = [tableView dequeueReusableCellWithIdentifier:@"noComentariosLugar"];
            
            if(celda == nil){
                
                celda = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"noComentariosLugar"];
                
                UILabel *noComentarios = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 200)];
                
                [noComentarios setFont:[UIFont systemFontOfSize:13]];
                
                [noComentarios setText:NSLocalizedString(@"Aún no hay comentarios",nil)];
                
                [noComentarios sizeToFit];
                
                [noComentarios setCenter:CGPointMake(130, celda.frame.size.height/2)];
                
                [celda addSubview:noComentarios];
            }
        }else{
        
            celda = [tableView dequeueReusableCellWithIdentifier:@"celdaComentario"];

            if(celda == nil){
                
                celda = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"celdaComentario"];
                
                UIImageView *foto = [[UIImageView alloc] initWithFrame:CGRectMake(13, 11, 30, 30)];
                
                [foto sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioID"] intValue],[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"foto"]]]];
                
                [foto setTag:1];
                
                UILabel *nombre = [[UILabel alloc] initWithFrame:CGRectMake(51, 11, tableView.frame.size.width-40, 30)];
                
                [nombre setText:[NSString stringWithFormat:@"%@ %@",[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioNombre"],[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioApellido"]]];
                
                [nombre setFont:[UIFont fontWithName:@"Verdana-Bold" size:11]];
                
                [nombre setTextColor:[UIColor colorWithRed:0 green:171.0/255.0 blue:207.0/255.0 alpha:1.0]];
                
                UILabel *publiacion = [[UILabel alloc] initWithFrame:CGRectMake(51, 51, tableView.frame.size.width-70, 30)];
                
                [publiacion setNumberOfLines:0];
                
                [publiacion setFont:[UIFont systemFontOfSize:14]];
                
                NSData *data = [[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensaje"] dataUsingEncoding:NSUTF8StringEncoding];
                
                NSString *publicacionFormateada;
                
                publicacionFormateada = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                
                if(publicacionFormateada == NULL){

                    publicacionFormateada = [[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensaje"];
                }
                
                [publiacion setText:publicacionFormateada];

                float numeroCaracteres = [publicacionFormateada length];
                
                if(numeroCaracteres == 0){
                    
                    numeroCaracteres = 0;
                }else{
                    
                    numeroCaracteres = numeroCaracteres/19;
                    
                    numeroCaracteres = floorf(numeroCaracteres);
                    
                    numeroCaracteres *= 10;
                }

                [publiacion setFrame:CGRectMake(publiacion.frame.origin.x, publiacion.frame.origin.y, publiacion.frame.size.width, 20+numeroCaracteres)];
                
                int topSeparador = 122;
                int topCorazones = 101;
                
                if(![[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]] && ![[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqualToString:@"null"]){
                    
                    UIImageView *fotoPublicacion = [[UIImageView alloc] initWithFrame:CGRectMake(25, 100, 180, 200)];
                    
                    fotoPublicacion.layer.cornerRadius = 5;
                    fotoPublicacion.layer.borderWidth = 0.5;
                    fotoPublicacion.layer.masksToBounds = YES;
                    fotoPublicacion.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4] CGColor];
                    fotoPublicacion.contentMode = UIViewContentModeScaleAspectFill;
                    fotoPublicacion.clipsToBounds = YES;
                    [fotoPublicacion setCenter:CGPointMake(celda.center.x-22, fotoPublicacion.center.y+numeroCaracteres-5)];
                    [fotoPublicacion setUserInteractionEnabled:YES];
                    [fotoPublicacion setTag:9];
                    
                    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(expandirImagen:)];
                    
                    [tap setNumberOfTapsRequired:1];
                    [fotoPublicacion addGestureRecognizer:tap];
                    
                    if([[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"esVideo"] intValue] == 0){
                        
                        [fotoPublicacion sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/%@",[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioID"],[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"]]]];
                        
                    }else if([[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"esVideo"] intValue] == 1){
                        
                        NSArray *nombreVideoA = [[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] componentsSeparatedByString:@"."];
                        
                        NSString *nombreVideo = [NSString stringWithFormat:@"%@.jpg",[nombreVideoA objectAtIndex:0]];
                        
                        NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/thumbnail/%@",[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioID"],nombreVideo]];

                        CALayer *playIconLayer = [[CALayer alloc] init];
                        
                        [playIconLayer setFrame:CGRectMake((fotoPublicacion.frame.size.width/2)-25,(fotoPublicacion.frame.size.height/2)-25, 50, 50)];
                        
                        [playIconLayer setContents:(id)[UIImage imageNamed:@"playicon.png"].CGImage];
                        
                        [fotoPublicacion.layer addSublayer:playIconLayer];
                        
                        [fotoPublicacion sd_setImageWithURL:videoURL];
                        
                    }
                    
                    topSeparador = 349;
                    topCorazones = 328;
                    
                    [celda addSubview:fotoPublicacion];
                }else{
                    
                    UIImageView *fotoPublicacion = (UIImageView*)[celda viewWithTag:9];
                    
                    if(![fotoPublicacion isEqual:[NSNull null]]){
                        
                        [fotoPublicacion removeFromSuperview];
                    }
                }
               
                UIButton *heart = [[UIButton alloc] initWithFrame:CGRectMake(80, topCorazones+numeroCaracteres, 12, 12)];

                UIButton *numDeAmor = [[UIButton alloc] initWithFrame:CGRectMake(94, topCorazones+numeroCaracteres, 49, 14)];
                
                [heart setTag:5];
                [numDeAmor setTag:6];
                
                [heart addTarget:self action:@selector(likeComentario:) forControlEvents:UIControlEventTouchUpInside];
                [numDeAmor addTarget:self action:@selector(likeComentario:) forControlEvents:UIControlEventTouchUpInside];
                
                if([[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"likeMio"] intValue] == 0){
                
                    [heart setBackgroundImage:[UIImage imageNamed:@"grayheart.png"] forState:UIControlStateNormal];
                    [numDeAmor.titleLabel setFont:[UIFont systemFontOfSize:15]];
                    [numDeAmor setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                }else{
                
                    [heart setBackgroundImage:[UIImage imageNamed:@"redheart.png"] forState:UIControlStateNormal];
                    [numDeAmor.titleLabel setFont:[UIFont systemFontOfSize:15]];
                    [numDeAmor setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                }

                if(![[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"likes"] isEqualToString:@"0"]){
                
                    [numDeAmor setTitle:[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"likes"] forState:UIControlStateNormal];
                }else{
                
                    [numDeAmor setTitle:@"" forState:UIControlStateNormal];
                }

                UIView *separador = [[UIView alloc] initWithFrame:CGRectMake(15, topSeparador+numeroCaracteres, celda.frame.size.width-15, 1)];

                [separador setBackgroundColor:[UIColor grayColor]];
                
                [celda addSubview:foto];
                [celda addSubview:nombre];
                [celda addSubview:publiacion];
                [celda addSubview:heart];
                [celda addSubview:numDeAmor];
                [celda addSubview:separador];
            }
        }
        
        [tableView setSeparatorColor:[UIColor clearColor]];
    }else if([tableView tag] == 102){
    
        celda = [[UITableViewCell alloc] init];

        if([[lugaresCheckinRapido objectAtIndex:0] objectForKey:@"cantidad"]){
        
            UILabel *nohaylugarestexto = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 100)];
            
            [nohaylugarestexto setText:@"No se encontraron lugares. ¡Agrega uno aquí!"];
            
            [nohaylugarestexto setFont:[UIFont fontWithName:[[nohaylugarestexto font] fontName] size:13]];
            
            [nohaylugarestexto sizeToFit];
            
            [celda addSubview:nohaylugarestexto];
        }else{
            UILabel *nombre = [[UILabel alloc] initWithFrame:CGRectMake(35, 7.5, 100, 100)];
            
            [nombre setText:[[lugaresCheckinRapido objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
            
            [nombre sizeToFit];
            
            UIImageView *foto = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 25, 25)];
            
            [foto sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[lugaresCheckinRapido objectAtIndex:indexPath.row] objectForKey:@"ID"],[[lugaresCheckinRapido objectAtIndex:indexPath.row] objectForKey:@"foto"]]]];
            
            [celda addSubview:nombre];
            [celda addSubview:foto];
        }
    }


    return celda;
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    UIPanGestureRecognizer *panRecongnizer = (UIPanGestureRecognizer *)gestureRecognizer;
    
    CGPoint velocidad = [panRecongnizer velocityInView:vistauno];
    
    CGPoint velocidadPantallaDos = [panRecongnizer velocityInView:vistados];

    if ((fabs(velocidad.y) > 0 || fabs(velocidad.y) < 0) || (fabs(velocidadPantallaDos.y) > 0 || fabs(velocidadPantallaDos.y) < 0)) {
        
        return NO;
    }else{

        return YES;
    }
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    NSInteger numero;
    
    if(tableView == tabla){

        if([arreglo count] > 0){
            if([[arreglo objectAtIndex:0] isEqual:@"valido"]){
                
                if([[lugaresMuestra objectAtIndex:0] objectForKey:@"valido"] == nil && [[lugaresMuestra objectAtIndex:0] objectForKey:@"localizacion"] == nil){
                    
                    numero = [lugaresMuestra count];
                }else{
                    numero = 1;
                }
            }else{
                
                numero = [arreglo count]+1;
            }
            
            /*if([[defaults objectForKey:@"verificado"] isEqualToString:@"0"]){
             
             numero++;
             }*/
        }else{

            numero = 1;
        }

    }else if([tableView tag] == 1000){
    
        if(arregloComentarios == nil){
            numero = 1;
        }else{
        
            numero = [arregloComentarios count];
        }
    }else if ([tableView tag] == 102){

        numero = [lugaresCheckinRapido count];
    }else{

        if([arregloLugares count] > 0){
            numero = [arregloLugares count];
        }else{
        
            numero = 0;
        }
    }

    return numero;
}

-(void)getPosts{
    
    NSUserDefaults *datosUsuario = [NSUserDefaults standardUserDefaults];
    NSString *identificador = [datosUsuario objectForKey:@"identifier"];
    
    NSMutableURLRequest *urlGetFriendsPosts = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getfriendsposts.php"]];
    
    [urlGetFriendsPosts setHTTPMethod:@"POST"];
    
    NSString *variables = [NSString stringWithFormat:@"id=%@&menor=%d&token=%@",identificador,menorIdPost,[defaults objectForKey:@"token"]];
    [urlGetFriendsPosts setHTTPBody:[variables dataUsingEncoding:NSUTF8StringEncoding]];

    NSError *error = nil;
    
    NSHTTPURLResponse *respuesta = nil;
    
    __block NSData *data;
    
    data = [NSURLConnection sendSynchronousRequest:urlGetFriendsPosts returningResponse:&respuesta error:&error];

    if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet){
    
        dispatch_async(dispatch_get_main_queue(), ^{
            UIApplication *app = [UIApplication sharedApplication];
            app.networkActivityIndicatorVisible = NO;
            
        });
        
      
    }else{
        
        NSArray *arr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &error];
        
        if([arr count] > 0){
           
            for(NSDictionary *intervalo in arr){
            
                if([intervalo isEqual:@"valido"]){
                    
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getlugaresmuestra.php"]];
                    
                    [request setHTTPMethod:@"POST"];
                    
                    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%d",[[defaults objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
                    
                    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                    
                    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                    
                    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                       
                        [arreglo addObject:intervalo];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [tabla reloadData];
                            already = 0;
                        });
                        
                        [self getUpdatePosts];
                        
                        
                        lugaresMuestra = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &error];

                        if([[lugaresMuestra objectAtIndex:0] objectForKey:@"valido"] == nil && [[lugaresMuestra objectAtIndex:0] objectForKey:@"localizacion"] == nil){
                            
                            [arreglo addObject:intervalo];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [tabla reloadData];
                                already = 0;
                            });
                            
                            [self getUpdatePosts];
                            
                        }else{
                            
                            [arreglo addObject:intervalo];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [tabla reloadData];
                                already = 0;
                            });
                            
                            [self getUpdatePosts];
                        }
                        
                        
                    }];
                    
                    [uploadTask resume];
                    
                }else{
                
                    [arreglo addObject:intervalo];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [tabla reloadData];
                        already = 0;
                    });
                   
                    [self getUpdatePosts];
                }
            }
        }
    }
    
}

-(void)getPostsViejos{
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    NSUserDefaults *datosUsuario = [NSUserDefaults standardUserDefaults];
    NSString *identificador = [datosUsuario objectForKey:@"identifier"];
    
    NSMutableURLRequest *urlGetFriendsPosts = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getfriendsposts.php"]];
    
    [urlGetFriendsPosts setHTTPMethod:@"POST"];
    NSString *variables = [NSString stringWithFormat:@"id=%@&menor=%d&token=%@",identificador,menorIdPost,[defaults objectForKey:@"token"]];
    [urlGetFriendsPosts setHTTPBody:[variables dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlGetFriendsPosts queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                app.networkActivityIndicatorVisible = NO;
            });
            
            
        }else{
            
            NSArray *arr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &error];
            
            @try{
                
                if(![arr isEqual:[NSNull null]] && [[[arr objectAtIndex:0] objectForKey:@"valido"] intValue] == 1){
                    
                    for(NSDictionary *intervalo in arr){
                        
                        if([intervalo isEqual:@"valido"]){
                            
                            [arreglo addObject:intervalo];
                        }else{
                            
                            [arreglo addObject:intervalo];
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [tabla reloadData];
                        already = 0;
                    });
                    
                }
            }@catch(NSException *e){}
        }
        
        app.networkActivityIndicatorVisible = NO;
    }];
    
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
  
    if(scrollview.contentOffset.x == 320.0){
    
        if(enLugar == NO && localizacionHab == YES){
            [campoCompartir becomeFirstResponder];
        }
        [notificaciones setText: NSLocalizedString(@"Comparte",nil)];
        if([[tutoriales objectForKey:@"iosEtiquetaLugar"] isEqualToString:@"0"]){
        
            [campoCompartir resignFirstResponder];
            
            if(![vistados viewWithTag:100]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIView *tutoetquetalugar = [[UIView alloc] initWithFrame:CGRectMake(0, campoCompartir.frame.origin.y, [UIScreen mainScreen].bounds.size.width, campoCompartir.frame.size.height)];
                    
                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cerrarEtiquetaLugarTuto:)];
                    
                    UIView *etiquetafondo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, campoCompartir.frame.size.height)];
                   
                    [etiquetafondo setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.81]];
                    [tutoetquetalugar setTag:100];
                    [tutoetquetalugar addGestureRecognizer:tapGesture];
                    
                    UILabel *tuto = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 280, 280)];
                    
                    [tuto setNumberOfLines:0];
                    [tuto setText:@"Para hacer checkin en un lugar escribe @nombre del lugar, también puedes escribir un mensaje."];
                    [tuto setTextColor:[UIColor colorWithWhite:1 alpha:0.81]];
                    [tuto sizeToFit];
                    [etiquetafondo addSubview:tuto];
                    [tutoetquetalugar addSubview:etiquetafondo];
                    
                    [vistados addSubview:tutoetquetalugar];
                });
            }
         
        }
    }else if(scrollView.contentOffset.x == 0.0){
    
        [campoCompartir resignFirstResponder];
        [notificaciones setText:NSLocalizedString(@"Actividad",nil)];
    }else if(scrollView.contentOffset.x == 640.0){
    
        [campoCompartir resignFirstResponder];
        [notificaciones setText:NSLocalizedString(@"Explora",nil)];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
   
    if(scrollview.contentOffset.x != 320.0){
    
        [campoCompartir resignFirstResponder];
    }
    
    if(dejoDeTocar == true && seActualiza == true){
    
    }
    
    if(tabla.contentOffset.y < 0 && tabla.contentOffset.y > -100.0){

        CGPoint centro = loadingPunto.center;
        loadingPunto.transform = CGAffineTransformMakeScale((tabla.contentOffset.y*-1)/2, (tabla.contentOffset.y*-1)/2);
        loadingPunto.center = centro;

    }else if(tabla.contentOffset.y <= -100.0){

        [tabla setContentOffset:CGPointMake(tabla.frame.origin.x, -100.0)];
    }
    
    dejoDeTocar = false;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    dejoDeTocar = true;
    offset = tabla.contentOffset.y;
    
    if(tabla.contentOffset.y < -80.0){
        
        seActualiza = true;
    }else{
        
        seActualiza = false;
    }
    
    if(tabla.contentOffset.y > -90.0 && tabla.contentOffset.y < 0){
    
        [tabla setContentOffset:CGPointMake(0, 0) animated:YES];
        //loadingPunto.center = CGPointMake(loadingPunto.center.x, -99);
    }
    
    if(seActualiza == true && already == 0){
        
        // se actualiza
        already = 1;
        
        
        loadingPunto.animationImages = [NSArray arrayWithObjects: [UIImage imageNamed:@"azul1"], [UIImage imageNamed:@"azul2"], [UIImage imageNamed:@"azul3"], [UIImage imageNamed:@"azul4"], [UIImage imageNamed:@"azul5"], [UIImage imageNamed:@"azul6"], [UIImage imageNamed:@"azul7"], [UIImage imageNamed:@"azul8"], [UIImage imageNamed:@"azul9"], [UIImage imageNamed:@"azul10"], [UIImage imageNamed:@"azul11"], [UIImage imageNamed:@"azul12"], [UIImage imageNamed:@"azul13"], [UIImage imageNamed:@"azul14"], [UIImage imageNamed:@"azul15"], [UIImage imageNamed:@"azul16"], nil];
        
        loadingPunto.animationDuration = 1.4;
        loadingPunto.animationRepeatCount = 0;
        [loadingPunto startAnimating];
        
        [tabla setContentInset:UIEdgeInsetsMake(offset*-1, 0, 0, 0)];
        NSUserDefaults *datosUsuario = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/update.php"]];
        
        [peticion setHTTPMethod:@"POST"];

        if([[arreglo objectAtIndex:0] isKindOfClass:[NSMutableDictionary class]]){
            
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&ultimoID=%d&token=%@",[[datosUsuario objectForKey:@"identifier"] intValue],[[[arreglo objectAtIndex:0] objectForKey:@"aconID"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        }else{
            
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&ultimoID=0&token=%@",[[datosUsuario objectForKey:@"identifier"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){}else{
                
                
                NSArray *llegadaUpdate = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                
                if([[[llegadaUpdate objectAtIndex:0] objectForKey:@"valido"] isEqual:[NSNumber numberWithInt:1]]){
                    
                    if(![[arreglo objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
                        if([[arreglo objectAtIndex:0] isEqualToString:@"valido"]){
                            
                            [arreglo removeAllObjects];
                        }
                    }
                    
                    int numeroDeCelda = 0;
                    
                    for(NSDictionary *intervalo in llegadaUpdate){
                        
                        if([[intervalo objectForKey:@"valido"] isEqual:[NSNumber numberWithInt:1]]){
                            
                            [arreglo insertObject:intervalo atIndex:numeroDeCelda];
                            
                            numeroDeCelda++;
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [tabla reloadData];
                    });
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [loadingPunto stopAnimating];
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    [tabla setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
                }];
            });
        }];
    
        already = 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float size;

    if(tableView == tabla){

        if([arreglo count] > 0){
            
            if([arreglo count] > 0 && indexPath.row == [arreglo count] && ![[arreglo objectAtIndex:0] isEqual:@"valido"]){

                size = 49.0;
            }else if([arreglo count] > 0 && indexPath.row < [arreglo count] && ![[arreglo objectAtIndex:0] isEqual:@"valido"]){

                float numeroCaracteres = [[[arreglo objectAtIndex:indexPath.row] objectForKey:@"acontesimiento"] length];
                
                if(numeroCaracteres == 0){
                    
                    numeroCaracteres = 35;
                }else{
                    
                    numeroCaracteres = numeroCaracteres/33;
                    
                    numeroCaracteres = ceilf(numeroCaracteres);
                    
                    numeroCaracteres *= 20;
                }
                
                size = 178.0+numeroCaracteres;
                
            }else if([[arreglo objectAtIndex:0] isEqual:@"valido"]){

                /*if([[UIScreen mainScreen] bounds].size.height >= 560){
                 return 422.0;
                 }else{
                 
                 return 380.0;
                 }*/
                
                if([[lugaresMuestra objectAtIndex:0] objectForKey:@"valido"] == nil && [[lugaresMuestra objectAtIndex:0] objectForKey:@"localizacion"] == nil){

                    size = 132;
                }else{

                    size = tabla.frame.size.height;
                }
                
            }
        }else{

            size = 100;
        }
        
    }else if([tableView tag] == 1000){
    
        float numeroCaracteres = 0;
        
        if(arregloComentarios == nil){
            return 45.0;
        }else if(![[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]] && ![[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqualToString:@"null"]){
            
            numeroCaracteres = [[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensaje"] length];
            
            if(numeroCaracteres == 0){
                
                numeroCaracteres = 0;
            }else{
                
                numeroCaracteres = numeroCaracteres/19;
                
                numeroCaracteres = floorf(numeroCaracteres);
                
                numeroCaracteres *= 15;
            }
            
            size = 350.0+numeroCaracteres;
        }else if([tableView tag] == 102){
        
            size = 150;
        }else{
            
            numeroCaracteres = [[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensaje"] length];
            
            if(numeroCaracteres == 0){
                
                numeroCaracteres = 0;
            }else{

                numeroCaracteres = numeroCaracteres/19;

                numeroCaracteres = floorf(numeroCaracteres);

                numeroCaracteres *= 15;
            }

            size = 123.0+numeroCaracteres;
        }
    }else{
    
        return 45.0;
    }

    return size;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([tableView isEqual:tablaLugares]){
        
        if([[arregloLugares objectAtIndex:0] isKindOfClass:[NSString class]]){
        
            [self showAgregarLugar:nil];
            
        }else{
            [scrollview setScrollEnabled:YES];
            
            nombreLugarAir.userInteractionEnabled = YES;
            
            nombreLugarAir.alpha = 1.0;
            
            [nombreLugarAir setTitle:[[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"nombre"] forState:UIControlStateNormal];
            
            lugarAir = [[[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue];
            
            if(![[arregloDeFotosDeLugares objectForKey:[[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"foto"]] isEqual:[NSNull null]]){
                
                if([[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"foto"] && [[[[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"foto"] componentsSeparatedByString:@"."][1] isEqualToString:@".jpg"]){
                    
                    fotolugarair = UIImageJPEGRepresentation([arregloDeFotosDeLugares objectForKey:[[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"foto"]], 1);
                    
                }else if([[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"foto"] && [[[[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"foto"] componentsSeparatedByString:@"."][1] isEqualToString:@".png"]){
                    
                    fotolugarair = UIImagePNGRepresentation([arregloDeFotosDeLugares objectForKey:[[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"foto"]]);
                }else if([[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"foto"]){
                    
                    fotolugarair = UIImagePNGRepresentation([arregloDeFotosDeLugares objectForKey:[[arregloLugares objectAtIndex:indexPath.row] objectForKey:@"foto"]]);
                }
            }else{
                
                fotolugarair = UIImagePNGRepresentation([UIImage imageNamed:@"tierra.png"]);
            }
            
            [arregloLugares removeAllObjects];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [tablaLugares reloadData];
            });
            
            [UIView animateWithDuration:0.4 animations:^{
                
                campoCompartir.frame = CGRectMake(campoCompartir.frame.origin.x, 3, campoCompartir.frame.size.width, 153);
                self.navigationController.view.frame = CGRectMake(self.navigationController.view.frame.origin.x, 0, self.navigationController.view.frame.size.width, self.navigationController.view.frame.size.height);
            }];
            
            campoCompartir.text = [[campoCompartir text] substringToIndex:palabraABuscar];
            
            seBusca = NO;
            
        }
    }else if([tableView tag] == 1000){
    
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ResponderMensajeViewController *responderMensaje = [storyboard instantiateViewControllerWithIdentifier:@"responderMensaje"];
        
        UITableViewCell *celda = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        UIImageView *fotoImagenView = (UIImageView*)[celda viewWithTag:1];
        
        responderMensaje.nombre = [NSString stringWithFormat:@"%@ %@",[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioNombre"],[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioApellido"]];
        responderMensaje.fotoUsuario = UIImagePNGRepresentation([fotoImagenView image]);
        responderMensaje.publicacion = [[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensaje"];
        responderMensaje.fecha = [[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"fecha"];
        responderMensaje.numDeAmor = [NSString stringWithFormat:@"%@",[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"likes"] ];
        responderMensaje.datosPublicacion = [arregloComentarios objectAtIndex:indexPath.row];
        responderMensaje.esVideo = [[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"esVideo"] intValue];
        
        if(![[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqual:[NSNull null]] && ![[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"] isEqualToString:@"(null)"]){

            if([[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"esVideo"] intValue] == 0){
                responderMensaje.dataImagenComentario = UIImageJPEGRepresentation([(UIImageView*)[celda viewWithTag:9] image], 1.0);
            }else if([[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"esVideo"] intValue] == 1){

                if(urlVideoApasar == nil){
                    responderMensaje.urlVideo = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/%@",[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensajeUsuarioID"],[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"fotoPublicacion"]]];
                }else{
                
                    responderMensaje.urlVideo = urlVideoApasar;
                }
            }
        }
        
        [popup dismiss:YES];
        
        [self.navigationController pushViewController:responderMensaje animated:YES];
    }else if([tableView tag] == 102){
    
        if([[lugaresCheckinRapido objectAtIndex:0] objectForKey:@"cantidad"]){
            
            [self showAgregarLugar:nil];
        
        }else{
            
            UIView *lugaresVista = [vistauno viewWithTag:101];
            
            [[lugaresVista viewWithTag:102] removeFromSuperview];
            [vistauno addSubview:[lugaresVista viewWithTag:1001]];
            
            UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake([lugaresVista viewWithTag:103].frame.size.width/2, [lugaresVista viewWithTag:103].frame.size.height/2, 25, 25)];
            [activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
            [activity startAnimating];
            
            [lugaresVista addSubview:activity];
            
            lugarAir = [[[lugaresCheckinRapido objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue];

            UIApplication *app = [UIApplication sharedApplication];
            app.networkActivityIndicatorVisible = YES;
            
            NSString *palabra = NULL;
            
            NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/publicaracontesimiento.php"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1.0];
            
            [peticion setHTTPMethod:@"POST"];
            
            [peticion setHTTPBody:[[NSString stringWithFormat:@"word=%@&lugar=%d&usuario=%d&token=%@",palabra,lugarAir, [[defaults objectForKey:@"identifier"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            [peticion setTimeoutInterval:3];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
                if(error.code == NSURLErrorTimedOut){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIAlertController *error = [UIAlertController alertControllerWithTitle:nil message:@"Estamos teniendo un problema, por favor inténtalo nuevamente" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *aceptar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Muy bien", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                            
                            if(lugaresVista){
                                
                                [lugaresVista removeFromSuperview];
                            }
                        }];
                        
                        [error addAction:aceptar];
                        
                        [self presentViewController:error animated:YES completion:nil];
                        app.networkActivityIndicatorVisible = NO;
                    });
                    
                }else if(error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIAlertController *error = [UIAlertController alertControllerWithTitle:@"No estás conectado a Internet" message:@"Por favor, verifica tu conexión a Internet" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *aceptar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Muy bien", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                            
                            if(lugaresVista){
                                [lugaresVista removeFromSuperview];
                            }
                        }];
                        
                        [error addAction:aceptar];
                        
                        [self presentViewController:error animated:YES completion:nil];
                        app.networkActivityIndicatorVisible = NO;
                    });
                }else{
                    
                    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getdatos.php"]];
                    
                    [peticion setHTTPMethod:@"POST"];
                    [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&recurso=2&token=%@",miID,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                    
                    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                        
                        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                            
                            NSLog(@"error");
                        }else{
                            
                            lugares = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                            
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                
                                if([[defaults objectForKey:@"publicarFacebook"] intValue] == 1){
                                    
                                    NSString *foto = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                    
                                    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                                    content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugar.php?id=%d",lugarAir]];
                                    content.contentTitle = [[lugaresCheckinRapido objectAtIndex:indexPath.row] objectForKey:@"lugar"];
                                    content.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%d/%@",lugarAir,foto]];
                                    
                                    [FBSDKShareAPI shareWithContent:content delegate:nil];
                                    
                                    
                                }
                                
                                [vistadecarga setFrame:CGRectMake(vistauno.frame.size.width, vistauno.frame.origin.y, vistadecarga.frame.size.width, vistadecarga.frame.size.height)];
                                
                                [vistadecarga setHidden:NO];
                                [campoCompartir setEditable:NO];
                                enLugar = YES;
                                
                                fechaPublicacionActual = [[NSDate date] timeIntervalSince1970];
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    timerAunAhiBoton = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(checarSiEstaEnLugar:) userInfo:@(fechaPublicacionActual) repeats:YES];
                                    
                                    [timerAunAhiBoton fire];
                                });
                                
                                [campoCompartir setEditable:NO];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [activityEstaLugar removeFromSuperview];
                                    [vistadecarga setBackgroundColor:[UIColor colorWithRed:22/255.0 green:179/255.0 blue:173/255.0 alpha:1.0]];
                                    
                                    UILabel *estasen = [[UILabel alloc] init];
                                    
                                    [estasen setText:NSLocalizedString(@"Ahora estas en:", nil)];
                                    
                                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                                        
                                        [estasen setFrame:CGRectMake(50, 10, estasen.frame.size.width, estasen.frame.size.height)];
                                    }else{
                                        [estasen setFrame:CGRectMake(50, 30, estasen.frame.size.width, estasen.frame.size.height)];
                                    }
                                    
                                    [estasen setTextColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0]];
                                    
                                    [estasen sizeToFit];
                                    
                                    [vistadecarga addSubview:estasen];
                                    
                                    UIActivityIndicatorView *cargaFotoLugar = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(135, 90, 50, 50)];
                                    
                                    [cargaFotoLugar startAnimating];
                                    
                                    [vistadecarga addSubview:cargaFotoLugar];
                                    
                                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                                        
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            
                                            UIImageView *imageFotoLugar = [[UIImageView alloc] initWithImage:[UIImage imageWithData:fotolugarair]];
                                            
                                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                                                
                                                [imageFotoLugar setFrame:CGRectMake(110, 50, 100, 100)];
                                            }else{
                                                [imageFotoLugar setFrame:CGRectMake(110, 90, 100, 100)];
                                            }
                                            
                                            [imageFotoLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%d/%@",lugarAir,[[lugares objectForKey:@"lugar"] objectForKey:@"foto"]]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                                
                                                [cargaFotoLugar stopAnimating];
                                                [cargaFotoLugar removeFromSuperview];
                                            }];
                                            
                                            [vistadecarga addSubview:imageFotoLugar];
                                            
                                        });
                                    });
                                    
                                    UIButton *lugar = [[UIButton alloc] init];
                                    
                                    [lugar setTitleColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0] forState:UIControlStateNormal];
                                    
                                    [lugar setTitle:[[lugaresCheckinRapido objectAtIndex:indexPath.row] objectForKey:@"lugar"] forState:UIControlStateNormal];
                                    
                                    [lugar.titleLabel setFont:[UIFont fontWithName:lugar.titleLabel.font.familyName size:20]];
                                    
                                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                                        
                                        [lugar setFrame:CGRectMake(0, 170, 320, lugar.frame.size.height)];
                                    }else{
                                        [lugar setFrame:CGRectMake(0, 210, 320, lugar.frame.size.height)];
                                    }
                                    
                                    [lugar setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
                                    
                                    [lugar addTarget:self action:@selector(irALugarYaAhi:) forControlEvents:UIControlEventTouchUpInside];
                                    
                                    [vistadecarga addSubview:lugar];
                                    
                                    UIButton *votar = [[UIButton alloc] init];
                                    
                                    [votar setTitle:NSLocalizedString(@"¡Votar!", nil) forState:UIControlStateNormal];
                                    
                                    [votar.titleLabel setFont:[UIFont fontWithName:votar.titleLabel.font.familyName size:13.0]];
                                    
                                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                                        [votar setFrame:CGRectMake(110, 220, 100, 30)];
                                    }else{
                                        [votar setFrame:CGRectMake(110, 300, 100, 30)];
                                    }
                                    
                                    [votar setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                                    
                                    [votar setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
                                    
                                    [votar.layer setCornerRadius:15];
                                    
                                    [votar addTarget:self action:@selector(votarLugar) forControlEvents:UIControlEventTouchUpInside];
                                    
                                    [vistadecarga addSubview:votar];
                                    
                                    UIButton *aunAhi = [[UIButton alloc] init];
                                    
                                    [aunAhi setTitle:NSLocalizedString(@"Aún estoy ahí", nil) forState:UIControlStateNormal];
                                    
                                    [aunAhi.titleLabel setFont:[UIFont fontWithName:aunAhi.titleLabel.font.familyName size:13.0]];
                                    
                                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                                        
                                        [aunAhi setFrame:CGRectMake(85, 270, 150, 30)];
                                    }else{
                                        [aunAhi setFrame:CGRectMake(85, 350, 150, 30)];
                                    }
                                    
                                    [aunAhi setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:1.0]];
                                    
                                    [aunAhi addTarget:self action:@selector(aunAhi:) forControlEvents:UIControlEventTouchUpInside];
                                    
                                    
                                    [aunAhi setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
                                    
                                    [aunAhi.layer setCornerRadius:15];
                                    
                                    [aunAhi setTag:10];
                                    
                                    [vistadecarga addSubview:aunAhi];
                                    
                                    UIButton *yano = [[UIButton alloc] init];
                                    
                                    [yano setTitle:NSLocalizedString(@"Ya no estoy en ese lugar", nil) forState:UIControlStateNormal];
                                    
                                    [yano.titleLabel setFont:[UIFont fontWithName:yano.titleLabel.font.familyName size:13.0]];
                                    
                                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                                        
                                        [yano setFrame:CGRectMake(60, 320, 200, 30)];
                                    }else{
                                        [yano setFrame:CGRectMake(60, 400, 200, 30)];
                                    }
                                    [yano setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                                    
                                    [yano setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
                                    
                                    [yano.layer setCornerRadius:15];
                                    
                                    [yano addTarget:self action:@selector(noAhi:) forControlEvents:UIControlEventTouchUpInside];
                                    
                                    [vistadecarga addSubview:yano];
                                    
                                    vistaVotar = [[UIView alloc] initWithFrame:CGRectMake(0, 568, 320, 568)];
                                    
                                    [vistaVotar setBackgroundColor:[UIColor colorWithRed:22/255.0 green:179/255.0 blue:173/255.0 alpha:1]];
                                    
                                    [self.view addSubview:vistaVotar];
                                    
                                    
                                    UITextField *comentariosLugar = [[UITextField alloc] initWithFrame:CGRectMake(22, 418, 237, 30)];
                                    
                                    [comentariosLugar setDelegate:self];
                                    
                                    [comentariosLugar setTag:1];
                                    
                                    [comentariosLugar setPlaceholder:NSLocalizedString(@"Envía un comentario", nil)];
                                    
                                    [comentariosLugar setBackgroundColor:[UIColor whiteColor]];
                                    
                                    comentariosLugar.layer.cornerRadius = 10;
                                    
                                    [comentariosLugar addTarget:self action:@selector(textoCambio:) forControlEvents:UIControlEventEditingChanged];
                                    
                                    
                                    UIButton *comentar = [[UIButton alloc] initWithFrame:CGRectMake(262, 418, 45, 30)];
                                    
                                    [comentar setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                                    
                                    [comentar setTitle:NSLocalizedString(@"Enviar",nil) forState:UIControlStateNormal];
                                    
                                    [comentar.titleLabel setFont:[UIFont fontWithName:comentar.titleLabel.font.familyName size:13]];
                                    
                                    [comentar addTarget:self action:@selector(enviarMensajeLugar:) forControlEvents:UIControlEventTouchUpInside];
                                    
                                    comentar.layer.cornerRadius = 15;
                                    
                                    [comentar setTag:2];
                                    
                                    [comentar setTitle:@"" forState:UIControlStateNormal];
                                    [comentar setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                                    [comentar setFrame:CGRectMake(comentar.frame.origin.x, comentar.frame.origin.y, 30, comentar.frame.size.height)];
                                    comentar.layer.cornerRadius = 5;
                                    
                                    UIButton *verComentarios = [[UIButton alloc] initWithFrame:CGRectMake(65, 200, 200, 30)];
                                    
                                    [verComentarios setCenter:CGPointMake(self.vistaVotar.center.x, verComentarios.center.y)];
                                    
                                    [verComentarios setBackgroundColor:[UIColor colorWithRed:0 green:111.0/255.0 blue:147.0/255.0 alpha:1]];
                                    
                                    [verComentarios.titleLabel setFont:[UIFont fontWithName:comentar.titleLabel.font.familyName size:13]];
                                    
                                    [verComentarios setTitle:NSLocalizedString(@"Ver todos los comentarios", nil) forState:UIControlStateNormal];
                                    
                                    verComentarios.layer.cornerRadius = 15;
                                    
                                    [verComentarios setTag:20];
                                    
                                    [verComentarios addTarget:self action:@selector(mostrarTodosComentarios:) forControlEvents:UIControlEventTouchUpInside];
                                    
                                    UIButton *votofavor;
                                    
                                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                                        
                                        votofavor = [[UIButton alloc] initWithFrame:CGRectMake(90, 210, 50, 30)];
                                    }else{
                                        votofavor = [[UIButton alloc] initWithFrame:CGRectMake(90, 250, 50, 30)];
                                    }
                                    
                                    [votofavor setBackgroundColor:[UIColor colorWithRed:0 green:0.9 blue:0 alpha:1.0]];
                                    
                                    [votofavor setTitle:NSLocalizedString(@"Cool",nil) forState:UIControlStateNormal];
                                    
                                    [votofavor.titleLabel setFont:[UIFont fontWithName:votofavor.titleLabel.font.familyName size:12]];
                                    
                                    votofavor.layer.cornerRadius = 15;
                                    
                                    [votofavor addTarget:self action:@selector(votar:) forControlEvents:UIControlEventTouchUpInside];
                                    
                                    [votofavor setTag:3];
                                    
                                    UIButton *votocontra;
                                    
                                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                                        
                                        votocontra = [[UIButton alloc] initWithFrame:CGRectMake(190, 210, 50, 30)];
                                    }else{
                                        votocontra = [[UIButton alloc] initWithFrame:CGRectMake(190, 250, 50, 30)];
                                    }
                                    
                                    [votocontra setBackgroundColor:[UIColor colorWithRed:0.7 green:0 blue:0 alpha:1]];
                                    
                                    [votocontra setTitle:NSLocalizedString(@"Chafa",nil) forState:UIControlStateNormal];
                                    
                                    [votocontra.titleLabel setFont:[UIFont fontWithName:votocontra.titleLabel.font.familyName size:11]];
                                    
                                    votocontra.layer.cornerRadius = 15;
                                    
                                    [votocontra addTarget:self action:@selector(votar:) forControlEvents:UIControlEventTouchUpInside];
                                    
                                    [votocontra setTag:4];
                                    
                                    [vistaVotar addSubview:votofavor];
                                    [vistaVotar addSubview:votocontra];
                                    
                                    UIButton *esconder = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 30, 30)];
                                    
                                    [esconder setBackgroundImage:[UIImage imageNamed:@"flechaabajo4.png"] forState:UIControlStateNormal];
                                    
                                    [esconder addTarget:self action:@selector(esconderVotoLugar) forControlEvents:UIControlEventTouchUpInside];
                                    
                                    [vistaVotar addSubview:comentariosLugar];
                                    [vistaVotar addSubview:verComentarios];
                                    [vistaVotar addSubview:comentar];
                                    [vistaVotar addSubview:esconder];
                                    
                                });
                                
                                
                                app.networkActivityIndicatorVisible = NO;
                                notificaciones.text = NSLocalizedString(@"Comparte",nil);
                                [scrollview setContentOffset:CGPointMake(320, scrollview.contentOffset.y) animated:YES];
                                if(lugaresVista){
                                    
                                    [lugaresVista removeFromSuperview];
                                }
                                [[vistauno viewWithTag:1001] removeFromSuperview];
                            });
                        }
                        
                    }];
                }
            }];
        }
    }
}

#pragma mark - action sheet

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{

    if(![[arreglo objectAtIndex:0] isEqual:@"valido"] && buttonIndex == 0){
            
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexMenuPublicacion inSection:0];
        
        UITableViewCell *celda = (UITableViewCell *)[tabla cellForRowAtIndexPath:indexPath];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIImageView *imagen = (UIImageView *)[celda viewWithTag:1];
        
        enviarReportePublicacionViewController *enviaReporte = [storyboard instantiateViewControllerWithIdentifier:@"enviarReporte"];
        
        
        enviaReporte.dataFoto = UIImageJPEGRepresentation([(UIButton *)[celda viewWithTag:1] backgroundImageForState:UIControlStateNormal], 1);
        
        
        enviaReporte.nombreUsuario = [[(UIButton *)[celda viewWithTag:2] titleLabel] text];
        
        enviaReporte.publicacion = [[arreglo objectAtIndex:indexMenuPublicacion] objectForKey:@"acontesimiento"];
        
        enviaReporte.nombreLugar = [[arreglo objectAtIndex:indexMenuPublicacion] objectForKey:@"lugar"];
        
        if([[[[arreglo objectAtIndex:indexPath.row] objectForKey:@"foto"] componentsSeparatedByString:@"."][1] isEqualToString:@".jpg"]){
            
            enviaReporte.dataFoto = UIImageJPEGRepresentation(imagen.image, 1);
        }else if([[[[arreglo objectAtIndex:indexPath.row] objectForKey:@"foto"] componentsSeparatedByString:@"."][1] isEqualToString:@".png"]){
            
            enviaReporte.dataFoto = UIImagePNGRepresentation(imagen.image);
        }else{
            
            enviaReporte.dataFoto = UIImagePNGRepresentation(imagen.image);
        }
        
        enviaReporte.userID = [[[arreglo objectAtIndex:indexPath.row] objectForKey:@"uID"] intValue];
        enviaReporte.fotoUsuario = [[arreglo objectAtIndex:indexPath.row] objectForKey:@"foto"];
    
        [self presentViewController:enviaReporte animated:YES completion:nil];
    }
}

#pragma mark - hechos por mi

-(void)spin{
    
    loadingPunto.animationImages = [NSArray arrayWithObjects: [UIImage imageNamed:@"azul1"], [UIImage imageNamed:@"azul2"], [UIImage imageNamed:@"azul3"], [UIImage imageNamed:@"azul4"], [UIImage imageNamed:@"azul5"], [UIImage imageNamed:@"azul6"], [UIImage imageNamed:@"azul7"], [UIImage imageNamed:@"azul8"], [UIImage imageNamed:@"azul9"], [UIImage imageNamed:@"azul10"], [UIImage imageNamed:@"azul11"], [UIImage imageNamed:@"azul12"], [UIImage imageNamed:@"azul13"], [UIImage imageNamed:@"azul14"], [UIImage imageNamed:@"azul15"], [UIImage imageNamed:@"azul16"], nil];
    
    loadingPunto.animationDuration = 1.4;
    loadingPunto.animationRepeatCount = 0;
    [loadingPunto startAnimating];
    


}

-(IBAction)opcionesPublicacion:(id)sender{

    
    UITableViewCell *celda = (UITableViewCell *)[[sender superview] superview];
    
    indexMenuPublicacion = [[tabla indexPathForCell:celda] row];

    UIAlertController *menu = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *reportar;
    
    if([[[arreglo objectAtIndex:indexMenuPublicacion] objectForKey:@"uID"] intValue] == [[defaults objectForKey:@"identifier"] intValue]){
    
        reportar = [UIAlertAction actionWithTitle:@"Eliminar publicacion" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        
            UIAlertController *confirmacion = [UIAlertController alertControllerWithTitle:@"¿Estás seguro?" message:@"Si eliminas esta publicación no podrás recuperarla." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *aceptar = [UIAlertAction actionWithTitle:@"Aceptar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
                NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/eliminarpublicacion.php"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1.0];
                
                [peticion setHTTPMethod:@"POST"];
                [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%@&u=%@&token=%@",[[arreglo objectAtIndex:indexMenuPublicacion] objectForKey:@"aconID"],[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
 
                NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                
                [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                    
                    if(error.code == NSURLErrorTimedOut){}else if(error.code == NSURLErrorNotConnectedToInternet){
                        
                    }else{
                        
                        dispatch_async(dispatch_get_main_queue(), ^{

                            [arreglo removeObjectAtIndex:indexMenuPublicacion];
                            [tabla deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexMenuPublicacion inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
                        });
                    }
                }];
            }];
            
            UIAlertAction *cancelar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancelar", nil) style:UIAlertActionStyleCancel handler:nil];
            
            [confirmacion addAction:aceptar];
            [confirmacion addAction:cancelar];
            
            [self presentViewController:confirmacion animated:YES completion:nil];
        }];
    }else{
        
        reportar = [UIAlertAction actionWithTitle:@"Reportar publicación" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
            
            if(![[arreglo objectAtIndex:0] isEqual:@"valido"]){
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexMenuPublicacion inSection:0];
                
                UITableViewCell *celda = (UITableViewCell *)[tabla cellForRowAtIndexPath:indexPath];
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                UIImageView *imagen = (UIImageView *)[celda viewWithTag:1];
                
                enviarReportePublicacionViewController *enviaReporte = [storyboard instantiateViewControllerWithIdentifier:@"enviarReporte"];
                
                
                enviaReporte.dataFoto = UIImageJPEGRepresentation([(UIButton *)[celda viewWithTag:1] backgroundImageForState:UIControlStateNormal], 1);
                
                
                enviaReporte.nombreUsuario = [[(UIButton *)[celda viewWithTag:2] titleLabel] text];
                
                enviaReporte.publicacion = [[arreglo objectAtIndex:indexMenuPublicacion] objectForKey:@"acontesimiento"];
                
                enviaReporte.nombreLugar = [[arreglo objectAtIndex:indexMenuPublicacion] objectForKey:@"lugar"];
                
                if([[[[arreglo objectAtIndex:indexPath.row] objectForKey:@"foto"] componentsSeparatedByString:@"."][1] isEqualToString:@".jpg"]){
                    
                    enviaReporte.dataFoto = UIImageJPEGRepresentation(imagen.image, 1);
                }else if([[[[arreglo objectAtIndex:indexPath.row] objectForKey:@"foto"] componentsSeparatedByString:@"."][1] isEqualToString:@".png"]){
                    
                    enviaReporte.dataFoto = UIImagePNGRepresentation(imagen.image);
                }else{
                    
                    enviaReporte.dataFoto = UIImagePNGRepresentation(imagen.image);
                }
                
                enviaReporte.userID = [[[arreglo objectAtIndex:indexPath.row] objectForKey:@"uID"] intValue];
                enviaReporte.aconID = [[[arreglo objectAtIndex:indexPath.row] objectForKey:@"aconID"] intValue];
                enviaReporte.fotoUsuario = [[arreglo objectAtIndex:indexPath.row] objectForKey:@"foto"];
                
                [self presentViewController:enviaReporte animated:YES completion:nil];
            }
            
        }];
        
    }
    
    
    UIAlertAction *cancelar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancelar",nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
    }];
    
    [menu addAction:reportar];
    [menu addAction:cancelar];
    
    [self presentViewController:menu animated:YES completion:nil];
    
}

-(void)getMasLugares{
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getdatoslugaresmas.php"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1.0];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%@&token=%@",[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

        if(error.code == NSURLErrorTimedOut || data == NULL){
        
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self getMasLugares];
            });
        }else if(error.code == NSURLErrorNotConnectedToInternet){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self getMasLugares];
            });
        }else{
            
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            if([[[json objectForKey:@"visitados"] objectForKey:@"cantidad"] intValue] > 0){
                
                UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithFrame:imagenMasVisitado.frame];
                
                if(imagenMasVisitado.alpha == 0.0){
                    
                    [activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
                    [activity startAnimating];
                    
                    [vistatres addSubview:activity];
                }else{
                    
                    activity = nil;
                }
                
                [imagenMasVisitado sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[json objectForKey:@"visitados"] objectForKey:@"ID"],[[json objectForKey:@"visitados"]objectForKey:@"foto"]]] forState:UIControlStateNormal completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                
                    [activity stopAnimating];
                    [activity removeFromSuperview];
                }];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [imagenMasVisitado setAlpha:1.0];
                    [imagenMasVisitado setEnabled:YES];
                    [nombreMasVisitado setEnabled:YES];
                    [nombreMasVisitado setTitle:[[json objectForKey:@"visitados"] objectForKey:@"lugar"] forState:UIControlStateNormal];
                    diccionarioMasVisitado = json;
                });
                
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    imagenMasVisitado.alpha = 0.0;
                    [nombreMasVisitado setTitle:NSLocalizedString(@"Aún no hay publicaciones",nil) forState:UIControlStateNormal];
                    [nombreMasVisitado setEnabled:NO];
                });
                
            }
            
            if([[[json objectForKey:@"votados"] objectForKey:@"numeroDeVotos"] intValue] > 0){
                
                __block UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithFrame:imagenMasVotado.frame];
                
                
                if(imagenMasVotado.alpha == 0.0){
                    
                    [activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
                    [activity startAnimating];
                    
                    [vistatres addSubview:activity];
                }else{
                    
                    activity = nil;
                }
                
                [imagenMasVotado sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[json objectForKey:@"votados"] objectForKey:@"ID"],[[json objectForKey:@"votados"]objectForKey:@"foto"]]] forState:UIControlStateNormal completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                
                    [activity stopAnimating];
                    [activity removeFromSuperview];
                }];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [nombreMasVotado setEnabled:YES];
                    [imagenMasVotado setAlpha:1.0];
                    [imagenMasVotado setEnabled:YES];
                    [nombreMasVotado setTitle:[[json objectForKey:@"votados"] objectForKey:@"lugar"] forState:UIControlStateNormal];
                    diccionarioMasVotado = json;
                });
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    imagenMasVotado.alpha = 0.0;
                    [nombreMasVotado setTitle:NSLocalizedString(@"Aún no hay votos",nil) forState:UIControlStateNormal];
                    [nombreMasVotado setEnabled:NO];
                });
            }
            
            [self getMasLugares];
            
        }
    }];

}

-(IBAction)removerLugarAir:(id)sender{
    
    nombreLugarAir.alpha = 0.0;
    nombreLugarAir.userInteractionEnabled = NO;
    lugarAir = 0;
    
    if([[[campoCompartir text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0){
    
        botonEnviar.userInteractionEnabled = NO;
        [botonEnviar setTitleColor:[UIColor colorWithRed:178/255.0 green:178/255.0 blue:185/255.0 alpha:1] forState:UIControlStateNormal];
    }
}

-(IBAction)enviar:(id)sender{

    [botonEnviar setUserInteractionEnabled:NO];
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    [botonEnviar setFrame:CGRectMake(222, botonEnviar.frame.origin.y, botonEnviar.frame.size.width, botonEnviar.frame.size.height)];
    [botonEnviar setTitle:NSLocalizedString(@"Publicando...", nil) forState:UIControlStateNormal];
    [constraintCompartirButton setConstant:10];
    [botonEnviar setTitleColor:[UIColor colorWithRed:178/255.0 green:178/255.0 blue:185/255.0 alpha:1] forState:UIControlStateNormal];
    
    if((![[[campoCompartir text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] && ![[campoCompartir text] isEqualToString:NSLocalizedString(@"¿Qué plan hay?", nil)]) || nombreLugarAir.alpha == 1.0){
    
        NSString *palabra = NULL;
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/publicaracontesimiento.php"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1.0];
        
        [peticion setHTTPMethod:@"POST"];
        
        if(![[[campoCompartir text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] && ![[campoCompartir text] isEqualToString:NSLocalizedString(@"¿Qué plan hay?", nil)]){
            
            NSData *data =[[campoCompartir text] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
            palabra = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        
        [peticion setHTTPBody:[[NSString stringWithFormat:@"word=%@&lugar=%d&usuario=%d&token=%@",palabra,lugarAir, [[defaults objectForKey:@"identifier"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        [peticion setTimeoutInterval:3];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
       
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if(error.code == NSURLErrorTimedOut){
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController *error = [UIAlertController alertControllerWithTitle:nil message:@"Estamos teniendo un problema, por favor inténtalo nuevamente" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *aceptar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Muy bien", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                        
                        botonEnviar.userInteractionEnabled = YES;
                        [botonEnviar setTitleColor:[UIColor colorWithRed:0 green:171/255.0 blue:207/255.0 alpha:1] forState:UIControlStateNormal];
                        [botonEnviar setTitle:NSLocalizedString(@"Compartir", nil) forState:UIControlStateNormal];
                    }];
                    
                    [error addAction:aceptar];
                    
                    [self presentViewController:error animated:YES completion:nil];
                    app.networkActivityIndicatorVisible = NO;
                });
                
            }else if(error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                    UIAlertController *error = [UIAlertController alertControllerWithTitle:@"No estás conectado a Internet" message:@"Por favor, verifica tu conexión a Internet" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *aceptar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Muy bien", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                        
                        botonEnviar.userInteractionEnabled = NO;
                        [botonEnviar setTitleColor:[UIColor colorWithRed:178/255.0 green:178/255.0 blue:185/255.0 alpha:1] forState:UIControlStateNormal];
                        [botonEnviar setTitle:NSLocalizedString(@"Compartir", nil) forState:UIControlStateNormal];
                    }];
                    
                    [error addAction:aceptar];
                    
                    [self presentViewController:error animated:YES completion:nil];
                    app.networkActivityIndicatorVisible = NO;
                });
            }else{
    
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if(nombreLugarAir.alpha == 1.0){
                
                        
                        //******************** publicar en facebook

                        
                        
                        /*,@"og:image":@"https://scontent-sea1-1.xx.fbcdn.net/hphotos-xap1/t39.2178-6/851565_496755187057665_544240989_n.jpg"
                        
                              NSDictionary *properties = @{@"og:type":@"place", @"og:title":[nombreLugarAir.titleLabel text], @"place:location:latitude":@(coordenadas.coordinate.latitude),@"place:location:longitude":@(coordenadas.coordinate.longitude),@"place:location:altitude":@(coordenadas.altitude)};
                             
                             FBSDKShareOpenGraphObject *objeto = [FBSDKShareOpenGraphObject objectWithProperties:properties];
                             
                             FBSDKShareOpenGraphAction *action = [[FBSDKShareOpenGraphAction alloc] init];
                             action.actionType = @"checkin";
                             [action setObject:objeto forKey:@"place"];
                             
                             FBSDKShareOpenGraphContent *content = [[FBSDKShareOpenGraphContent alloc] init];
                             content.action = action;
                             content.previewPropertyName = @"place";
                             
                             [FBSDKShareAPI shareWithContent:content delegate:nil];*/
                        
                        
                        
                        
                        if([[defaults objectForKey:@"publicarFacebook"] intValue] == 1){
                
                            NSString *foto = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

                            FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                            content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugar.php?id=%d",lugarAir]];
                            content.contentTitle = [nombreLugarAir.titleLabel text];
                            content.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%d/%@",lugarAir,foto]];
                            
                            [FBSDKShareAPI shareWithContent:content delegate:nil];
                            
                            
                        }
                        
                        
                        
                        //*******************
                        
                        
                        [vistadecarga setHidden:NO];
                        [campoCompartir setEditable:NO];
                        enLugar = YES;
                        
                        fechaPublicacionActual = [[NSDate date] timeIntervalSince1970];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            timerAunAhiBoton = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(checarSiEstaEnLugar:) userInfo:@(fechaPublicacionActual) repeats:YES];
                            
                            [timerAunAhiBoton fire];
                        });
                        
                        [campoCompartir setEditable:NO];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [activityEstaLugar removeFromSuperview];
                            [vistadecarga setBackgroundColor:[UIColor colorWithRed:22/255.0 green:179/255.0 blue:173/255.0 alpha:1.0]];
                            
                            UILabel *estasen = [[UILabel alloc] init];
                            
                            [estasen setText:NSLocalizedString(@"Ahora estas en:", nil)];
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                                
                                [estasen setFrame:CGRectMake(50, 10, estasen.frame.size.width, estasen.frame.size.height)];
                            }else{
                                [estasen setFrame:CGRectMake(50, 30, estasen.frame.size.width, estasen.frame.size.height)];
                            }
                            
                            [estasen setTextColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0]];
                            
                            [estasen sizeToFit];
                            
                            [vistadecarga addSubview:estasen];
                            
                            UIActivityIndicatorView *cargaFotoLugar = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(135, 90, 50, 50)];
                            
                            [cargaFotoLugar startAnimating];
                            
                            [vistadecarga addSubview:cargaFotoLugar];
                            
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                                
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                        UIImageView *imageFotoLugar = [[UIImageView alloc] initWithImage:[UIImage imageWithData:fotolugarair]];
                                        
                                        if([[UIScreen mainScreen] bounds].size.height <= 480){
                                            
                                            [imageFotoLugar setFrame:CGRectMake(110, 50, 100, 100)];
                                        }else{
                                            [imageFotoLugar setFrame:CGRectMake(110, 90, 100, 100)];
                                        }
                                        
                                        NSString *foto = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                        
                                        [imageFotoLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%d/%@",lugarAir,foto]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                        
                                            [cargaFotoLugar stopAnimating];
                                            [cargaFotoLugar removeFromSuperview];
                                        }];
                                        
                                        [vistadecarga addSubview:imageFotoLugar];
                                    //}
                                });
                            });
                            
                            UIButton *lugar = [[UIButton alloc] init];
                            
                            [lugar setTitleColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0] forState:UIControlStateNormal];
                        
                            [lugar setTitle:[nombreLugarAir.titleLabel text] forState:UIControlStateNormal];
                            
                            [lugar.titleLabel setFont:[UIFont fontWithName:lugar.titleLabel.font.familyName size:20]];
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                                
                                [lugar setFrame:CGRectMake(0, 170, 320, lugar.frame.size.height)];
                            }else{
                                [lugar setFrame:CGRectMake(0, 210, 320, lugar.frame.size.height)];
                            }
                            
                            [lugar setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
                            
                            [lugar addTarget:self action:@selector(irALugarYaAhi:) forControlEvents:UIControlEventTouchUpInside];
                            
                            [vistadecarga addSubview:lugar];
                            
                            UIButton *votar = [[UIButton alloc] init];
                            
                            [votar setTitle:NSLocalizedString(@"¡Votar!", nil) forState:UIControlStateNormal];
                            
                            [votar.titleLabel setFont:[UIFont fontWithName:votar.titleLabel.font.familyName size:13.0]];
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                                [votar setFrame:CGRectMake(110, 220, 100, 30)];
                            }else{
                                [votar setFrame:CGRectMake(110, 300, 100, 30)];
                            }
                            
                            [votar setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                            
                            [votar setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
                            
                            [votar.layer setCornerRadius:15];
                            
                            [votar addTarget:self action:@selector(votarLugar) forControlEvents:UIControlEventTouchUpInside];
                            
                            [vistadecarga addSubview:votar];
                            
                            UIButton *aunAhi = [[UIButton alloc] init];
                            
                            [aunAhi setTitle:NSLocalizedString(@"Aún estoy ahí", nil) forState:UIControlStateNormal];
                            
                            [aunAhi.titleLabel setFont:[UIFont fontWithName:aunAhi.titleLabel.font.familyName size:13.0]];
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                                
                                [aunAhi setFrame:CGRectMake(85, 270, 150, 30)];
                            }else{
                                [aunAhi setFrame:CGRectMake(85, 350, 150, 30)];
                            }
                            
                            [aunAhi setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:1.0]];
                                
                            [aunAhi addTarget:self action:@selector(aunAhi:) forControlEvents:UIControlEventTouchUpInside];
                            
                            
                            [aunAhi setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
                            
                            [aunAhi.layer setCornerRadius:15];
                            
                            [aunAhi setTag:10];
                            
                            [vistadecarga addSubview:aunAhi];
                            
                            UIButton *yano = [[UIButton alloc] init];
                            
                            [yano setTitle:NSLocalizedString(@"Ya no estoy en ese lugar", nil) forState:UIControlStateNormal];
                            
                            [yano.titleLabel setFont:[UIFont fontWithName:yano.titleLabel.font.familyName size:13.0]];
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                                
                                [yano setFrame:CGRectMake(60, 320, 200, 30)];
                            }else{
                                [yano setFrame:CGRectMake(60, 400, 200, 30)];
                            }
                            [yano setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                            
                            [yano setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
                            
                            [yano.layer setCornerRadius:15];
                            
                            [yano addTarget:self action:@selector(noAhi:) forControlEvents:UIControlEventTouchUpInside];
                            
                            [vistadecarga addSubview:yano];
                            
                            vistaVotar = [[UIView alloc] initWithFrame:CGRectMake(0, 568, 320, 568)];
                            
                            [vistaVotar setBackgroundColor:[UIColor colorWithRed:22/255.0 green:179/255.0 blue:173/255.0 alpha:1]];
                            
                            [self.view addSubview:vistaVotar];
                            
                            //UITextView *comentariosLugar = [[UITextView alloc] initWithFrame:CGRectMake(20, 100, 280, 100)];
                            
                            UITextField *comentariosLugar = [[UITextField alloc] initWithFrame:CGRectMake(22, 418, 237, 30)];
                            
                            [comentariosLugar setDelegate:self];
                            
                            [comentariosLugar setTag:1];
                            
                            [comentariosLugar setPlaceholder:NSLocalizedString(@"Envía un comentario", nil)];
                            
                            [comentariosLugar setBackgroundColor:[UIColor whiteColor]];
                            
                            comentariosLugar.layer.cornerRadius = 10;
                            
                            [comentariosLugar addTarget:self action:@selector(textoCambio:) forControlEvents:UIControlEventEditingChanged];
                            
                           /* UIButton *comentar = [[UIButton alloc] initWithFrame:CGRectMake(110, 250, 100, 30)];
                            
                            [comentar setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                            
                            [comentar setTitle:@"Comentar!" forState:UIControlStateNormal];
                            
                            [comentar.titleLabel setFont:[UIFont fontWithName:comentar.titleLabel.font.familyName size:13]];
                            
                            [comentar addTarget:self action:@selector(enviarMensajeLugar:) forControlEvents:UIControlEventTouchUpInside];
                            
                            comentar.layer.cornerRadius = 15;
                            
                            [comentar setTag:2];*/
                            
                            UIButton *comentar = [[UIButton alloc] initWithFrame:CGRectMake(262, 418, 45, 30)];
                            
                            [comentar setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                            
                            [comentar setTitle:NSLocalizedString(@"Enviar",nil) forState:UIControlStateNormal];
                            
                            [comentar.titleLabel setFont:[UIFont fontWithName:comentar.titleLabel.font.familyName size:13]];
                            
                            [comentar addTarget:self action:@selector(enviarMensajeLugar:) forControlEvents:UIControlEventTouchUpInside];
                            
                            comentar.layer.cornerRadius = 15;
                            
                            [comentar setTag:2];
                            
                            [comentar setTitle:@"" forState:UIControlStateNormal];
                            [comentar setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                            [comentar setFrame:CGRectMake(comentar.frame.origin.x, comentar.frame.origin.y, 30, comentar.frame.size.height)];
                            comentar.layer.cornerRadius = 5;
                            
                            UIButton *verComentarios = [[UIButton alloc] initWithFrame:CGRectMake(65, 200, 200, 30)];
                            
                            [verComentarios setCenter:CGPointMake(self.vistaVotar.center.x, verComentarios.center.y)];
                            
                            [verComentarios setBackgroundColor:[UIColor colorWithRed:0 green:111.0/255.0 blue:147.0/255.0 alpha:1]];
                            
                            [verComentarios.titleLabel setFont:[UIFont fontWithName:comentar.titleLabel.font.familyName size:13]];
                            
                            [verComentarios setTitle:NSLocalizedString(@"Ver todos los comentarios", nil) forState:UIControlStateNormal];
                            
                            verComentarios.layer.cornerRadius = 15;
                            
                            [verComentarios setTag:20];
                            
                            [verComentarios addTarget:self action:@selector(mostrarTodosComentarios:) forControlEvents:UIControlEventTouchUpInside];
                            
                            UIButton *votofavor;
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                                
                                votofavor = [[UIButton alloc] initWithFrame:CGRectMake(90, 210, 50, 30)];
                            }else{
                                votofavor = [[UIButton alloc] initWithFrame:CGRectMake(90, 250, 50, 30)];
                            }
                            
                            [votofavor setBackgroundColor:[UIColor colorWithRed:0 green:0.9 blue:0 alpha:1.0]];
                                
                            [votofavor setTitle:NSLocalizedString(@"Cool",nil) forState:UIControlStateNormal];
                                
                            [votofavor.titleLabel setFont:[UIFont fontWithName:votofavor.titleLabel.font.familyName size:12]];
                                
                            votofavor.layer.cornerRadius = 15;
                                
                            [votofavor addTarget:self action:@selector(votar:) forControlEvents:UIControlEventTouchUpInside];
                                
                            [votofavor setTag:3];
                                
                            UIButton *votocontra;
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                                
                                votocontra = [[UIButton alloc] initWithFrame:CGRectMake(190, 210, 50, 30)];
                            }else{
                                votocontra = [[UIButton alloc] initWithFrame:CGRectMake(190, 250, 50, 30)];
                            }
                            
                            [votocontra setBackgroundColor:[UIColor colorWithRed:0.7 green:0 blue:0 alpha:1]];
                                
                            [votocontra setTitle:NSLocalizedString(@"Chafa",nil) forState:UIControlStateNormal];
                                
                            [votocontra.titleLabel setFont:[UIFont fontWithName:votocontra.titleLabel.font.familyName size:11]];
                                
                            votocontra.layer.cornerRadius = 15;
                                
                            [votocontra addTarget:self action:@selector(votar:) forControlEvents:UIControlEventTouchUpInside];
                                
                            [votocontra setTag:4];
                                
                            [vistaVotar addSubview:votofavor];
                            [vistaVotar addSubview:votocontra];
                            
                            UIButton *esconder = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 30, 30)];
                            
                            [esconder setBackgroundImage:[UIImage imageNamed:@"flechaabajo4.png"] forState:UIControlStateNormal];
                            
                            [esconder addTarget:self action:@selector(esconderVotoLugar) forControlEvents:UIControlEventTouchUpInside];
                            
                            [vistaVotar addSubview:comentariosLugar];
                            [vistaVotar addSubview:verComentarios];
                            [vistaVotar addSubview:comentar];
                            [vistaVotar addSubview:esconder];
                            
                        });
                    }
                
                    campoCompartir.text = @"";
                    nombreLugarAir.alpha = 0.0;
                    botonEnviar.userInteractionEnabled = NO;
                    [botonEnviar setTitleColor:[UIColor colorWithRed:178/255.0 green:178/255.0 blue:185/255.0 alpha:1] forState:UIControlStateNormal];
                    [botonEnviar setTitle:@"Compartir" forState:UIControlStateNormal];
                    [constraintCompartirButton setConstant:44];
                    [botonEnviar setFrame:CGRectMake(botonEnviar.frame.origin.x+22, botonEnviar.frame.origin.y, botonEnviar.frame.size.width, botonEnviar.frame.size.height)];
                    app.networkActivityIndicatorVisible = NO;
                });
            
            }
        }];
    }else{
    
    }
}

-(IBAction)publicaAlgo:(id)sender{
    
    notificaciones.text = NSLocalizedString(@"Comparte",nil);
    [scrollview setContentOffset:CGPointMake(320, scrollview.contentOffset.y) animated:YES];
    if(enLugar == NO && localizacionHab == YES){
        
        [campoCompartir becomeFirstResponder];
    }
}

-(IBAction)mejorLugarVer:(id)sender{

    notificaciones.text = NSLocalizedString(@"Explora",nil);
    [scrollview setContentOffset:CGPointMake(640, scrollview.contentOffset.y) animated:YES];
    [campoCompartir resignFirstResponder];
}

-(IBAction)enLugarShowLugar:(id)sender{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    lugarViewController *lugar = [storyboard instantiateViewControllerWithIdentifier:@"Lugar"];
    
    lugar.idLugar = lugarAir;
    lugar.nombreLugar = nombreLugarAir.titleLabel.text;
    lugar.dataFoto = fotolugarair;
    
    [self.navigationController pushViewController:lugar animated:YES];
}

-(void)cordenadas{

    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        [locationManager requestAlwaysAuthorization];
    }
    
    [locationManager startUpdatingLocation];
    
}

-(IBAction)noAhi:(id)sender{

    defaults = [NSUserDefaults standardUserDefaults];
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/removejoin.php"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1.0];
    
    [peticion setHTTPMethod:@"POST"];
    
    [peticion setHTTPBody:[[NSString stringWithFormat:@"recurso=2&usuario=%d&token=%@", [[defaults objectForKey:@"identifier"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    [peticion setTimeoutInterval:3];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *err = [UIAlertController alertControllerWithTitle:nil message:@"Estamos teniendo problemas, por favor inténtalo nuevamente" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *muybien = [UIAlertAction actionWithTitle:NSLocalizedString(@"Muy bien", nil) style:UIAlertActionStyleDefault handler:nil];
                
                [err addAction:muybien];
                
                [self presentViewController:err animated:YES completion:nil];
            });
        }else if(error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            dispatch_async(dispatch_get_main_queue(), ^{
            
                UIAlertController *error = [UIAlertController alertControllerWithTitle:@"No estás conectado a Internet" message:@"Por favor, verifica tu conexión a Internet" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *aceptar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Muy bien", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
                
                [error addAction:aceptar];
                
                [self presentViewController:error animated:YES completion:nil];
                app.networkActivityIndicatorVisible = NO;
            });
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
            
                [UIView animateWithDuration:0.4 animations:^(){
                
                    vistadecarga.frame = CGRectMake(0, 468, 320, 468);
                    
                    for(UIView *elemento in [vistadecarga subviews]){
                        
                        [elemento removeFromSuperview];
                    }
                    
                    [vistadecarga setHidden:YES];
                    
                    [campoCompartir setEditable:YES];
                    
                    if(localizacionHab == YES){
                        [campoCompartir becomeFirstResponder];
                    }
                    enLugar = NO;
                    
                    [self activarBotonCheckinRapido];
                    app.networkActivityIndicatorVisible = NO;
                }];
            });
        }
    }];
}

-(IBAction)join:(id)sender{
    
    defaults = [NSUserDefaults standardUserDefaults];
    UIButton *boton = (UIButton *)sender;
    UIApplication *app = [UIApplication sharedApplication];
    
    if([boton.imageView.image isEqual:[UIImage imageNamed:@"joinT.png"]] && enLugar == NO){
        
        UITableViewCell *celda = (UITableViewCell *)[sender superview];
        
        NSIndexPath *indexPath = [tabla indexPathForCell:celda];
        
        [boton setImage:[UIImage imageNamed:@"joinFilled.png"] forState:UIControlStateNormal];
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/addjoin.php"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1.0];
        
        [peticion setHTTPMethod:@"POST"];
        
        [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%d&origen=%d&estado=1&token=%@",[[defaults objectForKey:@"identifier"] intValue],[[[arreglo objectAtIndex:indexPath.row] objectForKey:@"aconID"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                [sender setBackgroundImage:[UIImage imageNamed:@"joinT.png"] forState:UIControlStateNormal];
                
            }else{
                
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if([[json objectForKey:@"ID"] intValue] != 0 && ![[json objectForKey:@"nombre"] isEqualToString:@"error"]){
                        
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [vistadecarga setHidden:NO];
                            [campoCompartir setEditable:NO];
                            enLugar = YES;
                            
                            vistadecarga.frame = CGRectMake(320, 62, vistadecarga.frame.size.width, vistadecarga.frame.size.height);
                            
                            fechaPublicacionActual = [[NSDate date] timeIntervalSince1970];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                timerAunAhiBoton = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(checarSiEstaEnLugar:) userInfo:@(fechaPublicacionActual) repeats:YES];
                                
                                [timerAunAhiBoton fire];
                            });
                            
                            [campoCompartir setEditable:NO];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [activityEstaLugar removeFromSuperview];
                                [vistadecarga setBackgroundColor:[UIColor colorWithRed:22/255.0 green:179/255.0 blue:173/255.0 alpha:1.0]];
                                
                                UILabel *estasen = [[UILabel alloc] init];
                                
                                [estasen setText:NSLocalizedString(@"Ahora estas en:",nil)];
                                
                                if([[UIScreen mainScreen] bounds].size.height <= 480){
                                    
                                    [estasen setFrame:CGRectMake(50, 10, estasen.frame.size.width, estasen.frame.size.height)];
                                }else{
                                    [estasen setFrame:CGRectMake(50, 30, estasen.frame.size.width, estasen.frame.size.height)];
                                }
                                [estasen setTextColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0]];
                                
                                [estasen sizeToFit];
                                
                                [vistadecarga addSubview:estasen];
                                
                                UIActivityIndicatorView *cargaFotoLugar = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(135, 90, 50, 50)];
                                
                                [cargaFotoLugar startAnimating];
                                
                                [vistadecarga addSubview:cargaFotoLugar];
  
                                NSString *stringUrlFotoJoin = [NSString stringWithFormat:@"https://www.crowte.com/lugares/%d/%@",[[json objectForKey:@"ID"] intValue],[json objectForKey:@"foto"]];
                                
                                NSURL *urlFotoJoin = [NSURL URLWithString:stringUrlFotoJoin];
                                
                                UIImageView *imageFotoLugar = [[UIImageView alloc] init];
                                
                                [imageFotoLugar sd_setImageWithURL:urlFotoJoin completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                
                                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                                        
                                        [imageFotoLugar setFrame:CGRectMake(110, 50, 100, 100)];
                                    }else{
                                        [imageFotoLugar setFrame:CGRectMake(110, 90, 100, 100)];
                                    }
                                    [cargaFotoLugar stopAnimating];
                                    [cargaFotoLugar removeFromSuperview];
                                    
                                    [vistadecarga addSubview:imageFotoLugar];
                                }];
                                
          
                                UIButton *lugar = [[UIButton alloc] init];
                                
                                [lugar setTitleColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0] forState:UIControlStateNormal];
                                
                                [lugar setTitle:[json objectForKey:@"nombre"] forState:UIControlStateNormal];
                                
                                [lugar.titleLabel setFont:[UIFont fontWithName:lugar.titleLabel.font.familyName size:20]];
                                
                                if([[UIScreen mainScreen] bounds].size.height <= 480){
                                
                                    [lugar setFrame:CGRectMake(0, 170, 320, lugar.frame.size.height)];
                                }else{
                                    [lugar setFrame:CGRectMake(0, 210, 320, lugar.frame.size.height)];
                                }
                                
                                [lugar setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
                                
                                [lugar addTarget:self action:@selector(irALugarYaAhi:) forControlEvents:UIControlEventTouchUpInside];
                                
                                [vistadecarga addSubview:lugar];
                                
                                UIButton *votar = [[UIButton alloc] init];
                                
                                [votar setTitle:NSLocalizedString(@"¡Votar!", nil) forState:UIControlStateNormal];
                                
                                [votar.titleLabel setFont:[UIFont fontWithName:votar.titleLabel.font.familyName size:13.0]];
                                
                                if([[UIScreen mainScreen] bounds].size.height <= 480){
                                    
                                    [votar setFrame:CGRectMake(110, 220, 100, 30)];
                                }else{
                                    [votar setFrame:CGRectMake(110, 300, 100, 30)];
                                }
                                [votar setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                                
                                [votar setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
                                
                                [votar.layer setCornerRadius:15];
                                
                                [votar addTarget:self action:@selector(votarLugar) forControlEvents:UIControlEventTouchUpInside];
                                
                                [vistadecarga addSubview:votar];
                                
                                
                                UIButton *aunAhi = [[UIButton alloc] init];
                                
                                [aunAhi setTitle:NSLocalizedString(@"Aún estoy ahí", nil) forState:UIControlStateNormal];
                                
                                [aunAhi.titleLabel setFont:[UIFont fontWithName:aunAhi.titleLabel.font.familyName size:13.0]];
                                
                                if([[UIScreen mainScreen] bounds].size.height <= 480){
                                    
                                    [aunAhi setFrame:CGRectMake(85, 270, 150, 30)];
                                }else{
                                    [aunAhi setFrame:CGRectMake(85, 350, 150, 30)];
                                }
                                
                                [aunAhi setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:1.0]];
                                
                                [aunAhi addTarget:self action:@selector(aunAhi:) forControlEvents:UIControlEventTouchUpInside];
                                
                                
                                [aunAhi setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
                                
                                [aunAhi.layer setCornerRadius:15];
                                
                                [aunAhi setTag:10];
                                
                                [vistadecarga addSubview:aunAhi];
                                
                                UIButton *yano = [[UIButton alloc] init];
                                
                                [yano setTitle:NSLocalizedString(@"Ya no estoy en ese lugar", nil) forState:UIControlStateNormal];
                                
                                [yano.titleLabel setFont:[UIFont fontWithName:yano.titleLabel.font.familyName size:13.0]];
                                
                                if([[UIScreen mainScreen] bounds].size.height <= 480){
                                    
                                    [yano setFrame:CGRectMake(60, 320, 200, 30)];
                                }else{
                                    [yano setFrame:CGRectMake(60, 400, 200, 30)];
                                }
                                [yano setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                                
                                [yano setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
                                
                                [yano.layer setCornerRadius:15];
                                
                                [yano addTarget:self action:@selector(noAhi:) forControlEvents:UIControlEventTouchUpInside];
                                
                                [vistadecarga addSubview:yano];
                                
                                vistaVotar = [[UIView alloc] initWithFrame:CGRectMake(0, 568, 320, 568)];
                                
                                [vistaVotar setBackgroundColor:[UIColor colorWithRed:22/255.0 green:179/255.0 blue:173/255.0 alpha:1]];
                                
                                [self.view addSubview:vistaVotar];
                                
                              /*  UITextView *comentariosLugar = [[UITextView alloc] initWithFrame:CGRectMake(20, 100, 280, 100)];
                                */
                                
                                UITextField *comentariosLugar = [[UITextField alloc] initWithFrame:CGRectMake(22, 418, 237, 30)];
                                
                                [comentariosLugar setDelegate:self];
                                
                                [comentariosLugar setTag:1];
                                
                                [comentariosLugar setPlaceholder:NSLocalizedString(@"Envía un comentario", nil)];
                                
                                [comentariosLugar setBackgroundColor:[UIColor whiteColor]];
                                
                                comentariosLugar.layer.cornerRadius = 10;
                                
                                [comentariosLugar addTarget:self action:@selector(textoCambio:) forControlEvents:UIControlEventEditingChanged];
                                
                                
                                UIButton *comentar = [[UIButton alloc] initWithFrame:CGRectMake(262, 418, 45, 30)];
                                
                                [comentar setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                                
                                [comentar setTitle:NSLocalizedString(@"Enviar", nil) forState:UIControlStateNormal];
                                
                                [comentar.titleLabel setFont:[UIFont fontWithName:comentar.titleLabel.font.familyName size:13]];
                                
                                [comentar addTarget:self action:@selector(enviarMensajeLugar:) forControlEvents:UIControlEventTouchUpInside];
                                
                                comentar.layer.cornerRadius = 15;
                                
                                [comentar setTag:2];
                                
                                [comentar setTitle:@"" forState:UIControlStateNormal];
                                [comentar setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                                [comentar setFrame:CGRectMake(comentar.frame.origin.x, comentar.frame.origin.y, 30, comentar.frame.size.height)];
                                comentar.layer.cornerRadius = 5;
                                
                                UIButton *verComentarios = [[UIButton alloc] initWithFrame:CGRectMake(65, 200, 200, 30)];
                                
                                [verComentarios setCenter:CGPointMake(self.vistaVotar.center.x, verComentarios.center.y)];
                                
                                [verComentarios setBackgroundColor:[UIColor colorWithRed:0 green:111.0/255.0 blue:147.0/255.0 alpha:1]];
                                
                                [verComentarios.titleLabel setFont:[UIFont fontWithName:comentar.titleLabel.font.familyName size:13]];
                                
                                [verComentarios setTitle:NSLocalizedString(@"Ver todos los comentarios", nil) forState:UIControlStateNormal];
                                
                                verComentarios.layer.cornerRadius = 15;
                                
                                [verComentarios setTag:20];
                                
                                [verComentarios addTarget:self action:@selector(mostrarTodosComentarios:) forControlEvents:UIControlEventTouchUpInside];
                                
                                UIButton *votofavor;
                                
                                if([[UIScreen mainScreen] bounds].size.height <= 480){
                                    
                                    votofavor = [[UIButton alloc] initWithFrame:CGRectMake(90, 210, 50, 30)];
                                }else{
                                    votofavor = [[UIButton alloc] initWithFrame:CGRectMake(90, 250, 50, 30)];
                                }
                                
                                [votofavor setBackgroundColor:[UIColor colorWithRed:0 green:0.9 blue:0 alpha:1.0]];
                                
                                [votofavor setTitle:NSLocalizedString(@"Cool", nil) forState:UIControlStateNormal];
                                
                                [votofavor.titleLabel setFont:[UIFont fontWithName:votofavor.titleLabel.font.familyName size:12]];
                                
                                votofavor.layer.cornerRadius = 15;
                                
                                [votofavor addTarget:self action:@selector(votar:) forControlEvents:UIControlEventTouchUpInside];
                                
                                [votofavor setTag:3];
                                
                                UIButton *votocontra;
                                
                                if([[UIScreen mainScreen] bounds].size.height <= 480){
                                    
                                    votocontra = [[UIButton alloc] initWithFrame:CGRectMake(190, 210, 50, 30)];
                                }else{
                                    votocontra = [[UIButton alloc] initWithFrame:CGRectMake(190, 250, 50, 30)];
                                }
                                [votocontra setBackgroundColor:[UIColor colorWithRed:0.7 green:0 blue:0 alpha:1]];
                                
                                [votocontra setTitle:NSLocalizedString(@"Chafa",nil) forState:UIControlStateNormal];
                                
                                [votocontra.titleLabel setFont:[UIFont fontWithName:votocontra.titleLabel.font.familyName size:11]];
                                
                                votocontra.layer.cornerRadius = 15;
                                
                                [votocontra addTarget:self action:@selector(votar:) forControlEvents:UIControlEventTouchUpInside];
                                
                                [votocontra setTag:4];
                                
                                [vistaVotar addSubview:votofavor];
                                [vistaVotar addSubview:votocontra];
                                
                                UIButton *esconder = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 30, 30)];
                                
                                [esconder setBackgroundImage:[UIImage imageNamed:@"flechaabajo4.png"] forState:UIControlStateNormal];
                                
                                [esconder addTarget:self action:@selector(esconderVotoLugar) forControlEvents:UIControlEventTouchUpInside];
                                
                                [vistaVotar addSubview:comentariosLugar];
                                [vistaVotar addSubview:verComentarios];
                                [vistaVotar addSubview:comentar];
                                [vistaVotar addSubview:esconder];
                                
                                [[arreglo objectAtIndex:indexPath.row] removeObjectForKey:@"finguer"];
                                [[arreglo objectAtIndex:indexPath.row] setObject:[NSNumber numberWithInt:1] forKey:@"finguer"];
                                
                            });
                            
                            app.networkActivityIndicatorVisible = NO;
                        });
                        
                    }
                    
                    
                });
            }
        }];
    
    }else if([boton.imageView.image isEqual:[UIImage imageNamed:@"joinFilled.png"]]){
    
        UITableViewCell *celda = (UITableViewCell *)[sender superview];
        
        NSIndexPath *indexPath = [tabla indexPathForCell:celda];
        
        [boton setImage:[UIImage imageNamed:@"joinT.png"] forState:UIControlStateNormal];
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/removejoin.php"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1.0];
        
        [peticion setHTTPMethod:@"POST"];
        
        [peticion setHTTPBody:[[NSString stringWithFormat:@"recurso=2&usuario=%d&token=%@", [[defaults objectForKey:@"identifier"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                [sender setBackgroundImage:[UIImage imageNamed:@"joinFilled.png"] forState:UIControlStateNormal];
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [UIView animateWithDuration:0.4 animations:^(){
                        
                        vistadecarga.frame = CGRectMake(0, 468, 320, 468);
                        
                        [vistadecarga setHidden:YES];
                        
                        [campoCompartir setEditable:YES];
                        
                        enLugar = NO;
                        
                        [[arreglo objectAtIndex:indexPath.row] removeObjectForKey:@"finguer"];
                        [[arreglo objectAtIndex:indexPath.row] setObject:[NSNumber numberWithInt:0] forKey:@"finguer"];
                    }];
                });
            }
        }];
    }
}

-(void)getUpdatePosts{
    
    if([arreglo count] > 0){
        
        NSUserDefaults *datosUsuario = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/update.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        
        if(![[arreglo objectAtIndex:0] isEqual:@"valido"]){
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&ultimoID=%d&token=%@",[[datosUsuario objectForKey:@"identifier"] intValue],[[[arreglo objectAtIndex:0] objectForKey:@"aconID"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        }else if([[arreglo objectAtIndex:0] isEqual:@"valido"]){
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&ultimoID=0&token=%@",[[datosUsuario objectForKey:@"identifier"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                
                    [self getUpdatePosts];
                });
                
            }else{
                
                NSArray *llegadaUpdate = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                if([[[llegadaUpdate objectAtIndex:0] objectForKey:@"valido"] isEqual:[NSNumber numberWithInt:1]]){
                    
                    int numeroDeCelda = 0;
                    
                    float inicial = [tabla contentSize].height;
                    
                    for(NSDictionary *intervalo in llegadaUpdate){
                        
                        if([[intervalo objectForKey:@"valido"] isEqual:[NSNumber numberWithInt:1]]){
                            
                            [arreglo insertObject:intervalo atIndex:numeroDeCelda];
                            
                            numeroDeCelda++;
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [tabla reloadData];
                        
                        CGPoint final = CGPointMake([tabla contentOffset].x, [tabla contentSize].height - inicial);
                        [tabla setContentOffset:final];
                    });
                }
            }
        }];
    }
    
}

-(void)showNotificaciones:(id)sender{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NotificacionesController *notificacionesView = [storyboard instantiateViewControllerWithIdentifier:@"notificaciones"];

    [self.navigationController pushViewController:notificacionesView animated:YES];
    
}

-(void)irALugarYaAhi:(UIButton *)boton{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    lugarViewController *lugarAIr = [storyboard instantiateViewControllerWithIdentifier:@"Lugar"];
    
    lugarAIr.nombreLugar = [[boton titleLabel] text];
    
    lugarAIr.idLugar = [[[lugares objectForKey:@"lugar"] objectForKey:@"objID"] intValue];
    
    lugarAIr.fotoLugar = [[lugares objectForKey:@"lugar"] objectForKey:@"foto"];
    
    [self.navigationController pushViewController:lugarAIr animated:YES];
}

-(void)votarLugar{
    
    [self cerrarTutorialBotonVotar];
    
    [UIView animateWithDuration:0.4 animations:^{
    
        [constraintEstaLugar setConstant:-568];
        [constraintHeightScrollview setConstant:0];
        vistaVotar.frame = CGRectMake(0, 63, vistaVotar.frame.size.width, vistaVotar.frame.size.height);
        [self.view layoutIfNeeded];
    }];
}

-(void)esconderVotoLugar{
    
    UITextField *comentarioLugar = (UITextField *)[vistaVotar viewWithTag:1];
    
    [UIView animateWithDuration:0.4 animations:^{
        
        [constraintEstaLugar setConstant:62];
        [constraintHeightScrollview setConstant:568];
        vistaVotar.frame = CGRectMake(0, 568, vistaVotar.frame.size.width, vistaVotar.frame.size.height);

        [self.view layoutIfNeeded];
    }];
    
    if([comentarioLugar isFirstResponder]){
        
        [comentarioLugar resignFirstResponder];
        
        UIButton *enviarComentarioLugar = (UIButton *)[vistaVotar viewWithTag:2];
        
        UIButton *verComentarios = (UIButton*)[vistaVotar viewWithTag:20];
        
        [comentarioLugar resignFirstResponder];
        [UIView animateWithDuration:0.4 animations:^{
            
            [comentarioLugar setCenter:CGPointMake(comentarioLugar.center.x, comentarioLugar.center.y+250)];
            [enviarComentarioLugar setFrame:CGRectMake(enviarComentarioLugar.frame.origin.x, enviarComentarioLugar.frame.origin.y+250, enviarComentarioLugar.frame.size.width, enviarComentarioLugar.frame.size.height)];
            [verComentarios setAlpha:1];
        }];
    }
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITextField *comentarioLugar = (UITextField *)[vistaVotar viewWithTag:1];
    UIButton *enviarComentarioLugar = (UIButton *)[vistaVotar viewWithTag:2];
    
    
    if([[UIScreen mainScreen] bounds].size.height <= 480){
        
        if([comentarioLugar isFirstResponder]){
            
            [comentarioLugar resignFirstResponder];
            [UIView animateWithDuration:0.4 animations:^{
                
                [vistaVotar setFrame:CGRectMake(vistaVotar.frame.origin.x, vistaVotar.frame.origin.y + 100, vistaVotar.frame.size.width, vistaVotar.frame.size.height)];
                
                [enviarComentarioLugar setFrame:CGRectMake(enviarComentarioLugar.frame.origin.x, enviarComentarioLugar.frame.origin.y+40, enviarComentarioLugar.frame.size.width, enviarComentarioLugar.frame.size.height)];
            }];
        }
    }else{
        if([comentarioLugar isFirstResponder]){
            
            UIButton *verComentarios = (UIButton*)[vistaVotar viewWithTag:20];
            
            [comentarioLugar resignFirstResponder];
            [UIView animateWithDuration:0.4 animations:^{
                
                [comentarioLugar setCenter:CGPointMake(comentarioLugar.center.x, comentarioLugar.center.y+250)];
                [enviarComentarioLugar setFrame:CGRectMake(enviarComentarioLugar.frame.origin.x, enviarComentarioLugar.frame.origin.y+250, enviarComentarioLugar.frame.size.width, enviarComentarioLugar.frame.size.height)];
                [verComentarios setAlpha:1];
            }];
        }
    }
    
}

-(void)votar:(id)voto{
    
    
    UIButton *votofavor = (UIButton *)[vistaVotar viewWithTag:3];
    UIButton *votocontra = (UIButton *)[vistaVotar viewWithTag:4];
    
    [votofavor setEnabled:NO];
    [votocontra setEnabled:NO];
    
    __block int numeroDeVotos = 0;
    __block int votos = 0;
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    NSString *fuevoto;
    
    if([voto isEqual:votofavor]){
        
        fuevoto = @"positivo";
    }
    
    if([voto isEqual: votocontra]){
        
        fuevoto = @"negativo";
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    
        NSUserDefaults *datosUsuario = [NSUserDefaults standardUserDefaults];
    
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/addvoto.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%d&voto=%@&lugar=%@&token=%@",[[datosUsuario objectForKey:@"identifier"] intValue],fuevoto,[[lugares objectForKey:@"lugar"] objectForKey:@"objID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if(error.code == NSURLErrorNotConnectedToInternet || data == NULL){

                app.networkActivityIndicatorVisible = NO;
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController *nosepudo = [UIAlertController alertControllerWithTitle:@"Ups!" message:@"Ocurrió un error, por favor inténtalo otra vez" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *dismissnosepudo = [UIAlertAction actionWithTitle:NSLocalizedString(@"Muy bien", nil) style:UIAlertActionStyleCancel handler:nil];
                    
                    [nosepudo addAction:dismissnosepudo];
                    [self presentViewController:nosepudo animated:YES completion:nil];
                    app.networkActivityIndicatorVisible = NO;
                    
                    [votofavor setEnabled:YES];
                    [votocontra setEnabled:YES];
                });
            }else{
                
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                if(json != nil && ![[json objectForKey:@"numeroDeVotos"] isEqual:[NSNull null]] && ![[json objectForKey:@"votos"] isEqual:[NSNull null]]){
                
                    if(![[json objectForKey:@"numeroDeVotos"] isEqual:[NSNull null]] &&[[json objectForKey:@"numeroDeVotos"] intValue] != 0){
                    
                        numeroDeVotos = [[json objectForKey:@"numeroDeVotos"] intValue];
                        votos = [[json objectForKey:@"votos"] intValue];
                    }else{
                    
                        numeroDeVotos = 1;
                        votos = 100;
                    }
                    
                    app.networkActivityIndicatorVisible = NO;
                
                    dispatch_async(dispatch_get_main_queue(), ^{
                    
                        [UIView animateWithDuration:0.4 animations:^{
                        
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                            
                                votofavor.frame = CGRectMake(votofavor.frame.origin.x, votofavor.frame.origin.y-20, votofavor.frame.size.width, 10);
                            }else{
                                votofavor.frame = CGRectMake(votofavor.frame.origin.x, votofavor.frame.origin.y+20, votofavor.frame.size.width, 10);
                            }
                            
                            [votofavor setTitle:@"" forState:UIControlStateNormal];
                            votofavor.layer.cornerRadius = 0;
                            
                            if([[UIScreen mainScreen] bounds].size.height <= 480){
                                
                                votocontra.frame = CGRectMake(votocontra.frame.origin.x, votocontra.frame.origin.y-20, votocontra.frame.size.width, 10);
                            }else{
                                
                                votocontra.frame = CGRectMake(votocontra.frame.origin.x, votocontra.frame.origin.y+20, votocontra.frame.size.width, 10);
                            }

                            [votocontra setTitle:@"" forState:UIControlStateNormal];
                            votocontra.layer.cornerRadius = 0;
                            
                        } completion:^(BOOL finished){
                        
                            [UIView animateWithDuration:0.5 animations:^{
                            
                                votofavor.frame = CGRectMake(votofavor.frame.origin.x, votofavor.frame.origin.y, [[json objectForKey:@"votos"] intValue]*15000/100, 10);
                                votocontra.frame = CGRectMake(votofavor.frame.origin.x+votofavor.frame.size.width, votocontra.frame.origin.y, 150-([[json objectForKey:@"votos"] intValue]*15000/100), 10);
                            }];
                           
                        }];
                        
                        UILabel *numVotos;
                        
                        if([[UIScreen mainScreen] bounds].size.height <= 480){
                        
                            numVotos = [[UILabel alloc] initWithFrame:CGRectMake(0, 170, 340, 50)];
                        }else{
                            
                            numVotos = [[UILabel alloc] initWithFrame:CGRectMake(0, 360, 340, 50)];
                        }
                        
                        if([[json objectForKey:@"numeroDeVotos"] intValue] != 0){
                            [numVotos setText:[NSString stringWithFormat:NSLocalizedString(@"%d votos",nil),numeroDeVotos]];
                        }else{
                        
                            [numVotos setText:NSLocalizedString(@"0 votos",nil)];
                        }
                        
                        numVotos.textAlignment = NSTextAlignmentCenter;
                        
                        [numVotos setTextColor:[UIColor colorWithWhite:0.5 alpha:1]];
                        
                        [numVotos setFont:[UIFont fontWithName:numVotos.font.familyName size:13]];
                        
                        [vistaVotar addSubview:numVotos];
                
                    });
                }else{

                    dispatch_async(dispatch_get_main_queue(), ^{
                    
                        UIAlertController *nosepudo = [UIAlertController alertControllerWithTitle:@"Ups!" message:@"Ocurrió un error, por favor inténtalo otra vez" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *dismissnosepudo = [UIAlertAction actionWithTitle:NSLocalizedString(@"Muy bien", nil) style:UIAlertActionStyleCancel handler:nil];
                        
                        [nosepudo addAction:dismissnosepudo];
                        [self presentViewController:nosepudo animated:YES completion:nil];
                        app.networkActivityIndicatorVisible = NO;
                        
                        [votofavor setEnabled:YES];
                        [votocontra setEnabled:YES];
                    });
                }
            }
        
        }];
        
    });

}

-(void)aunAhi:(id)boton{

    UIColor *fondo = [[UIColor alloc] initWithWhite:0.5 alpha:1.0];
    UIButton *botonAunAhi = (UIButton*)boton;
    
    if([[botonAunAhi backgroundColor] isEqual:fondo]){
    
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Aún no", nil) message:NSLocalizedString(@"Este botón estará disponible 1 hora y 45 minutos después de tu publicación o join", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    }else{
    
        [boton setBackgroundColor:fondo];
        
        [timerAunAhiBoton invalidate];
        
        timerAunAhiBoton = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(checarSiEstaEnLugar:) userInfo:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]] repeats:YES];
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/addjoin.php"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1.0];
        
        [peticion setHTTPMethod:@"POST"];
        
        [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%d&origen=%d&estado=1&token=%@",[[defaults objectForKey:@"identifier"] intValue],[[[lugares objectForKey:@"lugar"] objectForKey:@"postOrigen"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                UIAlertController *error = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Ocurrió un error, por favor inténtalo de nuevo.", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *dismiss = [UIAlertAction actionWithTitle:NSLocalizedString(@"Muy bien", nil) style:UIAlertActionStyleCancel handler:nil];
                
                [error addAction:dismiss];
                
                [self presentViewController:error animated:YES completion:nil];
                
            }else{}
        }];
    }
    
}

-(IBAction)showAgregarLugar:(id)sender{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
   controladorTabControllerViewController *ida = [storyboard instantiateViewControllerWithIdentifier:@"tabController"];
    
    ida.selectedViewController = [ida.viewControllers objectAtIndex:2];
    [self presentViewController:ida animated:NO completion:nil];
    
}

-(void)checarSiEstaEnLugar:(NSTimer *)timer{
    
    if(![[timer userInfo] isEqual:[NSNull null]] &&[[NSDate date] timeIntervalSince1970] - [[timer userInfo] intValue] >= 6300){
        
        UIButton *aunahi = (UIButton*)[vistadecarga viewWithTag:10];
        
        [aunahi setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
    }
    
    if(![[timer userInfo] isEqual:[NSNull null]] && [[NSDate date] timeIntervalSince1970] - [[timer userInfo] intValue] > 7200){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [UIView animateWithDuration:0.4 animations:^(){
                
                vistadecarga.frame = CGRectMake(0, 468, 320, 468);
                
                for(UIView *elemento in [vistadecarga subviews]){
                
                    [elemento removeFromSuperview];
                }
                
                [vistadecarga setHidden:YES];
                
                [campoCompartir setEditable:YES];
                
                if(scrollview.contentOffset.x == 320.0 && localizacionHab == YES){
                    
                    [campoCompartir becomeFirstResponder];
                }
                
                [self activarBotonCheckinRapido];
                
                enLugar = NO;
            }];
        });
    }else{
    
        UIButton *botonahi = (UIButton*)[vistadecarga viewWithTag:10];
        
        if(botonahi == nil){
            
            UIButton *aunAhi = [[UIButton alloc] init];
            
            [aunAhi setTitle:NSLocalizedString(@"Aún estoy ahí", nil) forState:UIControlStateNormal];
            
            [aunAhi.titleLabel setFont:[UIFont fontWithName:aunAhi.titleLabel.font.familyName size:13.0]];
            
            if([[UIScreen mainScreen] bounds].size.height <= 480){
            
                [aunAhi setFrame:CGRectMake(85, 270, 150, 30)];
            }else{
                [aunAhi setFrame:CGRectMake(85, 350, 150, 30)];
            }
            
            
            if(![[[lugares objectForKey:@"lugar"] objectForKey:@"fecha"] isEqual:[NSNull null]] && ([[NSDate date] timeIntervalSince1970] - [[[lugares objectForKey:@"lugar"] objectForKey:@"fecha"] intValue]) <= 6300){
                
                [aunAhi setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:1.0]];
                
                [aunAhi addTarget:self action:@selector(aunAhi:) forControlEvents:UIControlEventTouchUpInside];
            }else{
                
                [aunAhi setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                
                [aunAhi addTarget:self action:@selector(aunAhi:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            [aunAhi setTitleColor:[UIColor colorWithRed:247/255.0 green:242/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
            
            [aunAhi.layer setCornerRadius:15];
            
            [aunAhi setTag:10];
            
            [vistadecarga addSubview:aunAhi];
        }
    }
}

-(IBAction)enviarMensajeLugar:(id)boton{
    
    UITextField *comentarLugar = (UITextField *)[vistaVotar viewWithTag:1];
    UIButton *botoncomentarios = (UIButton*)boton;
    UIButton *candado = [self.view viewWithTag:10];
    UIButton *verComentarios = (UIButton*)[vistaVotar viewWithTag:20];
    
    
    if(!seTomaFoto){
        
        if(!imagenCargada){
            
            if(![[comentarLugar text] isEqualToString:@""]){
                
                [boton setBackgroundColor:[UIColor grayColor]];
                [boton setEnabled:NO];
                
                UIApplication *app = [UIApplication sharedApplication];
                app.networkActivityIndicatorVisible = YES;
                
                if([comentarLugar isFirstResponder]){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [comentarLugar resignFirstResponder];
                        
                        [UIView animateWithDuration:0.1 animations:^{
                            [botoncomentarios setTitle:@"" forState:UIControlStateNormal];
                            [botoncomentarios setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                            [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y, 30, botoncomentarios.frame.size.height)];
                            
                            if(candado != nil){
                                [candado setFrame:CGRectMake(240, 482, 25, 30)];
                            }
                        }];
                        
                        [UIView animateWithDuration:0.4 animations:^{
                            
                            [comentarLugar setCenter:CGPointMake(comentarLugar.center.x, comentarLugar.center.y+250)];
                            [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y+250, botoncomentarios.frame.size.width, botoncomentarios.frame.size.height)];
                            [verComentarios setAlpha:1];
                        }];
                        seTomaFoto = YES;
                        app.networkActivityIndicatorVisible = NO;
                    });
                }else{
                    
                    [UIView animateWithDuration:0.1 animations:^{
                        [botoncomentarios setTitle:@"" forState:UIControlStateNormal];
                        [botoncomentarios setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                        [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y, 30, botoncomentarios.frame.size.height)];
                        
                        if(candado != nil){
                            [candado setFrame:CGRectMake(240, 482, 25, 30)];
                        }
                    }];
                }
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    
                    
                    NSData *dataComentarioLugar =[[comentarLugar text] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
                    
                    NSString *palabra = [[NSString alloc] initWithData:dataComentarioLugar encoding:NSUTF8StringEncoding];
                    
                    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/nuevomensajelugar.php"]];
                    
                    [peticion setHTTPMethod:@"POST"];
                    [peticion setHTTPBody:[[NSString stringWithFormat:@"msj=%@&idLugar=%d&usuario=%@&token=%@",palabra,[[[lugares objectForKey:@"lugar"] objectForKey:@"objID"] intValue],[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];

                    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                    
                    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                        
                        if(error.code == NSURLErrorTimedOut){
                            
                            
                        }else if(error.code == NSURLErrorNotConnectedToInternet){
                            
                            
                            
                        }else{
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [comentarLugar setText:@""];
                                [boton setEnabled:YES];
                                [boton setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                            });
                        }
                        
                    }];
                    
                });
                
            }else{
                
                if([comentarLugar isFirstResponder]){
                    
                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                        
                        [comentarLugar resignFirstResponder];
                        [UIView animateWithDuration:0.4 animations:^{
                            
                            [vistaVotar setFrame:CGRectMake(vistaVotar.frame.origin.x, vistaVotar.frame.origin.y + 100, vistaVotar.frame.size.width, vistaVotar.frame.size.height)];
                            
                            [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y+40, botoncomentarios.frame.size.width, botoncomentarios.frame.size.height)];
                        }];
                        
                    }else{
                        
                        [comentarLugar resignFirstResponder];
                        [UIView animateWithDuration:0.4 animations:^{
                            
                            [comentarLugar setCenter:CGPointMake(comentarLugar.center.x, comentarLugar.center.y+250)];
                            [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y+250, botoncomentarios.frame.size.width, botoncomentarios.frame.size.height)];
                            [verComentarios setAlpha:1];
                        }];
                    }
                }
            }
        }else{
            
            NSData *data =[[comentarLugar text] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
            NSString *textoFoto = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            [comentarLugar setText:@""];
            
            [UIView animateWithDuration:0.1 animations:^{
                [botoncomentarios setTitle:@"" forState:UIControlStateNormal];
                [botoncomentarios setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y, 30, botoncomentarios.frame.size.height)];
                
                if(candado != nil){
                    [candado setFrame:CGRectMake(240, 482, 25, 30)];
                }
            }];
            
            if([comentarLugar isFirstResponder]){
                
                UIButton *verComentarios = (UIButton*)[vistaVotar viewWithTag:20];
                
                [comentarLugar resignFirstResponder];
                [UIView animateWithDuration:0.4 animations:^{
                    
                    [comentarLugar setCenter:CGPointMake(comentarLugar.center.x, comentarLugar.center.y+250)];
                    [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y+250, botoncomentarios.frame.size.width, botoncomentarios.frame.size.height)];
                    [verComentarios setAlpha:1];
                }];
                
                [comentarLugar resignFirstResponder];
            }
            
            
            UIImageView *foto = (UIImageView *)[vistaVotar viewWithTag:101];
            //UIButton *cerrar = (UIButton *)[vistaVotar viewWithTag:102];
            
            //[cerrar removeFromSuperview];
            
            UIImageView *progressView = [[UIImageView alloc] initWithFrame:foto.frame];
            
            [progressView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
            
            [progressView setTag:104];
            
            [vistaVotar addSubview:progressView];
            
            UIApplication *app = [UIApplication sharedApplication];
            app.networkActivityIndicatorVisible = YES;
            
            
            bytesSubidosArchivoAsubir = 0;
            totalBytesArchivoAsubir = [dataFotoNueva length];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/subirfotoenlugar.php"]];
                
                [request setHTTPMethod:@"POST"];
                
                NSMutableData *cuerpo = [NSMutableData data];
                
                NSString *boundary = @"---------------------------14737809831466499882746641449";
                
                NSString *contenido = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                
                NSString *linea;
                
                [request addValue:contenido forHTTPHeaderField:@"Content-Type"];
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                if(videofoto == 1){
                    
                    linea = @"foto";
                    [cuerpo appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"foto\"; filename=\"%@.jpg\"\r\n",linea] dataUsingEncoding:NSUTF8StringEncoding]];
                }else if(videofoto == 2){
                    
                    linea = @"video";
                    [cuerpo appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"foto\"; filename=\"%@.mp4\"\r\n",linea] dataUsingEncoding:NSUTF8StringEncoding]];
                }
                
                linea = @"Content-Type: application/octet-stream\r\n\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                [cuerpo appendData:dataFotoNueva];
                linea = @"\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                linea = @"Content-Disposition: form-data; name=\"id\"\r\n\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                [cuerpo appendData:[[NSString stringWithFormat:@"%@",[defaults objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding]];
                linea = @"\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                linea = @"Content-Disposition: form-data; name=\"lugar\"\r\n\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                [cuerpo appendData:[[NSString stringWithFormat:@"%d",[[[lugares objectForKey:@"lugar"] objectForKey:@"objID"] intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
                linea = @"\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                linea = @"Content-Disposition: form-data; name=\"mensaje\"\r\n\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                [cuerpo appendData:[[NSString stringWithFormat:@"%@",textoFoto] dataUsingEncoding:NSUTF8StringEncoding]];
                linea = @"\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                [request setHTTPBody:cuerpo];
                
                NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                
                NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                horainicial = [[NSDate date] timeIntervalSince1970];
                NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{

                        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                            
                            NSLog(@"error al subir video/foto");
                        }else{
                            
                            NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                            
                            if(![respuesta isEqualToString:@"error"]){
                                
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    app.networkActivityIndicatorVisible = NO;
                                    [foto removeFromSuperview];
                                    [progressView removeFromSuperview];
                                    seTomaFoto = YES;
                                    imagenCargada = NO;
                                    
                                    UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:dataFotoNueva], nil, nil, nil);
                                });
                            }else{
                                
                                [[[UIAlertView alloc] initWithTitle:@"Hubo un error" message:@"La foto no pudo subirse al servidor, por favor intentalo nuevamente" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                                
                                app.networkActivityIndicatorVisible = NO;
                                [progressView removeFromSuperview];
                                
                                UIButton *cerrar = [[UIButton alloc] initWithFrame:CGRectMake((foto.frame.origin.x+foto.frame.size.width)-5, foto.frame.origin.y-5, 15, 15)];
                                
                                [cerrar setTag:102];
                                [cerrar setBackgroundImage:[UIImage imageNamed:@"tachafnegro.png"] forState:UIControlStateNormal];
                                
                                [cerrar addTarget:self action:@selector(cerrarImagen:) forControlEvents:UIControlEventTouchUpInside];
                                
                                [vistaVotar addSubview:cerrar];
                                
                            }
                        }
                    });
                }];
                
                [uploadTask resume];
                
            });
        }
    }else{
        
        if([comentarLugar isFirstResponder]){
            
            UIButton *verComentarios = (UIButton*)[vistaVotar viewWithTag:20];
            
            [comentarLugar resignFirstResponder];
            [UIView animateWithDuration:0.4 animations:^{
                
                [comentarLugar setCenter:CGPointMake(comentarLugar.center.x, comentarLugar.center.y+250)];
                [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y+250, botoncomentarios.frame.size.width, botoncomentarios.frame.size.height)];
                [verComentarios setAlpha:1];
            }];
        }
    
        if([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied){
            
            UIAlertController *noPermisoCamara = [UIAlertController alertControllerWithTitle:nil message:@"Crowte no tiene permiso para acceder a tu cámara." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){}];
            
            UIAlertAction *cambiar = [UIAlertAction actionWithTitle:@"Ir a la configuración" style:UIAlertActionStyleDefault handler:^(UIAlertAction *accion){
                
                [self dismissViewControllerAnimated:noPermisoCamara completion:nil];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }];
            
            [noPermisoCamara addAction:cambiar];
            [noPermisoCamara addAction:cancel];
            
            [self presentViewController:noPermisoCamara animated:YES completion:nil];
        }else if([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined){
        
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted){
            
                if(granted){
                    
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted){
                    
                    
                        if(granted){
                        
                            camara = [[CamaraOverlayView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
                            
                            picker = [[UIImagePickerController alloc] init];
                            
                            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                            
                            picker.showsCameraControls = NO;
                            picker.toolbarHidden = YES;
                            picker.navigationBarHidden = YES;
                            picker.cameraOverlayView = self.view;
                            
                            picker.edgesForExtendedLayout = UIRectEdgeAll;
                            CGAffineTransform translate = CGAffineTransformMakeTranslation(0.0, 25.0);
                            picker.cameraViewTransform = CGAffineTransformScale(translate, 480.0/430.0, 680.0/430.0);
                            
                            [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
                            
                            picker.cameraOverlayView = camara;
                            
                            [picker setDelegate:self];
                            
                            UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tomarVideo:)];
                            
                            [camara.trigger addGestureRecognizer:longTap];
                            [camara.trigger addTarget:self action:@selector(tomarFoto:) forControlEvents:UIControlEventTouchUpInside];
                            
                            [camara.cambiarCamara addTarget:self action:@selector(cambiarCamara) forControlEvents:UIControlEventTouchUpInside];
                            
                            [camara.flash addTarget:self action:@selector(flash) forControlEvents:UIControlEventTouchUpInside];
                            
                            [camara.tacha addTarget:self action:@selector(cerrarCamara) forControlEvents:UIControlEventTouchUpInside];
                            
                            CamaraOverlayViewController *cm = [[CamaraOverlayViewController alloc] init];
                            
                            [cm setDelegate:self];
                            
                            [self presentViewController:cm animated:YES completion:nil];
                        }
                    }];
                }
            }];
        }else{
            
            /* UIImagePickerController *picker = [[UIImagePickerController alloc] init];
             picker.delegate = self;
             picker.allowsEditing = NO;
             picker.sourceType = UIImagePickerControllerSourceTypeCamera;
             
             [self presentViewController:picker animated:YES completion:^{}];*/
            
            
            camara = [[CamaraOverlayView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
            
            picker = [[UIImagePickerController alloc] init];
            
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            picker.showsCameraControls = NO;
            picker.toolbarHidden = YES;
            picker.navigationBarHidden = YES;
            picker.cameraOverlayView = self.view;
            
            picker.edgesForExtendedLayout = UIRectEdgeAll;
            CGAffineTransform translate = CGAffineTransformMakeTranslation(0.0, 25.0);
            picker.cameraViewTransform = CGAffineTransformScale(translate, 480.0/430.0, 680.0/430.0);
            //picker.cameraViewTransform = CGAffineTransformScale(picker.cameraViewTransform, 1, 1.12412);
            
            [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
            
            picker.cameraOverlayView = camara;
            
            [picker setDelegate:self];
            
            UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tomarVideo:)];
            
            [camara.trigger addGestureRecognizer:longTap];
            [camara.trigger addTarget:self action:@selector(tomarFoto:) forControlEvents:UIControlEventTouchUpInside];
            
            [camara.cambiarCamara addTarget:self action:@selector(cambiarCamara) forControlEvents:UIControlEventTouchUpInside];
            
            [camara.flash addTarget:self action:@selector(flash) forControlEvents:UIControlEventTouchUpInside];
            
            [camara.tacha addTarget:self action:@selector(cerrarCamara) forControlEvents:UIControlEventTouchUpInside];
            
            CamaraOverlayViewController *cm = [[CamaraOverlayViewController alloc] init];
            
            [cm setDelegate:self];
            
            [self presentViewController:cm animated:YES completion:nil];
        }
    }
    
}

-(void)enviarMensajeLugar:(id)boton comentario:(NSString *)comentario{

    UITextField *comentarLugar = (UITextField *)[vistaVotar viewWithTag:1];
    UIButton *botoncomentarios = (UIButton*)boton;
    UIButton *candado = [self.view viewWithTag:10];
    UIButton *verComentarios = (UIButton*)[vistaVotar viewWithTag:20];
    
    
    if(!seTomaFoto){
        
        if(!imagenCargada){
            
            if(![[comentarLugar text] isEqualToString:@""]){
                
                [boton setBackgroundColor:[UIColor grayColor]];
                [boton setEnabled:NO];
                
                UIApplication *app = [UIApplication sharedApplication];
                app.networkActivityIndicatorVisible = YES;
                
                if([comentarLugar isFirstResponder]){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [comentarLugar resignFirstResponder];
                        
                        [UIView animateWithDuration:0.1 animations:^{
                            [botoncomentarios setTitle:@"" forState:UIControlStateNormal];
                            [botoncomentarios setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                            [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y, 30, botoncomentarios.frame.size.height)];
                            
                            if(candado != nil){
                                [candado setFrame:CGRectMake(240, 482, 25, 30)];
                            }
                        }];
                        
                        [UIView animateWithDuration:0.4 animations:^{
                            
                            [comentarLugar setCenter:CGPointMake(comentarLugar.center.x, comentarLugar.center.y+250)];
                            [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y+250, botoncomentarios.frame.size.width, botoncomentarios.frame.size.height)];
                            [verComentarios setAlpha:1];
                        }];
                        seTomaFoto = YES;
                        app.networkActivityIndicatorVisible = NO;
                    });
                }else{
                    
                    [UIView animateWithDuration:0.1 animations:^{
                        [botoncomentarios setTitle:@"" forState:UIControlStateNormal];
                        [botoncomentarios setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                        [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y, 30, botoncomentarios.frame.size.height)];
                        
                        if(candado != nil){
                            [candado setFrame:CGRectMake(240, 482, 25, 30)];
                        }
                    }];
                }
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    
                    
                    NSData *dataComentarioLugar =[[comentarLugar text] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
                    
                    NSString *palabra = [[NSString alloc] initWithData:dataComentarioLugar encoding:NSUTF8StringEncoding];
                    
                    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/nuevomensajelugar.php"]];
                    
                    [peticion setHTTPMethod:@"POST"];
                    [peticion setHTTPBody:[[NSString stringWithFormat:@"msj=%@&idLugar=%d&usuario=%@&token=%@",palabra,[[[lugares objectForKey:@"lugar"] objectForKey:@"objID"] intValue],[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                    
                    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                        
                        if(error.code == NSURLErrorTimedOut){
                            
                            
                        }else if(error.code == NSURLErrorNotConnectedToInternet){
                            
                            
                            
                        }else{
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [comentarLugar setText:@""];
                                [boton setEnabled:YES];
                                [boton setBackgroundColor:[UIColor colorWithRed:0 green:111/255.0 blue:147/255.0 alpha:1.0]];
                            });
                        }
                        
                    }];
                    
                });
                
            }else{
                
                if([comentarLugar isFirstResponder]){
                    
                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                        
                        [comentarLugar resignFirstResponder];
                        [UIView animateWithDuration:0.4 animations:^{
                            
                            [vistaVotar setFrame:CGRectMake(vistaVotar.frame.origin.x, vistaVotar.frame.origin.y + 100, vistaVotar.frame.size.width, vistaVotar.frame.size.height)];
                            
                            [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y+40, botoncomentarios.frame.size.width, botoncomentarios.frame.size.height)];
                        }];
                        
                    }else{
                        
                        [comentarLugar resignFirstResponder];
                        [UIView animateWithDuration:0.4 animations:^{
                            
                            [comentarLugar setCenter:CGPointMake(comentarLugar.center.x, comentarLugar.center.y+250)];
                            [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y+250, botoncomentarios.frame.size.width, botoncomentarios.frame.size.height)];
                            [verComentarios setAlpha:1];
                        }];
                    }
                }
            }
        }else{
            
            NSData *data =[[comentarLugar text] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
            NSString *textoFoto = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            [comentarLugar setText:@""];
            
            [UIView animateWithDuration:0.1 animations:^{
                [botoncomentarios setTitle:@"" forState:UIControlStateNormal];
                [botoncomentarios setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
                [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y, 30, botoncomentarios.frame.size.height)];
                
                if(candado != nil){
                    [candado setFrame:CGRectMake(240, 482, 25, 30)];
                }
            }];
            
            if([comentarLugar isFirstResponder]){
                
                UIButton *verComentarios = (UIButton*)[vistaVotar viewWithTag:20];
                
                [comentarLugar resignFirstResponder];
                [UIView animateWithDuration:0.4 animations:^{
                    
                    [comentarLugar setCenter:CGPointMake(comentarLugar.center.x, comentarLugar.center.y+250)];
                    [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y+250, botoncomentarios.frame.size.width, botoncomentarios.frame.size.height)];
                    [verComentarios setAlpha:1];
                }];
                
                [comentarLugar resignFirstResponder];
            }
            
            
            UIImageView *foto = (UIImageView *)[vistaVotar viewWithTag:101];
            //UIButton *cerrar = (UIButton *)[vistaVotar viewWithTag:102];
            
            //[cerrar removeFromSuperview];
            
            UIImageView *progressView = [[UIImageView alloc] initWithFrame:foto.frame];
            
            [progressView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
            
            [progressView setTag:104];
            
            [vistaVotar addSubview:progressView];
            
            UIApplication *app = [UIApplication sharedApplication];
            app.networkActivityIndicatorVisible = YES;
            
            
            bytesSubidosArchivoAsubir = 0;
            totalBytesArchivoAsubir = [dataFotoNueva length];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/subirfotoenlugar.php"]];
                
                [request setHTTPMethod:@"POST"];
                
                NSMutableData *cuerpo = [NSMutableData data];
                
                NSString *boundary = @"---------------------------14737809831466499882746641449";
                
                NSString *contenido = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                
                NSString *linea;
                
                [request addValue:contenido forHTTPHeaderField:@"Content-Type"];
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                if(videofoto == 1){
                    
                    linea = @"foto";
                    [cuerpo appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"foto\"; filename=\"%@.jpg\"\r\n",linea] dataUsingEncoding:NSUTF8StringEncoding]];
                }else if(videofoto == 2){
                    
                    linea = @"video";
                    [cuerpo appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"foto\"; filename=\"%@.mp4\"\r\n",linea] dataUsingEncoding:NSUTF8StringEncoding]];
                }
                
                linea = @"Content-Type: application/octet-stream\r\n\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                [cuerpo appendData:dataFotoNueva];
                linea = @"\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                linea = @"Content-Disposition: form-data; name=\"id\"\r\n\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                [cuerpo appendData:[[NSString stringWithFormat:@"%@",[defaults objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding]];
                linea = @"\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                linea = @"Content-Disposition: form-data; name=\"lugar\"\r\n\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                [cuerpo appendData:[[NSString stringWithFormat:@"%d",[[[lugares objectForKey:@"lugar"] objectForKey:@"objID"] intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
                linea = @"\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                linea = @"Content-Disposition: form-data; name=\"mensaje\"\r\n\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                [cuerpo appendData:[[NSString stringWithFormat:@"%@",textoFoto] dataUsingEncoding:NSUTF8StringEncoding]];
                linea = @"\r\n";
                [cuerpo appendData:[linea dataUsingEncoding:NSUTF8StringEncoding]];
                
                [cuerpo appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                [request setHTTPBody:cuerpo];
                
                NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
                
                NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
                horainicial = [[NSDate date] timeIntervalSince1970];
                NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                            
                            NSLog(@"error al subir video/foto");
                        }else{
                            
                            NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                            
                            if(![respuesta isEqualToString:@"error"]){
                                
                                NSError *error;
                                
                                NSDictionary *datosVideo = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    app.networkActivityIndicatorVisible = NO;
                                    [foto removeFromSuperview];
                                    [progressView removeFromSuperview];
                                    seTomaFoto = YES;
                                    imagenCargada = NO;
                                    
                                    NSArray *nomFormato = [[datosVideo objectForKey:@"nombre"] componentsSeparatedByString:@"."];
                                    NSString *formato = nomFormato[[nomFormato count]-1];
                                    
                                    if([formato isEqualToString:@"jpg"]){
                                        
                                        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/publicaciones/%@.jpg",[[defaults objectForKey:@"identifier"] intValue],[nomFormato objectAtIndex:0]]];
                                        FBSDKSharePhoto *photo = [FBSDKSharePhoto photoWithImageURL:imageURL userGenerated:NO];
                                        NSDictionary *properties = @{
                                                                     @"og:type": @"crowtew:photo",
                                                                     @"og:title": [[lugares objectForKey:@"lugar"] objectForKey:@"lugar"],
                                                                     @"og:url": [NSString stringWithFormat: @"https://www.crowte.com/lugar.php?id=%d",[[[lugares objectForKey:@"lugar"] objectForKey:@"objID"] intValue]],
                                                                     @"og:image": @[photo]
                                                                     };
                                        FBSDKShareOpenGraphObject *object = [FBSDKShareOpenGraphObject objectWithProperties:properties];
                                    
                                        FBSDKShareOpenGraphAction *action = [[FBSDKShareOpenGraphAction alloc] init];
                                        action.actionType = @"crowtew:took";
                                        
                                        [action setObject:object forKey:@"photo"];
                                        
                                        FBSDKShareOpenGraphContent *content = [[FBSDKShareOpenGraphContent alloc] init];
                                        content.action = action;
                                        content.previewPropertyName = @"photo";
                                        
                                        FBSDKShareAPI *shareAPI = [[FBSDKShareAPI alloc] init];
                                        // optionally set the delegate
                                         shareAPI.delegate = self;
                                        shareAPI.shareContent = content;
                                        
                                        if(![comentario isEqualToString:@""]){
                                            
                                            shareAPI.message = comentario;
                                        }
                                        
                                        [shareAPI share];
                                        
                                    }else if([formato isEqualToString:@"mp4"]){
                                        
                                        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/publicaciones/thumbnail/%@.jpg",[[defaults objectForKey:@"identifier"] intValue],[nomFormato objectAtIndex:0]]];
                                        FBSDKSharePhoto *photo = [FBSDKSharePhoto photoWithImageURL:imageURL userGenerated:NO];
                                        NSDictionary *properties = @{
                                                                     @"og:type": @"video.other",
                                                                     @"og:title": [[lugares objectForKey:@"lugar"] objectForKey:@"lugar"],
                                                                     @"og:url": [NSString stringWithFormat: @"https://www.crowte.com/lugar.php?id=%d",[[[lugares objectForKey:@"lugar"] objectForKey:@"objID"] intValue]],
                                                                     @"og:image": @[photo]
                                                                     };
                                        
                                        FBSDKShareOpenGraphObject *object = [FBSDKShareOpenGraphObject objectWithProperties:properties];
                                       
                                        FBSDKShareOpenGraphAction *action = [[FBSDKShareOpenGraphAction alloc] init];
                                        action.actionType = @"crowtew:record";
                                        [action setObject:object forKey:@"other"];
                                        FBSDKShareOpenGraphContent *content = [[FBSDKShareOpenGraphContent alloc] init];
                                        content.action = action;
                                        content.previewPropertyName = @"other";
                                        FBSDKShareAPI *shareAPI = [[FBSDKShareAPI alloc] init];
                                        // optionally set the delegate
                                        shareAPI.delegate = self;
                                        shareAPI.shareContent = content;
                                        
                                        if(![comentario isEqualToString:@""]){
                                            
                                            shareAPI.message = comentario;
                                        }
                                        
                                        [shareAPI share];
                                        
                                    }
                                    
                                    UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:dataFotoNueva], nil, nil, nil);
                                });
                            }else{
                                
                                [[[UIAlertView alloc] initWithTitle:@"Hubo un error" message:@"La foto no pudo subirse al servidor, por favor intentalo nuevamente" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                                
                                app.networkActivityIndicatorVisible = NO;
                                [progressView removeFromSuperview];
                                
                                UIButton *cerrar = [[UIButton alloc] initWithFrame:CGRectMake((foto.frame.origin.x+foto.frame.size.width)-5, foto.frame.origin.y-5, 15, 15)];
                                
                                [cerrar setTag:102];
                                [cerrar setBackgroundImage:[UIImage imageNamed:@"tachafnegro.png"] forState:UIControlStateNormal];
                                
                                [cerrar addTarget:self action:@selector(cerrarImagen:) forControlEvents:UIControlEventTouchUpInside];
                                
                                [vistaVotar addSubview:cerrar];
                                
                            }
                        }
                    });
                }];
                
                [uploadTask resume];
                
            });
        }
    }else{
        
        if([comentarLugar isFirstResponder]){
            
            UIButton *verComentarios = (UIButton*)[vistaVotar viewWithTag:20];
            
            [comentarLugar resignFirstResponder];
            [UIView animateWithDuration:0.4 animations:^{
                
                [comentarLugar setCenter:CGPointMake(comentarLugar.center.x, comentarLugar.center.y+250)];
                [botoncomentarios setFrame:CGRectMake(botoncomentarios.frame.origin.x, botoncomentarios.frame.origin.y+250, botoncomentarios.frame.size.width, botoncomentarios.frame.size.height)];
                [verComentarios setAlpha:1];
            }];
        }
        
        if([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied){
            
            UIAlertController *noPermisoCamara = [UIAlertController alertControllerWithTitle:nil message:@"Crowte no tiene permiso para acceder a tu cámara." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *accion){}];
            
            UIAlertAction *cambiar = [UIAlertAction actionWithTitle:@"Ir a la configuración" style:UIAlertActionStyleDefault handler:^(UIAlertAction *accion){
                
                [self dismissViewControllerAnimated:noPermisoCamara completion:nil];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }];
            
            [noPermisoCamara addAction:cambiar];
            [noPermisoCamara addAction:cancel];
            
            [self presentViewController:noPermisoCamara animated:YES completion:nil];
        }else if([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined){
            
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted){
                
                if(granted){
                    
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted){
                        
                        
                        if(granted){
                            
                            camara = [[CamaraOverlayView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
                            
                            picker = [[UIImagePickerController alloc] init];
                            
                            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                            
                            picker.showsCameraControls = NO;
                            picker.toolbarHidden = YES;
                            picker.navigationBarHidden = YES;
                            picker.cameraOverlayView = self.view;
                            
                            picker.edgesForExtendedLayout = UIRectEdgeAll;
                            CGAffineTransform translate = CGAffineTransformMakeTranslation(0.0, 25.0);
                            picker.cameraViewTransform = CGAffineTransformScale(translate, 480.0/430.0, 680.0/430.0);
                            
                            [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
                            
                            picker.cameraOverlayView = camara;
                            
                            [picker setDelegate:self];
                            
                            UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tomarVideo:)];
                            
                            [camara.trigger addGestureRecognizer:longTap];
                            [camara.trigger addTarget:self action:@selector(tomarFoto:) forControlEvents:UIControlEventTouchUpInside];
                            
                            [camara.cambiarCamara addTarget:self action:@selector(cambiarCamara) forControlEvents:UIControlEventTouchUpInside];
                            
                            [camara.flash addTarget:self action:@selector(flash) forControlEvents:UIControlEventTouchUpInside];
                            
                            [camara.tacha addTarget:self action:@selector(cerrarCamara) forControlEvents:UIControlEventTouchUpInside];
                            
                            CamaraOverlayViewController *cm = [[CamaraOverlayViewController alloc] init];
                            
                            [cm setDelegate:self];
                            
                            [self presentViewController:cm animated:YES completion:nil];
                        }
                    }];
                }
            }];
        }else{
            
            /* UIImagePickerController *picker = [[UIImagePickerController alloc] init];
             picker.delegate = self;
             picker.allowsEditing = NO;
             picker.sourceType = UIImagePickerControllerSourceTypeCamera;
             
             [self presentViewController:picker animated:YES completion:^{}];*/
            
            
            camara = [[CamaraOverlayView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
            
            picker = [[UIImagePickerController alloc] init];
            
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            picker.showsCameraControls = NO;
            picker.toolbarHidden = YES;
            picker.navigationBarHidden = YES;
            picker.cameraOverlayView = self.view;
            
            picker.edgesForExtendedLayout = UIRectEdgeAll;
            CGAffineTransform translate = CGAffineTransformMakeTranslation(0.0, 25.0);
            picker.cameraViewTransform = CGAffineTransformScale(translate, 480.0/430.0, 680.0/430.0);
            //picker.cameraViewTransform = CGAffineTransformScale(picker.cameraViewTransform, 1, 1.12412);
            
            [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
            
            picker.cameraOverlayView = camara;
            
            [picker setDelegate:self];
            
            UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tomarVideo:)];
            
            [camara.trigger addGestureRecognizer:longTap];
            [camara.trigger addTarget:self action:@selector(tomarFoto:) forControlEvents:UIControlEventTouchUpInside];
            
            [camara.cambiarCamara addTarget:self action:@selector(cambiarCamara) forControlEvents:UIControlEventTouchUpInside];
            
            [camara.flash addTarget:self action:@selector(flash) forControlEvents:UIControlEventTouchUpInside];
            
            [camara.tacha addTarget:self action:@selector(cerrarCamara) forControlEvents:UIControlEventTouchUpInside];
            
            CamaraOverlayViewController *cm = [[CamaraOverlayViewController alloc] init];
            
            [cm setDelegate:self];
            
            [self presentViewController:cm animated:YES completion:nil];
        }
    }
}

-(IBAction)getNuevasNotificaciones{
    
    UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStyleDone target:self action:nil];
    
    NSUserDefaults *datosUsuario = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/getnotificaciones.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&token=%@",[[datosUsuario objectForKey:@"identifier"] intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(data == NULL){
        
            [self getNuevasNotificaciones];
        }else{
            
            NSString *respuesta = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            if(![respuesta isEqual:[NSNull null]] && [respuesta intValue] > 0){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIBarButtonItem *notificacionesBoton = [[UIBarButtonItem alloc] initWithTitle:respuesta style:UIBarButtonItemStylePlain target:self action:@selector(showNotificaciones:)];
                    
                    [notificacionesBoton setBackgroundImage:[UIImage imageNamed:@"fondoNotificaciones.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
                    
                    self.navigationItem.leftBarButtonItem = notificacionesBoton;
                    
                });
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIBarButtonItem *notificacionesBoton = [[UIBarButtonItem alloc] initWithTitle:@"-" style:UIBarButtonItemStylePlain target:self action:@selector(showNotificaciones:)];
                    self.navigationItem.leftBarButtonItem = notificacionesBoton;
                });
            }
        }
        
    }];
    
    self.navigationItem.backBarButtonItem = atras;
}

-(void)actualizaNotificaciones:(NSNotification *)notificacion{
    
    NSString *notificacionString = (NSString *)[notificacion object];
    
    if(![notificacionString isEqual:[NSNull null]] && [notificacionString intValue] > 0){
        
        
        UIBarButtonItem *notificacionesBoton = [[UIBarButtonItem alloc] initWithTitle:notificacionString style:UIBarButtonItemStylePlain target:self action:@selector(showNotificaciones:)];
        
        [notificacionesBoton setBackgroundImage:[UIImage imageNamed:@"fondoNotificaciones.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
        self.navigationItem.leftBarButtonItem = notificacionesBoton;
        
    }else{
        
        
        UIBarButtonItem *notificacionesBoton = [[UIBarButtonItem alloc] initWithTitle:@"-" style:UIBarButtonItemStylePlain target:self action:@selector(showNotificaciones:)];
        self.navigationItem.leftBarButtonItem = notificacionesBoton;
    }
}

-(IBAction)verificarcorreo:(id)sender{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivarCuentaViewController *ida = [storyboard instantiateViewControllerWithIdentifier:@"insertacodigo"];
    [self presentViewController:ida animated:YES completion:NULL];
}

-(BOOL)verificarVoto{

    BOOL votado;
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/verificarvoto.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&lugar=%@&token=%@",miID,[[lugares objectForKey:@"lugar"] objectForKey:@"objID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
        }else{

            NSDictionary *voto = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            if([[voto objectForKey:@"voto"] intValue] == 1){
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIView *votosfavor;
                    
                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                    
                        votosfavor = [[UIView alloc] initWithFrame:CGRectMake(90, 310, [[[voto objectForKey:@"votos"] objectForKey:@"votos"] intValue]*15000/100, 10)];
                    }else{
                        
                        votosfavor = [[UIView alloc] initWithFrame:CGRectMake(90, 360, [[[voto objectForKey:@"votos"] objectForKey:@"votos"] intValue]*15000/100, 10)];
                    }
                    
                    [votosfavor setBackgroundColor:[UIColor colorWithRed:0 green:0.9 blue:0 alpha:1.0]];
                    
                    [vistaVotar addSubview:votosfavor];
                    
                    UIView *votoscontra;
                    
                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                    
                        votoscontra = [[UIView alloc] initWithFrame:CGRectMake(votosfavor.frame.origin.x+votosfavor.frame.size.width, 310, 150-votosfavor.frame.size.width, 10)];
                    }else{
                        
                        votoscontra = [[UIView alloc] initWithFrame:CGRectMake(votosfavor.frame.origin.x+votosfavor.frame.size.width, 360, 150-votosfavor.frame.size.width, 10)];
                    }
                    
                    [votoscontra setBackgroundColor:[UIColor colorWithRed:0.7 green:0 blue:0 alpha:1]];
                    
                    [vistaVotar addSubview:votoscontra];
                    
                    UILabel *numVotos;
                    
                    if([[UIScreen mainScreen] bounds].size.height <= 480){
                    
                        numVotos = [[UILabel alloc] initWithFrame:CGRectMake(0, 310, 340, 50)];
                    }else{
                        
                        numVotos = [[UILabel alloc] initWithFrame:CGRectMake(0, 360, 340, 50)];
                    }
                    
                    if(![[[voto objectForKey:@"votos"] objectForKey:@"numeroDeVotos"] isEqual:[NSNull null]] && [[[voto objectForKey:@"votos"] objectForKey:@"numeroDeVotos"] intValue] != 0){
                        
                        [numVotos setText:[NSString stringWithFormat:NSLocalizedString(@"%d votos",nil),[[[voto objectForKey:@"votos"] objectForKey:@"numeroDeVotos"] intValue]]];
                    }else{
                        
                        [numVotos setText:NSLocalizedString(@"0 votos",nil)];
                    }
                    
                    numVotos.textAlignment = NSTextAlignmentCenter;
                    
                    [numVotos setTextColor:[UIColor colorWithWhite:0.5 alpha:1]];
                    
                    [numVotos setFont:[UIFont fontWithName:numVotos.font.familyName size:13]];
                    
                    [vistaVotar addSubview:numVotos];
                    
                    UIButton *votofavor = (UIButton*)[vistaVotar viewWithTag:3];
                    UIButton *votocontra = (UIButton*)[vistaVotar viewWithTag:4];
                    
                    [votofavor removeFromSuperview];
                    [votocontra removeFromSuperview];
                });
                
            }else{
            
                dispatch_async(dispatch_get_main_queue(), ^{
                
                    [self verificarVoto];
                });
            }
        }
    }];
    
    return votado;
}

-(void)animacionTutoEtiquetar:(UILabel *)arroba doblew:(UILabel *)w i:(UILabel *)i n:(UILabel *)n{
    
    [UIView animateKeyframesWithDuration:2 delay:0.0 options: UIViewKeyframeAnimationOptionRepeat animations:^{
        
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:1 animations:^{
            
            [arroba setAlpha:1];
        }];
        
        [UIView addKeyframeWithRelativeStartTime:0.1 relativeDuration:1 animations:^{
            
            [w setAlpha:1];
        }];
        
        [UIView addKeyframeWithRelativeStartTime:0.2 relativeDuration:1 animations:^{
            
            [i setAlpha:1];
        }];
        
        [UIView addKeyframeWithRelativeStartTime:0.3 relativeDuration:1 animations:^{
            
            [n setAlpha:1];
        }];
    } completion:nil];
}

-(void)cerrarEtiquetaLugarTuto:(UIButton *)boton{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [tutoriales setValue:@"1" forKey:@"iosEtiquetaLugar"];
        [[vistados viewWithTag:100] removeFromSuperview];
        [campoCompartir becomeFirstResponder];
        
        NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarcompartirtuto.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%@&token=%@",[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){}];
    });
}

-(IBAction)mostrarTodosComentarios:(id)sender{

    arregloComentarios = nil;
    
    UIView *comentariosView = [[UIView alloc] initWithFrame:CGRectMake(50, 50, self.vistaVotar.frame.size.width-50, self.vistaVotar.frame.size.height-50)];
    
    if([[UIScreen mainScreen] bounds].size.height < 560){
    
        comentariosView.frame = CGRectMake(50, 50, self.vistaVotar.frame.size.width-50, self.vistaVotar.frame.size.height-150);
    }
    
    //UIView *comentariosView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, self.vistaVotar.frame.size.width, self.vistaVotar.frame.size.height-50)];
    
    [comentariosView setBackgroundColor:[UIColor whiteColor]];
    
    UIButton *cerrar = [[UIButton alloc] initWithFrame:CGRectMake(comentariosView.frame.size.width-15, 0, 15, 15)];
    
    [cerrar setBackgroundImage:[UIImage imageNamed:@"tachafnegro.png"] forState:UIControlStateNormal];

    [cerrar addTarget:self action:@selector(cerrarTodosComentarios:) forControlEvents:UIControlEventTouchUpInside];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [activityIndicator setCenter:CGPointMake(comentariosView.center.x-50, comentariosView.center.y)];
    
    [activityIndicator startAnimating];
    
    UITableView *comentariosTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 20, comentariosView.frame.size.width, comentariosView.frame.size.height-20)];
    
    [comentariosTableView setTag:1000];
    
    [comentariosTableView setDelegate:self];
    [comentariosTableView setDataSource:self];
    
    [comentariosTableView setAlpha:0];
    
    [comentariosView addSubview:comentariosTableView];
    [comentariosView addSubview:cerrar];
    [comentariosView addSubview:activityIndicator];
    
    popup = [KLCPopup popupWithContentView:comentariosView showType:KLCPopupShowTypeBounceIn dismissType:KLCPopupDismissTypeBounceOut maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:YES dismissOnContentTouch:NO];

    __weak typeof(self) selfDebil = self;
    
    popup.didFinishDismissingCompletion = ^{
        
        [selfDebil.reproductor pause];
    };
    
    [popup show];
    
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getmensajeslugar.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&usuario=%@&ultimo=0&token=%@",[[[lugares objectForKey:@"lugar"] objectForKey:@"objID"] intValue],[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
        }else{

            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

            if(json != nil){
                
                arregloComentarios = [NSMutableArray array];
                
                for(NSDictionary *intervalo in json){
                    
                    [arregloComentarios addObject:intervalo];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
            
                [activityIndicator stopAnimating];
            
                [comentariosTableView setAlpha:1];
                [comentariosTableView reloadData];
            });
        }
    }];
}

-(IBAction)cerrarTodosComentarios:(id)sender{

    [reproductor pause];
    [popup dismiss:YES];
}

-(IBAction)likeComentario:(id)sender{
    
    
    if([sender tag] == 5){
        UIButton *corazon = (UIButton *)sender;
        UIButton *numDeAmor = (UIButton *) [[sender superview] viewWithTag:6];
        UITableViewCell *celda = (UITableViewCell *)[[sender superview] superview];
        NSIndexPath *indexPath = [(UITableView*)[[[[sender superview] superview] superview] superview] indexPathForCell:celda];
        
        
        if([[corazon backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"grayheart.png"]]){
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&mensajeID=%@&token=%@&act=1",[[defaults objectForKey:@"identifier"] intValue], [[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensajeID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
            }];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [corazon setBackgroundImage:[UIImage imageNamed:@"redheart.png"] forState:UIControlStateNormal];
                [numDeAmor setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                int num = [[[numDeAmor titleLabel]text] intValue];
                [numDeAmor setTitle:[NSString stringWithFormat:@"%d",num++] forState:UIControlStateNormal];
            });
        }else{
            
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&mensajeID=%@&token=%@&act=2",[[defaults objectForKey:@"identifier"] intValue], [[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensajeID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [corazon setBackgroundImage:[UIImage imageNamed:@"grayheart.png"] forState:UIControlStateNormal];
                [numDeAmor setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                int num = [[[numDeAmor titleLabel]text] intValue];
                if(num == 0){
                    [numDeAmor setTitle:@"" forState:UIControlStateNormal];
                }else{
                    [numDeAmor setTitle:[NSString stringWithFormat:@"%d",--num] forState:UIControlStateNormal];
                }
            });
        }
        
    }else if([sender tag] == 6){
        UIButton *numDeAmor = (UIButton *)sender;
        UIButton *corazon = (UIButton *) [[sender superview] viewWithTag:5];
        UITableViewCell *celda = (UITableViewCell *)[[sender superview] superview];
        NSIndexPath *indexPath = [(UITableView*)[[[sender superview] superview] superview] indexPathForCell:celda];
        
        
        if([[corazon backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"grayheart.png"]]){
            
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&mensajeID=%@&token=%@&act=1",[[defaults objectForKey:@"identifier"] intValue], [[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensajeID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
            }];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [corazon setBackgroundImage:[UIImage imageNamed:@"redheart.png"] forState:UIControlStateNormal];
                [numDeAmor setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                int num = [[[numDeAmor titleLabel]text] intValue];
                [numDeAmor setTitle:[NSString stringWithFormat:@"%d",++num] forState:UIControlStateNormal];
            });
        }else{
            
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/setlikemensaje.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&mensajeID=%@&token=%@&act=2",[[defaults objectForKey:@"identifier"] intValue], [[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"mensajeID"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [corazon setBackgroundImage:[UIImage imageNamed:@"grayheart.png"] forState:UIControlStateNormal];
                [numDeAmor setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                int num = [[[numDeAmor titleLabel]text] intValue];
                if(num == 0){
                    [numDeAmor setTitle:@"" forState:UIControlStateNormal];
                }else{
                    [numDeAmor setTitle:[NSString stringWithFormat:@"%d",--num] forState:UIControlStateNormal];
                }
            });
        }
        
    }
}

- (IBAction)textoCambio:(id)sender {

    UIButton *comentarVistaVotar = (UIButton *)[vistaVotar viewWithTag:2];
    UITextField *comentarioVistaVotar = (UITextField *)[vistaVotar viewWithTag:1];
    UIButton *candado = [self.view viewWithTag:10];
    
    if([[comentarioVistaVotar text] isEqualToString:@""] && !imagenCargada){
        
        [UIView animateWithDuration:0.1 animations:^{
            [comentarVistaVotar setTitle:@"" forState:UIControlStateNormal];
            [comentarVistaVotar setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
            [comentarVistaVotar setFrame:CGRectMake(comentarVistaVotar.frame.origin.x, comentarVistaVotar.frame.origin.y, 30, comentarVistaVotar.frame.size.height)];
            
            if(candado != nil){
                [candado setFrame:CGRectMake(240, 482, 25, 30)];
            }
        }];
        seTomaFoto = YES;
    }else{
        
        [UIView animateWithDuration:0.1 animations:^{
            [comentarVistaVotar setTitle:NSLocalizedString(@"Enviar", nil) forState:UIControlStateNormal];
            [comentarVistaVotar setBackgroundImage:nil forState:UIControlStateNormal];
            [comentarVistaVotar setFrame:CGRectMake(comentarVistaVotar.frame.origin.x, comentarVistaVotar.frame.origin.y, 45, 30)];

            if(candado != nil){
                [candado setFrame:CGRectMake(230, 482, 25, 30)];
            }
        }];
        seTomaFoto = NO;
    }
}

-(IBAction)cerrarImagen:(id)sender{
    
    UIButton *comentarVistaVotar = (UIButton *)[vistaVotar viewWithTag:2];
    UITextView *comentarioVistaVotar = (UITextView *)[vistaVotar viewWithTag:1];
    
    UIImageView *imagen = (UIImageView *)[vistaVotar viewWithTag:101];
    UIButton *cerrar = (UIButton *)[vistaVotar viewWithTag:102];
    
    [imagen removeFromSuperview];
    [cerrar removeFromSuperview];
    
    
    imagenCargada = false;
    
    UIButton *candado = [self.view viewWithTag:10];
    
    if([[comentarioVistaVotar text] isEqualToString:@""] && !imagenCargada){
        
        [UIView animateWithDuration:0.1 animations:^{
            [comentarVistaVotar setTitle:@"" forState:UIControlStateNormal];
            [comentarVistaVotar setBackgroundImage:[UIImage imageNamed:@"camara.png"] forState:UIControlStateNormal];
            [comentarVistaVotar setFrame:CGRectMake(comentarVistaVotar.frame.origin.x, comentarVistaVotar.frame.origin.y, 30, comentarVistaVotar.frame.size.height)];
            
            if(candado != nil){
                [candado setFrame:CGRectMake(240, 482, 25, 30)];
            }
        }];
        seTomaFoto = YES;
    }
    
}

-(void)expandirImagen:(UIGestureRecognizer *)gesto{
   
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UITableView *tablaComentarios = [[popup contentView] viewWithTag:1000];
    UITableViewCell *celda = (UITableViewCell*)[gesto.view superview];
    NSIndexPath *indexpath = [tablaComentarios indexPathForCell:celda];
    UIImageView *imagenView = (UIImageView*)[celda viewWithTag:1];
    UIImageView *fotoPublicacion = (UIImageView*)gesto.view;
    
    if([[[arregloComentarios objectAtIndex:indexpath.row] objectForKey:@"esVideo"] intValue] == 0){
        
        UIGraphicsBeginImageContext(self.view.frame.size);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        [self.view.layer renderInContext:context];
        
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        ImagenPubLugarViewController *imgPub = [storyboard instantiateViewControllerWithIdentifier:@"imgPubLugarView"];
        
        imgPub.fotoAnterior = img;
        imgPub.fotoAmostrar = [(UIImageView*)gesto.view image];
        imgPub.imagenUsuario = UIImageJPEGRepresentation([imagenView image], 90);
        imgPub.json = [arregloComentarios objectAtIndex:indexpath.row];
        [self presentViewController:imgPub animated:NO completion:^{}];
        
        
    }else if([[[arregloComentarios objectAtIndex:indexpath.row] objectForKey:@"esVideo"] intValue] == 1){
        
        if(![[layer superlayer] isEqual:celda.layer]){
            [layer removeFromSuperlayer];
        }
        
        if(![[controlesReproductor superview] isEqual:celda]){
            [controlesReproductor removeFromSuperview];
        }
        
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        
        
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"crowtevideo%@.mp4",[[arregloComentarios objectAtIndex:indexpath.row] objectForKey:@"idMensaje"]]];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];

        if([fileManager fileExistsAtPath:filePath]){
            
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            
            if(reproductor){
                
                [reproductor removeObserver:self forKeyPath:@"status"];
                [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
            }
            
            reproductor = [[AVPlayer alloc] init];
            
            layer = [AVPlayerLayer playerLayerWithPlayer:reproductor];
            [layer setBackgroundColor:[[UIColor whiteColor] CGColor]];
            
            
            
            if([celda viewWithTag:10]){
                
                controlesReproductor = [celda viewWithTag:10];
                controlesReproductor.frame = fotoPublicacion.frame;
            }else{
                controlesReproductor = [[UIView alloc] init];
                controlesReproductor.frame = fotoPublicacion.frame;
                [controlesReproductor setTag:10];
            }
            
            AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:filePath]];
            
            AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
            
            reproductor = [AVPlayer playerWithPlayerItem:item];
            
            reproductor.actionAtItemEnd = AVPlayerActionAtItemEndNone;
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loopVideo:) name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
            
            [layer setPlayer:reproductor];
            
            [reproductor addObserver:self forKeyPath:@"status" options:0 context:nil];
            
            UITapGestureRecognizer *pausaVideo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pausarVideo)];
            
            UIPanGestureRecognizer *cerrarVideo = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(cerrarVideoFullScreen:)];
            
            UIView *fullScreenVideo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];

            layer.frame = fullScreenVideo.frame;
            [fullScreenVideo.layer addSublayer:layer];
            [fullScreenVideo addGestureRecognizer:pausaVideo];
            [fullScreenVideo addGestureRecognizer:cerrarVideo];
            popupFullSVideo = [KLCPopup popupWithContentView:fullScreenVideo showType:KLCPopupShowTypeBounceIn dismissType:KLCPopupDismissTypeBounceOut maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:YES dismissOnContentTouch:NO];
            
            [popupFullSVideo show];
            
            [reproductor play];
            
            urlVideoApasar = [NSURL fileURLWithPath:filePath];
            
        }else{
        
            NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%@/publicaciones/%@",[[arregloComentarios objectAtIndex:indexpath.row] objectForKey:@"mensajeUsuarioID"],[[arregloComentarios objectAtIndex:indexpath.row] objectForKey:@"fotoPublicacion"]]];
            
            
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            
            NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
            
            NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
            
            NSURLSessionDownloadTask *uploadTask = [urlSession downloadTaskWithURL:videoURL];
            
            [uploadTask resume];
            
            if(reproductor){
                
                [reproductor removeObserver:self forKeyPath:@"status"];
                [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
            }
            reproductor = [[AVPlayer alloc] init];
            
            layer = [AVPlayerLayer playerLayerWithPlayer:reproductor];
            [layer setBackgroundColor:[[UIColor blackColor] CGColor]];
            
            
            
            if([celda viewWithTag:10]){
                
                controlesReproductor = [celda viewWithTag:10];
                controlesReproductor.frame = fotoPublicacion.frame;
            }else{
                controlesReproductor = [[UIView alloc] init];
                controlesReproductor.frame = fotoPublicacion.frame;
                [controlesReproductor setTag:10];
            }
            
            UITapGestureRecognizer *pausaVideo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pausarVideo)];
            
            [controlesReproductor addGestureRecognizer:pausaVideo];
            
            UIProgressView *progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
            
            [progress setTrackTintColor:[UIColor grayColor]];
            [progress setProgressTintColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
            [progress setFrame:CGRectMake(controlesReproductor.frame.origin.x, controlesReproductor.frame.origin.y+(controlesReproductor.frame.size.height/2), controlesReproductor.frame.size.width, controlesReproductor.frame.size.height)];
            [progress setTag:100];
            
            [controlesReproductor setBackgroundColor:[UIColor blackColor]];
            [celda addSubview:controlesReproductor];
            [celda addSubview:progress];
        
        }
        
    }
    
}

-(IBAction)compartirFB:(id)sender{

    if([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]){
    
        publicarFb = true;
        
    }else{
    
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithPublishPermissions:@[@"publish_actions"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
        
            NSLog(@"listo !");
        }];
    }
}

-(void)tomarVideo:(UILongPressGestureRecognizer *)gesto{
 
    if(gesto.state == UIGestureRecognizerStateBegan){
        
        if(!alreadyVideo){

            // 1 foto, 2 video
            videofoto = 2;
            
            picker.mediaTypes = [NSArray arrayWithObject:(NSString*)kUTTypeMovie];
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [picker startVideoCapture];
            
            alreadyVideo = YES;
        }
    }
    
    if(gesto.state == UIGestureRecognizerStateEnded){
    
        [picker stopVideoCapture];
        alreadyVideo = NO;
    }
    
}

-(void)tomarFoto:(UILongPressGestureRecognizer *)gesto{

    videofoto = 1;
    [picker setCameraCaptureMode:UIImagePickerControllerCameraCaptureModePhoto];
    [picker takePicture];
    
}

-(void)cambiarCamara{
   
    if([picker cameraDevice] == UIImagePickerControllerCameraDeviceRear){
        
        [picker setCameraDevice:UIImagePickerControllerCameraDeviceFront];
    }else if([picker cameraDevice] == UIImagePickerControllerCameraDeviceFront){
    
        [picker setCameraDevice:UIImagePickerControllerCameraDeviceRear];
    }
}

-(void)flash{
    
    if([picker cameraFlashMode] == UIImagePickerControllerCameraFlashModeOn){

        [camara.flash setBackgroundImage:[UIImage imageNamed:@"flashno.png"] forState:UIControlStateNormal];
        picker.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
    }else if([picker cameraFlashMode] == UIImagePickerControllerCameraFlashModeOff){
        
        [camara.flash setBackgroundImage:[UIImage imageNamed:@"flashsi.png"] forState:UIControlStateNormal];
        picker.cameraFlashMode = UIImagePickerControllerCameraFlashModeOn;
    }
}

-(void)cerrarCamara{

    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)loopVideo:(NSNotification *)notification{

    AVPlayerItem *item = [notification object];
    
    [item seekToTime:kCMTimeZero];
}

-(void)pausarVideo{

    if([reproductor rate] == 0.0){
        [reproductor play];
    }else{
    
        [reproductor pause];
    }
}

-(void)cerrarVideoFullScreen:(UIPanGestureRecognizer *)gesto{

    [popupFullSVideo dismiss:YES];
    [reproductor pause];
    [controlesReproductor removeFromSuperview];
    [[UIApplication sharedApplication] setStatusBarHidden:NO    ];
}


#pragma mark - tutoriales

-(void)tutorialBotonVotar:(UIButton *)votar vista:(UIView *)vista{
    
    UIView *globo = [[UIView alloc] initWithFrame:CGRectMake(0, votar.frame.origin.y-80, vista.frame.size.width, 100)];
    
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"flechanegra.png"]];
    
    UITapGestureRecognizer *tapGestureCerrarTuto = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cerrarTutorialBotonVotar)];
    
    img.frame = CGRectMake(votar.frame.origin.x+(votar.frame.size.width/2)-25, globo.frame.size.height-50, 50, 25);
    
    [globo addSubview:img];
    
    UIView *globo2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, globo.frame.size.width, img.frame.origin.y)];
    
    [globo2 setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.81]];
    
    UILabel *texto = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, globo2.frame.size.width-20)];
    
    [texto setFont:[UIFont fontWithName:[texto.font fontName] size:14]];
    
    [texto setText:@"Vota el lugar, toma foto o un video"];
    [texto setTextColor:[UIColor whiteColor]];
    
    [texto sizeToFit];
    
    [globo2 addSubview:texto];
    [globo addSubview:globo2];
    
    [globo setTag:1000];
    [globo addGestureRecognizer:tapGestureCerrarTuto];
    
    [vista addSubview:globo];
}

-(void)cerrarTutorialBotonVotar{

    if([vistadecarga viewWithTag:1000]){
        
        [[vistadecarga viewWithTag:1000] removeFromSuperview];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarbotonvotartuto.php"]];
        
        [request setHTTPMethod:@"POST"];
        
        NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[defaults objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
        
        NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
            
            
        }];
        
        [uploadTask resume];
    }
}

#pragma mark - checkin rapido

-(void)cerrarTutoCheckinRapido{

    if([vistauno viewWithTag:1000]){
    
        [[vistauno viewWithTag:1000] removeFromSuperview];
        
        [self actualizarTutoCheckinrapido];
    }
}

-(void)activarBotonCheckinRapido{

    if(![vistauno viewWithTag:1001]){
        
        UIButton *checkinrapido = [[UIButton alloc] initWithFrame:CGRectMake(vistauno.frame.size.width-60, vistauno.frame.size.height-60, 50, 50)];
        
        [checkinrapido setBackgroundImage:[UIImage imageNamed:@"checkinfastazul.png"] forState:UIControlStateNormal];
        
        [checkinrapido addTarget:self action:@selector(checkinRapido:) forControlEvents:UIControlEventTouchUpInside];
        
        [vistauno addSubview:checkinrapido];
        
        if([[tutoriales objectForKey:@"checkinrapido"] intValue] == 0){
            
            UIView *globo = [[UIView alloc] initWithFrame:CGRectMake(checkinrapido.frame.origin.x-200, checkinrapido.frame.origin.y-80, 250, 100)];
            
            UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"flechanegra.png"]];
            
            UITapGestureRecognizer *tapGestureCerrarTuto = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cerrarTutoCheckinRapido)];
            
            
            img.frame = CGRectMake(globo.frame.size.width-49, globo.frame.size.height-50, 50, 25);
            
            [globo addSubview:img];
            
            UIView *globo2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, globo.frame.size.width, img.frame.origin.y)];
            
            [globo2 setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.81]];
            
            UILabel *texto = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, globo2.frame.size.width-20)];
            
            [texto setText:@"¡Haz check-in!"];
            [texto setTextColor:[UIColor whiteColor]];
            
            [texto sizeToFit];
            
            [globo2 addSubview:texto];
            [globo addSubview:globo2];
            
            [globo setTag:1000];
            [globo addGestureRecognizer:tapGestureCerrarTuto];
            
            [vistauno addSubview:globo];
            
        }
        
        [checkinrapido setTag:1001];
    }
}

-(IBAction)checkinRapido:(id)sender{

    [lugaresCheckinRapido removeAllObjects];
    
    [self cerrarTutoCheckinRapido];
   
    UIButton *checkin = (UIButton*)sender;
    
    if([vistauno viewWithTag:101] == NULL){
        
        UIView *fondoCheckin = [[UIView alloc] initWithFrame:CGRectMake(0, 0, vistauno.frame.size.width, vistauno.frame.size.height)];
        
        UIView *lugaresVista = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, vistauno.frame.size.height-(vistauno.frame.size.height-checkin.frame.origin.y))];
        
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(lugaresVista.frame.size.width/2, lugaresVista.frame.size.height/2, 25, 25)];
        [activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        [activity startAnimating];
        
        [fondoCheckin setBackgroundColor:[UIColor colorWithWhite:0.99 alpha:1]];
        [fondoCheckin setTag:101];
        
        [lugaresVista setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1]];
        [lugaresVista.layer setBorderColor:[[UIColor colorWithWhite:0.9 alpha:1] CGColor]];
        [lugaresVista.layer setBorderWidth:1];
        [lugaresVista setTag:103];
        
        [lugaresVista addSubview:activity];
        [fondoCheckin addSubview:lugaresVista];
        [fondoCheckin addSubview:checkin];
        [vistauno addSubview:fondoCheckin];
        
        [checkin setBackgroundImage:[UIImage imageNamed:@"quitarcheckinrapidoazl.png"] forState:UIControlStateNormal];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getlugarcheckinrap.php"]];
        
        [request setHTTPMethod:@"POST"];
        
        NSData *cuerpo = [[NSString stringWithFormat:@"latitud=%f&longitud=%f",coordenadas.coordinate.latitude, coordenadas.coordinate.longitude] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
        
        NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){

            [activity removeFromSuperview];

            if(data != NULL){
                
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                if([[json objectForKey:@"cantidad"] intValue] == 0){
                    
                    [lugaresCheckinRapido addObject:json];
                }else{
                    
                    for (NSDictionary *iteracion in [json objectForKey:@"lugares"]) {
                        
                        [lugaresCheckinRapido addObject:iteracion];
                    }
                }
                
                UITableView *tablaCheckinRapido = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, lugaresVista.frame.size.width, lugaresVista.frame.size.height)];
                
                [tablaCheckinRapido setTag:102];
                [tablaCheckinRapido setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1]];
                
                [tablaCheckinRapido setDelegate:self];
                [tablaCheckinRapido setDataSource:self];
                
                [lugaresVista addSubview:tablaCheckinRapido];
                
            }
            
        }];
        
        [uploadTask resume];
    }else{
    
        [checkin setBackgroundImage:[UIImage imageNamed:@"checkinfastazul.png"] forState:UIControlStateNormal];
        [vistauno addSubview:checkin];
        [[vistauno viewWithTag:101] removeFromSuperview];
    }
}

-(void)actualizarTutoCheckinrapido{

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarcheckinrapidotuto.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[defaults objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        

    }];
    
    [uploadTask resume];
}

#pragma mark - circular progress

-(void)dibujarProgresoCircular:(UIImageView*)imagenView progreso:(float)progreso{
    
    [vistaVotar addSubview:imagenView];
    
    UIGraphicsBeginImageContextWithOptions(imagenView.frame.size, NO, 0);
    
    UIBezierPath *bazierPath = [UIBezierPath bezierPath];
    
    [bazierPath moveToPoint:CGPointMake(50, 50)];
    [bazierPath addArcWithCenter:CGPointMake(50, 50) radius:50 startAngle:(0*M_PI)/2 endAngle:((progreso*0.04)*M_PI)/2  clockwise:YES];
    [bazierPath closePath];
    
    [bazierPath setLineWidth:2];
    [[UIColor colorWithWhite:1 alpha:0.7] setStroke];
    [bazierPath stroke];
    [[UIColor colorWithWhite:1 alpha:0.7] setFill];
    [bazierPath fill];
    
    [imagenView setImage:UIGraphicsGetImageFromCurrentImageContext()];
    
    UIGraphicsEndImageContext();
    
    
    imagenView.transform = CGAffineTransformMakeRotation(4.71239);
    
}

#pragma mark - observer

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{

    if(object == reproductor && [keyPath isEqualToString:@"status"]){
       
        if(reproductor.status == AVPlayerStatusReadyToPlay){
        
            [reproductor play];
        }else if(reproductor.status == AVPlayerStatusFailed){
        
            NSLog(@"Error: %@",reproductor.error);
        }
    }
}

#pragma mark - camaraoverlayviewcontrollerdelegate

-(void)getImageVideoTomado:(NSData *)data videoImagen:(int)videoimagen comentario:(NSString *)comentario compartirFacebook:(BOOL)compartirFacebook{
    
    UIButton *comentarVistaVotar = (UIButton *)[vistaVotar viewWithTag:2];
    /* UITextField *comentarioVistaVotar = (UITextField *)[vistaVotar viewWithTag:1];
     UIButton *candado = [self.view viewWithTag:10];*/
    
    videofoto = videoimagen;
    dataFotoNueva = data;
    
    if(videofoto == 1){
        
        UIImageView *imagen = [[UIImageView alloc] initWithFrame:CGRectMake(50, 300, 100, 100)];
        
        if([[UIScreen mainScreen] bounds].size.height < 560){
            
            imagen.frame = CGRectMake(50, 200, 100, 100);
        }
        
        [imagen setCenter:CGPointMake(vistaVotar.center.x, imagen.center.y)];
        
        imagen.layer.cornerRadius = 5;
        imagen.layer.borderWidth = 0.5;
        imagen.layer.masksToBounds = YES;
        imagen.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4] CGColor];
        [imagen setImage:[[UIImage alloc] initWithData:data]];
        [imagen setTag:101];
        
        [vistaVotar addSubview:imagen];
        
      /*  UIButton *cerrar = [[UIButton alloc] initWithFrame:CGRectMake((imagen.frame.origin.x+imagen.frame.size.width)-5, imagen.frame.origin.y-5, 15, 15)];
        
        [cerrar setTag:102];
        [cerrar setBackgroundImage:[UIImage imageNamed:@"tachafnegro.png"] forState:UIControlStateNormal];
        
        [cerrar addTarget:self action:@selector(cerrarImagen:) forControlEvents:UIControlEventTouchUpInside];
        
        [vistaVotar addSubview:cerrar];*/
        
        UIButton *candado = (UIButton*)[self.view viewWithTag:10];
        
        [UIView animateWithDuration:0.1 animations:^{
            [comentarVistaVotar setTitle:NSLocalizedString(@"Enviar", nil) forState:UIControlStateNormal];
            [comentarVistaVotar setBackgroundImage:nil forState:UIControlStateNormal];
            [comentarVistaVotar setFrame:CGRectMake(comentarVistaVotar.frame.origin.x, comentarVistaVotar.frame.origin.y, 45, 30)];
            
            if(candado != nil){
                [candado setFrame:CGRectMake(230, 482, 25, 30)];
                
                if([[UIScreen mainScreen] bounds].size.height < 560){
                    
                    imagen.frame = CGRectMake(230, 382, 25, 30);
                }
            }
        }];
        
        seTomaFoto = NO;
        imagenCargada = YES;
        
        
        [self enviarMensajeLugar:comentarVistaVotar comentario:comentario];
        
    }else if(videofoto == 2){
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"thefile.mp4"];
        
        NSURL *urlvd = [NSURL URLWithString:filePath];

        [[NSFileManager defaultManager] createFileAtPath:[urlvd path] contents:data attributes:nil];
        
        NSURL *videoUrl = [NSURL fileURLWithPath:filePath];
        
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoUrl options:nil];
        
        AVAssetImageGenerator *imgGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];

        imgGenerator.appliesPreferredTrackTransform = YES;
        
        NSError *error = NULL;
        
        CMTime time = CMTimeMake(1, 60);
        CGImageRef imgRef = [imgGenerator copyCGImageAtTime:time actualTime:NULL error:&error];

        UIImageView *imagen = [[UIImageView alloc] initWithFrame:CGRectMake(50, 300, 100, 100)];
        UIImage *imgCG = [[UIImage alloc] initWithCGImage:imgRef];

        if([[UIScreen mainScreen] bounds].size.height < 560){
            
            imagen.frame = CGRectMake(50, 200, 100, 100);
        }
        
        [imagen setCenter:CGPointMake(vistaVotar.center.x, imagen.center.y)];
        
        imagen.layer.cornerRadius = 5;
        imagen.layer.borderWidth = 0.5;
        imagen.layer.masksToBounds = YES;
        imagen.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4] CGColor];
        [imagen setImage:imgCG];
        [imagen setTag:101];
        
        [vistaVotar addSubview:imagen];
        
       /* UIButton *cerrar = [[UIButton alloc] initWithFrame:CGRectMake((imagen.frame.origin.x+imagen.frame.size.width)-5, imagen.frame.origin.y-5, 15, 15)];
        
        [cerrar setTag:102];
        [cerrar setBackgroundImage:[UIImage imageNamed:@"tachafnegro.png"] forState:UIControlStateNormal];
        
        [cerrar addTarget:self action:@selector(cerrarImagen:) forControlEvents:UIControlEventTouchUpInside];
        
        [vistaVotar addSubview:cerrar];*/
        
        UIButton *candado = (UIButton*)[self.view viewWithTag:10];
        
        [UIView animateWithDuration:0.1 animations:^{
            [comentarVistaVotar setTitle:NSLocalizedString(@"Enviar", nil) forState:UIControlStateNormal];
            [comentarVistaVotar setBackgroundImage:nil forState:UIControlStateNormal];
            [comentarVistaVotar setFrame:CGRectMake(comentarVistaVotar.frame.origin.x, comentarVistaVotar.frame.origin.y, 45, 30)];
            
            if(candado != nil){
                [candado setFrame:CGRectMake(230, 482, 25, 30)];
                
                if([[UIScreen mainScreen] bounds].size.height < 560){
                    
                    imagen.frame = CGRectMake(230, 382, 25, 30);
                }
            }
        }];
        
        seTomaFoto = NO;
        imagenCargada = YES;
        
        [self enviarMensajeLugar:comentarVistaVotar comentario:comentario];
    }
}

#pragma mark - text view editable


-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    UIButton *comentarVistaVotar = (UIButton *)[vistaVotar viewWithTag:2];
    UITextView *comentarioVistaVotar = (UITextView *)[vistaVotar viewWithTag:1];
    
    if([textView isEqual:campoCompartir] && [[campoCompartir text] isEqualToString:@""]){
    
        campoCompartir.text = NSLocalizedString(@"¿Qué plan hay?", nil);
        [campoCompartir setTextColor:[UIColor colorWithRed:187.0/255 green:187.0/255 blue:195.0/255 alpha:1.0]];
        campoCompartir.selectedRange = NSMakeRange(0,0);
    }else if([textView isEqual:campoCompartir] && [[campoCompartir text] isEqualToString:NSLocalizedString(@"¿Qué plan hay?",nil)]){
    
        campoCompartir.selectedRange = NSMakeRange(0,0);
    }
    
    if([textView isEqual:comentarioVistaVotar]){

        
        if([[UIScreen mainScreen] bounds].size.height <= 480){
            
            [UIView animateWithDuration:0.4 animations:^{
                
                [vistaVotar setFrame:CGRectMake(vistaVotar.frame.origin.x, vistaVotar.frame.origin.y - 100, vistaVotar.frame.size.width, vistaVotar.frame.size.height)];
                [comentarVistaVotar setFrame:CGRectMake(comentarVistaVotar.frame.origin.x, comentarVistaVotar.frame.origin.y-40, comentarVistaVotar.frame.size.width, comentarVistaVotar.frame.size.height)];
            }];
        }else{
            
            [UIView animateWithDuration:0.4 animations:^{
                
                [comentarVistaVotar setFrame:CGRectMake(comentarVistaVotar.frame.origin.x, comentarVistaVotar.frame.origin.y-40, comentarVistaVotar.frame.size.width, comentarVistaVotar.frame.size.height)];
            }];
        }
    }
    
}

-(void)textViewDidChange:(UITextView *)textView{
   
    
    if([textView isEqual:campoCompartir]){
        
        UIApplication *app =[UIApplication sharedApplication];
        
        [coneccionPublicar cancel];
        
        if((![[[campoCompartir text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] && ![[campoCompartir text] isEqualToString:NSLocalizedString(@"¿Qué plan hay?", nil)]) || nombreLugarAir.alpha == 1.0){
            
            botonEnviar.userInteractionEnabled = YES;
            [botonEnviar setTitleColor:[UIColor colorWithRed:0 green:171/255.0 blue:207/255.0 alpha:1] forState:UIControlStateNormal];
        }else{
            
            botonEnviar.userInteractionEnabled = NO;
            [botonEnviar setTitleColor:[UIColor colorWithRed:178/255.0 green:178/255.0 blue:185/255.0 alpha:1] forState:UIControlStateNormal];
        }
        
        if([campoCompartir.text isEqualToString:@""]){
            
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDuration:0.4];
            [UIView setAnimationBeginsFromCurrentState:YES];
            textView.frame = CGRectMake(textView.frame.origin.x, 3, textView.frame.size.width, 153);
            self.navigationController.view.frame = CGRectMake(self.navigationController.view.frame.origin.x, 0, self.navigationController.view.frame.size.width, self.navigationController.view.frame.size.height);
            [UIView commitAnimations];
            
            [scrollview setScrollEnabled:YES];
            
            app.networkActivityIndicatorVisible = NO;
        }
        
        if(seBusca == true && [[textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0 && [[textView.text substringFromIndex:palabraABuscar+1] length] > 0){
            
            if([arregloLugares count] > 0){
                [arregloLugares removeAllObjects];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [tablaLugares reloadData];
                });
            }
            
            UIApplication *app = [UIApplication sharedApplication];
            app.networkActivityIndicatorVisible = YES;
            
            NSString *palabra =[textView.text substringFromIndex:palabraABuscar+1];
            
            NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/buscarlugar.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"palabra=%@&id=%@&token=%@",palabra,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            coneccionPublicar = [[NSURLConnection alloc] initWithRequest:peticion delegate:self];
            
            if(coneccionPublicar){
                
                datosLugarPublicacion = [NSMutableData data];
                
                [coneccionPublicar start];
            }
        }
    }

}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    if([[campoCompartir text] isEqualToString:NSLocalizedString(@"¿Qué plan hay?", nil)]){
        campoCompartir.text = @"";
        [campoCompartir setTextColor:[UIColor colorWithWhite:0.0 alpha:1.0]];
    }
    
    if(([text isEqualToString:@"@"] && range.location == 0) || ([text isEqualToString:@"@"] && range.location > 0 && [[textView.text substringFromIndex:range.location-1] isEqualToString:@""]) || ([text isEqualToString:@"@"] && range.location > 0 && [[textView.text substringFromIndex:range.location-1] isEqualToString:@" "])){
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.4];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.navigationController.view.frame = CGRectMake(self.navigationController.view.frame.origin.x, self.navigationController.view.frame.origin.y-200, self.navigationController.view.frame.size.width, self.navigationController.view.frame.size.height);
        textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y+100, textView.frame.size.width, 60);
        [UIView commitAnimations];
        seBusca = YES;
        
        [scrollview setScrollEnabled:NO];
        
        palabraABuscar = textView.text.length;
    }
    
    
    if([text isEqualToString:@""] && [textView.text characterAtIndex:textView.text.length-1] == '@'){
    
        palabraABuscar = -1;
        seBusca = NO;
        [arregloLugares removeAllObjects];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [tablaLugares reloadData];
        });
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.4];
        [UIView setAnimationBeginsFromCurrentState:YES];
        textView.frame = CGRectMake(textView.frame.origin.x, 3, textView.frame.size.width, 153);
        self.navigationController.view.frame = CGRectMake(self.navigationController.view.frame.origin.x, 0, self.navigationController.view.frame.size.width, self.navigationController.view.frame.size.height);
        [UIView commitAnimations];
    }
    
    return YES;
}

#pragma mark - textfield

-(void)textFieldDidBeginEditing:(UITextField *)textField{

    UIButton *comentarVistaVotar = (UIButton *)[vistaVotar viewWithTag:2];
    UITextField *comentarioVistaVotar = (UITextField *)[vistaVotar viewWithTag:1];
    
    if([textField isEqual:comentarioVistaVotar]){
        
        
        if([[UIScreen mainScreen] bounds].size.height <= 480){
            
            [UIView animateWithDuration:0.4 animations:^{
                
                [vistaVotar setFrame:CGRectMake(vistaVotar.frame.origin.x, vistaVotar.frame.origin.y - 100, vistaVotar.frame.size.width, vistaVotar.frame.size.height)];
                [comentarVistaVotar setFrame:CGRectMake(comentarVistaVotar.frame.origin.x, comentarVistaVotar.frame.origin.y-40, comentarVistaVotar.frame.size.width, comentarVistaVotar.frame.size.height)];
            }];
        }else{
            
            UIButton *vercomentarios = (UIButton*)[vistaVotar viewWithTag:20];
            
            [UIView animateWithDuration:0.4 animations:^{
                
                [comentarioVistaVotar setCenter:CGPointMake(comentarioVistaVotar.center.x, comentarioVistaVotar.center.y-250)];
                [comentarVistaVotar setFrame:CGRectMake(comentarVistaVotar.frame.origin.x, comentarVistaVotar.frame.origin.y-250, comentarVistaVotar.frame.size.width, comentarVistaVotar.frame.size.height)];
                [vercomentarios setAlpha:0];
            }];
        }
    }
}

#pragma mark - nsurlsession

-(void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{

    
    NSLog(@"1 se envió data %ld esperados %ld de %ld",(long)bytesWritten, (long)totalBytesWritten, (long)totalBytesExpectedToWrite);
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{

    UITableViewCell *celda = (UITableViewCell*)[controlesReproductor superview];
    
    UITableView *tablaComentarios = (UITableView*)[[celda superview] superview];
    NSIndexPath *indexPath = [tablaComentarios indexPathForCell:celda];
   
     UIView *fullScreenVideo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    UIProgressView *progressView = (UIProgressView*)[celda viewWithTag:100];
    
    NSError *errormv;
    
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"crowtevideo%@.mp4",[[arregloComentarios objectAtIndex:indexPath.row] objectForKey:@"idMensaje"]]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    [fileManager copyItemAtPath:[location path] toPath:filePath error:&errormv];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:filePath]];
        
        AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
        
        reproductor = [AVPlayer playerWithPlayerItem:item];
        
        reproductor.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loopVideo:) name:AVPlayerItemDidPlayToEndTimeNotification object:[reproductor currentItem]];
      
        [layer setPlayer:reproductor];
        
        [reproductor addObserver:self forKeyPath:@"status" options:0 context:nil];
        
        [progressView removeFromSuperview];
        
        UITapGestureRecognizer *pausaVideo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pausarVideo)];
        
        UIPanGestureRecognizer *cerrarVideo = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(cerrarVideoFullScreen:)];
        
        layer.frame = fullScreenVideo.frame;
        [fullScreenVideo.layer addSublayer:layer];
        [fullScreenVideo addGestureRecognizer:pausaVideo];
        [fullScreenVideo addGestureRecognizer:cerrarVideo];
        popupFullSVideo = [KLCPopup popupWithContentView:fullScreenVideo showType:KLCPopupShowTypeBounceIn dismissType:KLCPopupDismissTypeBounceOut maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:YES dismissOnContentTouch:NO];
        
        [popupFullSVideo show];
        
        [reproductor play];
        
        urlVideoApasar = [NSURL fileURLWithPath:filePath];
        
    });
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        float porcentaje = ((totalBytesWritten*100)/totalBytesExpectedToWrite);
        
        porcentaje /= 100;
        
        UITableViewCell *celda = (UITableViewCell*)[controlesReproductor superview];
        
        UIProgressView *progressView = (UIProgressView*)[celda viewWithTag:100];
        
        [progressView setProgress:porcentaje];
    });
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes{}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{

}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend{
    
    NSLog(@"2 %lld de %lld",totalBytesSent, totalBytesExpectedToSend);
    
    dispatch_async(dispatch_get_main_queue(), ^{
    UIImageView *progressView = (UIImageView*)[vistaVotar viewWithTag:104];
    
    [self dibujarProgresoCircular:progressView progreso:(totalBytesSent*100)/totalBytesArchivoAsubir];
    });
}

-(void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session{

    NSLog(@"termino ahora si");
}

#pragma mark - nsurlrequest

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    
    if(connection == coneccionPublicar){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [campoCompartir resignFirstResponder];
            
            [UIView animateWithDuration:0.4 animations:^{
                
                self.navigationController.view.frame = CGRectMake(self.navigationController.view.frame.origin.x, self.navigationController.view.frame.origin.y-200, self.navigationController.view.frame.size.width, self.navigationController.view.frame.size.height);
                campoCompartir.frame = CGRectMake(campoCompartir.frame.origin.x, campoCompartir.frame.origin.y+100, campoCompartir.frame.size.width, 60);
            }];
            
            UIAlertController *servidorerror = [UIAlertController alertControllerWithTitle:nil message:@"No se puede establecer conexión con el servidor." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:NSLocalizedString(@"Muy bien", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                
                [UIView animateWithDuration:0.4 animations:^{
                    
                    campoCompartir.frame = CGRectMake(campoCompartir.frame.origin.x, 3, campoCompartir.frame.size.width, 153);
                    self.navigationController.view.frame = CGRectMake(self.navigationController.view.frame.origin.x, 0, self.navigationController.view.frame.size.width, self.navigationController.view.frame.size.height);
                }];
                
                [campoCompartir becomeFirstResponder];
            }];
            
            [servidorerror addAction:dismiss];
            
            [self presentViewController:servidorerror animated:YES completion:nil];
        });
    }else if(connection == coneccionSubirFoto){
    
        NSLog(@"fail");
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    if(connection == coneccionPublicar){
        
        [datosLugarPublicacion setLength:0];
    }else if(connection == coneccionSubirFoto){
    
        NSLog(@"llega");
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{

    if(connection == coneccionPublicar){
        NSLog(@"coneccion pubicar");
        [datosLugarPublicacion appendData:data];
    }else if(connection == coneccionSubirFoto){
    
        NSLog(@"coneccion subir foto %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    }
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{

    if(connection == coneccionPublicar){

        UIApplication *app = [UIApplication sharedApplication];
        
        NSError *error = nil;
        
        NSArray *json = [NSJSONSerialization JSONObjectWithData:datosLugarPublicacion options:NSJSONReadingMutableContainers error:&error];
        
        if(json == NULL && [json isEqual:nil]){
            
            [arregloLugares removeAllObjects];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [tablaLugares reloadData];
            });
        }else{
            
            if(json == nil){
                
                [arregloLugares removeAllObjects];
                
                [arregloLugares addObject:@"nohay"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [tablaLugares reloadData];
                });
                
            }else{
                
                for(NSDictionary *intervalo in json){
                    
                    [arregloLugares addObject:intervalo];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [tablaLugares reloadData];
                        app.networkActivityIndicatorVisible = NO;
                    });
                    
                    tablaLugares.opaque = YES;
                    
                }
            }
            
        }
    }else if(connection == coneccionSubirFoto){
    
        NSLog(@"coneccion subir foto");
    }
    
}

#pragma mark location

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{

    
    localizacionHab = NO;
    
    UIView *noLocalizacionView = [[UIView alloc] initWithFrame:CGRectMake(vistados.frame.origin.x, vistados.frame.origin.y, vistados.frame.size.width, vistados.frame.size.height)];
    
    [noLocalizacionView setTag:10];
    
    [noLocalizacionView setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
    
    UILabel *noLocalizacionText = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, vistados.frame.size.width-20, 100)];
    
    [noLocalizacionText setNumberOfLines:2];
    
    UILabel *noLocalizacionText2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 140, vistados.frame.size.width-20, 50)];
    
    [noLocalizacionText setText:NSLocalizedString(@"Para que Crowte funcione bien necesita tu ubicacón.",nil)];
    
    [noLocalizacionText2 setText:NSLocalizedString(@"Habilita el acceso a tu ubicación:", nil)];
    
    [noLocalizacionText setTextAlignment:NSTextAlignmentLeft];
    [noLocalizacionText setTextAlignment:NSTextAlignmentLeft];
    
    UIButton *noLocalizacionBoton = [[UIButton alloc] initWithFrame:CGRectMake(10, 150, vistados.frame.size.width-20, 200)];

    [noLocalizacionBoton setTitleColor:[UIColor colorWithRed:0.0 green:171/255.0 blue:207/255.0 alpha:1.0] forState:UIControlStateNormal];
    
    [noLocalizacionBoton setTitle:NSLocalizedString(@"Abrir configuración de Crowte", nil) forState:UIControlStateNormal];
    
    [noLocalizacionBoton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    [noLocalizacionBoton addTarget:self action:@selector(abrirConfiguracionApp) forControlEvents:UIControlEventTouchUpInside];
    
    [scrollview addSubview:noLocalizacionView];
    
    [noLocalizacionView addSubview:noLocalizacionText];
    
    [noLocalizacionView addSubview:noLocalizacionText2];
    
    [noLocalizacionView addSubview:noLocalizacionBoton];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{

    if(localizacionHab == NO){
    
        UIView *nolocalizacionView = (UIView *)[scrollview viewWithTag:10];
        
        [nolocalizacionView removeFromSuperview];
    }
    
    localizacionHab = YES;
    
    coordenadas = manager.location;
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarcoordenadas.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"latitud=%f&longitud=%f&ID=%@&token=%@",manager.location.coordinate.latitude,manager.location.coordinate.longitude,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){}];
    
    [locationManager stopUpdatingLocation];
    [locationManager startMonitoringSignificantLocationChanges];
}

-(void)abrirConfiguracionApp{

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

#pragma mark - image picker controller

-(void)imagePickerController:(UIImagePickerController *)picker2 didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIButton *comentarVistaVotar = (UIButton *)[vistaVotar viewWithTag:2];
   /* UITextField *comentarioVistaVotar = (UITextField *)[vistaVotar viewWithTag:1];
    UIButton *candado = [self.view viewWithTag:10];*/

    if(videofoto == 1){
        
        [picker2 dismissViewControllerAnimated:YES completion:^{
            
            
            /**************************************
             *                                    *
             * añade las fotos a un scrollview    *
             * para poder subir muchas a la vez   *
             
             
             
             numeroDeFotos++;
             
             UIImageView *imagen = [[UIImageView alloc] initWithFrame:CGRectMake(((numeroDeFotos-1)*100)+(numeroDeFotos*10), 0, 100, 100)];
             
             [imagen setImage:info[UIImagePickerControllerOriginalImage]];
             
             [scrollView setScrollEnabled:YES];
             [scrollView setContentSize:CGSizeMake(((numeroDeFotos+2)*100), 100)];
             
             [scrollView addSubview:imagen];
             
             if(numeroDeFotos > 3){
             
             [scrollView setContentOffset:CGPointMake(scrollView.contentSize.width - scrollView.bounds.size.width, 0) animated:YES];
             }
             
             ***************************************************/
            
            dataFotoNueva = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], 0.6);
            
            
            /*****************
             
             // if(publicarFb){
             
             FBSDKSharePhoto *fb = [[FBSDKSharePhoto alloc] init];
             fb.image = info[UIImagePickerControllerOriginalImage];
             fb.userGenerated = YES;
             //FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
             
             NSDictionary *propiedades = @{@"og:type":@"Comment", @"og:title":@"Titulo", @"og:description":@"descripcion"};
             
             FBSDKShareOpenGraphObject *objeto = [FBSDKShareOpenGraphObject objectWithProperties:propiedades];
             
             FBSDKShareOpenGraphAction *accion = [[FBSDKShareOpenGraphAction alloc] init];
             
             accion.actionType = @"Comment";
             [accion setObject:objeto forKey:@"Lugar"];
             
             [accion setArray:@[fb] forKey:@"image"];
             
             FBSDKShareOpenGraphContent *content = [[FBSDKShareOpenGraphContent alloc] init];
             
             content.action = accion;
             
             
             [FBSDKShareAPI shareWithContent:content delegate:nil];
             
             //  }
             
             *****************/
            
            
            
            UIImageView *imagen = [[UIImageView alloc] initWithFrame:CGRectMake(50, 300, 100, 100)];
            
            if([[UIScreen mainScreen] bounds].size.height < 560){
                
                imagen.frame = CGRectMake(50, 200, 100, 100);
            }
            
            [imagen setCenter:CGPointMake(vistaVotar.center.x, imagen.center.y)];
            
            imagen.layer.cornerRadius = 5;
            imagen.layer.borderWidth = 0.5;
            imagen.layer.masksToBounds = YES;
            imagen.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4] CGColor];
            [imagen setImage:info[UIImagePickerControllerOriginalImage]];
            [imagen setTag:101];
            
            [vistaVotar addSubview:imagen];
            
            UIButton *cerrar = [[UIButton alloc] initWithFrame:CGRectMake((imagen.frame.origin.x+imagen.frame.size.width)-5, imagen.frame.origin.y-5, 15, 15)];
            
            [cerrar setTag:102];
            [cerrar setBackgroundImage:[UIImage imageNamed:@"tachafnegro.png"] forState:UIControlStateNormal];
            
            [cerrar addTarget:self action:@selector(cerrarImagen:) forControlEvents:UIControlEventTouchUpInside];
            
            [vistaVotar addSubview:cerrar];
            
            UIButton *candado = (UIButton*)[self.view viewWithTag:10];
            
            [UIView animateWithDuration:0.1 animations:^{
                [comentarVistaVotar setTitle:NSLocalizedString(@"Enviar", nil) forState:UIControlStateNormal];
                [comentarVistaVotar setBackgroundImage:nil forState:UIControlStateNormal];
                [comentarVistaVotar setFrame:CGRectMake(comentarVistaVotar.frame.origin.x, comentarVistaVotar.frame.origin.y, 45, 30)];
                
                if(candado != nil){
                    [candado setFrame:CGRectMake(230, 482, 25, 30)];
                    
                    if([[UIScreen mainScreen] bounds].size.height < 560){
                        
                        imagen.frame = CGRectMake(230, 382, 25, 30);
                    }
                }
            }];
            
            seTomaFoto = NO;
            imagenCargada = YES;
            
            
        }];
    }else if(videofoto == 2){
    
        [picker2 dismissViewControllerAnimated:YES completion:^{
        
            NSURL *videoUrl = info[UIImagePickerControllerMediaURL];
            
            dataFotoNueva = [NSData dataWithContentsOfURL:videoUrl];
            
            AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoUrl options:nil];
            AVAssetImageGenerator *imgGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
            
            imgGenerator.appliesPreferredTrackTransform = YES;
            
            NSError *error = NULL;
            
            CMTime time = CMTimeMake(1, 60);
            CGImageRef imgRef = [imgGenerator copyCGImageAtTime:time actualTime:NULL error:&error];
            
            UIImageView *imagen = [[UIImageView alloc] initWithFrame:CGRectMake(50, 300, 100, 100)];
            UIImage *imgCG = [[UIImage alloc] initWithCGImage:imgRef];
            
            if([[UIScreen mainScreen] bounds].size.height < 560){
                
                imagen.frame = CGRectMake(50, 200, 100, 100);
            }
            
            [imagen setCenter:CGPointMake(vistaVotar.center.x, imagen.center.y)];
            
            imagen.layer.cornerRadius = 5;
            imagen.layer.borderWidth = 0.5;
            imagen.layer.masksToBounds = YES;
            imagen.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4] CGColor];
            [imagen setImage:imgCG];
            [imagen setTag:101];
            
            [vistaVotar addSubview:imagen];
            
            UIButton *cerrar = [[UIButton alloc] initWithFrame:CGRectMake((imagen.frame.origin.x+imagen.frame.size.width)-5, imagen.frame.origin.y-5, 15, 15)];
            
            [cerrar setTag:102];
            [cerrar setBackgroundImage:[UIImage imageNamed:@"tachafnegro.png"] forState:UIControlStateNormal];
            
            [cerrar addTarget:self action:@selector(cerrarImagen:) forControlEvents:UIControlEventTouchUpInside];
            
            [vistaVotar addSubview:cerrar];
            
            UIButton *candado = (UIButton*)[self.view viewWithTag:10];
            
            [UIView animateWithDuration:0.1 animations:^{
                [comentarVistaVotar setTitle:NSLocalizedString(@"Enviar", nil) forState:UIControlStateNormal];
                [comentarVistaVotar setBackgroundImage:nil forState:UIControlStateNormal];
                [comentarVistaVotar setFrame:CGRectMake(comentarVistaVotar.frame.origin.x, comentarVistaVotar.frame.origin.y, 45, 30)];
                
                if(candado != nil){
                    [candado setFrame:CGRectMake(230, 482, 25, 30)];
                    
                    if([[UIScreen mainScreen] bounds].size.height < 560){
                        
                        imagen.frame = CGRectMake(230, 382, 25, 30);
                    }
                }
            }];
            
            seTomaFoto = NO;
            imagenCargada = YES;
            
        }];
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker2{
    
    [picker2 dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - facebook delegados

-(void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{

    NSLog(@"hubo error %@",error);
}
-(void)sharerDidCancel:(id<FBSDKSharing>)sharer{}
-(void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{

    NSLog(@"se completo %@",results);
}

@end
