//
//  ReportarMensajeTableViewController.m
//  Crowte
//
//  Created by Paco Escobar on 10/03/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ReportarMensajeTableViewController.h"
#import "CerrarReportarMensajeViewController.h"

@interface ReportarMensajeTableViewController ()

@end

@implementation ReportarMensajeTableViewController

@synthesize campoTextoReporte, idRelMensajeLugar, idUsuarioABloquear;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    [self.navigationController.navigationBar setTintColor:[UIColor grayColor]];
    
    [campoTextoReporte.layer setBorderWidth:1];
    [campoTextoReporte.layer setBorderColor:[[UIColor grayColor] CGColor]];
   
}

-(void)viewDidAppear:(BOOL)animated{

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewWillDisappear:(BOOL)animated{

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 3;
}

#pragma mark - textview

-(void)textViewDidChange:(UITextView *)textView{

    NSString *ultimoCaracter;
    
    if([[textView text] length] > 0){
        ultimoCaracter = [[textView text] substringFromIndex:[[textView text] length]-1];
        
        if([ultimoCaracter isEqualToString:@"\n"]){
            
            [textView resignFirstResponder];
            
            [textView setText:[[textView text] substringWithRange:NSMakeRange(0, [[textView text] length]-1)]];
        }
    }
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{

    if([[textView text] isEqualToString:@"Escribe aquí el problema"]){
    
        [textView setText:@""];
        [textView setTextColor:[UIColor blackColor]];
    }
    
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if([[textView text] isEqualToString:@" "] || [[textView text] isEqualToString:@"\n"] || [[textView text] length] == 0){

        [textView setText:@"Escribe aquí el problema"];
        [textView setTextColor:[UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1]];
    }
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}

- (IBAction)cancelarReporte:(id)sender {

    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)enviarReporte:(id)sender {
    
    NSData *dataComentarioLugar =[[campoTextoReporte text] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    
    NSString *comentario = [[NSString alloc] initWithData:dataComentarioLugar encoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/denuncia/denunciarmensaje.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"idD=%@&idM=%@&com=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"], idRelMensajeLugar, comentario] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(error == nil){
            
            if(data != nil){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    CerrarReportarMensajeViewController *cerrar = [storyboard instantiateViewControllerWithIdentifier:@"reporteMensajeHecho"];
                    
                    [cerrar setIdUsuarioABloquear:idUsuarioABloquear];
                    [cerrar setObjetivoDelReporte:3];
                    
                    [self presentViewController:cerrar animated:YES completion:nil];
                    
                });
                
            }
        }
        
    }];
    
    [uploadTask resume];
}
@end
