//
//  noInternet.m
//  Crowte
//
//  Created by Paco Escobar on 18/09/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import "noInternet.h"
#import "principal.h"

@interface noInternet ()

@end

@implementation noInternet

@synthesize irprincipal;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [irprincipal addTarget:self action:@selector(reintentar:) forControlEvents:UIControlEventTouchUpInside];
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)reintentar:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    principal *principal= [storyboard instantiateViewControllerWithIdentifier:@"principal"];
    [self presentViewController:principal animated:NO completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
