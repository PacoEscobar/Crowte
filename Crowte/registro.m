//
//  registro.m
//  crowte
//
//  Created by Paco Escobar on 10/05/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "registro.h"
#import "NSString+MD5.h"
#import "TabBarPrincipalViewController.h"
#import "PageViewController.h"

@implementation registro
@synthesize nombreR,correoR, apellidoR, passwordR, alturaLogoCrowte, constraintCrowteCancelar,constraintLogo, textViewCondiciones;


-(BOOL)checarCorreo:(NSString *) correo
{
    NSString *expresion = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *probarCorreo = [NSPredicate predicateWithFormat:@"Self matches %@", expresion];
    
    return [probarCorreo evaluateWithObject:[correo stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    
}


-(IBAction)clickFondo:(id)sender
{
    [nombreR resignFirstResponder];
    [apellidoR resignFirstResponder];
    [correoR resignFirstResponder];
    [passwordR resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == correoR) {
  
    } else if (textField == passwordR) {
        
        [textField resignFirstResponder];
        [self enviar:nil];
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == self.passwordR){
        
        [UIView animateWithDuration:0.4 animations:^{
            
           // [constraintLogo setConstant:320];
            self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 200), self.view.frame.size.width, self.view.frame.size.height);
            [self.view layoutIfNeeded];
        }];
    }else if(textField == self.correoR){
        
        [UIView animateWithDuration:0.4 animations:^{
            
            [constraintLogo setConstant:45];
            self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 30), self.view.frame.size.width, self.view.frame.size.height);
            [self.view layoutIfNeeded];
        }];

    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.passwordR){
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 200), self.view.frame.size.width, self.view.frame.size.height);
            
            [constraintLogo setConstant:40];
            [self.view layoutIfNeeded];
        }];
    }else if(textField == self.correoR){
        
        [UIView animateWithDuration:0.5 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 30), self.view.frame.size.width, self.view.frame.size.height);
            [constraintLogo setConstant:40];
            [self.view layoutIfNeeded];
        }];
    }
}

-(IBAction)siguiente:(UITextField *)textField
{
    if(textField == nombreR)
    {
        [apellidoR becomeFirstResponder];
    }else if(textField == apellidoR){
        [correoR becomeFirstResponder];
    }else if(textField == correoR){
        [passwordR becomeFirstResponder];
    }
}

-(void)viewDidLoad{

    [super viewDidLoad];
        
    NSMutableAttributedString *stringCondiciones = textViewCondiciones.attributedText.mutableCopy;
    
    [stringCondiciones setAttributes:@{NSLinkAttributeName:@"condiciones"} range:NSMakeRange(28, 18)];
    [stringCondiciones setAttributes:@{NSLinkAttributeName:@"privacidad"} range:NSMakeRange(78, 22)];
    textViewCondiciones.attributedText = stringCondiciones;
    textViewCondiciones.delegate = self;
}


-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    
    if([URL.absoluteString isEqualToString:@"condiciones"]){
   
        [self performSegueWithIdentifier:@"condicionesSegue" sender:self];
        
    }else if([URL.absoluteString isEqualToString:@"privacidad"]){
    
        [self performSegueWithIdentifier:@"privacidadSegue" sender:self];
    }
    
    return NO;
}


-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(IBAction)enviar:(id)boton{

    [boton setEnabled:NO];
    
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    [nombreR resignFirstResponder];
    [apellidoR resignFirstResponder];
    [correoR resignFirstResponder];
    [passwordR resignFirstResponder];

    if([[nombreR text] length] > 0 && [[apellidoR text] length] > 0 && [[correoR text] length] > 0 && [[passwordR text] length] > 0){
        
        if([self checarCorreo:[correoR text]] == 1)
        {
            
            NSString *password = [NSString stringWithFormat:@"%@%@",[[[[passwordR text] MD5String] lowercaseString] substringFromIndex:14],[[[[passwordR text] MD5String] lowercaseString] substringToIndex:13]];
            
            NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/registrar.php"]];
            
            [peticion setHTTPMethod:@"POST"];

            [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%@&apeido=%@&correo=%@&contra=%@",[nombreR text],[apellidoR text],[correoR text],password] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
                if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                    
                    app.networkActivityIndicatorVisible = NO;
                }else{
                    
                    NSString *strid = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                    if([strid intValue] == 0){
                    
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            UIAlertController *yaexiste = [UIAlertController alertControllerWithTitle:nil message:@"Este usuario ya está registrado" preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction *muybien = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                
                                [boton setEnabled:YES];
                            }];
                            
                            [yaexiste addAction:muybien];
                            
                            [self presentViewController:yaexiste animated:YES completion:nil];
                        });
                        
                    }else{
                        
                        NSUserDefaults *ide = [NSUserDefaults standardUserDefaults];
                        [ide setObject:[NSNumber numberWithInt:1] forKey:@"id"];
                        [ide setObject:strid forKey:@"identifier"];
                        [ide synchronize];
                        
                        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
                        [[UIApplication sharedApplication] registerForRemoteNotifications];
                        
                        
                        NSURL *url = [NSURL URLWithString:@"https://www.crowte.com/app/getdatos.php"];
                        
                        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                        [request setHTTPMethod:@"POST"];
                        [request setHTTPBody:[[NSString stringWithFormat:@"id=%d&recurso=1&p=1",[strid intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
                        
                        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                        
                        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                            
                            NSMutableDictionary *datosUsuario = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
                                
                                [[UINavigationBar appearance] setTranslucent:YES];
                                
                                [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
                                
                                UIStoryboard *storyboard;
                                
                                TabBarPrincipalViewController *tabBarPrincipalVC;
                                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                tabBarPrincipalVC= [storyboard instantiateViewControllerWithIdentifier:@"mainTabBar"];
                                
                                [tabBarPrincipalVC setCheckinsTodosOtros:[datosUsuario objectForKey:@"todosOtrosPost"]];
                                [tabBarPrincipalVC setCheckinsTodosUltimos:[datosUsuario objectForKey:@"todosUltimosPosts"]];
                                [tabBarPrincipalVC setDatosCheckinEnLugar:[datosUsuario objectForKey:@"checkinAlready"]];
                                [tabBarPrincipalVC setDatosMios:[datosUsuario objectForKey:@"datos"]];
                                
                                app.networkActivityIndicatorVisible = NO;
                                
                                [self presentViewController:tabBarPrincipalVC animated:NO completion:nil];
                                
                            });
                        }];
                        
                    }
                }
                
            }];
            
        }else
        {
            
            [boton setEnabled:YES];
            
            UIAlertController *correoinvalido = [UIAlertController alertControllerWithTitle:nil message:@"Verifica tu correo" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okas = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleDefault handler:nil];
            
            [correoinvalido addAction:okas];
            [self presentViewController:correoinvalido animated:YES completion:nil];
            
        }
        
    }else{
        
        [boton setEnabled:YES];
        
        UIAlertController *rellenatodo = [UIAlertController alertControllerWithTitle:nil message:@"Por favor, rellena todos los campos" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okas = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleDefault handler:nil];
        
        [rellenatodo addAction:okas];
        [self presentViewController:rellenatodo animated:YES completion:nil];
    }
}

@end
