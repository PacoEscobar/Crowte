//
//  BuscarViewController.m
//  Crowte
//
//  Created by Paco Escobar on 18/08/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import "BuscarViewController.h"
#import "PerfilUsuarioViewController.h"
#import "PerfilLugarViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface BuscarViewController ()

@end

@implementation BuscarViewController

@synthesize campoBusqueda, arregloResultadosBusquedaLugares, arregloResultadosBusquedaPersonas, tabla, envioActual,datos, palabra, coneccion;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    envioActual = 0;
    
    arregloResultadosBusquedaLugares = [NSMutableArray array];
    arregloResultadosBusquedaPersonas = [NSMutableArray array];
    
    [campoBusqueda addTarget:self action:@selector(enviarBusqueda) forControlEvents:UIControlEventEditingChanged];
    
    [campoBusqueda becomeFirstResponder];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    float size = 0;
    
    if(([arregloResultadosBusquedaLugares count] > 0 && [arregloResultadosBusquedaPersonas count] > 0) && ([[arregloResultadosBusquedaLugares objectAtIndex:0] isEqual:[NSNumber numberWithInt:-1]] && [[arregloResultadosBusquedaLugares objectAtIndex:0] isEqual:[NSNumber numberWithInt:-1]])){
    
        size = 97.0;
    }else{
    
        size = 78.0;
    }
    
    return size;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    NSInteger tam;

    if(([arregloResultadosBusquedaLugares count] > 0 && [arregloResultadosBusquedaPersonas count] > 0) && ([[arregloResultadosBusquedaLugares objectAtIndex:0] isEqual:[NSNumber numberWithInt:-1]] && [[arregloResultadosBusquedaLugares objectAtIndex:0] isEqual:[NSNumber numberWithInt:-1]])){
    
        tam = 1;
    }else{
        
        if([arregloResultadosBusquedaPersonas count] > 0 || [arregloResultadosBusquedaLugares count] > 0){
            
            tam = [arregloResultadosBusquedaPersonas count]+[arregloResultadosBusquedaLugares count];
        }else{
            
            tam = 1;
        }
    }
    
    return tam;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *celda;

    [tabla setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    if(([arregloResultadosBusquedaLugares count] > 0 && [arregloResultadosBusquedaPersonas count] > 0) && ([[arregloResultadosBusquedaLugares objectAtIndex:0] isEqual:[NSNumber numberWithInt:-1]] && [[arregloResultadosBusquedaLugares objectAtIndex:0] isEqual:[NSNumber numberWithInt:-1]])){
    
        celda = [tabla dequeueReusableCellWithIdentifier:@"noEncontrado"];
        
        [tabla setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }else{
        
        if([arregloResultadosBusquedaLugares count] > 0 && indexPath.row < [arregloResultadosBusquedaLugares count]){
            
            celda = [tabla dequeueReusableCellWithIdentifier:@"celdaBusqueda"];
            
            UIImageView *foto = (UIImageView *)[celda viewWithTag:1];
            
            [foto setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
            [foto.layer setCornerRadius:25];
            [foto.layer setBorderWidth:1];
            [foto.layer setBorderColor:[[UIColor colorWithWhite:1 alpha:1] CGColor]];
            
            [foto setContentMode:UIViewContentModeCenter];
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%d/%@",[[[arregloResultadosBusquedaLugares objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue], [[arregloResultadosBusquedaLugares objectAtIndex:indexPath.row] objectForKey:@"foto"]]];
            
            [foto sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
            
                if(error == nil){
                    
                    [foto.layer setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                    [foto setContentMode:UIViewContentModeScaleToFill];
                }
            }];
            
            foto.layer.masksToBounds = YES;
            
            UILabel *nombre = (UILabel *)[celda viewWithTag:2];
            nombre.text = [[arregloResultadosBusquedaLugares objectAtIndex:indexPath.row] objectForKey:@"nombre"];
            
            UILabel *es = (UILabel *)[celda viewWithTag:3];
            es.text = @"Lugar";
        }else if([arregloResultadosBusquedaPersonas count] > 0 && indexPath.row >= [arregloResultadosBusquedaLugares count] && indexPath.row < [arregloResultadosBusquedaLugares count]+[arregloResultadosBusquedaPersonas count]){
            
            celda = [tabla dequeueReusableCellWithIdentifier:@"celdaBusqueda"];
            
            UIImageView *foto = (UIImageView *)[celda viewWithTag:1];
            
            [foto.layer setCornerRadius:25];
            [foto.layer setBorderWidth:1];
            [foto.layer setBorderColor:[[UIColor colorWithWhite:1 alpha:1] CGColor]];
            foto.layer.masksToBounds = YES;
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",[[[arregloResultadosBusquedaPersonas objectAtIndex:indexPath.row - [arregloResultadosBusquedaLugares count]] objectForKey:@"ID"] intValue],[[arregloResultadosBusquedaPersonas objectAtIndex:indexPath.row - [arregloResultadosBusquedaLugares count]] objectForKey:@"foto"]]];
            
            [foto sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
                
                if(error == nil){
                    
                    [foto.layer setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                    [foto setContentMode:UIViewContentModeScaleToFill];
                }
            }];
            
            UILabel *nombre = (UILabel *)[celda viewWithTag:2];
            
            NSString *strnombre;
            NSString *strapellido;
            
            if(![[[arregloResultadosBusquedaPersonas objectAtIndex:indexPath.row-[arregloResultadosBusquedaLugares count]] objectForKey:@"nombre"] isEqualToString:@"(null)"]){
                
                strnombre = [[arregloResultadosBusquedaPersonas objectAtIndex:indexPath.row-[arregloResultadosBusquedaLugares count]] objectForKey:@"nombre"];
            }else{
            
                strnombre = @"";
            }
            
            if(![[[arregloResultadosBusquedaPersonas objectAtIndex:indexPath.row-[arregloResultadosBusquedaLugares count]] objectForKey:@"apeido"] isEqualToString:@"(null)"]){
                
                strapellido = [[arregloResultadosBusquedaPersonas objectAtIndex:indexPath.row-[arregloResultadosBusquedaLugares count]] objectForKey:@"apeido"];
            }else{
            
                strapellido = @"";
            }
            
            nombre.text = [NSString stringWithFormat:@"%@ %@",strnombre,strapellido];
            
            UILabel *es = (UILabel *)[celda viewWithTag:3];
            es.text = @"Persona";
        }else if([arregloResultadosBusquedaLugares count] == 0 && [arregloResultadosBusquedaPersonas count] == 0){
            
            celda = [[UITableViewCell alloc] init];
        }
    }
    
    return celda;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda = [tableView cellForRowAtIndexPath:indexPath];
    
    NSString *tipo = [(UILabel *)[celda viewWithTag:3] text];
    
    if([tipo isEqualToString:@"Lugar"]){

        UIStoryboard *storybard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

        PerfilLugarViewController *lugar = (PerfilLugarViewController*)[storybard instantiateViewControllerWithIdentifier:@"perfilLugar"];
        
        [lugar setIdLugar:[[[arregloResultadosBusquedaLugares objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue]];
        
        [lugar setNombreLugar:[[arregloResultadosBusquedaLugares objectAtIndex:indexPath.row] objectForKey:@"nombre"]];
        
        [lugar setFotoPrincipalLugar:[[arregloResultadosBusquedaLugares objectAtIndex:indexPath.row] objectForKey:@"foto"]];
        
        [lugar setFotoSecundariaLugar:[[arregloResultadosBusquedaLugares objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[arregloResultadosBusquedaLugares objectAtIndex:indexPath.row] objectForKey:@"latitud"] floatValue] longitude:[[[arregloResultadosBusquedaLugares objectAtIndex:indexPath.row] objectForKey:@"longitud"] floatValue]];
        
        [lugar setLocation:location];
        
        
       /* UIImageView *foto = (UIImageView*)[celda viewWithTag:1];
        
        lugarViewController *lugar = [self.storyboard instantiateViewControllerWithIdentifier:@"Lugar"];
        
        lugar.nombreLugar = [[arregloResultadosBusquedaLugares objectAtIndex:indexPath.row] objectForKey:@"nombre"];
        
        lugar.dataFoto = UIImageJPEGRepresentation([foto image], 1);
        
        lugar.idLugar = [[[arregloResultadosBusquedaLugares objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue];
        
        lugar.fotoLugar = [[arregloResultadosBusquedaLugares objectAtIndex:indexPath.row] objectForKey:@"foto"];*/
        
        [tabla deselectRowAtIndexPath:indexPath animated:YES];
        
        [self.navigationController pushViewController:lugar animated:YES];
        
    }else if([tipo isEqualToString:@"Persona"]){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PerfilUsuarioViewController *perfil = [storyboard instantiateViewControllerWithIdentifier:@"perfilUsuario"];
        
        [perfil setIdUsuario:[[[arregloResultadosBusquedaPersonas objectAtIndex:indexPath.row-[arregloResultadosBusquedaLugares count]] objectForKey:@"ID"] intValue]];
        
        [perfil setNombreUsuario:[NSString stringWithFormat:@"%@ %@",[[arregloResultadosBusquedaPersonas objectAtIndex:indexPath.row-[arregloResultadosBusquedaLugares count]] objectForKey:@"nombre"] ,[[arregloResultadosBusquedaPersonas objectAtIndex:indexPath.row-[arregloResultadosBusquedaLugares count]] objectForKey:@"apeido"]]];
        
        [perfil setFotoUsuario:[[arregloResultadosBusquedaPersonas objectAtIndex:indexPath.row-[arregloResultadosBusquedaLugares count]] objectForKey:@"foto"]];
        
        [tabla deselectRowAtIndexPath:indexPath animated:YES];
        
        [self.navigationController pushViewController:perfil animated:YES];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [campoBusqueda resignFirstResponder];
}


#pragma mark - metodos campo texto


-(void)enviarBusqueda{
    
    [coneccion cancel];
    
    [arregloResultadosBusquedaPersonas removeAllObjects];
    [arregloResultadosBusquedaLugares removeAllObjects];
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
        [tabla reloadData];
    });
    
    if([[campoBusqueda text] length] > 0){
    
        envioActual++;
        
        UIApplication *app = [UIApplication sharedApplication];
        app.networkActivityIndicatorVisible = YES;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        palabra =campoBusqueda.text;
        
        NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/busqueda.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"key=%@&id=%@&token=%@",palabra,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        coneccion = [[NSURLConnection alloc] initWithRequest:peticion delegate:self];
        
        if(coneccion){
        
            datos = [NSMutableData data];
            
            [coneccion start];
        }
        
    }else if([[campoBusqueda text] length] == 0){
    
        [arregloResultadosBusquedaLugares removeAllObjects];
        [arregloResultadosBusquedaPersonas removeAllObjects];
        dispatch_async(dispatch_get_main_queue(), ^{
            [tabla reloadData];
        });
    }
}

#pragma mark - nsurlrequest

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{

   /* dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *seenvia = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"error: %@",error] message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:nil];
        
        [seenvia addAction:ok];
        
        [self presentViewController:seenvia animated:YES completion:nil];
    });*/
    NSLog(@"error");
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{

    [datos setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{

    [datos appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{

   /* UIAlertController *seenvia = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"llego: %@",[[NSString alloc] initWithData:datos encoding:NSUTF8StringEncoding]] message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:nil];
    
    [seenvia addAction:ok];
    
    [self presentViewController:seenvia animated:YES completion:nil];*/
    
    @try{
    
        NSError *error = nil;
    
        __block NSDictionary *json = [NSJSONSerialization JSONObjectWithData:datos options:NSJSONReadingMutableContainers error:&error];
 
    
        __block NSRegularExpression *regexcorreo = [[NSRegularExpression alloc] initWithPattern:@"^[0-9a-z_\\-\\.]+@[0-9a-z\\-\\.]+\\.[a-z]{2,4}$" options: NSRegularExpressionCaseInsensitive error:&error];
    
        __block NSRegularExpression *regexsecreto = [[NSRegularExpression alloc] initWithPattern:@"[0-9a-zA-Z_\\-]+@[0-9a-zA-Z]+$" options:NSRegularExpressionCaseInsensitive error:&error];
    
        if([[regexcorreo matchesInString:palabra options:0 range:NSMakeRange(0, [palabra length])] count] == 1){
        
            [arregloResultadosBusquedaPersonas addObject:json];
        }else if([[regexsecreto matchesInString:palabra options:0 range:NSMakeRange(0, [palabra length])] count] == 1){
        
            [arregloResultadosBusquedaPersonas addObject:json];
        }
    
        if(json == NULL){
        
            [arregloResultadosBusquedaLugares removeAllObjects];
            [arregloResultadosBusquedaPersonas removeAllObjects];
            dispatch_async(dispatch_get_main_queue(), ^{
                [tabla reloadData];
            });
            
        }else{
        
            NSArray *lugares = [json objectForKey:@"lugares"];
            NSArray *personas = [json objectForKey:@"personas"];
        
        
            if(![lugares isEqual:[NSNull null]]){
                for(NSDictionary *intervalo in lugares){
                
                    if(intervalo != NULL){
                        [arregloResultadosBusquedaLugares addObject:intervalo];
                    }
                }
            }
        
            if(![personas isEqual:[NSNull null]]){
            
                for(NSDictionary *intervalo in personas){
                
                    if(intervalo != NULL){
                        [arregloResultadosBusquedaPersonas addObject:intervalo];
                    }
                }
            }
            
            if([lugares isEqual:[NSNull null]] && [personas isEqual:[NSNull null]]){

                [arregloResultadosBusquedaLugares addObject:[NSNumber numberWithInt:-1]];
                [arregloResultadosBusquedaPersonas addObject:[NSNumber numberWithInt:-1]];
            }
        
            dispatch_async(dispatch_get_main_queue(), ^{
            
                [tabla reloadData];

            });
        }
    }@catch(NSException *ex){}

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
