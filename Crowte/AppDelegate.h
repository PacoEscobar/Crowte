//
//  AppDelegate.h
//  Crowte
//
//  Created by Paco Escobar on 22/04/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


-(void)saveContext;
-(NSURL *)applicationDocumentsDirectory;
-(void)initializeStoryBoardBasedOnScreenSize;

-(void)updateNotifToken;


@end
