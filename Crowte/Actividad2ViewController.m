//
//  ActividadViewController.m
//  Crowte
//
//  Created by Paco Escobar on 09/01/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIImageView+WebCache.h>
#import <CoreLocation/CoreLocation.h>
#import <stdlib.h>
#import <Lottie/Lottie.h>
#import "Actividad2ViewController.h"
#import "CustomPinAnnotation.h"
#import "CustomPointAnnotation.h"
#import "BuscarViewController.h"
#import "ReportarCheckinTableViewController.h"
#import "CheckinViewController.h"
#import "NavCheckinViewController.h"
#import "PerfilLugarViewController.h"
#import "EasyTip/RCEasyTipView.h"

@interface Actividad2ViewController ()

@end

@implementation Actividad2ViewController

@synthesize seMuestraMapa, mapa, fondoMapa, listaCheckin, checkinsTodosOtros, checkinsTodosUltimos, numeroCambiosMapa,previousScrollingViewYOffset, constraintTablaOriginY, intervaloNumSolicitudes, intervaloActualizarLista, miLocation, vistaEmptyState, vistaNoLocation, ultimoRelIDTodos, ultimoRelIDAmigos, backgroundTutorial, opcionesSeleccionadas, tutoriales;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    seMuestraMapa = NO;
    numeroCambiosMapa = 0;
    ultimoRelIDTodos = 0;
    ultimoRelIDAmigos = 0;
    
    opcionesSeleccionadas = [NSMutableDictionary dictionary];
    
    UIImage *imageSearch = [UIImage imageNamed:@"search-icon.png"];
    
    UIButton *search = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imageSearch.size.width, imageSearch.size.height)];
    
    [search setImage:imageSearch forState:UIControlStateNormal];
    
    [search addTarget:self action:@selector(buscar:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barbtnsearch = [[UIBarButtonItem alloc] initWithCustomView:search];
    
    self.navigationItem.rightBarButtonItem = barbtnsearch;
    
    [[UINavigationBar appearance] setTranslucent:YES];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"transparente.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@"transparente.png"]];
    
    UIView *division = [[UIView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.width, 0.5)];
    
    [division setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.3]];
    
    [division setTag:1];
    
    [self.navigationController.navigationBar addSubview:division];
    
    self.navigationItem.title = NSLocalizedString(@"Actividad", nil);
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
        
    //intervaloNumSolicitudes = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getNotificacionesNuevas) userInfo:nil repeats:YES];
    
    if([CLLocationManager locationServicesEnabled]){
        
        if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse){
            [self getCheckins];
        }else{
            
            [self setNoLocalizacion];
        }
    }else{
        
        [self setNoLocalizacion];
    }
    
    [self getNotificacionesNuevas];
    
    UIView *globoDialogo = [backgroundTutorial viewWithTag:10];
    
    [[globoDialogo layer] setCornerRadius:20];
    [[globoDialogo layer] setMasksToBounds:YES];
    
    UIImageView *pinicito = (UIImageView*)[backgroundTutorial viewWithTag:11];

    UITapGestureRecognizer *tapPinicito = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cerrarTutorial:)];
    
    [pinicito addGestureRecognizer:tapPinicito];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - lista check-ins

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *vista = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    
    [vista setBackgroundColor:[[tableView backgroundColor] colorWithAlphaComponent:0.8]];
    
    UILabel *titulo = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    
    [titulo setTextColor:[UIColor whiteColor]];
    [titulo setFont:[UIFont fontWithName:@"Hind-Regular" size:13]];
    
    if(section == 0){
        
        [titulo setText:@"Justo ahora"];
    }else{
        
        [titulo setText:@"Pasados"];
    }
    
    [titulo setTextAlignment:NSTextAlignmentCenter];
    
    [vista addSubview:titulo];
    
    return vista;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(section == 0){
        
            if([checkinsTodosUltimos count] > 0 && ![[checkinsTodosUltimos objectAtIndex:0] objectForKey:@"valido"]){
                
                return [checkinsTodosUltimos count];
            }else{
                
                return 0;
            }
        
    }else{
        
            if(![[checkinsTodosOtros objectAtIndex:0] objectForKey:@"valido"]){
                
                return [checkinsTodosOtros count];
            }else{
                
                return 0;
            }
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 375;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *celda;
    NSMutableArray *lugaresPlaceholder;
    
    celda = [tableView dequeueReusableCellWithIdentifier:@"celdaCheckin"];
    
        if(indexPath.section == 0){
            
            lugaresPlaceholder = [NSMutableArray arrayWithArray:checkinsTodosUltimos];
        }else if(indexPath.section == 1){
            
            lugaresPlaceholder = [NSMutableArray arrayWithArray:checkinsTodosOtros];
        }
        
        
        UIImageView *fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoceldanuevo.png"]];
        
        [celda setBackgroundView:fondo];
        [celda setBackgroundColor:[UIColor clearColor]];
        
        UILabel *numeroDeMegusta = (UILabel*)[celda viewWithTag:1];
        
        if([[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"cantidad"] isEqualToString:@"1"]){
            
            [numeroDeMegusta setText:[NSString stringWithFormat: NSLocalizedString(@"%@ persona está aquí", nil),[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"cantidad"]]];
        }else{
            
            [numeroDeMegusta setText:[NSString stringWithFormat:NSLocalizedString(@"%@ personas están aquí", nil),[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"cantidad"]]];
        }
        
        UILabel *nombreLugar = (UILabel*)[celda viewWithTag:2];
        
        [nombreLugar setText:[NSString stringWithFormat:@"%@",[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"lugar"]]];
        
        if(indexPath.section == 0){
            
            UILabel *tiempoDeCheckin = (UILabel*)[celda viewWithTag:3];
            [tiempoDeCheckin setText: NSLocalizedString(@"En vivo", nil)];
            
            [tiempoDeCheckin setTextColor:[UIColor redColor]];
            
        }else if(indexPath.section == 1){
            
            UILabel *tiempoDeCheckin = (UILabel*)[celda viewWithTag:3];
            [tiempoDeCheckin setText:[NSString stringWithFormat:NSLocalizedString(@"%@ a esta hora", nil),[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"fechaDescripcion"]]];
            
            [tiempoDeCheckin setTextColor:[UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1]];
            
        }
        
        
        NSArray *constraints = [[celda contentView] constraints];
        NSLayoutConstraint *constraintInferior;
        
        for (NSLayoutConstraint *constraint in constraints) {
            
            if([[constraint identifier] isEqualToString:@"constraintImagenBottom"]){
                
                constraintInferior = constraint;
            }
        }
        
        
        UIImageView *fondoLugar = (UIImageView*)[celda viewWithTag:4];
        [fondoLugar setImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
        
        NSString *mensaje = [[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"acontesimiento"];
        
        NSData *data = [mensaje dataUsingEncoding:NSUTF8StringEncoding];
        
        mensaje = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        
        mensaje = [mensaje stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        float adicional = 0;
        
        if(constraintInferior != nil){
            
            if(![mensaje isEqualToString:@""]){
                
                if([mensaje length] > 44){
                    
                    adicional = ceil([mensaje length]/44);
                    adicional = adicional*55;
                }else{
                    
                    adicional = 55;
                }
            }
            
            [constraintInferior setConstant:62+adicional];
        }
        
        if([celda viewWithTag:11]){
            
            [[celda viewWithTag:11] removeFromSuperview];
        }
        
        if(![mensaje isEqualToString:@""]){
            
            UITextView *texto = [[UITextView alloc] initWithFrame:CGRectMake(fondoLugar.frame.origin.x, fondoLugar.frame.origin.y+fondoLugar.frame.size.height, fondoLugar.frame.size.width-100, 100)];
            
            [texto setText:mensaje];
            
            [texto setFont:[UIFont fontWithName:@"Raleway-Regular" size:13]];
            [texto setTextColor:[UIColor colorWithRed:48.0/255.0 green:52.0/255.0 blue:63.0/255.0 alpha:1]];
            
            [texto setBackgroundColor:[UIColor colorWithRed:246.0/255.0 green:244.0/255.0 blue:243.0/255.0 alpha:1]];
            
            [texto setTextContainerInset:UIEdgeInsetsMake(20, 20, 20, 20)];
            
            [texto setTag:11];
            
            [texto setUserInteractionEnabled:NO];
            [texto setScrollEnabled:NO];
            [texto setEditable:NO];
            
            [texto setContentMode:UIViewContentModeCenter];
            
            texto.translatesAutoresizingMaskIntoConstraints = NO;
            
            [celda addSubview:texto];
            
            [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:fondoLugar attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-302]];
            
            [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:[texto superview] attribute:NSLayoutAttributeTrailingMargin relatedBy:NSLayoutRelationEqual toItem:texto attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:1]];
            
            [[texto superview] addConstraint:[NSLayoutConstraint constraintWithItem:texto attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:fondoLugar attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
            
        }else{
            
            UITextView *texto = (UITextView*)[celda viewWithTag:11];
            
            [texto removeFromSuperview];
        }
        
        UIImageView *fotoPrincipalLugar = (UIImageView*)[celda viewWithTag:5];
        [fotoPrincipalLugar setBackgroundColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
        
        [[fotoPrincipalLugar layer] setBorderWidth:1];
        [[fotoPrincipalLugar layer] setBorderColor:[[UIColor whiteColor] CGColor]];
        [[fotoPrincipalLugar layer] setCornerRadius:50];
        [[fotoPrincipalLugar layer] setMasksToBounds:YES];
    
        [fotoPrincipalLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ID"],[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"foto"]]] placeholderImage:[UIImage imageNamed:@"cblancav3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType imageCacheType, NSURL *imageURL){
            
            if(error.code == 0){
                
                [fotoPrincipalLugar.layer setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1] CGColor]];
                [fotoPrincipalLugar setContentMode:UIViewContentModeScaleToFill];
            }else{
                
                [fotoPrincipalLugar.layer setBorderColor:[[UIColor whiteColor] CGColor]];
                [fotoPrincipalLugar setContentMode:UIViewContentModeCenter];
            }
        }];
        
        UIImageView *fotoSecundariaLugar = (UIImageView*)[celda viewWithTag:4];
        
        [fotoSecundariaLugar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/lugares/%@/%@",[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ID"],[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]]] placeholderImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
        
        
        UILabel *enEsteMomentoTexto = (UILabel*)[celda viewWithTag:7];
        
        [enEsteMomentoTexto setHidden:YES];
        
        UIScrollView *contenedorOpciones = (UIScrollView*)[celda viewWithTag:6];
    
    if(indexPath.section == 0){
        
        [contenedorOpciones setHidden:NO];
                
        if(![celda viewWithTag:109]){
            
            int i=0;
            
            for(id key in [[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ambiente"]){
                
                if(![contenedorOpciones viewWithTag:110+i]){
                    LOTAnimationView *animacion;
                    
                    if([key  isEqual: @"meGusta"]){
                        
                        if([[[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:key] isEqual:[NSNumber numberWithInt:1]]){
                            
                            animacion = [LOTAnimationView animationNamed:@"me_gusta"];
                            
                            [[animacion layer] setBorderColor:[[UIColor colorWithRed:39.0/255.0 green:140.0/255.0 blue:233.0/255.0 alpha:1] CGColor]];
                            
                            [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                            
                            
                            [animacion setTag:110+i];
                            
                            [contenedorOpciones addSubview:animacion];
                            
                            [animacion setAnimationProgress:0.2];
                            
                            [[animacion layer] setBorderWidth:1];
                            [[animacion layer] setCornerRadius:15];
                            
                            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionMeGusta:)];
                            
                            [animacion addGestureRecognizer:tapGesture];
                            
                            i++;
                            
                        }else if([[[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:key] isEqual:[NSNumber numberWithInt:0]]){
                            
                            animacion = [LOTAnimationView animationNamed:@"no_me_gusta"];
                            
                            [[animacion layer] setBorderColor:[[UIColor redColor] CGColor]];
                            
                            [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                            
                            
                            [animacion setTag:110+i];
                            
                            [contenedorOpciones addSubview:animacion];
                            
                            [animacion setAnimationProgress:0.2];
                            
                            [[animacion layer] setBorderWidth:1];
                            [[animacion layer] setCornerRadius:15];
                            
                            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionMeGustaNegativo:)];
                            
                            [animacion addGestureRecognizer:tapGesture];
                            
                            i++;
                        }
                        
                        [animacion setAnimationProgress:0.9];
                        
                    }else if([key isEqual:@"estaLleno"]){
                        
                        if([[[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:key] isEqual:[NSNumber numberWithInt:1]]){
                            animacion = [LOTAnimationView animationNamed:@"aun_lugares"];
                            
                            [[animacion layer] setBorderColor:[[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] CGColor]];
                            
                            [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                            
                            
                            [animacion setTag:110+i];
                            
                            [contenedorOpciones addSubview:animacion];
                            
                            [animacion setAnimationProgress:0.2];
                            
                            [[animacion layer] setBorderWidth:1];
                            [[animacion layer] setCornerRadius:15];
                            
                            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionEstaLleno:)];
                            
                            [animacion addGestureRecognizer:tapGesture];
                            
                            i++;
                        }else if([[[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:key] isEqual:[NSNumber numberWithInt:0]]){
                            
                            animacion = [LOTAnimationView animationNamed:@"esta_lleno"];
                            
                            [[animacion layer] setBorderColor:[[UIColor colorWithRed:228.0/255.0 green:0 blue:124.0/255.0 alpha:1] CGColor]];
                            
                            [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                            
                            
                            [animacion setTag:110+i];
                            
                            [contenedorOpciones addSubview:animacion];
                            
                            [animacion setAnimationProgress:0.2];
                            
                            [[animacion layer] setBorderWidth:1];
                            [[animacion layer] setCornerRadius:15];
                            
                            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionEstaLlenoNegativo:)];
                            
                            [animacion addGestureRecognizer:tapGesture];
                            
                            i++;
                        }
                        
                        [animacion setAnimationProgress:0.9];
                        
                    }else if([key isEqual:@"meGustaMusica"]){
                        
                        if([[[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:key] isEqual:[NSNumber numberWithInt:1]]){
                            animacion = [LOTAnimationView animationNamed:@"check_nota"];
                            
                            [[animacion layer] setBorderColor:[[UIColor purpleColor] CGColor]];
                            
                            [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                            
                            
                            [animacion setTag:110+i];
                            
                            [contenedorOpciones addSubview:animacion];
                            
                            [animacion setAnimationProgress:0.2];
                            
                            [[animacion layer] setBorderWidth:1];
                            [[animacion layer] setCornerRadius:15];
                            
                            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionMeGustaMusica:)];
                            
                            [animacion addGestureRecognizer:tapGesture];
                            
                            i++;
                            
                        }else if([[[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:key] isEqual:[NSNumber numberWithInt:0]]){
                            
                            animacion = [LOTAnimationView animationNamed:@"check_nota_nogusta"];
                            
                            [[animacion layer] setBorderColor:[[UIColor grayColor] CGColor]];
                            
                            [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                            
                            
                            [animacion setTag:110+i];
                            
                            [contenedorOpciones addSubview:animacion];
                            
                            [animacion setAnimationProgress:0.2];
                            
                            [[animacion layer] setBorderWidth:1];
                            [[animacion layer] setCornerRadius:15];
                            
                            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionMeGustaMusicaNegativo:)];
                            
                            [animacion addGestureRecognizer:tapGesture];
                            
                            i++;
                        }
                        
                        [animacion setAnimationProgress:0.9];
                    }else if([key isEqual:@"servicioRapido"]){
                        
                        if([[[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:key] isEqual:[NSNumber numberWithInt:1]]){
                            
                            animacion = [LOTAnimationView animationNamed:@"servicio_rapido"];
                            
                            [[animacion layer] setBorderColor:[[UIColor blueColor] CGColor]];
                            
                            [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                            
                            
                            [animacion setTag:110+i];
                            
                            [contenedorOpciones addSubview:animacion];
                            
                            [animacion setAnimationProgress:0.2];
                            
                            [[animacion layer] setBorderWidth:1];
                            [[animacion layer] setCornerRadius:15];
                            
                            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionServicioRapido:)];
                            
                            [animacion addGestureRecognizer:tapGesture];
                            
                            i++;
                        }else if([[[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:key] isEqual:[NSNumber numberWithInt:0]]){
                            
                            animacion = [LOTAnimationView animationNamed:@"servicio_lento"];
                            
                            [[animacion layer] setBorderColor:[[UIColor grayColor] CGColor]];
                            
                            [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                            
                            
                            [animacion setTag:110+i];
                            
                            [contenedorOpciones addSubview:animacion];
                            
                            [animacion setAnimationProgress:0.2];
                            
                            [[animacion layer] setBorderWidth:1];
                            [[animacion layer] setCornerRadius:15];
                            
                            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionServicioRapidoNegativo:)];
                            
                            [animacion addGestureRecognizer:tapGesture];
                            
                            i++;
                        }
                        
                        [animacion setAnimationProgress:0.9];
                    }else if([key isEqual:@"servicioBueno"]){
                        
                        if([[[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:key] isEqual:[NSNumber numberWithInt:1]]){
                            
                            animacion = [LOTAnimationView animationNamed:@"buen_servicio"];
                            
                            [[animacion layer] setBorderColor:[[UIColor colorWithRed:0 green:170.0/255.0 blue:0 alpha:1] CGColor]];
                            
                            [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                            
                            [animacion setTag:110+i];
                            
                            [contenedorOpciones addSubview:animacion];
                            
                            [animacion setAnimationProgress:0.2];
                            
                            [[animacion layer] setBorderWidth:1];
                            [[animacion layer] setCornerRadius:15];
                            
                            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionServicioBueno:)];
                            
                            [animacion addGestureRecognizer:tapGesture];
                            i++;
                        }else if([[[[lugaresPlaceholder objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:key] isEqual:[NSNumber numberWithInt:0]]){
                            animacion = [LOTAnimationView animationNamed:@"mal_servicio"];
                            
                            [[animacion layer] setBorderColor:[[UIColor redColor] CGColor]];
                            
                            [animacion setFrame:CGRectMake((30*i)+(20*i), contenedorOpciones.frame.size.height-40, 30, 30)];
                            
                            [animacion setTag:110+i];
                            
                            [contenedorOpciones addSubview:animacion];
                            
                            [animacion setAnimationProgress:0.2];
                            
                            [[animacion layer] setBorderWidth:1];
                            [[animacion layer] setCornerRadius:15];
                            
                            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(altOpcionServicioBuenoNegativo:)];
                            
                            [animacion addGestureRecognizer:tapGesture];
                            i++;
                        }
                        
                        [animacion setAnimationProgress:0.9];
                    }
                }
            }
                        
            [contenedorOpciones setContentSize:CGSizeMake(contenedorOpciones.frame.size.width, contenedorOpciones.frame.size.height)];
            
            
        }
    }else{
        
        [contenedorOpciones setHidden:YES];
    }
    
    
    
    return celda;
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 1){
        
            if([cell viewWithTag:109]){
                
                LOTAnimationView *animacion1 = (LOTAnimationView*)[cell viewWithTag:109];
                [animacion1 setAnimationProgress:0.2];
                [animacion1 play];
                
                LOTAnimationView *animacion2 = (LOTAnimationView*)[cell viewWithTag:110];
                [animacion2 setAnimationProgress:0.2];
                [animacion2 play];
                
                LOTAnimationView *animacion3 = (LOTAnimationView*)[cell viewWithTag:111];
                [animacion3 setAnimationProgress:0.2];
                [animacion3 play];
                
            }
        
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIStoryboard *storybard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    PerfilLugarViewController *lugar = (PerfilLugarViewController*)[storybard instantiateViewControllerWithIdentifier:@"perfilLugar"];
    
    
    if(indexPath.section == 0){
        
            [lugar setIdLugar:[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"objeto"] intValue]];
            
            [lugar setNombreLugar:[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
            
            [lugar setFotoPrincipalLugar:[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"foto"]];
            
            [lugar setFotoSecundariaLugar:[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]];
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"latitud"] floatValue] longitude:[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"longitud"] floatValue]];
            
            [lugar setLocation:location];
            
            [lugar setOpcionesAmbienteLugar:[@{@"meGusta":[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:@"meGusta"],@"estaLleno":[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:@"estaLleno"],@"meGustaMusica":[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:@"meGustaMusica"],@"servicioRapido":[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:@"servicioRapido"],@"servicioBueno":[[[checkinsTodosUltimos objectAtIndex:indexPath.row] objectForKey:@"ambiente"] objectForKey:@"servicioBueno"]} mutableCopy]];
            
            
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            
            UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
            self.navigationItem.backBarButtonItem = atras;
            
            [self.navigationController pushViewController:lugar animated:YES];
            
        
    }else{
        
            [lugar setIdLugar:[[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"ID"] intValue]];
            
            [lugar setNombreLugar:[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"lugar"]];
            
            [lugar setFotoPrincipalLugar:[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"foto"]];
            
            [lugar setFotoSecundariaLugar:[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"fotoSecundaria"]];
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"latitud"] floatValue] longitude:[[[checkinsTodosOtros objectAtIndex:indexPath.row] objectForKey:@"longitud"] floatValue]];
            
            [lugar setLocation:location];
            
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            
            UIBarButtonItem *atras = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil];
            self.navigationItem.backBarButtonItem = atras;
            
            [self.navigationController pushViewController:lugar animated:YES];
        
    }
}

#pragma mark - lista metodos



- (IBAction)cambiarListaTodosAmigos:(id)sender {
    
    [listaCheckin reloadData];
    
        if([[[checkinsTodosUltimos objectAtIndex:0] objectForKey:@"valido"] intValue] == 0 && [[[checkinsTodosOtros objectAtIndex:0] objectForKey:@"valido"] intValue] == 0){
            
            [listaCheckin setHidden:YES];
            if(!seMuestraMapa){
                [vistaEmptyState setHidden:NO];
            }
        }else{
            
            if(!seMuestraMapa){
                [listaCheckin setHidden:NO];
            }
            [vistaEmptyState setHidden:YES];
        }
        
        [self agregarPines:checkinsTodosUltimos viejos:checkinsTodosOtros];
        
    
    
}

#pragma mark - mapa

-(void)mostrarMapa:(id)sender{
    
    if(!seMuestraMapa){
        
        
        [self agregarPines:checkinsTodosUltimos viejos:checkinsTodosOtros];
        
        
        [listaCheckin setHidden:YES];
        [fondoMapa setHidden:NO];
        [vistaEmptyState setHidden:YES];
        
        UIImage *imageLista = [UIImage imageNamed:@"lista.png"];
        
        UIButton *lista = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imageLista.size.width, imageLista.size.height)];
        
        [lista setImage:imageLista forState:UIControlStateNormal];
        
        [lista addTarget:self action:@selector(mostrarMapa:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barbtnlista = [[UIBarButtonItem alloc] initWithCustomView:lista];
        
        self.navigationItem.leftBarButtonItem = barbtnlista;
        
        
    }else{
        
        [listaCheckin setHidden:NO];
        [fondoMapa setHidden:YES];
        
            if([[[checkinsTodosUltimos objectAtIndex:0] objectForKey:@"valido"] intValue] == 0 && [[[checkinsTodosOtros objectAtIndex:0] objectForKey:@"valido"] intValue] == 0){
                
                [vistaEmptyState setHidden:NO];
            }else{
                
                [vistaEmptyState setHidden:YES];
            }
        
        
        UIImage *imageMapview = [UIImage imageNamed:@"mapview.png"];
        
        UIButton *mapview = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, imageMapview.size.width, imageMapview.size.height)];
        
        [mapview setImage:imageMapview forState:UIControlStateNormal];
        
        [mapview addTarget:self action:@selector(mostrarMapa:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barbtnmapview = [[UIBarButtonItem alloc] initWithCustomView:mapview];
        
        self.navigationItem.leftBarButtonItem = barbtnmapview;
        
    }
    
    seMuestraMapa = !seMuestraMapa;
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    
    if(numeroCambiosMapa <= 1){
        
        float spanX = 0.09905913041;
        float spanY = 0.09905913041;
        
        MKCoordinateRegion region;
        region.center.latitude = mapa.userLocation.location.coordinate.latitude;
        region.center.longitude = mapa.userLocation.location.coordinate.longitude;
        region.span.latitudeDelta = spanX;
        region.span.longitudeDelta = spanY;
        
        [mapa setRegion:region animated:YES];
    }
    
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    if(![view.annotation isKindOfClass:[MKUserLocation class]]){
        
        CustomPinAnnotation *customPin = (CustomPinAnnotation*)view;
        
        UIView *vista = [[UIView alloc] initWithFrame:CGRectMake(view.bounds.origin.x-(mapa.frame.size.width/2),view.bounds.origin.y-150, mapa.frame.size.width-20, 150)];
        [vista setTag:1];
        
        UIImageView *fotoPersona = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
        [fotoPersona sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://crowte.com/usuarios/%d/%@",[customPin idUsuario],[customPin fotoPersona]]] placeholderImage:[UIImage imageNamed:@"imageprofileempty.png"]];
        
        [[fotoPersona layer] setBorderColor:[[UIColor whiteColor] CGColor]];
        [[fotoPersona layer] setBorderWidth:1];
        [[fotoPersona layer] setCornerRadius:15];
        [[fotoPersona layer] setMasksToBounds:YES];
        
        UIImageView *fondo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, vista.frame.size.width, vista.frame.size.height)];
        
        [fondo setImage:[UIImage imageNamed:@"fondolugarvacio.png"]];
        
        UIImageView *fotoLugar = [[UIImageView alloc] initWithFrame:CGRectMake(vista.frame.size.width-(vista.frame.size.width/2)-25, vista.frame.size.height-(vista.frame.size.height/2)-25, 50, 50)];
        [fotoLugar setImage:[UIImage imageNamed:@"cblanca.png"]];
        [[fotoLugar layer] setBorderColor:[[UIColor colorWithWhite:1 alpha:0] CGColor]];
        [[fotoLugar layer] setBorderWidth:1];
        [[fotoLugar layer] setCornerRadius:25];
        [[fotoLugar layer] setMasksToBounds:YES];
        
        UILabel *lugar = [[UILabel alloc] initWithFrame:CGRectMake(0, [vista frame].size.height-23, vista.frame.size.width, 23)];
        [lugar setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.2]];
        [lugar setTextColor:[UIColor whiteColor]];
        [lugar setTextAlignment:NSTextAlignmentCenter];
        [lugar setText:[customPin lugar]];
        
        [lugar setFont:[UIFont fontWithName:@"Hind-Light" size:17]];
        
        UILabel *nombre = [[UILabel alloc] initWithFrame:CGRectMake(fotoPersona.frame.origin.x+33, fotoPersona.frame.origin.y+5, vista.frame.size.width-(fotoPersona.frame.origin.x+33), 20)];
        
        [nombre setText:[customPin persona]];
        [nombre setTextColor:[UIColor whiteColor]];
        [nombre setFont:[UIFont fontWithName:@"Hind-Light" size:12]];
        
        [vista addSubview:fondo];
        [vista addSubview:fotoLugar];
        [vista addSubview:lugar];
        [vista addSubview:fotoPersona];
        [vista addSubview:nombre];
        
        [view addSubview:vista];
    }
    
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
    
    UIView *vista = [view viewWithTag:1];
    [vista removeFromSuperview];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    
    numeroCambiosMapa++;
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    if([annotation isKindOfClass:[MKUserLocation class]]){
        
        return nil;
    }
    
    int idUsuario = [(CustomPinAnnotation*)annotation idUsuario];
    
    BOOL enVivo = [(CustomPointAnnotation*)annotation enVivo];
    NSString *lugar = [(CustomPointAnnotation*)annotation lugar];
    NSString *nombre = [(CustomPinAnnotation*)annotation persona];
    NSString *fotoUsuario = [(CustomPinAnnotation*)annotation fotoPersona];
    
    CustomPinAnnotation *pin = [[CustomPinAnnotation alloc] initWithAnnotation:annotation type:enVivo idUsuario:idUsuario lugar:lugar persona:nombre fotoPersona:fotoUsuario fotoLugar:nil fondoLugar:nil];
    
    if(enVivo){
        
        [[pin superview] bringSubviewToFront:pin];
    }
    
    return pin;
}

-(void)agregarPines:(NSArray *)checkinesNuevos viejos:(NSArray *)checkinesViejos{
    
    NSMutableDictionary *repetidos = [[NSMutableDictionary alloc] init];
    NSMutableArray *listaCheckinsRepetidos = [NSMutableArray array];
    
    [mapa removeAnnotations:[mapa annotations]];
    
    
    if([[[checkinesViejos objectAtIndex:0] objectForKey:@"valido"] intValue] != 0){
        
        for(int i = 0; i<[checkinesViejos count]; i++){
            
            if([repetidos objectForKey:[[checkinesViejos objectAtIndex:i] objectForKey:@"lugarID"]] == NULL){
                
                NSMutableArray *arreglo = [NSMutableArray array];
                
                [arreglo addObject:[checkinesViejos objectAtIndex:i]];
                
                [repetidos setObject:arreglo forKey:[[checkinesViejos objectAtIndex:i] objectForKey:@"lugarID"]];
                
                [listaCheckinsRepetidos addObject:[[checkinesViejos objectAtIndex:i] objectForKey:@"lugarID"]];
            }else{
                
                NSMutableArray *arreglo = [repetidos objectForKey:[[checkinesViejos objectAtIndex:i] objectForKey:@"lugarID"]];
                
                [arreglo addObject:[checkinesViejos objectAtIndex:i]];
                
                [repetidos setObject:arreglo forKey:[[checkinesViejos objectAtIndex:i] objectForKey:@"lugarID"]];
            }
            
        }
        
        
        for(int i=0; i<[listaCheckinsRepetidos count]; i++){
            
            NSArray *arreglo = [repetidos objectForKey:[listaCheckinsRepetidos objectAtIndex:i]];
            
            for(int j=0; j<[arreglo count]; j++){
                
                CustomPointAnnotation *punto = [[CustomPointAnnotation alloc] init];
                
                
                double latitud = 0.00004502695651/j;
                double longitud = 0.00004502695651/j;
                
                if(latitud == INFINITY){
                    
                    latitud = 0;
                }
                
                if(longitud == INFINITY){
                    
                    longitud = 0;
                }
                
                CLLocationCoordinate2D result = CLLocationCoordinate2DMake([[[arreglo objectAtIndex:j] objectForKey:@"latitudLugar"] doubleValue]+latitud, [[[arreglo objectAtIndex:j] objectForKey:@"longitudLugar"] doubleValue]+longitud);
                
                punto.coordinate = CLLocationCoordinate2DMake(result.latitude, result.longitude);
                
                [punto setIdUsuario:[[[arreglo objectAtIndex:j] objectForKey:@"uID"] intValue]];
                [punto setPersona:[NSString stringWithFormat:@"%@ %@",[[arreglo objectAtIndex:j] objectForKey:@"nombre"],[[arreglo objectAtIndex:j] objectForKey:@"apellido"]]];
                [punto setFotoPersona:[[arreglo objectAtIndex:j] objectForKey:@"foto"]];
                [punto setLugar:[[arreglo objectAtIndex:j] objectForKey:@"lugar"]];
                [punto setEnVivo:NO];
                
                [mapa addAnnotation:punto];
                
            }
        }
    }
    
    
    [repetidos removeAllObjects];
    [listaCheckinsRepetidos removeAllObjects];
    
    
    if([[[checkinesNuevos objectAtIndex:0] objectForKey:@"valido"] intValue] != 0){
        
        for(int i = 0; i<[checkinesNuevos count]; i++){
            
            if([repetidos objectForKey:[[checkinesNuevos objectAtIndex:i] objectForKey:@"lugarID"]] == NULL){
                
                NSMutableArray *arreglo = [NSMutableArray array];
                
                [arreglo addObject:[checkinesNuevos objectAtIndex:i]];
                
                [repetidos setObject:arreglo forKey:[[checkinesNuevos objectAtIndex:i] objectForKey:@"lugarID"]];
                
                [listaCheckinsRepetidos addObject:[[checkinesNuevos objectAtIndex:i] objectForKey:@"lugarID"]];
            }else{
                
                NSMutableArray *arreglo = [repetidos objectForKey:[[checkinesNuevos objectAtIndex:i] objectForKey:@"lugarID"]];
                
                [arreglo addObject:[checkinesNuevos objectAtIndex:i]];
                
                [repetidos setObject:arreglo forKey:[[checkinesNuevos objectAtIndex:i] objectForKey:@"lugarID"]];
            }
            
        }
        
        
        for(int i=0; i<[listaCheckinsRepetidos count]; i++){
            
            NSArray *arreglo = [repetidos objectForKey:[listaCheckinsRepetidos objectAtIndex:i]];
            
            for(int j=0; j<[arreglo count]; j++){
                
                CustomPointAnnotation *punto = [[CustomPointAnnotation alloc] init];
                
                
                double latitud = 0.00004502695651/j;
                double longitud = 0.00004502695651/j;
                
                if(latitud == INFINITY){
                    
                    latitud = 0;
                }
                
                if(longitud == INFINITY){
                    
                    longitud = 0;
                }
                
                CLLocationCoordinate2D result = CLLocationCoordinate2DMake([[[arreglo objectAtIndex:j] objectForKey:@"latitudLugar"] doubleValue]+latitud, [[[arreglo objectAtIndex:j] objectForKey:@"longitudLugar"] doubleValue]+longitud);
                
                punto.coordinate = CLLocationCoordinate2DMake(result.latitude, result.longitude);
                
                [punto setIdUsuario:[[[arreglo objectAtIndex:j] objectForKey:@"uID"] intValue]];
                [punto setPersona:[NSString stringWithFormat:@"%@ %@",[[arreglo objectAtIndex:j] objectForKey:@"nombre"],[[arreglo objectAtIndex:j] objectForKey:@"apellido"]]];
                [punto setFotoPersona:[[arreglo objectAtIndex:j] objectForKey:@"foto"]];
                [punto setLugar:[[arreglo objectAtIndex:j] objectForKey:@"lugar"]];
                [punto setEnVivo:YES];
                
                [mapa addAnnotation:punto];
                
            }
        }
    }
    
}

#pragma mark - miselaneos

-(IBAction)buscar:(id)sender{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    BuscarViewController *buscador = [storyboard instantiateViewControllerWithIdentifier:@"buscador"];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.navigationController pushViewController:buscador animated:YES];
}

-(void)getNotificacionesNuevas{

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getnotificacionesnuevas.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"id=%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"] intValue]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if(data != nil){
            
            if(error == nil){
                
                NSDictionary *numNotif = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                UITabBar *tabBar = self.tabBarController.tabBar;
                
                UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
                
                if(![[numNotif objectForKey:@"notificaciones"] isEqualToString:@"0"] || ![[numNotif objectForKey:@"solicitudes"] isEqualToString:@"0"]){
                    
                    [tabBarItem4 setImage:[[UIImage imageNamed:@"tabnotifnotnotif.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                }else{
                    
                    [tabBarItem4 setImage:[[UIImage imageNamed:@"tabnotifnot.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                }
            }
        }
    }];
    
    [uploadTask resume];
}

-(void)actualizarCheckins{
    
    NSMutableArray *listaIdsLugares = [NSMutableArray array];
    
    for(int i=0; i < [checkinsTodosUltimos count]; i++){

        [listaIdsLugares addObject:[[checkinsTodosUltimos objectAtIndex:i] objectForKey:@"objeto"]];
    }
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:listaIdsLugares options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getinfolugaresactividad.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"jId=%@&latitud=%f&longitud=%f",json,miLocation.coordinate.latitude, miLocation.coordinate.longitude] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){

        if(error == nil){
            
            if(data != nil){
                
                NSMutableArray *opcionesActualizadas = [NSJSONSerialization JSONObjectWithData:data options:NSUTF8StringEncoding error:&error];
            }
        }
        
    }];
    
    [uploadTask resume];
    
}

-(void)getCheckins{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getalllugarescercanos.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){

        if(error == nil){
            
            if(data != nil){
                
                NSDictionary *lugaresCercanos = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                NSLog(@"llega datos %@",lugaresCercanos);
                
                if(![[[lugaresCercanos objectForKey:@"lugaresViejos"] objectAtIndex:0] objectForKey:@"valido"] || ![[[lugaresCercanos objectForKey:@"lugaresNuevos"] objectAtIndex:0] objectForKey:@"valido"]){
                
                    if(![[[lugaresCercanos objectForKey:@"lugaresViejos"] objectAtIndex:0] objectForKey:@"valido"]){
                        
                        checkinsTodosOtros = [lugaresCercanos objectForKey:@"lugaresViejos"];
                    }
                    
                    if(![[[lugaresCercanos objectForKey:@"lugaresNuevos"] objectAtIndex:0] objectForKey:@"valido"]){
                        
                        checkinsTodosUltimos = [lugaresCercanos objectForKey:@"lugaresNuevos"];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [listaCheckin reloadData];
                    });
                                        
                 //   intervaloActualizarLista = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(actualizarCheckins) userInfo:nil repeats:YES];
                    
                }else{
                    if(![vistaEmptyState viewWithTag:10]){
                        LOTAnimationView *lotAnimation = [[LOTAnimationView alloc] initWithContentsOfURL:[NSURL URLWithString:@"https://www.crowte.com/lotties/loading_pin.json"]];
                        
                        [lotAnimation setTag:10];
                        
                        [lotAnimation setFrame:CGRectMake((vistaEmptyState.frame.size.width/2)-30, vistaEmptyState.frame.size.height-98, 60.02, 100)];
                       // [vistaEmptyState addSubview:lotAnimation];
                        
                        [lotAnimation setLoopAnimation:YES];
                        
                        [lotAnimation play];
                    }
                    
                    [listaCheckin setHidden:YES];
                    [vistaEmptyState setHidden:NO];
                }
            }
        }
        
    }];
    
    [uploadTask resume];
}

-(void)setNoLocalizacion{
    
    [listaCheckin setHidden:YES];
    [vistaNoLocation setHidden:NO];
}

-(void)setSiLocalizacion{
    
    [listaCheckin setHidden:NO];
    [vistaNoLocation setHidden:YES];
}

- (IBAction)activarLocalizacion:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (IBAction)cerrarTutorial:(id)sender {

    if(![sender isEqual:[NSNull null]]){
        
        [self.tabBarController setSelectedIndex:2];
    }
    
    [backgroundTutorial setHidden:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarhacercheckintuto.php"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *cuerpo = [[NSString stringWithFormat:@"usuario=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:cuerpo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){}];
    
    [uploadTask resume];
    
}

-(void)altOpcionMeGusta:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"Me gusta este lugar";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionEstaLleno:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"Aún hay lugares disponibles";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionMeGustaMusica:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"Me gusta la música";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionServicioRapido:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"El servicio es rápido";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionServicioBueno:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"El servicio es bueno";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionMeGustaNegativo:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"No me gusta este lugar";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionEstaLlenoNegativo:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"El lugar está lleno";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionMeGustaMusicaNegativo:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"No me gusta la música";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionServicioRapidoNegativo:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"El servicio es lento";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

-(void)altOpcionServicioBuenoNegativo:(UITapGestureRecognizer *)gesture{
    
    LOTAnimationView *animacion = (LOTAnimationView*)[gesture view];
    
    RCEasyTipPreferences *preferenciasTip = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferenciasTip.drawing.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    preferenciasTip.drawing.arrowPostion = Bottom;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferenciasTip];
    
    tipView.text = @"El servicio es malo";
    
    [tipView showAnimated:YES forView:animacion withinSuperView:[[[animacion superview] superview] viewWithTag:6]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [tipView dismissWithCompletion:nil];
    });
}

#pragma mark - url task

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

