//
//  Perfil.m
//  Crowte
//
//  Created by Paco Escobar on 11/05/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "Perfil.h"
#import "controladorTabControllerViewController.h"
#import "ComentariosController.h"
#import "ReportarUsuarioViewController.h"


@interface Perfil ()

@end

@implementation Perfil

@synthesize posicionScroll, foto, dataFoto, stringNombre, amigoID, offset, imagenPMostrar, publicacion,lugar, publicaciones, estatus, tabla, ninguno, app, nombreFoto, postMinimo, topConstraintDisplay, offsetImagen, ultimooffset, index, ultimacelda;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    postMinimo = 0;
    offsetImagen = 0;
    ultimacelda = 0;
    app = [UIApplication sharedApplication];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/checarexistencia.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%d&rec=1&bloqueador=%@",amigoID,[defaults objectForKey:@"identifier"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

        if(data != NULL){
            
            char resultado[3];
            [data getBytes:resultado length:2];
            resultado[2] = '\0';
            
            NSInteger iresultado = atoi(resultado);
            
            if(iresultado == 0){
                
                dispatch_async(dispatch_get_main_queue(), ^(){
                    
                    [[[UIAlertView alloc] initWithTitle:@"Ha ocurrido un error." message:@"Este usuario no está registrado." delegate:self cancelButtonTitle:@"Muy bien" otherButtonTitles:nil] show];
                    
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else if(iresultado == 3){
                
                dispatch_async(dispatch_get_main_queue(), ^(){
                    
                    [[[UIAlertView alloc] initWithTitle:@"Ha ocurrido un error." message:nil delegate:self cancelButtonTitle:@"Muy bien" otherButtonTitles:nil] show];
                    
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
        }else{
        
            dispatch_async(dispatch_get_main_queue(), ^(){
                
                [[[UIAlertView alloc] initWithTitle:@"Ha ocurrido un error." message:@"Este usuario no está registrado." delegate:self cancelButtonTitle:@"Muy bien" otherButtonTitles:nil] show];
                
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
        
    }];

    
    ninguno = YES;
    
    publicaciones = [NSMutableArray array];
    
    imagenPMostrar.layer.cornerRadius = imagenPMostrar.frame.size.width / 2;
    imagenPMostrar.clipsToBounds = YES;

    if(dataFoto != nil){
    
        [imagenPMostrar setImage:[UIImage imageWithData:dataFoto]];
    }else if(nombreFoto != nil && ![nombreFoto isEqualToString:@"(null)"]){
    
        UIActivityIndicatorView *cargaImagen = [[UIActivityIndicatorView alloc] initWithFrame:imagenPMostrar.frame];
        [cargaImagen startAnimating];
        [self.view addSubview:cargaImagen];
        
        [imagenPMostrar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",amigoID, nombreFoto]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
    
            [cargaImagen removeFromSuperview];
        }];
    
    }else{
        
        UIActivityIndicatorView *cargaImagen = [[UIActivityIndicatorView alloc] initWithFrame:imagenPMostrar.frame];
        [cargaImagen startAnimating];
        [self.view addSubview:cargaImagen];
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getdatos.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&recurso=1&token=%@",amigoID,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                NSLog(@"error");
            }else{
                
                
                NSDictionary *todos = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &error];
                
                
                [imagenPMostrar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.crowte.com/usuarios/%d/%@",amigoID,[[todos objectForKey:@"datos"] objectForKey:@"foto"]]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                    
                    [cargaImagen removeFromSuperview];
                }];
            }
        }];
        
    }
    
    
    offset = 133.0;
    
    int identificador = [[defaults objectForKey:@"identifier"] intValue];
    if (amigoID == identificador) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        controladorTabControllerViewController *ida = [storyboard instantiateViewControllerWithIdentifier:@"tabController"];
        
        ida.selectedViewController = [ida.viewControllers objectAtIndex:2];
        [self presentViewController:ida animated:NO completion:NULL];
        
    }else{
        
        
        app.networkActivityIndicatorVisible = YES;
        
        [self getPosts];
        
    }
}

-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewWillDisappear:(BOOL)animated{
}

-(void)viewWillAppear:(BOOL)animated{

    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"fondonavigationbar.jpg"] forBarMetrics:UIBarMetricsDefault];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        return 188.0;
    }else if(indexPath.row < [publicaciones count]+1 && ninguno == NO){
        
        return 295.0;
    }else{
    
        return 50.0;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda;
    
    index = (int)indexPath.row-1;
    
    ultimacelda = 0;
    
    if (indexPath.row == 0) {
        
        celda = [tableView dequeueReusableCellWithIdentifier:@"celdaPublicaciones"];
        
        UILabel *nombre = (UILabel *)[celda viewWithTag:1];
        nombre.text = stringNombre;
        
        UIButton *botonEstatus;
        
        /*if(estatus == 1){

            botonEstatus = (UIButton *)[celda viewWithTag:2];
            [botonEstatus setTitle:@"Ya son amigos" forState:UIControlStateNormal];
        }else if(estatus == 0){
        
            botonEstatus = (UIButton *)[celda viewWithTag:2];
            [botonEstatus setTitle:@"Agrégalo como amigo" forState:UIControlStateNormal];
            botonEstatus.titleLabel.font = [UIFont systemFontOfSize:13.0];
        }else if(estatus == 2){
        
            botonEstatus = (UIButton *)[celda viewWithTag:2];
            [botonEstatus setTitle:@"Esperando confirmación" forState:UIControlStateNormal];
            [[botonEstatus titleLabel] setFont:[UIFont systemFontOfSize:12.0]];
        }*/
        [botonEstatus addTarget:self action:@selector(accionBoton:) forControlEvents:UIControlEventTouchUpInside];

        
    }else if(ninguno == NO && indexPath.row <= [publicaciones count]){

        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        
        if([publicaciones count] > 0 && publicaciones != NULL && estatus == 1){
            
            celda = [tableView dequeueReusableCellWithIdentifier:@"segundaCeldaPublicaciones"];
            
            UIImageView *display = (UIImageView*)[celda viewWithTag:1];
            
            display.layer.cornerRadius = imagenPMostrar.frame.size.width / 2;
            display.clipsToBounds = YES;
           // offsetImagen = ((indexPath.row-1)*295)+18;
            
           // offsetImagen = 206+((indexPath.row-2)*295);
            
            if(![[[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"acontecimiento"] isEqual:[NSNull null]] && ![[[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"acontecimiento"] isEqualToString:@"(null)"]){

                NSData *data = [[[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"acontecimiento"] dataUsingEncoding:NSUTF8StringEncoding];
                
                NSString *publicacionFormateada = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
                
                UILabel *publicacionTabla = (UILabel *)[celda viewWithTag:3];
                publicacionTabla.text = publicacionFormateada;

                [publicacionTabla setHidden:NO];
                
                
            }else{
            
                UILabel *publicacionTabla = (UILabel *)[celda viewWithTag:3];
                [publicacionTabla setHidden:YES];
            }
            
            if([[[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"objeto"] isEqual:[NSNull  null]]){
            
                UIButton *place = (UIButton *)[celda viewWithTag:4];
                [place setTitle:@"" forState:UIControlStateNormal];
                UILabel *en = (UILabel *)[celda viewWithTag:5];
                [en setHidden:YES];
                
                UIButton *join = (UIButton *)[celda viewWithTag:8];
                [join setHidden:YES];
                
                UILabel *numJoin = (UILabel *)[celda viewWithTag:9];
                [numJoin setHidden:YES];
                
                if([[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"comentarios"] != [NSNull null] && [[[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"comentarios"] intValue] != 0){
                    
                    UIButton *comentarios = (UIButton *)[celda viewWithTag:7];
                    [comentarios setTitle:[[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"comentarios"] forState:UIControlStateNormal];
                }else{
                    
                    UIButton *comentarios = (UIButton *)[celda viewWithTag:7];
                    [comentarios setTitle:@"" forState:UIControlStateNormal];
                }
                
            }else{
                
                UIButton *place = (UIButton *)[celda viewWithTag:4];
                [place setTitle:[NSString stringWithFormat:@"%@",[[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"objeto"]] forState:UIControlStateNormal];
                [place setHidden:NO];
                
                UILabel *en = (UILabel *)[celda viewWithTag:5];
                en.text = @"En";
                [en setHidden:NO];
                
                if(![[[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"comentarios"] isEqual:[NSNull null]] && ![[[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"comentarios"] isEqualToString:@"(null)"] && [[[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"comentarios"] intValue] != 0){
                
                    UIButton *comentarios = (UIButton *)[celda viewWithTag:7];
                    [comentarios setTitle:[[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"comentarios"] forState:UIControlStateNormal];
                }else{
                
                    UIButton *comentarios = (UIButton *)[celda viewWithTag:7];
                    [comentarios setTitle:@"" forState:UIControlStateNormal];
                }
                
                if([[[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"elira"] intValue] == 1){
                
                    UIButton *join = (UIButton *)[celda viewWithTag:8];
                    [join setImage:[UIImage imageNamed:@"joinFilled.png"] forState:UIControlStateNormal];
                    [join setHidden:NO];
                    
                    UILabel *numJoin = (UILabel *)[celda viewWithTag:9];
                    numJoin.text = [[publicaciones objectAtIndex:indexPath.row-1] objectForKey:@"joins"];
                    [numJoin setHidden:NO];
                }else{
                
                    UIButton *join = (UIButton *)[celda viewWithTag:8];
                    [join setImage:[UIImage imageNamed:@"joinT.png"] forState:UIControlStateNormal];
                    [join setHidden:NO];
                    
                    UILabel *numJoin = (UILabel *)[celda viewWithTag:9];
                    numJoin.text = @"";
                    [numJoin setHidden:NO];
                }
            }
        }else{
            
            celda = [tableView dequeueReusableCellWithIdentifier:@"ultimaCelda"];
            
        }
    }else if((indexPath.row == [publicaciones count]+1 && ninguno == NO) || (ninguno == YES && indexPath.row == 2)){

        celda = [tableView dequeueReusableCellWithIdentifier:@"ultimaCelda"];
        app.networkActivityIndicatorVisible = NO;
        ultimacelda = 1;
        
        [UIView animateWithDuration:0.4 animations:^{
            
            [topConstraintDisplay setConstant:172];
            [self.view layoutIfNeeded];
        }];
        
        [self getPosts];
    }else if(ninguno == YES){
        
        celda = [tableView dequeueReusableCellWithIdentifier:@"noPubCell"];
        
        celda.selectionStyle = UITableViewCellSelectionStyleNone;
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        imagenPMostrar.frame = CGRectMake(imagenPMostrar.frame.origin.x, 75, imagenPMostrar.frame.size.width, imagenPMostrar.frame.size.height);

    }
    
    return celda;
}

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
   
    if(ultimacelda == 0){
    
        [UIView animateWithDuration:0.4 animations:^{
            
            [topConstraintDisplay setConstant:11];
            [self.view layoutIfNeeded];
        }];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if(ultimacelda == 0){
        
        [topConstraintDisplay setConstant:11];
    }    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGPoint velocidad = [[scrollView panGestureRecognizer] velocityInView:scrollView];
    
    if(velocidad.y < 0){
    
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }else if(velocidad.y > 0){
    
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
    
    if(ultimooffset > scrollView.contentOffset.y){
    
        offsetImagen--;
    }else if(ultimooffset < scrollView.contentOffset.y){
    
        offsetImagen++;
    }
    
    ultimooffset = scrollView.contentOffset.y;
    
    if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height) && ninguno == NO){
        
        [UIView animateWithDuration:0.4 animations:^{
        
           // [topConstraintDisplay setConstant:100];
        }];
        
       /* [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.4];
        imagenPMostrar.frame = CGRectMake(imagenPMostrar.frame.origin.x, 260, imagenPMostrar.frame.size.width, imagenPMostrar.frame.size.height);
        [UIView commitAnimations];*/
    }else{
        
       /* [UIView animateWithDuration:0.4 animations:^{
        
            [topConstraintDisplay setConstant:11];
        }];*/
        
        /*[UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.4];
        imagenPMostrar.frame = CGRectMake(imagenPMostrar.frame.origin.x, 75, imagenPMostrar.frame.size.width, imagenPMostrar.frame.size.height);
        [UIView commitAnimations];*/
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    NSInteger numero;

    if(ninguno == NO){
        numero = [publicaciones count]+2;
    }else if(ninguno == YES){
    
        numero = [publicaciones count]+2;
    }
    
    return numero;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    
    if ((scrollView.contentOffset.y < offset+20 && scrollView.contentOffset.y > offset-20) || (scrollView.contentOffset.y < offset+60 && scrollView.contentOffset.y > offset+21)) {

        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, offset+20) animated:YES];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    [self.navigationController setNavigationBarHidden:NO];
    
    ComentariosController *controlador = (ComentariosController*)segue.destinationViewController;
    
    UITableViewCell *celda = (UITableViewCell*)[[sender superview] superview];
    
    controlador.foto = UIImageJPEGRepresentation([imagenPMostrar image], 1);
    controlador.nombrePersona = stringNombre;
    
    if(![(UILabel *)[celda viewWithTag:3] isHidden]){

        controlador.textoDePublicacion = [(UILabel *)[celda viewWithTag:3] text];
    }else{

        controlador.textoDePublicacion = nil;
    }

    controlador.lugar = [[publicaciones objectAtIndex:[[tabla indexPathForCell:celda] row]-1] objectForKey:@"objeto"];
    controlador.postID = [[[publicaciones objectAtIndex:[tabla indexPathForCell:celda].row-1] objectForKey:@"aconID"] intValue];
    controlador.joinsDePublicacion = [[publicaciones objectAtIndex:[[tabla indexPathForCell:celda] row]-1] objectForKey:@"joins"];
    controlador.joinFilled = [[[publicaciones objectAtIndex:[[tabla indexPathForCell:celda] row]-1] objectForKey:@"elira"] boolValue];
}

-(IBAction)accionBoton:(id)boton{
    
    app.networkActivityIndicatorVisible = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    if([[[boton titleLabel] text] isEqualToString:@"Ya son amigos"]){
    
        UIAlertController *eliminar = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *borrar = [UIAlertAction actionWithTitle:@"Eliminar amigo" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        
            UIAlertController *confirmacion = [UIAlertController alertControllerWithTitle:nil message:@"¿Estás seguro?" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *afirmativo = [UIAlertAction actionWithTitle:@"Si, eliminar" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
            
                NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/eliminaramigo.php"]];
                
                [peticion setHTTPMethod:@"POST"];
                [peticion setHTTPBody:[[NSString stringWithFormat:@"id=%d&amigo=%d&token=%@",[[defaults objectForKey:@"identifier"] intValue],amigoID,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
                
                NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                
                [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                    
                    if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                        
                        NSLog(@"error");
                    }else{
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            app.networkActivityIndicatorVisible = NO;
                            [boton setTitle:@"Agrégalo como amigo" forState:UIControlStateNormal];
                            [boton setEnabled:YES];
                        });
                    }
                }];
            }];
            
            UIAlertAction *negativo = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
                [boton setEnabled:YES];
            }];
            
            [confirmacion addAction:afirmativo];
            [confirmacion addAction:negativo];
            
            [self presentViewController:confirmacion animated:YES completion:nil];
        }];
        
        UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:nil];
        
        [eliminar addAction:borrar];
        [eliminar addAction:cancelar];
        
        [self presentViewController:eliminar animated:YES completion:nil];
        
    }else if([[[boton titleLabel] text] isEqualToString:@"Agrégalo como amigo"]){
    
        [boton setEnabled:NO];
        
        app.networkActivityIndicatorVisible = YES;
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/enviarsolicitudamigo.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"ID=%d&usuario=%d&token=%@",[[defaults objectForKey:@"identifier"] intValue],amigoID,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                NSLog(@"error");
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    app.networkActivityIndicatorVisible = NO;
                    [boton setTitle:@"Esperando confirmación" forState:UIControlStateNormal];
                    [boton setEnabled:YES];
                });
            }
        }];
    }else if([[[boton titleLabel] text] isEqualToString:@"Esperando confirmación"]){
    
        UIActionSheet *cancelar = [[UIActionSheet alloc] initWithTitle:@"Solicitud" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:@"Cancelar solicitud" otherButtonTitles: nil];
        
        cancelar.actionSheetStyle = UIActionSheetStyleDefault;
        [cancelar showInView:self.view];
    }else if([[[boton titleLabel] text] isEqualToString:@"Aceptar solicitud"]){
    
        [boton setEnabled:NO];
        
        app.networkActivityIndicatorVisible = YES;
        
        NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/aceptarsolicitudamistad.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"solado=%d&solnte=%d&token=%@",[[defaults objectForKey:@"identifier"] intValue],amigoID,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            dispatch_async(dispatch_get_main_queue(), ^{

            [[[UIAlertView alloc] initWithTitle:nil message:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil] show];
            });
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                NSLog(@"error");
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    app.networkActivityIndicatorVisible = NO;
                    [boton setTitle:@"Ya son amigos" forState:UIControlStateNormal];
                    [boton setEnabled:YES];
                });
            }
        }];
    }
}

#pragma mark - hechos por mi

- (IBAction)opciones:(id)sender {
    
    UIAlertController *opciones = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *reportar = [UIAlertAction actionWithTitle:@"Reportar usuario" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        
        UIAlertController *confirmar = [UIAlertController alertControllerWithTitle:@"¿Deseas también bloquear este usuario?" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *si = [UIAlertAction actionWithTitle:@"Si" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/bloqueos/bloquear.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%d&bloqueador=%@&token=%@",amigoID,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    ReportarUsuarioViewController *reportUsuario = [storyboard instantiateViewControllerWithIdentifier:@"reportarUsuario"];
                    
                    reportUsuario.idUsuario = amigoID;
                    reportUsuario.nombre = stringNombre;
                    reportUsuario.dataFoto = dataFoto;
                    reportUsuario.nombreFoto = nombreFoto;
                    
                    [self presentViewController:reportUsuario animated:YES completion:nil];
                });
            }];
        }];
        
        UIAlertAction *no = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ReportarUsuarioViewController *reportUsuario = [storyboard instantiateViewControllerWithIdentifier:@"reportarUsuario"];
            
            reportUsuario.idUsuario = amigoID;
            reportUsuario.nombre = stringNombre;
            reportUsuario.dataFoto = dataFoto;
            reportUsuario.nombreFoto = nombreFoto;
            
            [self presentViewController:reportUsuario animated:YES completion:nil];
        }];
        
        UIAlertAction *cancelarBloqueo = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:nil];
        
        [confirmar addAction:si];
        [confirmar addAction:no];
        [confirmar addAction:cancelarBloqueo];
        
        [self presentViewController:confirmar animated:YES completion:nil];
    
    }];
    
    UIAlertAction *bloquear = [UIAlertAction actionWithTitle:@"Bloquear usuario" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        
        
        UIAlertController *confirmacion = [UIAlertController alertControllerWithTitle:@"¿Realmente deseas bloquear a esta persona?" message:@"Si la bloqueas ya no tendrás acceso a su perfil, publicaciones ni comentarios." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *aceptar = [UIAlertAction actionWithTitle:@"Si" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/bloqueos/bloquear.php"]];
            
            [peticion setHTTPMethod:@"POST"];
            [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%d&bloqueador=%@&token=%@",amigoID,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }];
        }];
        
        UIAlertAction *denegar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:nil];
        
        [confirmacion addAction:aceptar];
        [confirmacion addAction:denegar];
        
        [self presentViewController:confirmacion animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:nil];
    
    [opciones addAction:reportar];
    [opciones addAction:bloquear];
    [opciones addAction:cancelar];
    
    [self presentViewController:opciones animated:YES completion:nil];
}

-(void)getPosts{
    
    app.networkActivityIndicatorVisible = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/getfriendposts.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"usuario=%d&amigo=%d&minimo=%d&token=%@",[[defaults objectForKey:@"identifier"] intValue],amigoID,postMinimo,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            NSLog(@"error");
        }else{
            
            
            NSDictionary *todos = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &error];
            NSLog(@"todos %@",todos);
            if([[todos objectForKey:@"estado"] isEqualToString:@"si"]){
                
                estatus = [[todos objectForKey:@"estatus"] intValue];
                
                if(estatus == 1){
                
                    dispatch_async(dispatch_get_main_queue(), ^{
                    
                        UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    
                        UIButton *botonEstatus = (UIButton *)[celda viewWithTag:2];
                        [botonEstatus setTitle:@"Ya son amigos" forState:UIControlStateNormal];
                        
                        [botonEstatus addTarget:self action:@selector(accionBoton:) forControlEvents:UIControlEventTouchUpInside];
                    });
                }else if(estatus == 2){
                
                    UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIButton *botonEstatus = (UIButton *)[celda viewWithTag:2];
                        [botonEstatus setTitle:@"Agrégalo como amigo" forState:UIControlStateNormal];
                        [[botonEstatus titleLabel] setFont:[UIFont systemFontOfSize:13.0]];
                        [botonEstatus addTarget:self action:@selector(accionBoton:) forControlEvents:UIControlEventTouchUpInside];
                    });
                }
                
                if(![[todos objectForKey:@"posts"] isEqual:[NSNull null]]){
                    
                    for(NSDictionary *paso in [todos objectForKey:@"posts"]){
                        
                        [publicaciones addObject:paso];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [tabla reloadData];
                        ninguno = NO;
                        app.networkActivityIndicatorVisible = NO;
                    });
                }else if(postMinimo == 0){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        ninguno = YES;
                        [tabla reloadData];
                    });
                }else{
                
                    app.networkActivityIndicatorVisible = NO;
                }
            }else if([[todos objectForKey:@"estado"] isEqualToString:@"esperando"]){
                
                UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIButton *botonEstatus = (UIButton *)[celda viewWithTag:2];
                    [botonEstatus setTitle:@"Esperando confirmación" forState:UIControlStateNormal];
                    [[botonEstatus titleLabel] setFont:[UIFont systemFontOfSize:12.0]];
                    [botonEstatus addTarget:self action:@selector(accionBoton:) forControlEvents:UIControlEventTouchUpInside];
                });
            }else if([[todos objectForKey:@"estado"] isEqualToString:@"preguntado"]){
                
                estatus = [[todos objectForKey:@"status"] intValue];
                
                if(estatus != 2){
                    
                    UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIButton *botonEstatus = (UIButton *)[celda viewWithTag:2];
                        [botonEstatus setTitle:@"Aceptar solicitud" forState:UIControlStateNormal];
                        [botonEstatus addTarget:self action:@selector(accionBoton:) forControlEvents:UIControlEventTouchUpInside];
                    });
                }else if(estatus == 2){
                
                    UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIButton *botonEstatus = (UIButton *)[celda viewWithTag:2];
                        [botonEstatus setTitle:@"Agrégalo como amigo" forState:UIControlStateNormal];
                        [[botonEstatus titleLabel] setFont:[UIFont systemFontOfSize:13.0]];
                        [botonEstatus addTarget:self action:@selector(accionBoton:) forControlEvents:UIControlEventTouchUpInside];
                    });
                }
                
            }else if([[todos objectForKey:@"estado"] isEqualToString:@"no"]){
            
                UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIButton *botonEstatus = (UIButton *)[celda viewWithTag:2];
                    [botonEstatus setTitle:@"Agrégalo como amigo" forState:UIControlStateNormal];
                    [[botonEstatus titleLabel] setFont:[UIFont systemFontOfSize:13.0]];
                    [botonEstatus addTarget:self action:@selector(accionBoton:) forControlEvents:UIControlEventTouchUpInside];
                });
            }
        }
        
    }];
    
    postMinimo+=20;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
