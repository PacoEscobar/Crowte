//
//  ReportarUsuarioViewController.m
//  Crowte
//
//  Created by Paco Escobar on 02/08/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "ReportarUsuarioViewController.h"
#import "Perfil.h"

@interface ReportarUsuarioViewController ()

@end

@implementation ReportarUsuarioViewController

@synthesize tabla,idUsuario, dataFoto, nombre, nombreFoto;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    float size;
    
    if(indexPath.row == 0){
        
        size = 150;
    }else{
    
        size = 228;
    }
    
    return size;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 2;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *celda;
    
    if(indexPath.row == 0){
        celda = [tableView dequeueReusableCellWithIdentifier:@"info"];
        
        if(dataFoto != nil){
        
            UIImageView *foto = (UIImageView*)[celda viewWithTag:1];
            
            [foto setImage:[UIImage imageWithData:dataFoto]];
        }
        
        if(nombre != nil){
        
            UILabel *nombreUsuario = (UILabel*)[celda viewWithTag:2];
            [nombreUsuario setText:nombre];
        }
    }else{
        
        celda = [tableView dequeueReusableCellWithIdentifier:@"comentario"];
        
        UITextView *comentario = (UITextView*)[celda viewWithTag:1];
        
        [comentario.layer setBorderColor:[[UIColor grayColor] CGColor]];
        [comentario.layer setBorderWidth:1];
    }
    
    return celda;
}

#pragma mark - text view

-(void)textViewDidBeginEditing:(UITextView *)textView{

    [UIView animateWithDuration:0.4 animations:^{
    
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - 100, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{

    UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    UITextView *comentarios = (UITextView*)[celda viewWithTag:1];
    
    if([comentarios isFirstResponder]){
        
        [UIView animateWithDuration:0.4 animations:^{
            
            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 100, self.view.frame.size.width, self.view.frame.size.height);
        }];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([[segue destinationViewController] isKindOfClass:[Perfil class]]){
    
        Perfil *perfil = (Perfil*)segue.destinationViewController;
        
        perfil.amigoID = (int)idUsuario;
        perfil.dataFoto = dataFoto;
    }
}


- (IBAction)enviar:(id)sender {
    
    __block UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    UITableViewCell *celda = [tabla cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    UITextView *comentario = (UITextView*)[celda viewWithTag:1];
    __block UIButton *boton = (UIButton*)[celda viewWithTag:2];
    
    [boton setEnabled:NO];
    NSString *texto = [[NSString alloc] init];
    if(![[comentario text] isEqualToString:@""] && ![[comentario text] isEqualToString:@"Escribe un comentario..."]){
        
        texto = [comentario text];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/denuncia/denunciarusuario.php"]];
    
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[[NSString stringWithFormat:@"ID=%d&denunciado=%ld&denuncia=%@&token=%@",[[defaults objectForKey:@"identifier"] intValue],idUsuario,texto,[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            NSLog(@"error");
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [UIView animateWithDuration:0.5 animations:^{
                    
                    
                    [boton setBackgroundColor:[UIColor colorWithRed:0 green:0.8 blue:0 alpha:1]];
                    [boton setTitle:@"Gracias!" forState:UIControlStateNormal];
                    app.networkActivityIndicatorVisible = NO;
                    
                    
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
            });
        }
        
    }];
}

- (IBAction)regresar:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
