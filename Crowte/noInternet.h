//
//  noInternet.h
//  Crowte
//
//  Created by Paco Escobar on 18/09/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface noInternet : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *irprincipal;

-(IBAction)reintentar:(id)sender;
@end
