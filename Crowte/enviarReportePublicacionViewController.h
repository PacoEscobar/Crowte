//
//  enviarReportePublicacionViewController.h
//  Crowte
//
//  Created by Paco Escobar on 04/07/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface enviarReportePublicacionViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>

@property int aconID;
@property int userID;
@property NSString *fotoUsuario;
@property NSString *nombreUsuario;
@property NSData *dataFoto;
@property NSString *nombreLugar;
@property NSString *publicacion;
@property (weak, nonatomic) IBOutlet UITableView *tabla;

-(void)enviar;
-(IBAction)cancelar;

@end
