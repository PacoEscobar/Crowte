//
//  CondicionesViewController.m
//  Crowte
//
//  Created by Paco Escobar on 26/09/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "CondicionesViewController.h"

@interface CondicionesViewController ()

@end

@implementation CondicionesViewController

@synthesize condicionesContainer, activity;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/asincronos/legal/getcondiciones.php"]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if(error.code == NSURLErrorTimedOut){
            
            UIAlertController *noconexion = [UIAlertController alertControllerWithTitle:nil message:@"Estamos teniendo algunos problemas, por favor intenta reiniciando la app nuevamente o verificando tu conexión a Internet." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
            
            [noconexion addAction:dismiss];
            
            [self presentViewController:noconexion animated:YES completion:nil];
            
        }else if(error.code == NSURLErrorNotConnectedToInternet || data == NULL){
            
            UIAlertController *noconexion = [UIAlertController alertControllerWithTitle:nil message:@"Ha ocurrido un error, por favor verifica tu conexión a Internet." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Muy bien" style:UIAlertActionStyleCancel handler:nil];
            
            [noconexion addAction:dismiss];
            
            [self presentViewController:noconexion animated:YES completion:nil];
            
        }else{
            
            NSString *condiciones = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSError *error = nil;
            NSAttributedString *attString = [[NSAttributedString alloc] initWithData:[condiciones dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)} documentAttributes:nil error:&error];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [activity removeFromSuperview];
                [condicionesContainer setHidden:NO];
               // [condicionesContainer setText:condiciones];
                
                [condicionesContainer.textStorage appendAttributedString:attString];
            });
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
