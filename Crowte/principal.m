//
//  principal.m
//  crowte
//
//  Created by Paco Escobar on 16/05/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "principal.h"
#import "controladorTabControllerViewController.h"
#import "noInternet.h"
#import "registro.h"
#import "login.h"
#import "RegistroFaceViewController.h"
#import "AceptarCondicionesUsoViewController.h"
#import "AceptarPoliticaPrivacidadViewController.h"
#import "AceptarCondicionesLugarViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "TabBarPrincipalViewController.h"

@implementation principal

@synthesize app, constraintTopLogo, constraintLogoCentrado, logo;

-(void)viewDidLoad
{
    
    app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    
    FBSDKLoginButton *fbLoginButton = [[FBSDKLoginButton alloc] init];
    
    fbLoginButton.delegate = self;
    
    fbLoginButton.center = self.view.center;
    
    [fbLoginButton setTag:4];
    
    [fbLoginButton setAlpha:0];
    
    fbLoginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    
    [self.view addSubview:fbLoginButton];
    
}

-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error{

    if([FBSDKAccessToken currentAccessToken]){
    
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"id, first_name, last_name, email"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *coneccion, id result, NSError *error){
        
            if(!error){
                
                UIView *vistacarga = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                
                [vistacarga setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
                
                UIActivityIndicatorView *aciv = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
                
                [aciv startAnimating];
                
                aciv.center = vistacarga.center;
                
                [vistacarga addSubview:aciv];
                
                [self.view addSubview:vistacarga];
            
                NSDictionary *resultados = [NSDictionary alloc];
                
                resultados = result;
                
                NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/logincface.php"]];
                
                [peticion setHTTPMethod:@"POST"];
                [peticion setHTTPBody:[[NSString stringWithFormat:@"usr=%@&fbid=%@&nombre=%@&apellido=%@&foto=%@",[resultados objectForKey:@"email"],[resultados objectForKey:@"id"],[resultados objectForKey:@"first_name"],[resultados objectForKey:@"last_name"],[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",[resultados objectForKey:@"id"]]] dataUsingEncoding:NSUTF8StringEncoding]];
                
                NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                
                [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                    
                    if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                        
                        app.networkActivityIndicatorVisible = NO;
                    }else{
                        
                        /*NSString *llega = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

                        if([llega isEqualToString:@"2"]){
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                
                                RegistroFaceViewController *registroFace = [storyboard instantiateViewControllerWithIdentifier:@"registroface"];
                                
                                registroFace.strNombre = [NSString stringWithFormat:@"%@ %@",[resultados objectForKey:@"first_name"],[resultados objectForKey:@"last_name"]];
                                registroFace.correo = [resultados objectForKey:@"email"];
                                registroFace.strFoto = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",[resultados objectForKey:@"id"]];
                                
                                [vistacarga removeFromSuperview];
                                
                                [self presentViewController:registroFace animated:YES completion:nil];
                            });
                            
                        }else{*/
                            
                            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                        NSString *resultao = [json objectForKey:@"inicio"];
                            NSString *ident = [json objectForKey:@"id"];
                            
                            if ([resultao isEqualToString:@"1"]) {
                                
                                NSUserDefaults *ide = [NSUserDefaults standardUserDefaults];
                                [ide setObject:resultao forKey:@"id"];
                                [ide setObject:ident forKey:@"identifier"];
                                [ide synchronize];
                                
                                [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
                                [[UIApplication sharedApplication] registerForRemoteNotifications];
                                
                                
                                
                                NSURL *url = [NSURL URLWithString:@"https://www.crowte.com/app/getdatos.php"];

                                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                                [request setHTTPMethod:@"POST"];
                                [request setHTTPBody:[[NSString stringWithFormat:@"id=%d&recurso=1&p=1",[ident intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
                                
                                NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                                
                                [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                                    
                                    NSMutableDictionary *datosUsuario = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        /*if([[[datosUsuario objectForKey:@"tutoriales"] objectForKey:@"iosPrincipal"] intValue] == 0){
                                         
                                         
                                         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                         PageViewController *tutorial = [storyboard instantiateViewControllerWithIdentifier:@"tutorialPrincipalComienzo"];
                                         
                                         tutorial.datosUsuario = datosUsuario;
                                         
                                         [self presentViewController:tutorial animated:YES completion:nil];
                                         
                                         }else{*/
                                        
                                        [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
                                        
                                        [[UINavigationBar appearance] setTranslucent:YES];
                                        
                                        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
                                        
                                        UIStoryboard *storyboard;
                                        
                                        TabBarPrincipalViewController *tabBarPrincipalVC;
                                        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                        tabBarPrincipalVC= [storyboard instantiateViewControllerWithIdentifier:@"mainTabBar"];

                                        [tabBarPrincipalVC setCheckinsTodosOtros:[datosUsuario objectForKey:@"todosOtrosPost"]];
                                        [tabBarPrincipalVC setCheckinsTodosUltimos:[datosUsuario objectForKey:@"todosUltimosPosts"]];
                                        [tabBarPrincipalVC setDatosCheckinEnLugar:[datosUsuario objectForKey:@"checkinAlready"]];
                                        [tabBarPrincipalVC setDatosMios:[datosUsuario objectForKey:@"datos"]];
                                        
                                        app.networkActivityIndicatorVisible = NO;
                                        
                                        [self presentViewController:tabBarPrincipalVC animated:NO completion:nil];
                                        //}
                                    });
                                }];
                                
                            }
                        //}
                    }
                    
                }];
            }
        }];
    }
}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{}

-(void)hacer: (BOOL) animated{

    NSUserDefaults *ide = [NSUserDefaults standardUserDefaults];
    NSString *side = [ide objectForKey:@"id"];
    
    if(side){
        
        NSUserDefaults *usuario = [NSUserDefaults standardUserDefaults];
        NSString *identificador = [usuario objectForKey:@"identifier"];
        
        NSURL *url = [NSURL URLWithString:@"https://www.crowte.com/app/getdatos.php"];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        
        [request setHTTPBody:[[NSString stringWithFormat:@"id=%d&recurso=1&p=1",[identificador intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

            NSMutableDictionary *datosUsuario = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0 green:170.0/255.0 blue:170.0/255.0 alpha:1]];
                
                [[UINavigationBar appearance] setTranslucent:YES];
                
                [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
                
                UIStoryboard *storyboard;
                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                if([[[datosUsuario objectForKey:@"legal"] objectForKey:@"uso"] isEqual:[NSNumber numberWithInt:1]]){
                
                    AceptarCondicionesUsoViewController *condiciones = [storyboard instantiateViewControllerWithIdentifier:@"aceptarCondiciones"];
                    
                    [self presentViewController:condiciones animated:YES completion:nil];
                }
                
                if([[[datosUsuario objectForKey:@"legal"] objectForKey:@"privacidad"] isEqual:[NSNumber numberWithInt:1]]){
                    
                    AceptarPoliticaPrivacidadViewController *condiciones = [storyboard instantiateViewControllerWithIdentifier:@"aceptarPrivacidad"];
                    
                    [self presentViewController:condiciones animated:YES completion:nil];
                }
                
                if([[[datosUsuario objectForKey:@"legal"] objectForKey:@"privacidad"] isEqual:[NSNumber numberWithInt:1]]){
                    
                    AceptarCondicionesLugarViewController *condiciones = [storyboard instantiateViewControllerWithIdentifier:@"aceptarCondicionesLugar"];
                    
                    [self presentViewController:condiciones animated:YES completion:nil];
                }
                
                TabBarPrincipalViewController *tabBarPrincipalVC;
                tabBarPrincipalVC= [storyboard instantiateViewControllerWithIdentifier:@"mainTabBar"];
                
                [tabBarPrincipalVC setCheckinsTodosOtros:[datosUsuario objectForKey:@"todosOtrosPost"]];
                [tabBarPrincipalVC setCheckinsTodosUltimos:[datosUsuario objectForKey:@"todosUltimosPosts"]];
                [tabBarPrincipalVC setDatosCheckinEnLugar:[datosUsuario objectForKey:@"checkinAlready"]];
                [tabBarPrincipalVC setDatosMios:[datosUsuario objectForKey:@"datos"]];

                app.networkActivityIndicatorVisible = NO;
                
                [self presentViewController:tabBarPrincipalVC animated:NO completion:nil];
                
            });
        }];
        
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIButton *iniciaSesion = (UIButton *)[self.view viewWithTag:2];
            UIButton *registrate = (UIButton *)[self.view viewWithTag:3];
            FBSDKLoginButton *fbloginbtn = (FBSDKLoginButton*)[self.view viewWithTag:4];
            
            if([[UIScreen mainScreen] bounds].size.height < 560){
                
                fbloginbtn.center = CGPointMake(fbloginbtn.center.x, fbloginbtn.center.y+60);
            }
            
            [UIView animateWithDuration:0.8 animations:^{
                
                [[logo superview] removeConstraint:constraintLogoCentrado];
                
                if([[UIScreen mainScreen] bounds].size.height <= 480){
                    
                    [constraintTopLogo setConstant:50];
                    [self.view layoutIfNeeded];
                }else{
                    
                    [constraintTopLogo setConstant:50];
                    [self.view layoutIfNeeded];
                }
                
            }];
            
            [UIView animateWithDuration:0.8 delay:0.8 options:UIViewAnimationOptionTransitionNone animations:^{
                
                [iniciaSesion setAlpha:1.0];
                [registrate setAlpha:1.0];
                [fbloginbtn setAlpha:1.0];
                
            } completion:nil];
            
        });
        
    }
    
    app.networkActivityIndicatorVisible = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
 
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/conexiontest.php"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30.0];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){

        if(error== nil && data != NULL){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self performSelector:@selector(hacer:) withObject:NULL];
            });
        }else if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet){
        
            dispatch_async(dispatch_get_main_queue(), ^{
            
                UIStoryboard *storyboard;
                noInternet *nointernet;
            
                if([[UIScreen mainScreen] bounds].size.height >= 560){
               
            
                    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    nointernet= [storyboard instantiateViewControllerWithIdentifier:@"nointernet"];
                }else{
            
                    
                    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    nointernet= [storyboard instantiateViewControllerWithIdentifier:@"nointernet"];
                }
            
                [self presentViewController:nointernet animated:NO completion:NULL];
            });
        }
    }];
   
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
   
    [UIView animateWithDuration:0.8 animations:^{

        logo.frame = CGRectMake(37, 53, logo.frame.size.width, logo.frame.size.height);
        
    }];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
