//
//  AgregaNuevoLugarController.m
//  Crowte
//
//  Created by Paco Escobar on 21/05/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import "AgregaNuevoLugarController.h"
#import <Lottie/Lottie.h>

@interface AgregaNuevoLugarController ()

@end

@implementation AgregaNuevoLugarController

@synthesize distanciasDelLugar, cercaniaLugar, nombreLugar, locationManager, numVecesLugar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor grayColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
    
    [[UINavigationBar appearance] setTranslucent:YES];
    
    self.navigationItem.title = @"Registra un lugar";
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setTintColor:[UIColor grayColor]];
    
    UIBarButtonItem *enviar = [[UIBarButtonItem alloc] initWithTitle:@"Enviar" style:UIBarButtonItemStylePlain target:self action:@selector(enviar:)];
    
    self.navigationItem.rightBarButtonItem = enviar;
    
    distanciasDelLugar = [[NSArray alloc] initWithObjects:@"Estoy en el lugar", @"Estoy cerca del lugar", nil];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    LOTAnimationView *lotAnimation = (LOTAnimationView*)[[self.view viewWithTag:1] viewWithTag:1];
    
    if(lotAnimation != nil){
    
        if(![lotAnimation isAnimationPlaying]){
        
            [lotAnimation play];
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated{

    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Hind-Light" size:20], NSFontAttributeName, nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)enviar:(id)sender{
    
    numVecesLugar = 0;
    
    if(![nombreLugar.text isEqual:@""]){

        dispatch_async(dispatch_get_main_queue(), ^{
        UIView *vistaLoading = [[UIView alloc] initWithFrame:CGRectMake(20, 80, self.view.frame.size.width-40, self.view.frame.size.height-160)];
        
        [vistaLoading setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.8]];
        
        [vistaLoading setTag:1];
        
        [self.view addSubview:vistaLoading];
        
        LOTAnimationView *lotAnimation = [[LOTAnimationView alloc] initWithContentsOfURL:[NSURL URLWithString:@"https://www.crowte.com/lotties/loading_pin.json"]];
        
        [lotAnimation setTag:1];
        
        [lotAnimation setFrame:CGRectMake((vistaLoading.frame.size.width/2)-30, (vistaLoading.frame.size.height/2)-50, 60.02, 100)];
        
        [vistaLoading addSubview:lotAnimation];
        
        [lotAnimation setLoopAnimation:YES];
        
        [lotAnimation play];
        });
        
        UIApplication *app = [UIApplication sharedApplication];
        app.networkActivityIndicatorVisible = YES;
        
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
        
        locationManager = [[CLLocationManager alloc] init];
        
        [self getCoordenadas];

    }else{
    
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:@"Falta algo" message:@"Por favor, dinos el nombre del lugar" delegate:self cancelButtonTitle:@"Muy bien" otherButtonTitles:nil, nil] show];
        });
    }
  
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{

    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{

    return 2;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    return [distanciasDelLugar objectAtIndex:row];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    
    [nombreLugar resignFirstResponder];
    
    return NO;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

    [nombreLugar resignFirstResponder];
}

-(void)getCoordenadas{

    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager requestAlwaysAuthorization];
    
    [locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{

    numVecesLugar++;
    
    if(numVecesLugar == 1){
        
        UIApplication *app = [UIApplication sharedApplication];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *peticion = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/actualizarcoordenadas.php"]];
        
        [peticion setHTTPMethod:@"POST"];
        [peticion setHTTPBody:[[NSString stringWithFormat:@"latitud=%f&longitud=%f&ID=%@&token=%@",manager.location.coordinate.latitude,manager.location.coordinate.longitude,[defaults objectForKey:@"identifier"],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:peticion queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                
                [[[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil] show];
                
                [[[self.view viewWithTag:1] viewWithTag:1] removeFromSuperview];
                
            }else{
                
                NSInteger distancia = [cercaniaLugar selectedRowInComponent:0]+1;
                
                NSUserDefaults *datosUsuario = [NSUserDefaults standardUserDefaults];
                NSString *identificador = [datosUsuario objectForKey:@"identifier"];
                
                NSMutableURLRequest *url = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.crowte.com/app/addlugar.php"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
                
                [url setHTTPMethod:@"POST"];
                [url setHTTPBody:[[NSString stringWithFormat:@"distanciaDelLugar=%ld&nombre=%@&ID=%d&token=%@",(long)distancia,nombreLugar.text,[identificador intValue],[defaults objectForKey:@"token"]] dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                
                [NSURLConnection sendAsynchronousRequest:url queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                    
                    
                    if(error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet || data == NULL){
                        
                        UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"No hay Internet" message:@"Por favor, verifica tu conexión a Internet" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles: nil];
                        
                        [error show];
                    }else{
                        int statusPeticion = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] intValue];

                        if(statusPeticion == 1){
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                
                                [[[self.view viewWithTag:1] viewWithTag:1] removeFromSuperview];
                                [self.navigationItem.rightBarButtonItem setEnabled:YES];
                                [nombreLugar resignFirstResponder];
                                [self.navigationController popViewControllerAnimated:YES];
                                app.networkActivityIndicatorVisible = NO;
                            });
                        }
                    }
                }];
            }
        }];
    }
    [locationManager stopUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
