//
//  CamaraOverlayViewController.h
//  Crowte
//
//  Created by Paco Escobar on 26/07/16.
//  Copyright © 2016 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@protocol CamaraOverlayViewControllerDelegate <NSObject, NSURLSessionTaskDelegate>

-(void)getImageVideoTomado:(NSData*)data videoImagen:(int)videoimagen comentario:(NSString*)comentario compartirFacebook:(BOOL)compartirFacebook;

@end

@interface CamaraOverlayViewController : UIViewController <AVCaptureFileOutputRecordingDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate, UITextFieldDelegate>{

    __unsafe_unretained id delegate;
}


@property (nonatomic, assign) id<CamaraOverlayViewControllerDelegate> delegate;

@property BOOL isVideo;
@property BOOL flashActivado;
@property BOOL alreadyGrabando;
@property BOOL alreadyGrabandoAnimacion;
@property BOOL seComparteEnFacebook;
@property BOOL tutoTomarFotoVideo;
@property int tiempoVideo;
@property int idLugar;
@property AVCaptureSession *session;
@property AVCaptureMovieFileOutput *MovieFileOutput;
@property AVCaptureStillImageOutput *stillOutput;
@property AVCaptureConnection *conexionStillOutput;

@property UIButton *trigger;
@property UIButton *cambiarCamara;
@property UIButton *flash;
@property UIButton *tacha;
@property UIView *contadorVideo;
@property NSTimer *timer;
@property UIButton *enviarComentario;
@property UIButton *tachaPreview;

@property UIImageView *fotoFinal;

@property NSUserDefaults *defaults;

#pragma mark - reproductor

@property AVURLAsset *urlAssetVideo;
@property AVPlayer *reproductor;
@property AVPlayerLayer *layerVideo;
@property UIView *vistaVideo;


#pragma mark - subir videofoto

@property unsigned long totalBytesArchivoAsubir;
@property NSData *dataFotoVideo;


-(void)cambiarCam;
-(void)tomarFoto;
-(void)tomarVideo:(UILongPressGestureRecognizer*)recognizer;
-(void)toggleFlash;
-(void)cerrarCamara;
-(void)setBotonRojo;
-(void)contarSegundosVideo;

-(void)desaparecerMensajeTuto;

-(void)pausarVideoGesture:(UITapGestureRecognizer*)recognizer;
-(void)cerrarFotoPreview:(id)sender;

#pragma mark - compartir con facebook

-(void)enviar:(UIButton*)sender;

@end
