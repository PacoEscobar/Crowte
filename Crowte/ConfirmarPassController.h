//
//  ConfirmarPassController.h
//  Crowte
//
//  Created by Paco Escobar on 24/08/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmarPassController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textocontra;
@property (weak, nonatomic) IBOutlet UIView *contenedor;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTop;


-(void)enviar;

@end
