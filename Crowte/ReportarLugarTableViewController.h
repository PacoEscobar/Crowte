//
//  ReportarLugarTableViewController.h
//  Crowte
//
//  Created by Paco Escobar on 15/03/17.
//  Copyright © 2017 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportarLugarTableViewController : UITableViewController <UITextViewDelegate, NSURLSessionDelegate, NSURLSessionDownloadDelegate>

@property NSString *idLugar;

@property (strong, nonatomic) IBOutlet UITextView *campoTextoReporte;



- (IBAction)cancelarReporte:(id)sender;
- (IBAction)enviarReporte:(id)sender;

@end
