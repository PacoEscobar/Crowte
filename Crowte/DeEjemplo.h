//
//  DeEjemplo.h
//  Crowte
//
//  Created by Paco Escobar on 27/04/14.
//  Copyright (c) 2014 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "KLCPopup.h"
#import "CamaraOverlayView.h"
#import "CamaraOverlayViewController.h"
long horainicial;
@interface DeEjemplo : UIViewController <UIGestureRecognizerDelegate, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, CLLocationManagerDelegate, NSURLConnectionDataDelegate, UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, CamaraOverlayViewControllerDelegate, NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate, FBSDKSharingDelegate>

@property __block BOOL already;
@property BOOL alreadyVideo;
@property BOOL enLugar;
@property BOOL seTomaFoto;
@property BOOL imagenCargada;
@property BOOL publicarFb;
@property int miID;
@property int videofoto;
@property int menorIdPost;
@property BOOL seActualiza;
@property BOOL dejoDeTocar;
@property BOOL seBusca;
@property float offset;
@property int lugarAir;
@property int fechaPublicacionActual;
@property BOOL localizacionHab;
@property unsigned long totalBytesArchivoAsubir;
@property unsigned long bytesSubidosArchivoAsubir;




@property NSUInteger palabraABuscar;
@property NSInteger indexMenuPublicacion;
@property UIView *yaEnLugar;
@property NSData *fotolugarair;
@property NSData *dataFotoNueva;
@property CLLocationManager *locationManager;
@property UIView *vistaVotar;
@property __block NSUserDefaults *defaults;
@property NSURLConnection *coneccionPublicar;
@property NSURLConnection *coneccionSubirFoto;
@property NSMutableData *datosLugarPublicacion;
@property NSTimer *timerAunAhiBoton;

@property KLCPopup *popup;
@property KLCPopup *popupFullSVideo;
@property CLLocation *coordenadas;
@property UIImagePickerController *picker;

@property AVPlayer *reproductor;
@property AVPlayerLayer *layer;
@property UIView *controlesReproductor;
@property NSURL *urlVideoApasar;

#pragma mark - iboutlets

@property (weak, nonatomic) IBOutlet UIView *vistauno;
@property (weak, nonatomic) IBOutlet UIView *vistados;
@property (weak, nonatomic) IBOutlet UIView *vistatres;
@property (weak, nonatomic) IBOutlet UITableView *tabla;

@property (weak, nonatomic) IBOutlet UILabel *notificaciones;
@property int tablaOffset;
@property (weak, nonatomic) IBOutlet UIImageView *loadingPunto;
@property (weak, nonatomic) IBOutlet UITextView *campoCompartir;
@property (weak, nonatomic) IBOutlet UITableView *tablaLugares;
@property (weak, nonatomic) IBOutlet UIButton *nombreMasVisitado;
@property (weak, nonatomic) IBOutlet UIButton *imagenMasVisitado;
@property (weak, nonatomic) IBOutlet UIButton *nombreMasVotado;
@property (weak, nonatomic) IBOutlet UIButton *imagenMasVotado;
@property (weak, nonatomic) IBOutlet UIButton *nombreLugarAir;
@property (weak, nonatomic) IBOutlet UIButton *botonEnviar;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIView *vistadecarga;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityEstaLugar;

#pragma mark - dictionarys y arrays

@property NSDictionary *datosLugarYaAhi;
@property NSDictionary *tutoriales;
@property NSMutableArray *arregloComentarios;
@property NSMutableArray *lugaresMuestra;
@property (retain, nonatomic) NSDictionary *diccionarioMasVotado;
@property (retain, nonatomic) NSDictionary *diccionarioMasVisitado;
@property (strong, nonatomic) __block NSDictionary *lugares;
@property (retain, nonatomic) NSMutableArray *arreglo;
@property (retain, nonatomic) NSMutableArray *arregloLugares;
@property (retain, nonatomic) NSMutableDictionary *arregloDeFotos;
@property (retain, nonatomic) NSMutableDictionary *arregloDeFotosDeLugares;
@property (retain, nonatomic) NSMutableArray *lugaresCheckinRapido;

#pragma mark - constraints

@property (retain, nonatomic) IBOutlet NSLayoutConstraint *constraintEstaLugar;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *constraintVistaVotar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightScrollview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCompartirButton;

@property CamaraOverlayView *camara;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintEtiqVisitado;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintImgVisitado;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintNombreVisitado;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintEtiqVotado;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintImbVotado;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintNombreVotado;


-(IBAction)opcionesPublicacion:(id)sender;
-(void)getPosts;
-(void)getPostsViejos;
-(void)spin;
-(void)getMasLugares;
-(void)cordenadas;
-(IBAction)removerLugarAir:(id)sender;
-(IBAction)enviar:(id)sender;
-(IBAction)publicaAlgo:(id)sender;
-(IBAction)mejorLugarVer:(id)sender;
-(IBAction)noAhi:(id)sender;
-(IBAction)enLugarShowLugar:(id)sender;
-(IBAction)join:(id)sender;
-(void)getUpdatePosts;
-(void)showNotificaciones:(id)sender;
-(void)irALugarYaAhi:(UIButton*)boton;
-(void)abrirConfiguracionApp;
-(void)votarLugar;
-(void)esconderVotoLugar;
-(void)votar:(id)voto;
-(void)aunAhi:(id)boton;
-(IBAction)showAgregarLugar:(id)sender;
-(void)checarSiEstaEnLugar:(NSTimer*)timer;
-(IBAction)enviarMensajeLugar:(id)boton;
-(IBAction)enviarMensajeLugar:(id)boton comentario:(NSString*)comentario;
-(IBAction)getNuevasNotificaciones;
-(void)actualizaNotificaciones:(NSNotification*)notificacion;
-(IBAction)verificarcorreo:(id)sender;
-(BOOL)verificarVoto;
-(void)cerrarEtiquetaLugarTuto:(UIButton*)boton;
-(void)animacionTutoEtiquetar:(UILabel*)arroba doblew:(UILabel*)w i:(UILabel*)i n:(UILabel*)n;
-(IBAction)mostrarTodosComentarios:(id)sender;
-(IBAction)cerrarTodosComentarios:(id)sender;
-(IBAction)likeComentario:(id)sender;
- (IBAction)textoCambio:(id)sender;
-(IBAction)cerrarImagen:(id)sender;
-(void)expandirImagen:(UIGestureRecognizer *)gesto;
-(IBAction)compartirFB:(id)sender;


#pragma mark - camara

-(void)tomarVideo:(UILongPressGestureRecognizer *)gesto;
-(void)tomarFoto:(UILongPressGestureRecognizer *)gesto;
-(void)cambiarCamara;
-(void)flash;
-(void)cerrarCamara;

#pragma mark - video control

-(void)pausarVideo;
-(void)loopVideo:(NSNotification *)notification;
-(void)cerrarVideoFullScreen:(UIPanGestureRecognizer*)gesto;

#pragma mark - checkin rapido

-(void)cerrarTutoCheckinRapido;
-(void)activarBotonCheckinRapido;
-(void)actualizarTutoCheckinrapido;
-(IBAction)checkinRapido:(id)sender;

#pragma mark - circular progress

-(void)dibujarProgresoCircular:(UIImageView*)imagenView progreso:(float)progreso;

#pragma mark - tutoriales

-(void)tutorialBotonVotar:(UIButton*)votar vista:(UIView*)vista;
-(void)cerrarTutorialBotonVotar;


@end
