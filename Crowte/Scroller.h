//
//  Scroller.h
//  Crowte
//
//  Created by Paco Escobar on 04/01/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Scroller : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scroll;

@end
