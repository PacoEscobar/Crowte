//
//  ReclamarLugarUno.m
//  Crowte
//
//  Created by Paco Escobar on 10/04/15.
//  Copyright (c) 2015 Crowte Inc. All rights reserved.
//

#import "ReclamarLugarUno.h"
#import "ReclamarLugarDosTableViewController.h"

@interface ReclamarLugarUno ()

@end

@implementation ReclamarLugarUno

@synthesize idlugar, texto1, texto2, constraintT1_T2, constraintT2_comenzar, constraintT1height;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if([[UIScreen mainScreen] bounds].size.height <= 480){
        
        [texto1 setFont:[UIFont fontWithName:@"Verdana" size:12]];
        [texto2 setFont:[UIFont fontWithName:@"Verdana" size:12]];
        [constraintT1_T2 setConstant:0];
        [constraintT2_comenzar setConstant:20];
        [constraintT1height setConstant:100];
        
        [self.view layoutIfNeeded];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelar) name:@"dismissreclamarone" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelar{

    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)comenzar{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    ReclamarLugarDosTableViewController *comenzar = [storyboard instantiateViewControllerWithIdentifier:@"reclamarLugarDos"];
    
    comenzar.idlugar = idlugar;
    
    [self presentViewController:comenzar animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
